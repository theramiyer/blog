---
title: About
---

Life without adventures is as meaningful as a candle without its wick. But adventures are not all about going bungee jumping or white-water rafting. They are the different out-of-normal things you do. It is about how and how much you push yourself. To me, this includes [going on a 100 km bicycle ride]({{< ref "my-first-100-km-bicycle-ride.md" >}}), or doing my [first YouTube video](//www.youtube.com/watch?v=GPXL59_PDbs), or [building an online resume from scratch](http://about.ramiyer.me/making).

By profession, I am an IT Infrastructure automation specialist. Outside of work, I am a cyclist, a [typography enthusiast](https://www.scribd.com/theramiyer), a [published author](https://amzn.to/3b3JkX2) and [a writer](https://iam.ramiyer.me/), [a podcaster](https://anchor.fm/ramiyer/) and [storyteller](https://anchor.fm/iam-ramiyer-me), [a YouTuber](https://youtube.com/ramiyer), a foodie, a tinkerer, a polyglot, a WhatsApp University drop-out, and above all, a humanist.

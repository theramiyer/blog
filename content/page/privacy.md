---
title: Privacy
description: Put plainly, I don't want your data.
---

Put bluntly, I couldn't care less about getting data about you. This site uses [Plausible Analytics](https://plausible.io/)---a privacy-friendly and GDPR-compliant analytics service---to track readership; to know some rudimentary metrics about general audience behaviour, like, to see when there is the most amount of activity, and what posts people like to read. While I do not base my content on it, it tells me which posts are easy to read and understand, or what posts people refer. That helps me tweak my writing style and the design of the blog.

Whatever I do track, I do not share with anybody. Tracking on my site is purely for the purposes of my tuning the content to be better.

## Twitter

Any Twitter embeds you might see within the posts respect your browser's Do Not Track requests. If you do not have it set, Twitter may track you using its cookies.

Read [Twitter's Privacy Policy](https://twitter.com/en/privacy) for more information.

## YouTube

I have enabled "Privacy Enhanced" mode for YouTube video embeds. Your activity is not recorded to personalise your viewing experience. [Read more](https://support.google.com/youtube/answer/171780?hl=en) about it on Google Support.

Read [Google's Privacy Policy](https://policies.google.com/privacy?hl=en) for more information.

## Podcast episodes

I embed my podcast episodes from Spotify. I have not yet figured out a way to disable cookies on them. Spotify does give you options to set up your cookies, though, as per their [Cookie Policy](https://www.spotify.com/legal/cookies-policy/). In other words, Spotify may track your activity.

Read [Spotify's Privacy Policy](https://www.spotify.com/us/legal/privacy-policy/) for more information.

I also use podcast buttons so that you can take listening to the respective platform (Apple Podcasts, Google Podcasts and Spotify) if you so choose.

## Instagram

As much as I hate Facebook's attitude towards privacy, a handful of the blog posts on this site do contain embedded Instagram posts. Facebook may collect information about you when you open those blog posts. Ever since Oct 24, 2020 when Facebook blocked use of the "legacy" embedding ability, I have moved what I could, out of Instagram and into my own non-tracking storage for this blog, to minimise use of the platform.

Read [Instagram's Data Policy](https://help.instagram.com/519522125107875) for more information.

## Share buttons

The share buttons (Facebook, Twitter, LinkedIn and WhatsApp) _do not_ process your social media data on this site. They use the sharer URL of the respective service with _details about the post_ (post title, post URL, post summary, etc.) as input parameters. All processing happens on the destination site/service. In other words, the share buttons do not track you until you click them. Once you click them, the destination site may track you. As such, this site collects no information from you using the share buttons.

---

All claims about not tracking you hold good as long as the [privacy features of Hugo](https://gohugo.io/about/hugo-and-gdpr/) and those of the services work as expected. These do not come with any warranty; they are beyond my control.

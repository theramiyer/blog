---
date: "2019-06-10T00:00:00Z"
description: Government freebies have become ubiquitous in India. Are they bad for
  the economy, or good for the citizens? Or are they both?
subtitle: or are they, in fact, good?
tags:
- India
- economy
- politics
- populism
- free
- subsidies
- schemes
- socialism
title: Are Government Freebies Bad
---

Ever since the word, "Socialist", got added to the Preamble of our constitution---or perhaps much before that---political parties, in their manifestos, have continued to promise freebies to us citizens. This is so common, that we now take the freebies and subsidies for granted.

Understand that I am not talking from a political standpoint; I am talking of politics, but not from a political standpoint. Take this post in the sense of an economy and its members, which is us; everyone of us. And I will keep this short.

At work and with friends, I always tend to get into debates on how the quality of our political manifestos is plummeting. The parties give undue importance to subsidies and waivers. And freebies---tonnes of them---starting from free television sets, to wet grinders, to free bus rides!

As a consequence of them being so ubiquitous, we now think that this is what a socialist economy is all about. 'But, isn't it? We pay taxes so that we reap benefits. This is our benefits; this is what we paid the taxes for. If the government is giving us what we paid for, what is wrong with it? This is the return of investment. I call it return, you call it a freebie.'

Isn't it?

Yesterday, someone mentioned the term, _Broken Window Economics_, and that, for a second, took me back to school, and apart from bringing back some fantastic memories (I have too little of them from school, by the way), it got me thinking, 'Someone realised it at last!' Of course, the one who realised it, is politically against the one who announced a certain subsidy.

Subsidies are a necessary evil, like [oxygen and water](https://iam.ramiyer.me/not-a-mother-nature/). Subsidies bring in better economic equality among the citizens. For example, the government subsidises railway tickets for senior citizens, because they do not have the purchasing power of a thirty-year-old. But they should be able to travel as well as the thirty-year-old. Government employees of some classes get paid less compared to others, and subsidising their telephone bills, for example, will benefit them. Encouraging Farmers to use modern equipment so they are able to plough more land and produce more food is necessary, but the farmers may not have the means for it---do you expect a farmer who owns two acres of land to afford a tractor with all the necessary attachments for farming, all by himself? To counter this inequality, the government subsidises the rent on farm equipment.

## Why freebies are good

Understand, that as taxpaying individuals, this is your money and mine going into balancing off the loss from these subsidies. Allow me to break down the multifaceted statement.

### The "return of investment"

Some of us think, since we pay taxes, the government should give back a return in our investment. To think so is fine. Feeling upset that the government gives subsidies to farmers as well as the so-called farmers, and that what we pay for does not come back to us at all, is normal. We can even justify some of this disapproval. But we are a socialist economy (whether you like it or not), and in a socialist economy, life belongs to everyone, and the right to good life is everyone's. Those higher than a baseline (and the number that defines the baseline in India is highly controversial) ought to help those below, have a good life.

Medical facilities should be free for at least a certain class of individuals. Education should be free because every Indian has a right to it, but not every Indian can afford educating their child in schools. Those who earn well should help those who do not, in getting their children at least primary education. This is a return of investment in a long term, because if every future citizen of India is not merely literate, but also educated, they will run the country better by contributing to the national income, which in turn contributes to the GDP, which is directly linked to the performance of an economy and the standard of life in the economy.

### Indirect investment

We don't directly pay for the roads in our locality, or for the water or sewage infrastructure, or for laying the electricity lines or the telephone cables. We pay taxes. The government lays the railway tracks, the roads, the water and sewage infrastructure, etc. for us.

These are assets. These assets have a long life. For example, an eight-inch layer of road should live for decades. A hundred kilometre stretch of such road takes millions of rupees to lay. But once laid, it _should_ serve us for decades. For highways, you pay toll. For local roads, you pay road tax when purchasing your vehicle.

Some local transport authorities subsidise travel, thereby encouraging the public to use public transport. This may not be true in Bangalore, but in most other cities, there is real subsidy in public transport. In Chennai, you pay a mere ₹10 to travel from one end of the city to another by the suburban train. Technically, you are paying a little for the operating cost with this amount---say, the electrical energy used. The rest of the cost (coaches, railway tracks, electricity line, etc.) is borne by the Indian Railways. The Indian Railways get a quota allocated from the yearly budget.

We collectively contribute to building and maintaining this infrastructure in the form of taxes---income tax, GST, etc.

### The socialistic aspect

We already discussed education. In a socialistic economy like ours, healthcare and other life-essential facilities should also be free or government-aided. That brings us to government-run and government-supported hospitals. Private hospitals are also instructed to treat economically weak patients at heavily subsidised rates. The government pays the offset.

In a socialistic economy, good food is everyone's right. India has the Public Distribution System that gives food at subsidised rates. Ideally, the government should directly buy grains and other food items from the farmers, and distribute the food to the economically weak citizens under the not-for-profit model.

## Twisting the model

Political parties of late have twisted the whole model and have gotten people to think that what they pay for is direct investment. Now, we citizens "buy" from the government. We pay with taxes, and by voting for candidates, get wet grinders and free bus or Metro rides in return---who gives us the best "bang for the buck" is what matters.

## What made it happen

Misunderstanding is to partly blame for this misconception. By paying taxes, we contribute to everyone's good life, namely good roads that last, public transport infrastructure that works for everyone, good water and proper sewage infrastructure for all, and so on.

I theorise that at a point, the governments had good infrastructure and other public necessities taken care of, or the inflow of taxes was more than what was necessary at a point to take care of the necessities. Some political parties may have seen this as 'Well, the government coffers have some money to spare, so, why not announce free soda for everyone by placing soda machines at every street corner? Sodas are popular, and people would love us!' And people, perhaps, did love them.

The next year, though, a flood hit and the government had to use some four billion rupees in relief and rebuilding of lost or damaged infrastructure. This, now, they had to do apart from maintaining the soda spots. Soda spots were fantastic freebies. In the interest of populism, the political entities would continue to run them at four million rupees a day, lest the opposition say, 'Well, this is how the incumbent government is; they could not even keep up their word on free soda. Do you expect them to run the state?'

## The state today

As governments changed, and political parties had to one-up one another, everyone perhaps lost sight of the _maintenance_ of assets. We forgot that building an asset alone was not enough. Maintenance got neglected. As officials and parties became more and more corrupt, the quality of whatever maintenance used to take place (if at all), deteriorated. Today, we are at a point where there are more individuals going to hospitals about spine issues than there has ever been. Public transport is grossly failing in cities like Bangalore, where the authorities are [cutting trips to reduce traffic](https://timesofindia.indiatimes.com/city/bengaluru/traffic-snarls-force-bmtc-to-slash-trips/articleshow/69708929.cms) (which is a snowball in itself).

Reduced public transport increases private vehicle movement, which increases traffic, which further reduces public transport. Increase in vehicular traffic reduces productivity, deteriorates life, and further forces people to ride two-wheelers, which may increase traffic a little, but further deteriorates health, which decreases quality of life, to counter which, people spend more money in medication.

## What if...

Assets, when maintained well, can live longer. At the same time, everything has its life, despite maintenance. If you construct a railway station for four billion rupees, it will last with minimal maintenance for twenty years. But after twenty years, it will require an overhaul. The tiles will need a change, the wall plasters will have to be redone, and after forty years, the entire structure will have to be more or less rebuilt. Forty years later, the cost of rebuilding will be twelve billion rupees. If you had not maintained the station properly, the station will need a re-build in twenty-five years. At that time, the reconstruction will cost nine billion rupees.

In other words, you will have to start saving for this in advance. One way of doing this is investing the money the government or the government agencies have, into something that has good returns. The returns should be good despite the inflation. If the government was busy giving freebies that are unnecessary (or non-critical), where will development come from?

Here is an example: A politician promised laptops to students. One laptop costs, say, ten thousand rupees (when purchased in bulk, with customised hardware). One million students enrolled for education during the year. The total cost was 10 billion rupees. The next year, the government will have to spend another ten billion rupees. And ten billion the next year, and so on.

Instead, if the government built state-of-the-art computer labs in schools and made them available twenty-four hours a day, with the same ten billion rupees, spent in one year, students will benefit for three or even four years. The minor downside will be that not each student will own a piece of hardware. Which is acceptable; we are a socialist economy.

The benefit of free rides to women is not great. Women avoid buses not because they cannot afford it, but because of safety. Instead, assign an armed female police officer to each bus in the city, and more women will travel. You will also increase women employment in the police force, thereby empowering them better. Buy more buses as the number of commutes increases. A plus to this is reduction in traffic as well as pollution. This and much more is achievable at 12 billion rupees a year.

As a citizen, think of what is a good freebie, and what is not. Some freebies are nice, but they kill our economy.

Comments? Take it to your own favourite platform and engage!

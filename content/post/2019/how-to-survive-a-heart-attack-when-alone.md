---
date: "2019-11-05T00:00:00Z"
tags:
- health
- tips
- mythbuster
- lifestyle
- whatsapp university
title: How to survive a heart attack when alone
---

I am part of a family WhatsApp group, and my people are quite active there. This is my door to what is known as _WhatsApp University_. One of the messages I received there was about surviving a heart attack when alone. The message that followed this was:

> I have received this msg several times... It has intrigued me. (sic)

The message reads (sic):

> This is from Dr. Geetha Krishnaswamy, Please give your 2min and read this:-
>
> Let’s say it’s 7:45pm and you’re going home (alone of course) after an unusually hard day on the job.
> You’re really tired, upset and frustrated.
>
> Suddenly you start experiencing severe pain in your chest that starts to drag out into your arm and up in to your jaw. You are only about five km from the hospital nearest your home.
>
> Unfortunately you don’t know if you’ll be able to make it that far.
>
> You have been trained in CPR, but the guy who taught the course did not tell you how to perform it on yourself.
>
> {{< smallcaps "HOW TO SURVIVE A HEART ATTACK WHEN ALONE?" >}}  
> Since many people are alone when they suffer a heart attack without help, the person whose heart is beating improperly and who begins to feel faint, has only
> about 10 seconds left before losing consciousness.
>
> However, these victims can help themselves by coughing
> repeatedly and very vigorously. A deep breath should be taken before each cough, and the cough must be deep and prolonged, as when producing sputum from deep inside the chest.
>
> A breath and a cough must be repeated about every two
> seconds without let-up until help arrives, or until
> the heart is felt to be beating normally again.
>
> Deep breaths get oxygen into the lungs and coughing
> movements squeeze the heart and keep the blood circulating. The squeezing pressure on the heart also helps it regain normal rhythm. In this way, heart attack victims can get to a hospital.
>
> Tell as many other people as possible about this. It could save their lives!!
>
> A cardiologist says If everyone who gets this mail
> kindly send it to 10 people, you can bet that we’ll save at least one life.
>
> Rather than sending jokes, please..contribute by forwarding this mail which can save a person’s life….
>
> If this message comes around you ……more than once…..please don’t get irritated……U need to be happy that you have many friends who care about you & being reminded of how to tackle….Heart attacks….AGAIN…
>
> From:  
> DR.N Siva  
> (Senior Cardiologist)

But, how much of this is true? How much of what is true is valid in 2019? So, I set out to find out some ways to increase the chances of surviving a heart attack when alone.

{{< toc >}}

Before we go any further, a legal disclaimer: Understand that I am NOT a medical professional. This post is NOT medical advice, and comes without warranties. This is merely a post with general knowledge, and I am not responsible for the good or the bad that happens to you after reading this post and/or following what it says. The same applies to this arbitrary Dr. N. Siva as well---you do not know who he is, or whether he even exists. Treat these things with a grain of salt. Sucks, I know, but nothing is a substitute for a medical professional.

## What causes a heart attack

I have been around cardiologists enough to know a thing or two about heart attacks. In very general terms, a heart attack is caused by the heart finding it hard or failing to pump blood, because the heart itself cannot get enough oxygen-rich blood.

To be a little more elaborate (skip this paragraph if not interested), our hearts have arteries that carry oxygenated blood to the heart. Our heart is a muscle, and it needs oxygen like any other muscle, to function. This oxygen is delivered by blood via the Coronary Arteries. Over time, cholesterol deposits within the walls of the arteries cause deficiency in flow, or sometimes, one of these arteries tends to rupture, causing blood to spill out. Just like anywhere in the body, blood platelets in this blood that is spilling out, tend to clot the blood. This clot may stop the blood from flowing through the artery. This causes the muscle that this artery supplies blood to, to die of lack of oxygen. This in turn forms a scar, leading to more problems. In oversimplified terms, a heart attack is the outcome of this part of the muscle dying.

## How do you know you're having a heart attack

Chances are, you cannot _know_ whether you're having a heart attack. All _you_ can do is guess based on symptoms (and the more of these you have, the more likely it is that it's a heart attack). Worse, sometimes you don't even realise you're having a heart attack---the symptoms could be that faint. However, here is a non-exhaustive list:

1. Pressure in the chest
2. Breathlessness
3. Cold sweat
4. Severe fatigue
5. Fainting feeling
6. Nausea
7. Pain in the chest
8. Pain in the chest, extending to your arms, jaw and the back
9. Chest burn
10. Pain in the upper abdomen

It's also possible that you experienced five of these and your doctor, after running tests, says you did not have a heart attack.

## What to do when you get these symptoms

Two sentences: Do NOT ignore them. Get medical help.

Understand that the best chance of your survival is under medical care. A heart attack is serious, and could be life-threatening.

But there is something called first aid, and these are the steps:

1. Breathe. Handling the situation calmly will increase your chances of survival manifold.
2. Pick up your phone and {{< amazon "a tablet of Aspirin" "https://amzn.to/3aZArO6" >}}, and sit reclining on a wall or the head of a bed (half-sit).
3. Prop up your legs with {{< amazon "a pillow or something" "https://amzn.to/3z33wAm" >}} (something light enough for you to easily lift/pull).
4. Call Emergency Services---in India, it's 108 (try 112 in the unlikely event of 108 not working). In 2019, you can press the power button on your phone thrice in quick succession to call emergency services.
5. Breathe evenly and tell them it's urgent, and that you think you're having a heart attack. Give them your name and location. Finish the call in about 20 seconds.
6. Take the Aspirin if the medicines you take do not interfere with it, and you're not allergic to Aspirin. (Ask your doctor beforehand.) Make sure it is uncoated Aspirin so that the absorption is faster. If you can, chew the Aspirin and keep the particles under your tongue. You may drink half a gulp of water to help you swallow, but if possible, avoid it.
7. Keep the tablet wrapper clutched in your fist so that if you faint, those who treat you know you've taken the Aspirin. Better yet, carry a note that says, 'Aspirin taken' or something along those lines.
8. Call someone for help---a friend, a neighbour, or someone nearby. They could help guide the ambulance or with the hospitalisation.
9. Stay put. Wait for help to arrive.

These days, medical technology has advanced enough to help you monitor your general cardiovascular parameters. For instance, {{< amazon "a pulse oximeter" "https://amzn.to/3b2tPyw" >}} tells you the saturation of oxygen in your blood. You can keep an eye on your blood pressure on a regular basis by measuring it every day using a {{< amazon "BP monitor" "https://amzn.to/3zszzLi" >}}. My mother's physician likes to see a trend of it over a period of a couple of weeks so that he can tune her medicines accordingly---we maintain an {{< amazon "Excel sheet" "https://amzn.to/3b0M47z" >}} for it. Some monitors {{< amazon "like this one" "https://amzn.to/3OKkI3v" >}} even connect to your phone to keep a trend.

## What about the self-CPR

When I read the WhatsApp forward, only a couple of things stuck out as odd (apart from one _major_ flaw). But applying further thought to it brought more to light. Here it is:

First of all, a CPR is NOT done in case of a heart attack; a CPR is for cardiac arrests. And yes, the two are different. The former can lead to the latter, but CPR is not a treatment for a heart attack.

I repeat: CPR is for cardiac arrests, not heart attacks.

I've received emergency response training in first aid. So, I know enough to tell you that a CPR is given for cardiac arrests. And I know for fact that no cardiologist would ever suggest a self-CPR for a heart attack. If you don't know what ABC means in this context, you're automatically disqualified from handling any CPR, let alone self-CPR. Is ABC some "classified" laypeople first-aider term or something? No. The point is that if you didn't know the first thing about swimming, you wouldn't jump into the ocean to rescue someone drowning in it.

Now, notice something of importance that the message says: "You have been trained in CPR, but the guy who taught the course did not tell you how to perform it on yourself." Notice, 'You have been trained in CPR'. This must be understood as, 'If you're not trained in CPR, you should not do this.' CPR isn't as easy as they show in films; it must be handled with much more seriousness. And it's taxing at some level. But if you asked me if I'd give myself a CPR of this kind because I'm trained in CPR, no, not based on these instructions. Here is why:

I'm healthy enough to be able to handle a trial run of self-CPR, when I'm conscious, so, I tried doing it exactly as instructed in the message. I breathed in, deep, and then coughed as though I'm throwing out stuff from within my chest. I did it once. Twice. Thrice. Four times. Five. I was already panting. Six. Panting harder. Seven. Eight. Nine. Ten. Eleven. And I stopped. Checked my heart rate. And decided never to do this again.

Do not attempt self-CPR when having a heart attack; without medical supervision, it could kill you. How?

Your normal rate of breathing is roughly fifteen per minute. During a heart attack if you're feeling short of breath, I'd imagine that rate itself would be hard for you to maintain. Depending on how much pain you are in, you may or may not be able to breathe deep or cough "as when peoducing sputum from deep inside the chest". The CPR procedure helps restore normal heart rhythm mainly because of the rhythm at which it is performed. And sometimes it may not, but could keep the victim's brain alive. This also means that the procedure is effective only in situations that pertain to the rhythm of the heart.

Yes, consciousness depends on the blood flow, which in turn depends on the rhythm of the heart. But, in case of a heart attack when your heart is already not receiving enough oxygen and is probably finding it difficult to maintain a normal rhythm, do you want to worsen the situation with arrhythmia by misfiring, and possibly die because of it? The instructions talk nothing about the rhythm! Or having you check your heart rate, blood pressure or any of your vitals _before_ performing a self-CPR. It says "until help arrives or the heart is felt to be beating normally again". But it doesn't define "normal" here---normal rate, normal rhythm, normal pressure? Normal what? All three? What if your heart begins to fibrillate? Or you exert too much and cause more damage to your heart?

It also says, "A cardiologist says If everyone who gets this mail kindly send it to 10 people, you can bet that we’ll save at least one life." Not only is the cardiologist (or the validity of the claim of their saying this) unknown, one case might truly benefit out of thousands, but this might also end up killing thousands of those who would've survived if they'd only called and waited for medical help.

## How are my steps any better

A valid question.

First of all, maintaining a calm, regular breath may help with the cardiac rhythm and the blood pressure. It also helps you think straight. Your instinct would be to panic; conscious, controlled, calm breaths will help counteract the ill effects of adrenaline. If you're losing balance or fainting, well-defined rhythmic breath could help.

Second, you need to limit your movements. Therefore, you pick up the things you need in one go and then get to a state of rest. Your phone is a lifesaver. It helps you communicate. Next, taking an Aspirin reduces the chances of a clot forming in the artery that may have ruptured (and could prevent a cardiac arrest). Aspirin acts on the platelets and prevents them from coming together to coagulate. But remember, an open artery will lead to internal bleeding, and hence the necessity for medical help. This is about weighing the less harmful option; a little internal bleeding against cardiac complications.

Half-sitting might prevent you from fainting. Fainting occurs when your brain (or more specifically, the cerebellum) does not receive enough oxygen, and fails to maintain posture. Sitting so will reduce the need for the heart to pump the blood hard enough against the gravity (pressure head, from your physics class), for the brain to receive the required blood. Propping up your legs accelerates the travel of blood to the heart and the brain from the lower parts of the body. This helps you maintain consciousness for longer.

Calling for emergency services is a no-brainer. Calling for an ambulance is better than having someone drive you to the hospital because the ambulance has the necessary equipment as well as trained paramedics who can help with the condition on the way. The objectivity, training and professionalism are the key. Besides, an ambulance has a greater chance of quickly navigating the traffic than your friend in a car. However, if you know the ambulance will take too long to arrive, you may need to weigh your options. Again, if you are a cardiac patient, you should probably have a Plan B. Although, remember never to travel pillion on a two-wheeler in that condition. If you faint on the way (which is likely), it will all go south.

It's important that you're accurate and crisp on the call with emergency services. All they need is information, and that's all that's worth your effort.

Crushed Aspirin under your tongue absorbs quickly because the area has a rich supply of blood. Absorption of swallowed Aspirin could take about forty minutes. If you are a cardiac patient, your cardiologist must have prescribed Aspirin (e.g. Ecosprin---even Disprin would do) and/or Atorvastatin (e.g. Ecosprin AV). Avoiding water is also advised so that you don't choke and worsen the situation while you are probably finding it hard to breathe. Chewing the tablet increases the surface area making absorption significantly faster. Despite this, if you choose to drink water, that should be about two tablespoons.

Medical professionals working on you must know that you've taken the Aspirin. They'll treat you accordingly. Aspirin could interfere with some medicines, and the medics could base their prescriptions on whether you've taken the Aspirin.

Your friend might be able to help the medics with information about your medical issues, whether you're allergic to something, and many other things that need human presence. Therefore, call for help.

The rest really is up to the various factors governing the situation. Have faith in people. Yes, time is of essence, but it is not meant in seconds. Though a heart attack is serious, you're quite likely to survive, provided you call for help.

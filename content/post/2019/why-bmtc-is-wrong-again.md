---
date: "2019-10-11T00:00:00Z"
tags:
- bmtc
- bangalore
- commute
- traffic
- public transport
title: Why BMTC is wrong (again)
---

Sometimes, some pieces of news bewilder you to such levels that you feel utterly hopeless about life around you. It happened to me today, when I read:

> BMTC seeks congestion tax on high-density corridors; bus lane trial run on ORR from Oct 20.

To anyone who lives in Bangalore and commutes every day (the commute involving using at least five kilometres of the pothole-ridden adventure tracks, commonly known as "roads" in Bangalore), this is no less than a crude joke. Show this to any Bangalorean, and the response would be, 'This was the only tax remaining to be imposed. My life is complete.'

## The tax on roads

The goal of taxes is to help implement practices, infrastructure and other facilities thereby making a citizen's life better. Road taxes in Bangalore is among the highest in the country. Yet, Bangalore is among the cities with the worst road infrastructure. Bangalore is the city with the coolest video depicting the sorry state of its roads (so cool that cities in other countries are using the method to shame their civic bodies).

A 200 m stretch of road near where I live has been laid thrice in the last four years. Once the road was said to be dug by a telecom provider so cables could be laid. Then, after a few months, the road was relaid. A few months later, BWSSB dug it up again to lay the Cauvery pipeline. The road was relaid a few months later. A few months ago, the road was dug again, by the same BWSSB for some other reason---some other pipeline---some said sewage (I don't know why, because we already have underground sewage in our locality), some said Cauvery water (again, something we already have in our locality). The road was relaid a few weeks ago.

This last exercise seems to have undertaken in most areas of Bangalore, thereby further crippling the already-crippled local transport by road.

## Commute

I have lived in Chennai for three years. Commute in Chennai has never been a pain for me. Thousands of (or even a million) commuters in Chennai will tell you the same thing. Yes, there are situations when the MTC bus for your route will take forever to arrive and when it arrives, would arrive with people overflowing, but in general, there are well-planned routes connecting you to strategic locations in the city. Then, there is the suburban railway. And MRTS. And now, Metro. There is virtually no place in Chennai that cannot be reached by public transport, without much hassle. Even today, I rarely use Ola or Uber in Chennai.

In Bangalore, public transport is a nightmare. Once I had to travel 6.5 kilometres. Of the 2.5 hours I spent in all, 1:40 were spent on commute. In what sane world does one take 50 minutes to travel 6.5 kilometres? And this is not even the worst case.

My brother used to spend 5.5 hours in commute. Every day. My colleague now spends the same time---she travels from Ramamurthynagara to Electronic City. I used to spend four hours when I used to work in the "regular" work timings of 10:30 AM to 7:30 PM. The peak hour traffic woes in Bangalore is simply unimaginable to a non-Bangalorean.

## The problem with BMTC, in short

BMTC doesn't "get it". I wrote a long blog post on the BMTC Snowball Syndrome. To sum it up, BMTC has routes that seem to have been created out of whims. Their fares are high. With every passing month, the number of passengers using BMTC drops. It drops partly because the fares are too high, and partly because people find travelling with BMTC extremely time-intensive. Because people stop using public transport, the number of vehicles plying on the roads increases (people have to go around, one way or another), leading to more time consumed by the buses because of the increased congestion. This also leads to the increase in the fares because of BMTC having to use all of their resources to serve a smaller number of users.

At the same time, BMTC has not been maintaining their buses, and expanding their fleets. You still see thirty-year-old buses plying on the roads. This leads to increase in the maintenance costs. Since the number of trips overall cannot drop, a smaller number of buses end up taking more trips than usual. This leads to further wear of the buses, increasing the maintenance costs further. This has been going on.

Now, BMTC thinks that it cannot afford to maintain their entire fleet of Volvos, and so, are planning to dispose of some. This would further reduce the fleet size, and lead to increase in costs. Of course, some of it will be partially covered by what they recover from disposing of the Volvos, but those funds will exhaust sooner than we think.

> Half the fare, double the fleet.  
> ---Bangaloreans

BMTC, just like every other piece of physical infrastructure in Bangalore, did not grow with the city. Everyone involved, including the commuters, is to blame for the state of public transport in Bangalore today. However, BMTC's steps have only exacerbated the condition.

## Induced Demand

This effect was pointed out by a [great article on traffic in Bangalore](https://www.thenewsminute.com/article/what-s-behind-bengaluru-s-traffic-woes-82-lakh-vehicles-too-many-one-ways-and-more-108409) that I read a while ago. The article says that, according to research, increasing the width of the roads, (or constructing flyovers or underpasses) also gives rise to a corresponding increase in the traffic, thereby quickly filling up the roads again. This is because when you widen a road or construct a flyover or an underpass, people tend to think, 'Well, now the roads are wider, and there is space for more vehicles.'

I have been here nine years, and I can say for fact that there are much more flyovers than there were when I first came here, and many of the roads have been widened. Some of the roads that were essentially mud paths, are now asphalted. There is much more area covered under asphalted roads today (even if you don't count the potholes as road area). And yet, the time taken today for me to get from one place to another is roughly 160% of what it used to be for the same distance, nine years ago. In 2010, I have travelled from Manyata Tech Park to Uttarahalli in eighteen minutes. Today, it takes me 35 minutes at the same time of the day (or more appropriately, night).

The average speed of vehicles has gone down over the years. We simply cannot average 75 km/h today---it's unthinkable. The average speed of vehicles was calculated to be [18.7 km/h in August 2019](https://www.deccanherald.com/city/life-in-bengaluru/average-commute-speed-in-bengaluru-drops-to-187-kmph-757943.html). And get this. I rode my bicycle to work one day. I averaged 21.6 km/h (including the two short breaks), while my cab averaged 19.8 km/h, around the same time of the day. This fact is known to every two-wheeler-rider in Bangalore. Therefore, the two-wheeler registrations in Bangalore per month is [35,000 (2019)](https://www.thenewsminute.com/article/what-s-behind-bengaluru-s-traffic-woes-82-lakh-vehicles-too-many-one-ways-and-more-108409).

## Congestion tax

Understand this, please. The only incentive that BMTC can offer today, which will positively impact its usage is perhaps _make travel by BMTC free_. BMTC observed an increase in the number of users when they reduced the fares as a short experiment. But the fares were brought back to normal after the short period. And the number of commuters came down to what it was before the experiment.

Next, understand that urban Bangaloreans care more about getting to their destination in the shortest-possible time. We would not mind paying a few rupees as a premium towards it. The number of vehicles that take the NICE Road every day are an example for this. If the goal is to reduce the traffic and encourage people to use public transport, this is a terrible idea. Of course, this move, if approved, could help a little with the cash crunch that BMTC is suffering from at the moment. But this is not going to help anyone else. This is not a fair deal---the only entity to benefit from this move is BMTC.

On the flip side, this will not go well with Bangaloreans if the tax is too high. As a citizen, this is how I see it: I face delays because of bad roads and traffic jams (which is the authorities' fault, not mine). My vehicles undergo tremendous stress because of the current road conditions, and that translates to money spent in maintenance. I have to pay significant amount of money to take alternative routes, such as the NICE Road. Now, because of the traffic and bad roads, I have to pay money to even take the badly-laid regular roads with chaotic levels of congestion. In other words, I have to pay money to go from one location in the city to another, apart from the fuel charges, no matter which way I take. Again, the citizen is at the losing end of the deal.

What's worse is that today, we have mapping applications. The public would start taking narrower roads as alternative routes. This will lead to cacophony in the hitherto calm residential streets.

## Possible solutions

I don't have many. The first is to, of course, speed up road and maintenance work. The authorities have taken up way too many development projects at the same time, across the city. (This tells me, Bangalore has not learnt anything in the last decade and a half.) The way out now is to aim at quickly finishing the projects. I am unaware of whether project work goes on during the night. If it does not, it should. The digging could be done in the day, the laying in the night so the sleep of people in the surroundings is not disturbed. Road laying is easier and faster in the night.

The next is not for the authorities, but for the tech companies that operate in Bangalore. We are in 2019, and a lot has changed since the company policies were made in most of these firms. It is perhaps time to tweak those to allow employees to work from home. My workplace is lenient about working from home; as a company, we don't care where someone works from, as long as the work is done. Of course, the onus of ensuring that we have good network connectivity and power is on us, but it is still manageable in Bangalore. What if this could reduce the traffic on roads by about 30%?

The third is for the citizens. Going bumper-to-bumper is not only a risk to the vehicles, but also, leaving enough space for movement of vehicles speeds up the overall movement of vehicles on the road. Take for example, someone wants to turn around on a road. The moment the person tries to manoeuvre the vehicle, two-wheeler riders from both the sides try to squeeze through the few inches of gap on either side of the turning vehicle. The others halt their vehicles as close to the turning vehicle as they can, so as to be "space-efficient". It does no one any good. Nobody moves. Similarly, crossing over to the opposite lane so that they can fill an arbitrary spot that seen a few vehicles ahead is another factor contributing to congestion. What is this obsession with utilising every square-inch of road? Studies have shown a significant reduction in travel time if everybody followed some simple traffic rules.

Finally, BMTC. BMTC has become a miscalculation machine. Despite BMTC travel being so expensive, it does not compare in quality with the ease of use offered by other bus transport corporations in the country. Their fleet is depleting quickly, the fares are increasing and the number of passengers is dropping. The corporation must take a step back and reflect on its operations, its route planning and the fleet strength. There are serious flaws in how the services are operated, and they need to be fixed.

Taxing the citizens further is only going to prove counter-productive.

---
date: "2019-07-21T00:00:00Z"
tags:
- null
title: My Granny and Burma
---

On a regular visit to my grandmother's house in Chennai, we would hear one or the other episode of their exodus from Burma (now Myanmar). And some of these stories, we would lip-sync with her, to mock her. She would notice it, but continue to tell the story. Defiantly.

This nature of ours was so ingrained in us that we did not even give it a second thought before we mocked her or attempted to ignore her when she told us the story. Of course, one couldn't blame us; we'd heard a thousand repetitions of those stories.

A couple of days ago, a friend asked me if I had any artefacts from way back in the past: material history. I said, 'None at the moment.' I was in Chennai. Either way, I don't think we carried forward anything of much importance. The oldest thing in our house may be, what, thirty years old. But I told her, 'If you are okay with a story, I could ask my grandmother for one from her Burma days.'

'That would do!' she said.

That evening, I went to my grandmother asking her if she could narrate the exodus while I recorded. That surprised Granny. This was perhaps the first time in decades that I'd volunteered to listen to the story. Doubt lingering in the back of her mind, she narrated the story. And I listened, this time, because I wanted to ensure all the details were right, and I was capturing the mood. For my friend.

I felt ashamed for the first time in the context. Here I was, listening to the story of survival defeating all odds, not because I wanted to know (and I should know), but because my friend wanted it.

This is the first time I realised how I was missing the details in the story, despite the number of times I had heard them. When I first listened to the recording, I apologised. To myself. And shamefully, forgave myself on behalf of Granny.

But now I know.

And now I've heard the recording tens of times in the course of making this video, that I now know the details of their travel. Not that I'm proud; I'm ashamed it took me three decades to realise this.

## The story, in short

My grandmother is 90 today. A month short of 91. This story is from when she was 14. For a woman of that age, to recollect all this information after 76 years is nothing short of a feat. This tells us what impact those times had on her. Of course, like every story, it may be a little corrupt here and there, because that is how human memory works, but those little corruption happen in the little details, not the framework.

Granny's parents had settled in Myanmar. Her father worked for Brooke Bond, and so did a maternal uncle. Another maternal uncle was a jailer in Burma. He served as a jailer in the famous Insein Prison as well, for some time.

When she was little, Mahatma Gandhi had visited their school in Kamayut, Rangoon. She had shaken hands with him. Her teacher, a Brit, did not like it. He beat her and sent her out of the school. Her uncle took her along with him; she studied with her cousins.

In 1942, Japan was trying to capture Burma. American troops came to the aid of the British; Myanmar was a British colony. But normal life was farther than a dream. Granny was in Magwe (Magway) at the time, with her uncle's family. Uncle had asked Granny's family to move in with him. Everyone other than Granny's father and elder brother had stayed back.

The rest is in this video.

<div style='text-align:center;'>
  <iframe src="https://www.youtube-nocookie.com/embed/D9y2NJ2sNQ8?rel=0&theme=light&color=white" width="560" height="420" frameborder="0"></iframe>
</div>

## Some anecdotes missed in the video

The video is actually a mellowed-down version of what happened at the time. She was in a hurry to finish the story because we had told her we would not be able to play anything more than ten minutes.

First, the Chief Jailer's daughter's kidnap. They had all entered the trench one day, during a bombing episode. They remained there until they heard the all-clear siren. When the opened the trench and came out, they could not find the CJ's eldest daughter. She was a beautiful lass, Granny says. The army personnel had kidnapped her, and no one knows what happened of her after that. But they did not even have time to cry.

Soon, they had to leave their life there behind. Their houses, their belongings, their everything.

When they were passing through the forests, they saw hundreds of dead bodies strewn around. Keep in mind, half the population was of kids. Impressionable innocent children who did not understand death or anything that serious.

When the rains began, the rotting of the corpses sped up. And the top layer of soil would cover them. There were times when they stepped on the corpses, and their feet would sink into the rotting flesh and bones. Imagine the fear in their minds, of catching the same infections that had caused these deaths.

One ounce of water at a time. That was all they could drink. They had to walk long distances. They could not carry much, even water. Carrying too much food or water would weigh them down. To add to the situation, this was wartime. Supplies were strictly regulated. All they could manage was a handful of rice a day. They would light a fire, pour all the water they had gathered during the day, each threw in their handful of grain, boiled it into a watery porridge, and called it their daily meal.

The forests were dense. Dense rainforests of the north-eastern states of India are well-known. While they perhaps did not have a threat from wild animals, partly because they moved in large crowds and partly because of such animals not being part of those forests (except the tigers of Bengal), there was always the possibility of someone getting lost. The Naga tribes being cannibalistic is perhaps a myth, but those were not the times when education was common.

## All that aside

The fact that these men and women survived such a hostile environment at the time is nothing short of miraculous. I cannot imagine myself doing that.

A wartime environment, with danger lurking at every bend, with an oppressive government that thanked all goodness for every civilian death; when a civilian death meant less than a number to mourn, but meant one less mouth to feed, one less soul to care about. The resolve these men and women had at the time, being on their own, the sense of sticking together despite differences, helping strangers, treading strange and dangerous paths, being responsible for those younger than them (Granny was merely fourteen at the time), going through the tough days, falling down, getting up, brushing off the bruises, wiping the tears, ignoring the pain and marching on in sickness and health, with the faint hope at every uphill climb that this was the last, and the belief that someday, somehow, they would get home, take a bath, wear clean and warm clothes, hug their grandparents, sit at the porch merrymaking, not having to worry about digging trenches or watching bombs explode in front of their eyes seeing everything within the reaches of the shockwave pulverise, munch on groundnuts and throw pebbles at each other while Mother called them in for lunch. Warm food that awakened the palates, that gave them the sense of satisfaction and a mild buzz, followed by dreams of gardens and bicycles.

We take these for granted today; to them, every next breath was a surprise.

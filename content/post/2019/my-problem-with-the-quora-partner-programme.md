---
date: "2019-09-06T00:00:00Z"
tags:
- quora
- crowdsourcing
- opinion
- abuse
title: My problem with the Quora Partner Programme
---

One of the things I do is answer questions on Quora. I'm not a regular or anything. I crossed the 10,000 mark only a few days ago. There are those with millions of views per week. I'm nowhere near, I'm nowhere that dedicated to anything in the world.

But I do get answer requests every few days. These range from "okay" to utterly stupid/obvious. I am against systems that make people intellectually lazy, and unfortunately, with Quora rolling out the Partner Programme, it is slowly but surely becoming exactly that. Instead of boosting reasoning and thinking, Quora may soon become what Yahoo Answers had.

I subscribe to typography. And typography is one of the areas I answer on Quora. A few answer requests I received were for questions like this (in ascending order of meaninglessness):

> What is the best font for professional websites?  
---[Source](https://www.quora.com/What-is-the-best-font-for-professional-websites)

> What is the maximum font size in MS Word?  
---[Source](https://www.quora.com/What-is-the-maximum-font-size-in-MS-Word)

> What fonts in Word 2016 are serif fonts?  
---[Source](https://www.quora.com/What-fonts-in-Word-2016-are-serif-fonts)

> What is the Helvetica font?  
---[Source](https://www.quora.com/unanswered/What-is-the-Helvetica-font)

Everyone is looking to make a buck. At the same time, there are quite a few platforms that benefited a great deal from crowdsourcing, such as Google Maps. We don't mind answering questions without being paid for it; this is how we record what we know and pass it on to the future generations. Some of the knowledge certainly is trivial; such as, 'Why does the sun rise in the east and set in the west?' I've asked that question as a child. Most of you have.

And by all means, one could use Quora for it.

But would you ask a question like, 'What time is 6 PM?' Not 'What time is 6 PM IST in Espoo?', but 'What time is 6 PM?' Is that a sincere question? I would even argue that 'What time is 6 PM IST in Espoo?' is not a sincere question. You could look at the world map with time zones and find it out for yourselves. But of course, in the age of tools, you could use Google's time calculator, or add it to your world clock on your phone, or something. Make an effort, first.

To someone who answers questions out of nothing but the sense of serving the learning community, this feels like dirty work. It is annoying.

Quora might say, 'Well, go ahead, downvote the question.' To that, I ask, is that my responsibility? So, you encourage intellectual laziness---and laziness in general---by literally paying for it, and you have others, who are doing the actual work of answering, clean up for you? For what? In what sane world is this a fair proposition?

Again, Quora might say, 'Well, if the questions aren't worthy enough, they will not be read, and the person asking the question will not be rewarded. Also, go ahead and ignore such questions; questions that don't have an answer don't get shown in results.' But again, there are thousands of us spending our mental energy to read these questions and ignore them. And ignoring doesn't really help. Those that ask these questions actually get our names suggested to "Request an answer". We have to spend our physical energy and time to clear up our inboxes. So no, this is not harmless. Slowly, but steadily, we will stop answering questions plainly because it's annoying. It has been more so since the roll-out of the _Partner Programme_.

But of course, Quora gets new registrations every day. Everyone is dispensable. All that, yes. But is this not harming the community in general?

Here is another reality check. I'm no fan of Google, but the fact is, Google is most people's homepage. Most people use the Google search bar to get answers to questions. Some directly go to Wikipedia. This is NOT changing anytime soon. Unless, of course, Quora builds its own browser and forces people to use the Quora search bar first, which would use Google in the back-end but first, look for answers to questions within Quora and display those as the initial results. Honestly, Quora, what is the percentage of organic traffic on your platform? And how many times of that is traffic from search engines like Google?

I'm not against Quora in any way, yet; I have the same enemy as Quora---those who misuse the platform. But the current model of Quora is flawed. I know there are those in Quora that are working on sorting this mess, but I want to be one of the external voices that calls out the abuse.

I refuse to clean-up after abusers. And so does most of the community that answers. The sooner you fix this, the better it is.

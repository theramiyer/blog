---
date: "2019-03-01T00:00:00Z"
subtitle: 'In retrospect: the 26th Feb 2019 attack and beyond'
tags:
- opinion
- government
- politics
- defence
- democracy
- diplomacy and alliances
title: Surgical Strike 2.0
---

I am an ordinary citizen. And as an ordinary citizen of a country that hates terror attacks, and personally holding the same stand as my country, I was upset and immensely angry on the 14th of February 2019, when forty CRPF _jawans_ got killed in a terror attack. The terror outfit, _Jaish-e-Mohammed_ claimed responsibility for the strike. I was upset that India’s September 2016 strike on terror camps in Pakistan hadn’t borne fruit---that the terrorists hadn’t received the message the right way. The first thought that crossed my mind was the wish that our intelligence find Masood Azhar and our defence castrate him in public, after which, any citizen who liked to kick him in the gut, was allowed to.

On the 26th, I woke up to the piece of news that a dozen Mirage 2000 aircrafts of Indian Air Force had entered Pakistan-occupied Kashmir, and bombed a region, destroying a terror camp of JeM. I was ecstatic. Why? In a little bit. But of course, none of this is a “political” or “defence” analysis. This is just what a common Indian citizen knows and understands. Here is a gist of the topics touched upon:

{{< toc >}}

This article is available as a [typeset printable PDF](https://www.scribd.com/document/422074817/Surgical-Strike-2-0) as well.

It was fantastic news. What I had read at the time was that twelve jets entered PoK, and only while they were returning, Pakistan identified them and scrambled its jets. But there was no military engagement, no civilian casualties. This was just another example for why we are the fourth-strongest air force in the world. Having been part of the Aero India 2019 show (which I had been to on the 23rd) and having interacted with IAF personnel on a few things, I could much easily relate to the whole operation.

Needless to say, Pakistan convened an internal meeting, and long story short, decided that they would like to show us that they can enter our air space, too. Of course, our defence were on high alert following the strike, and a dogfight ensued when their jets entered our air space, at the end of which, two planes had been shot down, one of ours and one of Pakistan’s. None of the pilots landed on our side of the Line of Control, and our Wing Commander who fell in Pakistan’s region was captured by them. Talks of de-escalation began.

Social media was on fire. There were some saying India should go out on a full-blown war, while others said Pakistan was right in what it was doing. Then there were personal comments. And there was the so-called “warmongering”. The worst of what I saw was Rahul Kanwal of India Today. Disgraceful, to say the least about what Kanwal said. What B.S. Yedyurappa said on Thursday was probably the only worse thing to Kanwal’s statements. Of course, the latter later “clarified” that his statement was taken “out of context”. Anyway.

## On India’s actual problem

Understand that this particular strike was never about Pakistan. Imran Khan had said, what the world now has is a new Pakistan, which likes peace and quiet, and is anti-terror and development-oriented. He’d said that this Pakistan wants to look at better ties with the rest of the world and so on. Fair enough. India does not have a problem with Pakistan, per se. We have a problem with terror outfits operating out of Pakistan. One of them is the JeM. Pakistan has maintained that it cannot see them in their land. (Cough, Abbottabad, cough!) India could. In my opinion, India said in its gesture, ‘No worries, fella, we got this.’ As per my understanding, the International law gives every country the right to eliminate an imminent threat to it. Our attack was _specifically targeted_ on the JeM camp, which _happened to be in Pakistan_. Because our intelligence reports indicated at near-future attacks by the JeM, we were within limits to go hit the target.

An important point to remember is that in this strike, no civilians were injured or killed. (That great news I spoke about.) No military establishments were attacked. The camp was far away from any civilian or military presence. Therefore, I don’t see a problem in India’s attacking the terror base, which was allegedly not only used by the JeM, but other outfits as well. I will not comment on the numbers because there is no official word on it.

## Pakistan’s response

Here is where perspective comes into play. Pakistan (unwittingly, if I may say) considered this an aggression. Their problem statement seemed to be, ‘India crossed the Line of Control into our air space and did what they did, on our land.’ Their stand was that there were no JeM camps in the areas that the IAF had attacked. Their stand was that the IAF dropped bombs on empty lands. Therefore, adding their A and B, this was an aggression on non-military Pakistani land which had nothing to do with terror.

Now, is this true? As an Indian who goes by what my defence organisations and my government say, I am relieved to be told that  the said terror camp was destroyed by the IAF, and I would say, Pakistan is wrong. However, I will not go as far as speculating that Pakistan government is run by its defence, which in turn takes instructions from the militants. But think about it this way: If Pak accepts that India did indeed drop bombs on terror camps, it would be accepting that there were terror camps on Pak soil. If there were terror camps on Pak soil and Pak hadn’t done anything about it, then it had failed as a peace-loving country, it had shamefully failed in its surveillance, and had not kept the promise it had made to the world that it would curb all forms of terrorism on its soil. The _only_ stand Pakistan can take at the moment is maintaining that there are no terror camps. Now, if it said there were no terror camps, then what did the IAF hit?

## Some definitions

Aggression: When someone violates borders or lines of control with an intent to attack a military or civilian establishment.

Surgical strike: A military strike conducted with a specific target in mind, without collateral damage. This target, usually, is a military establishment. (Source: Wikipedia)

India’s strike: I deliberately add this to clarify that India’s 26th Feb strike was not a conventional surgical strike, but a _non-military pre-emptive strike_ targeted on Jaish-e-Mohammed. This has been more than made clear by our government. This strike was surgical only in that it specifically targeted a camp, and caused zero collateral damage. No military establishment lost anything---on either side---no civilians were injured or eliminated. Anyone who can see the difference between the sun and the moon can see the difference between an aggression and a non-military strike.

## Imran Khan’s obvious stand

Given the above stand that Pakistan (obviously) took, it is in Imran Khan’s interest---as a Prime Minister and as a Pakistani---that he stand by that statement. Again, I would not speculate on what happened to Ayub Khan, or Yahya Khan, or even the Bhuttos. As usual, Pakistan demanded that we give them “proof” that the Pulwama attack was done by JeM. There is no place for proof in my opinion because JeM themselves owned up to it. But as usual, the Indian government agreed, and handed over a dossier to them as always.

Credit is due where it is due: When Khan spoke out to the public, I did find it refreshingly “statesman-like”, and very mature on Pakistan’s part. Great articulation. The approach was different, in a good way, from those approaches by the past Pak leaders. However, coming to think of it, in the address to the world and to India in specific on the 27th, I’m sorry, but what content was new in it? He did not accept the argument that there was a terror camp in Balakot, he was misinformed about the number of pilots, and ultimately, asked for peace. His statement, though, was not obnoxious like in the past; it was simple and one acknowledging both the powers as opposed to blindly yapping:

> With the weapons you have and the weapons we have, can we really afford a miscalculation?  
> ---Imran Khan, Prime Minister, Pakistan

## The Indian stand

But here is what is wrong with Khan’s statement, which of course, many will not accept. He spoke of war---on the past wars, which rightly, have all been miscalculated. India didn’t speak of war. Everyone on both the sides needs to understand that India---neither the defence nor the government (though they don’t operate as different entities on this side)---said we want war. The government only said that we were ready for any eventuality. War is only _one of the eventualities_. Every sovereign country that has a military establishment has to be ready for any eventuality. So are we. Where is the talk of war?

Ostensibly, in the newsrooms.

## On the Prime Minister’s address

Our politicians are as shameless as any out there in the rest of the world. Therefore, there are those asking why Prime Minister Modi never addressed anyone like Pakistani Prime Minister Khan did. Some of our politicians are more disgraceful than the rest; they go around beating their chests about the strike, and talking of the number of seats. The media organisations that are calling them disgraceful are themselves being jingos in their newsrooms. But let us keep all that aside for a while.

There was a time when I criticised PM Modi for always taking the centre stage---such as announcing demonetisation---and that he never gave any other minister their due voice or power. It seemed like autocracy to me. And to many others as well. This time, though, (unquestionably valid criticism that he is busy with rallies even at this situation aside for the moment) something good has happened: If this is the Minister of External Affairs’ purview, what is the need for the Prime Minister to address the nation or hold talks with other countries? He’s being part of the talks that he’s needed in. If we felt that the PM was going around to several countries building ties with India, and doing what was actually supposed to be done by the Minister of External Affairs, I think it is good that EAM Swaraj is doing what is her job to do. Just because Pak chose to let its PM speak, why should India? We work in our way, they work theirs, isn’t it? But having said all that, PM Modi should ideally be addressing the crowds as the Prime Minister, and not a politician; speaking of the valour of our soldiers, building their confidence and the confidence of the common public, and not taking jibes at political opponents. Why will we not be angry with a party that behaves in this way?

## “Better PR” by Pakistan

One of the accusations made on the Indian government is how we are releasing statements hours after the fact. How Pakistan is “setting the narrative” while “we’re playing catch-up”. I would like you to think about the number of things the Pak government said in an attempt to have “better PR”, which turned out to be false later. A few cases in point: The number of jets used by Pakistan, the type of jets used by Pakistan, the number of Indian pilots they captured, etc. Most of their initial statements were retracted at a later point, which ultimately conceded with the statements that we gave.

India is among the soft superpowers in the world. The approach we took was: There is always one statement, and only one statement. We should continue with that model of working. Every statement made to the public needs thorough fact-checking. And fact-checking takes time. Better quality of information over “Better PR”. Any false statement that comes from the government out to the world makes us look bad. An example is how one of our government officials once gave fictitious numbers about the percentage of doctors in the US, and NASA scientists, etc. being Indians.

Another aspect I loved about the Indian government’s handling of this situation is how there is only one voice. There is no chaos. There is no clamour. There is fact gathering, there’s assessment, there is review, and finally, a single statement (and no looking back). This process is necessary, especially in a world of information. Information needs to be ascertained _before_ being released. I am happy as long as this model is being stuck to.

## Call for peace

Understand: A war is detrimental to everyone involved in it, except those selling the weapons. Pakistan is only getting started on its journey of development. If they do get involved in a war with us, whether they win or lose, they have very little to lose. At most, they will lose a bad democracy, an almost-crashing economy, of course, apart from millions of innocent lives. India, on the other hand, even upon winning the war, would have lost much more than Pakistan can even begin to imagine. We will lose our morale. We will be economically twenty years further backward. Not only will we lose a lot of funds in equipping our military establishments for the war, we will have to rebuild a lot in the country, and bringing it back to what we are today will take decades, by which time, the world would have progressed a great deal.

And all this for what? We never had a problem with Pakistan as a State. We have a problem with the terror outfits that are operating out of it (again, I will not speculate on whether they orchestrate the Pakistani government). But think about this:

- Would destroying Pakistan eliminate terrorism?
- Would this war not lead to a destruction of Kashmir, which is what is actually at the base of all this? (For the benefit of the short-sighted: LeT, JeM, etc. are causing terror here in India, in order to claim Kashmir---either make it an independent state, or conjoin it with Pakistan.)
- Do we still understand that Pakistani people, Pakistani government, Pakistani defence, and Pakistani militants are different entities? While the second, third and the fourth may be operating on a single agenda, the first of the four is the majority, and the majority of the majority have nothing to do with all this. Even though a good chunk of the said majority of the majority are short-sighted (within the rights of human beings, just like us) to ask for a war.

## On defence

Indian military establishments are collectively called the Indian Defence. For a second, sit back and concentrate on the meaning of “defence”. Military establishments are not for a nation to go on a war, but to defend it from attacks. And India still is the country that does not offend. We don’t violate ceasefire. We don’t attack neighbouring civilian or military establishments.

The only change in India’s behaviour is that today we enter neighbouring territory to attack _terror groups_---not civilians, not the military establishments, but terror groups. And this is well within international laws.

## Return of Wing Commander Abhi

Many see this as a “peace gesture”, while in reality, it is just something Pakistan (along with several other countries including India) agreed to: Geneva Conventions. This is not some unconditional gesture Pakistan is showing. This is not a leverage for de-escalation. This is simply following the rules. I wouldn’t make a personal attack on Pakistan saying things like, for the first time they are following a rule without making a fuss about it. That would, first, be fictitious (because a lot of fuss did happen, unfortunately) and second ... well, let’s leave it at that.

One point of stupidity was how people on (social and other) media posted family history and other things about the Wing Commander, in spite of the man saying that he could not tell Pakistan anything else about himself other than his name, his service number and for reasons best known to him, his religion.

And one point that I am immensely proud of is how the gentleman, in the enemy territory, surrounded by enemy force personnel, was composed like cold steel. He knew exactly what to say, and said it with resolve.

## On knowing what to say

Part of it, I said already. The rest is, notice how our Indian officials have so far used measured words to accurately convey messages. We (as per our tradition) have been open about claims we made about losing an aircraft, an IAF pilot being “missing in action”, etc. We did not make claims on who he was until very recently. None of what we said was said without proper analysis, and we said nothing more, nothing less. This speaks volumes about transparency and good communication, and why we are a respected nation in the international space. Someone once asked me, ‘You say you’re proud to be an Indian. What exactly are you proud of?’ This is one of the things I’m proud of.

It was easy to say ‘Our Wing Commander Clooney McRobertson was captured by the Pakistani Army. His family lives in Townsville. His father is an ex-Mayor of Townsville. His mother is a surgeon in the Townsville Medical College. But of course, none of this is related.’ It is also easy to say, ‘Well, our boys played well, and brought down four hundred F-16s, even though Pakistan has only 160 of them.’ Instead, our statements were, ‘It is premature to comment on it at the moment.’ or ‘The concerned officials will take a decision and revert with a statement.’ or ‘We lost _a MiG-21_ in the process, and _the pilot_ is missing in action.’

Having said that, there are those asking what attack was done on the terror base, and what was the damage done to it. There has not been a word about it from our side, while Pakistan is circulating satellite pictures of craters seemingly created by our attacks, the craters being in forests, at least two hundred metres from what could be considered “targets”. This is an important point. In order for us to claim that we did a non-military pre-emptive strike on a JeM training camp, we need to prove that we did hit a JeM training camp. That piece of evidence is the only one that says that this was indeed a non-military pre-emptive strike, and not an aggression. In other words, without that piece of evidence, what we did will be construed as an aggression.

Am I saying that I don’t trust my government or the defence? Absolutely not. Does the question upset people? Yes. Is the question necessary? Yes; I want my country to be seen as a trustworthy player in the international arena, and this piece of evidence is necessary to ensure that it is.

It is commendable that the Indian Air Force is exercising restraint by not revealing anything beyond what the government has authorised, but it is important the government make available the data about the strike and the locations, or at least explain why the data has not yet been released. No government official was sent to survey the damage done (in spite of Pakistan’s open invitation), no independent body has verified it from our side, either. Those keeping quiet are keeping quiet only because of the faith that the Indian government will not lie about these things. Parties lie, politicians lie…

## An important question

Should the matter de-escalate? One hundred percent. Should we stop fighting terror? Never.

Understand that the situation is taking a very different turn which is not in the interest of anybody. And I think that’s India’s stand as well. India says, ‘We are ready for any eventualities.’, Not ‘We want war.’ We want an end to terror. If calling for an end to terror leads to war, we are ready for it---that’s the meaning. Being ready for something and wanting it are two very different things. I want peace, not only between India and Pakistan, but more importantly, within the Indian territory. _Eliminating terror is an important step in the direction._ And my country has enough wise individuals in the right places who are doing what they have to. Not making unnecessary noises will go a long way in letting them concentrate.

I stand by my government. I stand by my country’s defence. In that order, because my country is a _parliamentary_ democratic republic. And I trust that my government (not an individual, not a political party, but the government) cares for my country’s defence as much as it stands for the citizens, if not more.

Jai Hind.

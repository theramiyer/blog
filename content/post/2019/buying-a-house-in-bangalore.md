---
date: "2019-05-12T00:00:00Z"
tags:
- bangalore
- real estate
- life
- traffic
- development
- sensibility
- economics
title: The economics of home ownership in Bangalore
---

When I was in college, my dream was to buy a house in the Kathipara area in Chennai. I was a simple, unthoughtful middle-class child back then. As an unthoughtful middle-class child, my nature was to roll with the mainstream, whether something mattered or not. And the mainstream, more often than not, works on sentiments. Buying a house and "settling down" is one such sentiment.

When I moved to Bangalore, my aunt told me, 'You know what? Real estate is much cheaper in Bangalore compared to Chennai. Buy a house there.' Of course, with a gross CTC of a mere 2.04 lac rupees per annum, no investment was an option. I did not fret over it. Then came higher education, and that drew out six thousand rupees a month out of my pay. Investment stopped being an option once again. I tried to buy a plot somewhere in Kanchipuram, but I dropped the plan once again because I understood that I would have to forgo one meal a day if I took up the investment as well as education with the income I made.

One day, when talking about regular life, I mentioned to a senior, 'Well, I thought I would buy a good house here and settle down. Perhaps bring down my parents here, and after brother graduates, reunite the family under a single roof. But ...' My senior stopped eating what he was eating, and chuckled. '_Buy_ a house? In Bangalore?'

I nodded.

'Well, you may want to rethink that. It's not worth it unless you have a significant amount of black money to spare. A house in Bangalore is not an investment. Think about it.'

I had never seen it that way. I had never given it a thought on whether I should go for a house or not. All I had ever thought about was _how_ to buy a house. One of my friends had already booked a house in Bangalore, and she was running around to get the documentation and everything straightened (which was proving to be a real pain for her). I overheard people in the elevator, the rest room, everywhere, talking about buying a house. They discussed prices, locality, amenities, and all those aspects you consider before you make the decision. And here I was, at the edge of some potential new revelations in that regard.

Thoughts about a house in Bangalore were a mess. These were the questions in my mind. Is it worthwhile to buy a property in Bangalore? Does it make sense to buy a plot or a house in Bangalore? What are the different points you should look at before buying a house in Bangalore? Is it sustainable to buy a house in Bangalore? Is real estate a scam in Bangalore? I thought I should break down some points before I went ahead with any concrete thought on buying property in Bangalore.

## Basic requirements

The first point I looked at were my requirements. What kind of house did I need? What was the area I needed? I figured that I needed a three-BHK house, of about fifteen hundred square feet. It would allow for a decent amount of moving space, and would comfortably contain all the furniture, appliances and other necessities that we would have as a family. The next question was, where I wanted this house to be. I am a typical South Bangalorean. I like the Banashankari and the Uttarahalli areas. I chose the place; this is where I wanted the house to be.

The next question I asked was whether I was all right with a flat. Turned out, I was not. I wanted an individual house. I do not like being part of a dysfunctional association which is indecisive. Flats in the urban middle-class Bangalore is typically unorganized, unmaintained and sometimes, even unruly. I was not comfortable with flats that had more than four houses. Four-house flats in Bangalore South, though, per my observations, did not come with three bed rooms. Two bed rooms were a norm. Independent house was the more logical answer.

I started checking out listings of plots in Bangalore. All the plots I saw in South Bangalore costed over three million. I started looking for listings in other localities that I knew and liked. I found one plot listed for 2.5 million. But the locality was not convenient. The plot was too far from a main road, and the surroundings were not ... maintained. Some good options were available at about 3.7 million. But this was beyond my budget, and banks typically did not give good amounts in loan to buy land⁠---banks liked houses better; banks preferred flats.

## The loan

I went and asked a bank official nonetheless, 'How much of the total can I expect a loan for? I would like to buy a land worth about forty lac.' The official asked me for my monthly take-home and all, and told me, 'Well, if you would like to buy a flat, we sometimes go up to 90%. For a plot, we don't loan so much. Besides, even if we do loan it, you are better off going for a home loan than land loan. The condition will be that you start constructing on the land soon.'

'How soon?'

'Well, almost as soon as you buy the plot. We typically release funds in instalments.'

I calculated: Four million for the plot along with the registration fee. Two million for the construction. One million for the interiors. The official said the bank tries its best to ensure well-being of the customers, and offers an amount that lets the customer have at least forty percent of his monthly pay for himself. I asked him if I was eligible for a loan of seven million. He said I was eligible for about two million, given the kind of loan, my credit score and my monthly income. This was apparently with a _conservative_, safe interest rate.

That night, I created an EMI calculator in Excel, and started calculating how much I would be paying, what the total amount of interest going to the bank and so on. The tool painted a grim picture.

What I also learned was, I would be paying almost 210% back to the bank by the end of the EMI tenure. Reducing the tenure was not an option because of my income, and keeping in mind that the bank would not take more than 60% of my salary.

Going for a flat was the alternative.

## Depreciation

A building is an immovable asset. Those who do not know what that is, a building is an asset that cannot move (I know). Why does this matter? The value of the asset depends on its surroundings as well. If the surroundings develop well, the value of the property goes up. Bus stops, malls, schools, Metro stations, etc. appreciate the value. If the surroundings deteriorate, or two years later, someone finds out that the sewage line and the water line are right next to each other in the locality (and the information goes public⁠---ahem), the value of the property will plummet. Apart from this, the property being movable or immovable determines the _depreciation_.

I would not have to worry much about the depreciation if I were owning an entire plot, because the value of a plot almost always appreciates. Compared to the appreciation of the land value, the depreciation of the constructed building would be too low to worry about. But not in case of a flat. When buying a flat, you would be owning an _undivided share_ (or UDS) of the plot where the flat is. Essentially, you divide the total area of the land among the owners of the houses in the building, based on how big their houses are. For instance, if a plot (60″ × 40″) has four houses divided among two floors, of which two houses are 800 sqft. big and the other two are 600 sqft. big, the UDS for those who own the 800 sqft. houses would be:

> area of the house owned  
> ÷ total area of all the houses combined  
> × the total area of the land
>
> that is,
>
> 800  
> ÷ (2 × (800 + 600))  
> × (60 × 40)  
> = 686 sqft.

The value of this 686 sqft. does not depreciate. But the value of the 800 sqft. constructed will. When you calculate the price of the property during resale, more often than not, the price will always be higher than when you bought it, if you own an individual house. If you own a flat that has four houses, the chances of the price going down is still low. But in Bangalore, the norm is having 40 to 140 houses in a complex. The builders almost always deviate from the approved plan (construct five floors instead of four). This leads to more houses in the complex, thereby reducing the UDS further. To builders, this is "forward thinking" because some day, the development authorities will make the norms more lenient (history says that this is always a matter of time).

If two acres get divided among forty houses, what are you left with? In this case, would the value of your property appreciate or depreciate? Yes, in the initial years, the value will appreciate. But soon, the depreciation will begin. By the time I complete paying back the loan, the value of the house would have gone lower than I paid in all.

And remember, you cannot sell your UDS as a piece of land. You have zero freedom with it. The one way it helps is by reducing the depreciation by an almost insignificant amount.

The construction is not great either. Most developers (I will refrain from naming them⁠---because this is a norm, and probably no builder is an exception) use substandard materials, are sometimes negligent⁠---for example, you can knock on some of the tiles in my house, and you can hear a hollow underneath⁠---and construct without leaving enough time for the concrete to cure. Seepage of water, cracked plasters and chipped edges are a disturbingly-common sight in Bangalore.

## On area

You should take a good look at the area you get as well. Your builder would say that the house is 1400 sqft., but do not take that at face value. The area includes a part of the common areas like the corridor, the elevator, and everything within the premises that you can think of. Typically, you end up paying for these as well. If you are among the ones who like every square inch accounted for, consider the walls. Not more than 800 sqft. of your 1,400 sqft. house may be usable.

This is not a real problem as long as you feel the area is adequate for you. Most of us do not measure the area in a house. By looking at different houses and hearing their owners tell us the area of the house, we have built our own scale of judgement. If someone tells us the house is 1,500 sqft., we automatically come to a conclusion on whether the space is enough. The point is, two houses may say 1,500 sqft. on paper, but the carpet area on both of them would measure somewhere around 850 sqft. The point is that we know the general size of the house we need; the actual numbers are a means of judgement. And of course, they decide the price.

But it may not be so when you try to resell your house.

## Resources

My flat does not have a borewell. We rely on the corporation water. On the days that the corporation water is insufficient, we buy water from private entities, popularly known as "The Tankers".

In Bangalore, ground water is a real issue. The population of Bangalore is far more than the piece of land can handle. To top it off, trees are getting cut, land in the lake areas are getting reclaimed, and the city is now a concrete jungle with no real water table recharge capabilities; they exist on paper, not in reality. All the water from the rains that fall in Bangalore, flows out of the city. The non-existent trees and lakes cannot aid the replenishment of water anymore. More are people sucking out water than there are who help with the recharge. Naturally, the water level in some localities of Bangalore is over 1,300 feet deep. This is certainly abnormal by all scales, but that is the reality.

Flats typically charge maintenance fee. This can be anywhere between two thousand rupees to twelve thousand rupees. May be more. It depends on the locality and the amenities that you get.

The amenities are sometimes a scam as well. The builder builds a gymnasium, a swimming pool, a children's park, a walkers' area, and what not. But two years after all the houses sell in the complex, the maintenance of these amenities deteriorates. The water is seldom changed in swimming pools, damaged gym equipment is not replaced, etc. Builders charge for these amenities when selling the house to you, saying something like, 'Amenities cost you about ₹150 per sqft.' This does not sound like much right away, but for a 1,500 sqft. house, the number comes to ₹2,25,000. The most you can do at a later point is say you will pay a smaller maintenance fee, which the builder will almost readily agree. This way, he does not have to repair or replace the equipment, and at the same time, has the official right to not maintain it⁠---the maintenance is subject to the maintenance fee, which you have now refused to pay. He made ₹2,25,000 already from each house anyway! Oh, and your loan will not cover this ₹2,25,000. Good, you do not have to pay interest for nothing, but you still have to shell out the money you could have used for, say, better furniture.

Scam? I would say so.

## Land ownership

Owning a land alone in Bangalore is not safe either. Land mafia in Bangalore is public knowledge, and I know of individuals who almost lost their lands to the mafia. One of them got saved because BEML, which is a government enterprise, had allotted the piece of land to his father⁠---a then employee⁠---and the family had lived on that land for over twenty years. Corner plots in good localities, plot surrounded by empty plots, plots in semi-commercial or purely-residential areas (that covers most liveable plots anyway) are targets. The general consensus in Bangalore is that owning a flat is much safer.

## Daily life

Next comes an aspect of paramount importance. The location of your house governs aspects of life. For instance, my brother used to travel from Uttarahalli to Whitefield a couple of years ago. His commute was a nightmare. I cannot do something like that on a daily basis. He used to spend five hours on the road every day. To me, this is unthinkable. If I owned a house in Uttarahalli, and my office were in Whitefield, I would either have to quit my job or sell my house if I wanted anything that even remotely resembles life. In other words, there is little you can do if you own a house in an inconvenient locality in Bangalore.

You may say that depending on public transport or taking a cab is useless for such travel. Using a two-wheeler is an option, which is a different kind of risk, given the unfriendly roads (or their lack thereof). [Hospitals are reporting](http://www.newindianexpress.com/cities/bengaluru/2019/may/01/bumpy-roads-in-bengaluru-give-young-adults-pain-in-the-back-1971276.html) cases of back issues hitherto uncommon in youngsters, all caused due to the road conditions in Bangalore. If you rented a house, movement is easier. The other alternative that people are using is renting a house near their workplace and leasing their own houses to tenants. Ask yourself: Do I want to do this?

## Accessibility

Some of my friends are stuck in certain places because they think it is risky if they moved to elsewhere. For example, the last time we had a medical emergency at home, the emergency services told me that the ambulance will take twenty minutes to arrive, given the traffic at the time of the day. Driving to the hospital in that traffic was a secondary challenge.

## Real estate prices

Real estate in Bangalore is serious (good) business. One of my mother's cousins went from nothing to owning a swanky house in a swanky neighbourhood and a BMW because of real estate. At the same time, real estate is a safe haven for those with black money⁠---this is no secret. Those with a lot of black money do not mind spending; the prices go up as a result. If you are spending your hard-earned money, you should think where the scales are, and who is tipping them.

But is it not better if prices keep appreciating? As it turns out, no. Localities get crowded once all the plots get sold. Real estate businessmen are not interested in resale of properties; the margin is low in it. Developing a place that has nothing is more interesting. And lucrative. A place that has nothing but barren fields will sell at a much higher margin than one in a residential locality. Businessmen like converting barren fields into residential plots. This way, they buy an acre at four million and sell it at forty. The one that buys this plot wants to construct as many houses as he can. He crams forty houses there. Spends a million on each house, and sells each house at 5 million.

Bigger players buy the plot at four million, construct forty houses and sell each house at four million (5 million, but ­buy NOW at 20% discount: The never-again-in-a-lifetime offer!). ("Once in a lifetime" is a cliché.)

This brings me to:

## Resale

Reselling the house is something you should consider when buying a house. We urban Indians are becoming more and more mobile. You can see thousands of Chennaiites in Bangalore, thousands of Hyderabadis in Chennai, thousands of Delhiites in Hyderabad, thousands of Bangaloreans in Pune ... you get the idea. Even within the city, your house may stop meeting some of your deal-making or -breaking requirements.

Besides, understand that when you try to sell your house, you would have lived in that house for some time. A potential buyer of your house would come with his idea of a great house. Given that flats are not constructed all that well these days, the potential buyer's dream will be crushed when he enters the basement of your building (the least maintained place in a building). That creaking elevator with flickering lights will discourage him further. The peeling paints and growing moulds on the exteriors will poke his eye. The smell in the dimly-lit corridors will seem to suffocate him. The fresh paint on your walls may seem like a fresh breath, but it will help his troubled mind too little to make a positive impact. He would compare the furniture and interiors with what is current in the market. He would notice the tiles and the doors. He would feel that he is losing out on too much by buying your house.

You, on the other hand, would say, 'Well, this is a prime locality. You have everything starting from a _subzi mandi_ to the Metro station within a five-minute walking distance.' You would quote a price almost equal to what a house of a similar size would sell in the outskirts. But the buyer would think, 'Well, why not pay a couple hundred thousand more and buy a house in the outskirts? I would get all the bells and whistles of a new building, and the locality would develop further anyway. It is only a matter of time. The realtor said an IT park would be coming up there, and that it would not surprise him if the next phase of Metro covered the area.'

And this is not baseless. I am a first-hand witness of this situation. Here are two more:

## Case in point: A bad sale

Someone I know bought a two-bedroom house in a nice locality for about thirty lac (three million) rupees, about twelve years ago. He sold the house (because he wanted a bigger house now that his family had grown) and bought a bigger house in a developing neighbourhood, about three kilometres from base. He found it hard to sell the house, because nobody was ready to buy an old house at the price that the realtors claimed the houses in the locality were worth.

Some owners claim that their house should be worth a certain (high) price because they have furnished the house well with good furnishings. What they fail to understand is that everyone's taste is different. What may seem great to you may not seem so to a potential buyer. For all you know, the potential buyer thinks that your taste is too gaudy. Or what they already own conflicts with what you have, or your design kills space for what matters to them.

In such a situation, you could end up selling the house for a mere million more than what was the price when you bought the house. You could end up losing almost two million rupees in bank interest. Not to mention that you probably cannot use most of the furnishings in the new house, because, "It just doesn't work that way!"

A two bedroom house in the locality costs you a rent of ₹14,000 at the most. Whether you are an owner or a tenant, you would pay a maintenance of ₹1,500. Twelve years ago, the rent would have been about ₹4,000. A five percent increment in rent, term-on-term would have cost him a total of ₹7,79,371. Even a generous ten percent increment in rent every term would have cost him ₹10,78,999 at the end of thirteen terms (a term is 11 months: 144 months ÷ 11). He lost almost double that sum by buying the house⁠---or triple, with the actual market trend of 5% increment. Here is the calculation:

| | Rent at 5% increment | Total rent for the term | Rent at 10% increment | Total rent for the term |
| --- | --: | --: | --: | --: |
|  Term 1 |  4,000.00 |  44,000.00 |  4,000.00 |  44,000.00 |
|  Term 2 |  4,200.00 |  46,200.00 |  4,400.00 |  48,400.00 |
|  Term 3 |  4,410.00 |  48,510.00 |  4,840.00 |  53,240.00 |
|  Term 4 |  4,630.50 |  50,935.50 |  5,324.00 |  58,564.00 |
|  Term 5 |  4,862.03 |  53,482.28 |  5,856.40 |  64,420.40 |
|  Term 6 |  5,105.13 |  56,156.39 |  6,442.04 |  70,862.44 |
|  Term 7 |  5,360.38 |  58,964.21 |  7,086.24 |  77,948.68 |
|  Term 8 |  5,628.40 |  61,912.42 |  7,794.87 |  85,743.55 |
|  Term 9 |  5,909.82 |  65,008.04 |  8,574.36 |  94,317.91 |
|  Term 10 |  6,205.31 |  68,258.44 |  9,431.79 |  103,749.70 |
|  Term 11 |  6,515.58 |  71,671.36 |  10,374.97 |  114,124.67 |
|  Term 12 |  6,841.36 |  75,254.93 |  11,412.47 |  125,537.14 |
|  Term 13 |  7,183.43 |  79,017.68 |  12,553.71 |  138,090.85 |
| **Total** | **@5%** |  7,79,371.25 | **@10%** |  10,78,999.33 |

Now, those who pitch buying a house as an investment so that they can rent it out to tenants and earn money on a monthly basis, look at this calculation and combine it with the interest you would be paying to the bank. Does buying a house to rent it out still look like an investment? On a long term⁠---say, five _decades_---may be. But that is too long to start making profits; I would argue that it is not good investment.

## Case in point: A good sale candidate

My uncle bought a house in Chennai for five lac (half a million) rupees in 2005. He got an undivided share of 600 sqft. In 2015, they got their house reconstructed. A builder approached them, saying he would reconstruct the house if everyone in the flat complex agreed to his proposal. His proposal was that he would demolish the entire complex (four houses⁠---two on each floor), and construct a new building which would have six houses split in two floors, plus a parking area on the ground floor. The builder would give a house to each of the current owners, and would sell two for profit. As an added benefit, each of the current owners would get 150 sqft. more in the new house (because they were forgoing a part of their UDS).

At the moment, the price of a plot (60″ × 40″) of land in that locality sells for about a crore (10 million) rupees. The value of the UDS itself in his case is about 1.7 million, over three times of what he bought the house for. Plus, the last house that got sold in the building, sold for over four million.

But do you see that kind of appreciation in property value anymore?

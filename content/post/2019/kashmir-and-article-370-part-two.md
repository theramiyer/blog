---
date: "2019-08-12T00:00:00Z"
subtitle: The Present
tags:
- kashmir
- India
- politics
- diplomacy
- geo-politics
- insurgency
- accession
- history
- civics
- constitution
title: 'Kashmir and Article 370: Part Two'
---

This is the second part of the three-part series on the _Jammu and Kashmir Reorganisation Act, 2019_. If you came here directly, this post alone will not do justice to the situation. [Go back and read Part One.]({{< ref "kashmir-and-article-370" >}}) Reading the series partially will make you judgemental with limited knowledge. This article is available as a [typeset printable PDF](https://www.scribd.com/document/422074646/Kashmir-and-Article-370) as well.

This post talks about the current situation in Jammu and Kashmir. Here are the topics:

{{< toc >}}

## Article 370 over time

Over the last six decades or so, the power of Article 370 has gotten eroded. Nehru himself admitted to this in 1963. The Article also gives the President of India the power to bring changes to the Article by a presidential order (why, this is how Article 35A got added in the first place).

The government has also used Article 370 to extend provisions of our constitution to Jammu and Kashmir, a whopping forty-five times. Our constitution contains the Union List, which are aspects that the Constitution governs, also known as subjects. Ninety-four out of ninety-seven subjects in the Union List of the Constitution now directly apply to Jammu and Kashmir, without the necessity of consulting with its (now-non-existent) constituent assembly. Over 65% of the articles in the Constitution are directly applicable in Jammu and Kashmir.

Question: was "scrapping" Article 370 such a big deal after all?

Coming to Article 35A, one of the paragraphs in the Instrument of Accession protects the rights of the domiciles of Jammu & Kashmir. Article 35A is almost a repetition of it. Which brings us to the question, did it matter that Article 35A got scrapped?

## The story of Assemblies

We talk of two terms: _Constituent Assembly_, and _Legislative Assembly_. In simple terms, the constituent assembly formulates the constitution, and dissolves; the legislative assembly then takes over.

The initial plan for Jammu and Kashmir was that the two countries would withdraw forces, after which, through a referendum, the people would choose to which country they would like to accede.

When even in 1951 the countries had not withdrawn forces, the Jammu and Kashmir National Conference recommended convening a constituent assembly. The United Nations Security Council, two months before this recommendation, had said that the voice of these elected members of the constituent assembly is not a substitute for the plebiscite. Meanwhile, we added Article 370 to the Constitution of India.

The Constituent Assembly of Jammu and Kashmir began forming the Constitution of Jammu and Kashmir, and planned to conclude the matter of accession. In 1954, the Assembly unanimously ratified the state's accession to India, and they recorded in their constitution that the former princely state of Jammu and Kashmir was an integral part of the Union of India. The Assembly finalised the Constitution in 1956, and the Constitution of Jammu and Kashmir came into effect from 1957. The Constituent Assembly dissolved after creating their constitution, and the Legislative Assembly came into existence in 1957.

## A note on our model of operation

We are a federal parliamentary democratic republic. (A mouthful to say.) In a _federal_ democratic system, each region has its state government, which governs the region, in its own way, by listening to its people. This is the role of the state governments in our country. The central government runs the country as a whole, while the state governments run their respective states. Each state has its governor, who is part of the central government.

This is the reason we have two elections: MLA and MP elections. The state government has a Legislative Assembly. The MLA, or the Member of the Legislative Assembly, is part of the state government. The Members of the Legislative Assembly elect their chief, Chief Minister of the state.

During the parliament (or more precisely, the Lok Sabha) elections, registered voters of every constituency elect their MP, (Member of Parliament). The head of these members is the Prime Minister.

Why do we need this system? The reason is that each region is different in its own way, and each region should have its autonomy in how we run it. The area and the population of India is rather large, and diverse. In essence, what applies to Gujarat may not necessarily apply to Kerala. Management of state resources, for example, is better done by the state government than the central government.

In India (and most other federal republic nations), the central government manages areas such as defence, finance, external affairs, while the state government manages areas such as physical infrastructure and education. The state governments have autonomy in areas they manage. This autonomy is necessary.

Union Territories are directly managed by the central government, wherein, the President appoints a Lieutenant-Governor, who heads the union territory. A union territory is less autonomous compared to a state. This is the reason for the expression, '_Reducing_ a state to a union territory'.

Fact is, during the years of formation of the Constitution, three states formed their own Constituent Assemblies, and all the states sent their representatives to the Constituent Assembly of India. The State Department developed a model constitution for the states. But later, the rulers and chief ministers of all the states, in a meeting, decided that separate constitutions for the states were unnecessary; the suggestions made by the states that had their constituent assemblies anyway got accepted in the Constitution of India. Every state, except the state of Jammu and Kashmir accepted the Constitution of India as their constitution by 1950; Article 370 was hence the temporary patch. But Article 370 was the way the amendments to the Constitution of India could flow to the Constitution of Jammu and Kashmir without the Constituent Assembly of J&K.

## Political parties of Kashmir

The political parties of Kashmir have their own agendas that keep changing unpredictably, as days pass. The stand taken by the NDA government is that the autonomy of Jammu and Kashmir did not help the common men and women of the region. In fact, it worsened the situation for regions such as Leh and Kargil. While the members of the Ladakh region are majority Buddhist, they feel the Muslim majority regions have had the most attention from the politicians of the region. Those in the Ladakh region feel let down.

Also, the political parties have aided the separatist groups as well, either directly or indirectly, on more than one occasion. Based on their agenda, the parties spin the narratives and incite people. But the people of Kashmir often feel that these parties are their hope, that these parties will deliver justice. And the parties have indeed, on occasion.

The parties have been unable to amend the Constitution of Jammu and Kashmir to keep up with the Constitution of India over time, making people of Kashmir lead a substandard life, compared to the rest of India.

## The Jammu and Kashmir Reorganisation Act

August 2019 brought drastic changes to the way Jammu and Kashmir operated. While for so long, Jammu and Kashmir had "great" autonomy (highly debatable, of course), the central government stripped it of the autonomy, through the Jammu and Kashmir Reorganisation Act.

What is so wrong about it, why is everyone making so much noise?

This is difficult to explain, in part because the validity or relevance of Article 370 itself is in question, given the eroded nature of the article. The Government of India had almost full control over the state anyway. Elections have been difficult in the state, and the Legislative Assembly had gotten dissolved, effectively bringing in President's Rule in the state.

This meant that the state had already lost all autonomy. Although, there was hope that there will be Assembly elections.

The inclusion of Jammu and Kashmir, as per the norm of democracy, and as per what the United Nations had said, should have happened by holding a plebiscite in Jammu and Kashmir, asking the people what they wanted. A plebiscite is out of question given that the region is still highly militarised. Another way of abrogating the Article, was by the Constituent Assembly of Jammu and Kashmir (as per Article 370), but the Constituent Assembly does not exist.

The Shimla Agreement states that India and Pakistan will handle all disputes about Kashmir bilaterally, and the two parties will entertain no interference from any third party, unless both parties, together, felt the need to. This includes the involvement of the United Nations. This makes the India-administered part of Jammu and Kashmir an internal matter. The Government of India, through a Presidential Order, can amend Article 370, which it did. Since there was President's Rule in the region, the President could get the concurrence of the Governor about the abrogation of Article 370 in the absence of the Legislative Assembly, which had taken over from the Constituent Assembly. The Governor concurred.

## Prime Minister Modi's speech

In his speech to the Nation on August 8, the Prime Minister spoke about critical points about stripping Jammu and Kashmir of its special status. The intended message of the Act essentially was, 'People of Jammu and Kashmir, we will no more treat you different from the rest of the states of India. Welcome to the family.' PM Modi subtly says, 'We have tried ways to make life better for you, to give you the life that the other Indians enjoy, but the attempts have failed. This time, I am trying something new, give me a chance. Let us see how this goes.'

But in the process, the Kashmiris feel that the government has not consulted them in the last seven decades. That this situation is nothing new. They feel that India alienates their Muslim population, and that India sees Kashmir as not people, but as a piece of territory. This is a valid stand; look at what the politicians of the lower cadres are saying. Look at the fear that makes India not let any of the hundreds of television channels run in Kashmir. The Kashmiri stand is, if India trusts the allegiance of the Kashmiris, India should open free speech and free press and television and other communication channels in the region.

But given the situation that prevails, will Kashmir be able to make their decision? Given the militancy and the increase in religious extremism, can India open up these channels of communication? Communication was anyway ceded to India in 1947; the Constitution of India took precedence over the State of Jammu and Kashmir in matters of defence, external affairs and communication per the Instrument of Accession.

## What changes now

In reality, not much in the functioning of people of Kashmir changes now. There was President's Rule in the state, the Governor being the head of the state. The President still is the head of the state. Defence had its presence in the state, and the President's Rule had stripped the police of all power; the central government now controls the forces because Kashmir is a union territory. What difference does it make to Ground Zero?

The statehood and the spirit of autonomy that was still a piece of hope in Jammu and Kashmir is now lost. Though done legally and constitutionally, this move was undemocratic without a question.

But, again, could the government do this democratically? How?

How does direct control over the law and order make anything in Kashmir different? The central government controlled the law and order in the state anyway. Even when Kashmir had its Legislative Assembly and the Chief Minister led the state, the central defence forces effectively took precedence over the local forces.

Then, what changes now?

In my view (based on what I know on the subject), taking away the autonomy of the state does not change much within the state of Jammu and Kashmir given the current conditions. The ground zero does not change one bit. What politicians claim does not matter.

## But, what changes now

India has effectively cemented the connection to Jammu and Kashmir, and snapped the thread that precariously connected it to the Constitution of India. This Act goes a step further from Shimla Agreement and says what is internal is internal, stay away from meddling with it. If you meddle with it, India will no more see it as an aggression against Jammu and Kashmir, but as a direct aggression against the Union of India itself.

India's message to Pakistan is also that there will no more be "going soft" over skirmishes by "friendly" state authorities. Indian Defence will directly take you on.

An important message is, Kashmir is no more a "Flash Point".

Given the bilateral nature of the issue after Shimla Agreement, the world has no say over what is India's own. The point of temporary uncertainty that the provision of autonomy to Kashmir by Article 370 is no more. This Act inseparably integrates Kashmir to India. Kashmir is no more "negotiable".

Pakistan has received this message without ambiguity. Its actions that followed the Act erase all doubt about it.

And the world has received the message. What we heard from the United Nations is enough evidence. The world now sees Kashmir as an internal, inseparable part of India. Of course, China had issues with that the new map showed Aksai Chin as part of Ladakh, but Aksai Chin is a different story; a story of negligence on part of India. A story little known, but that's for another time.

With this Act, and the Prime Minister's speech, India tells the world, Kashmir is not negotiable as Pakistan may have you believe. That Pakistan is probably forgetting the Shimla Agreement. That Kashmir is an internal matter of India. You are free to side either way. Remember, though, what India is and what Pakistan is, both strategically and economically. No matter who chooses what, Kashmir is India, and we entertain no dispute in it anymore. In order that Kashmir is on par with the other Indian states, we abrogated the hoops that we had to jump through, called Article 370.

## Summing up

In this part, we discuss how India operates as a federal democratic republic. We also discuss the civics of this change, the constitutional technicalities, and the geo-political take on the situation. The final chunk that remains is the nuances of these technicalities, to sand out the edges.

That makes this article incomplete. What you have learned from this part alone, is a third of what I feel is important. To complete the picture, you should [read Part One]({{< ref "kashmir-and-article-370" >}}) (which I suppose you have), and [Part Three]({{< ref "kashmir-and-article-370-part-three" >}}) that will follow. Without all the three pieces, knowledge on the subject is incomplete. (Why, I would argue that all our collective knowledge on the subject is incomplete even after reading everything we have on the subject.)

Go ahead, [read Part Three]({{< ref "kashmir-and-article-370-part-three" >}}) to complete the picture.

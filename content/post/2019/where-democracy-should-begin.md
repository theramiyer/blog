---
date: "2019-03-04T00:00:00Z"
tags:
- politics
- democracy
- politicians
- satire
- education
- civics
title: Where democracy should begin
---

Education is perhaps the most powerful weapon in the world. Education, starting from our childhood, shapes our thoughts, actions, interactions, and overall, our lives. Not only are our perceptions based on the learnings of our past, but our assimilation of new learnings depends strongly on the basis created by our experiences and past learnings.

The talk of education brings to us the thought of literacy. We start thinking about how our rate of literacy is improving year on year. While the mainstream may suggest that literacy is a precursor to education, logically, they need not be correlated. And it is not, if we look at how we work as humans. For example, there may be literate members of the society, who stopped schooling after the fourth grade.

This is also not to say that those who underwent schooling are educated. There is an age-old, well-known point about the difference between the learned and the educated. But the line can be blurred in _this_ case.

## Modes of education

All sources of education can be broadly classified into two: formal and informal. The former is imparted by established institutions such as schools and colleges, and the latter are the other modes, which will be talked about in a moment. Statistically, informal education is more accessible to the common public than the formal, despite efforts by the government and the non-governmental organisations.

Folklore and cultural performances are two of the informal ways of acquiring knowledge. And today, I thought I would talk about the importance of this mode of education from the perspective of a democracy.

## On satire

Digressing a little: In my third grade, I was introduced to the "art class". My art teacher was a big consumer of cartoons. He endorsed R.K. Lakshman's _You said it_. Cartoons that my teacher drew used to have some elements from the great Lakshman's style. One day (still in the third grade), I decided to have a look at the cartoon in the daily. I saw the cartoon, I read the caption. I did not understand what was so great about the piece of work; what made the cartoon so funny. I did not (could not) appreciate the satirical value of the piece. Unfortunately, I don't remember, two decades later, what the cartoon was about that day.

The point is, assimilation of anything of satirical value requires the viewer/reader to have some level of understanding of what is being attacked. If the taunt is short-lived, it may not make a serious impact on the minds of those who don't understand the context, however, if satire is misused, and if the same taunt is repeated over and over, the impact may be adverse.

Take for example, two of the most talked-about aspects: democracy and secularism. Taunts on these have corrupted the actual understanding of the idea for those who did not understand what they are. Superficial knowledge (of the said taunt) is openly available, in the form of political speeches and debates involving politicians and garbed lobbyists. These terms that refer to great ideas have been reduced to being derogatory terms, such as _demo-crazy_ and _sick-ularism_.

Today, while I was pondering over how some of the powerful are misleading the common man into forgetting the difference between a party and a government, an epiphany hit me, when taken in conjunction with the fact about the low rate of education in our country: We don't teach enough people about democracy in the first place, while videos where people laugh at 'demo-crazy' is being made freely available to everyone! Factor in the amplification of this effect by the open and free availability of access to the Internet and on-demand media in every nook and corner of the country, and you are looking at the India of today, where even the learned don't appreciate the concept of democracy anymore.

This has led to political parties making a mockery of it by leading people into thinking that democracy is only about (insignificant) votes, and about forming coalition governments. And there are parties that are making statements about how a coalition government is bad for the country. People's cheering for this idea in a democracy is nothing short of appalling.

But this brings us back to the point on education.

## The role of informal education

Here is the epiphany I spoke about:

Today's folklore is full of stories of kings and princes and princesses; our epics are about kings and their duels. There are stories of invasions (and not about inclusions). Not one story I heard in my childhood was directed towards anything close to democracy. Even if any were, it was never pointed out in specific, in a way that it contested the firm understanding of---and belief in---monarchy, subtly infused into us by the sheer number of stories of kings and queens and their families.

While the idea of using our current terminology (such as _rājya_ for state) was to make the understanding easier for the common folk, I feel that these terms may be subtly conveying a message that we did not intend to: That we, the "subjects", are still "ruled" by the "rulers".

Result: We are intrinsically biased towards monarchy and dynasticism.

## On rulers

We would like to think that our rulers of the past, such as Ashōka, Rājarāja Chōla or Akbar were great, a majority of them have not really been the kind to really empower people the way they should have. All our stories are about them having a lavish life, with boatloads of respect from their subjects, with every aspect of their life being celebrated in the entire kingdom. While there are stories (and/or chronicles) about how their sons and daughters were educated to great levels, there is hardly any about the children of common men being educated. Mostly, education comes in the context of the children of the elite members of those societies.

This not only subtly gives the idea that education is not an important aspect of the commons' children, it mostly talks about what a great life the kings and their families had. It seems that this is the general attitude we still carry, as much as we would like to think that we are progressing intellectually.

As a corollary, this also makes the current "rulers" mainly focus on the power, the wealth, the comfort and the respect. And we still continue with the ocean of gap between the "high society" and the commons, in terms of financial status, status in the society, the lifestyle, etc. We have completely forgotten the basis on which the Republic of India was formed---that we are a sovereign socialist democratic republic.

## On democracy

Democracy is a relatively new concept[^old-democracy]. Democracy is not how the world at large worked in the past. If democracy were common, the fact that I say that democracy is a relatively new concept, should speak about how it was never emphasised upon in my childhood. I was introduced to democracy in my civics class. Only later did I gather more from editorials and other columns in newspapers and magazines. And Wikipedia, of course. As mentioned above, the understanding I gathered from the latter sources was based on what I gathered in my civics class. In other words, my first source of understanding democracy was _formal education_.

[^old-democracy]: Democracy was not unknown to the Indian subcontinent, per se. Some sects within the Indian subcontinent do seem to have followed the democratic system, such as the Gopālas of Bengal, the Pāndyas/Chōlas of Uthiramērūr, and Guru Gobind Singh. However, all of this is little-known, and while elaborate for those ages, these systems only share some similarities with modern democracy.

Given that formal education is, statistically, much weaker in our society, there should be more stories told to the common folk, that talk about the concept of democracy. And this should happen _before_ politicians misuse their reach to deliver the message that monarchy, autocracy or aristocracy or a combination of some or all of these is the way for India, and that the public should push for it. If such campaigning succeeds, we would leap from the current conservative-party-led position to the extreme right, which would be quite the opposite of democracy[^extreme-left].

[^extreme-left]: An extreme-left-led position is equally anarchic. Read more in my post, [Capitalism, communism, socialism … WTF-ism]({%- post_url 2019/2019-02-19-capitalism-communism-socialism-wtf-ism %}).

## On the common sources of informal knowledge

There is no question that the government makes efforts towards providing children with free education at least until the age of fourteen, but the mere numbers of schools that are actively teaching children (let alone the quality of education in the schools), and the number of parents sending their children to school is short of desirable. Informal education has a better chance of delivering ideas to the commons.

Doordarshan seems to be among the better ways that we currently have, to deliver knowledge, but again, given the reach of satellite television and the popularity of commercial channels over government-run channels, the situation is discouraging. This is the age of _Chhota Bheem_ and _Little Singham_. Unfortunately, the former also builds upon our past of monarchy (and the latter is simply "masala"). It instils in the minds of the children that kings are great, and everyone is happy when a good king rules. Granted, it may be true (and it is, in many countries out there), but India of the twenty-first century is a democratic republic.

Perhaps the government, along with learned citizens, can begin a programme to create a set of modern folk tales and fables that talk about self-governance, democracy, the greatness of the secular philosophy[^secular-hinduism].

The idea is to make these learnings less classroom-like and more fun. Memes are another form of conveying these ideas. Of course, the lateral thinking required to convey these complex ideas in memes is beyond my capacity (to be optimistically realistic, I would add "as of now"). But there certainly are those who can handle this. Short films like the ones on cancer awareness do not seem to have much of an effect, though, despite the unquestionable reach.

Regardless, we are a billion minds (no pun intended---this has nothing to do with the allegation that an organisation with a similar name has been hired by a political party in India to manipulate voters). We should not be working on shortcuts when political parties are investing all of their time and effort in propagating disinformation, and celebrating ignorance and misinformation.

Let us work towards a better nation; a better democracy---the democracy that the makers of our nation and the writers of our constitution dreamt about and worked towards, shedding their blood, sweat and tears.

[^secular-hinduism]: Hinduism is, in fact, a secular philosophy in itself. The sceptical can---and should---read at least a little of Swami Vivekananda, who talks beautifully about why Hinduism is great in its inclusive nature, along with perhaps a little history on how Hinduism was never a single religion, but a conglomeration of several schools of thought, philosophies and theologies, which has never feared evolution.

## Beyond zero

Those fortunate ones who already understand the current democratic system that we follow, and are satisfied with it, go ahead and watch the TEDx talk by Prof. Lawrence Lessig of Harvard, called, [Our democracy no longer represents the people. Here's how we fix it.](https://www.youtube.com/watch?v=PJy8vTu66tE) His talk is, of course, concentrated on the USA, but it is easy to superimpose most of what he talks about, with the Indian democratic system of the 21st century. The video talks about the problem that we need to solve _after_ the one that this post talks about.

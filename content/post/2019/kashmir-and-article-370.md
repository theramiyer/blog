---
date: "2019-08-11T00:00:00Z"
subtitle: The Background
tags:
- kashmir
- India
- politics
- diplomacy
- geo-politics
- insurgency
- accession
- history
- civics
- constitution
title: 'Kashmir and Article 370: Part One'
---

We heard a lot of noise throughout the last week over the abrogation of Article 370 (and subsequently, Article 35A). Everyone kept saying: Article 370 is "no longer valid" in the state of Jammu and Kashmir. Why, Jammu and Kashmir was no more a state! But then, as with all noises, this noise added to the confusion in our minds. And  before we go any further, let me say in simple English, what Article 370 and Article 35A are. Easy one-line statement, you think?

Think again.

The topic is rather broad, and going into it in a single post made the post "too long for human consumption". I have hence split the post into three parts. Reading the entire series will give you knowledge, reading it partially will make you judgemental. Make your choice. This article is available as a [typeset printable PDF](https://www.scribd.com/document/422074646/Kashmir-and-Article-370) as well.

Here are the topics I will touch upon in this post:

{{< toc >}}

## The Articles

Article 370 recognises Jammu and Kashmir as a special state, and provides it with autonomy. The reason for this goes back to the time when Maharaja Hari Singh (the ruler of Kashmir when the British left India) signed the _Instrument of Accession_ (more on that in a moment).

Article 370 states that the Government of India has direct control over three areas in Jammu and Kashmir: Defence, External Affairs and Communication. Any amendments to the Constitution of India will not directly apply to Kashmir (barring the said three areas). The State of Jammu and Kashmir will have their own constitution, drafted by the Constituent Assembly of Jammu and Kashmir.

To know more about the "temporary" status of the Article, we would need to know a little history, which we will get to in a moment.

For now, also note that Kashmir is not the lone state with special status. Though the provisions under special status may vary, regions within Maharashtra, Nagaland, Karnataka, Gujarat, etc., are also covered under special provisions that concern the domiciles of the regions. Article 371 of the Constitution of India defines these provisions for the other states.

Article 35A defines "permanent residents of Jammu and Kashmir", and gives them benefits and protections, such as buying and owning property in Jammu and Kashmir, getting state government jobs in Jammu and Kashmir, and getting other benefits from the state government, including scholarships. This article says that everything that is within the state belongs to the domiciles of the state, and nobody else. Explaining the necessity of this article requires bringing history into view.

## The pieces that made India

Understand that India was not a single nation during the British rule. I know that there are talks of the ancient _Aryavarta_, a single empire, and the kingdoms being vassals of it. We are not going there. That is longer back than I can handle; I will not bite more than I can chew.

When the British were leaving the subcontinent, the two major political parties at the time, the Indian National Congress and the All-India Muslim League proposed that the princely states join a _dominion_ each.

Some of the major icons of the freedom movement proposed the [Two-Nation Theory](https://en.wikipedia.org/wiki/Two-nation_theory), and the two parties suggested that Jinnah (and the Muslim League) would create a dominion that will be of the Muslim majority areas, called the _Dominion of Pakistan_, and the Indian National Congress would create a dominion on secular grounds, called the _Dominion of India_.

Understand the statement: To be in India, you did not have to be a Hindu. You could be an Indian as a Muslim or a Christian or a Buddhist or even an atheist. Muslims across the Indian subcontinent had a choice they could make: either remain in the secular Dominion of India, or join the Islamic Dominion of Pakistan.

But there was an issue. Some states had a majority Muslim population, but had Hindu rulers (Kashmir---78% Muslim, Hindu ruler: Hari Singh) and some states had a majority Hindu population, but had Muslim rulers (example, Junagadh and Hyderabad).

Sardar Vallabhbhai Patel took up the task of meeting with the rulers of all the princely states and asking them to join the Indian dominion. Instrument of Accession is the paperwork the parties (Dominion of India and the princely state) signed for this purpose. The terms were that the Government of India would directly govern Defence, External Affairs and Communication. All other powers will remain with the state. Each state will form their constituent assembly and design their own constitutions. The State Department will define a model constitution, which the princely states can adapt, based on their needs. India also suggested that the states send their representatives to the Constituent Assembly of India.

By August 1947, all states except four---Sikkim, Jammu and Kashmir, Junagadh and Hyderabad---had signed the Instrument of Accession.

Sardar Patel (and much of Congress, in fact) were not interested in the accession of Jammu and Kashmir; everyone's main concern was Hyderabad, because it sat right within the Indian territory (think: most of the Deccan region). Junagadh was not much of a point of concern. In a way, Sardar Patel even used Junagadh as a pawn to "send a message" to Hyderabad and Pakistan, to ensure that Hyderabad joined India. Kashmir was not yet a region of focus. Jinnah was not interested in Kashmir, either, per se. He was eyeing Hyderabad. According to some anecdotal evidence, Jinnah refused the deal of India letting Kashmir join Pakistan if Pakistan stopped meddling with Hyderabad. That was the disinterest in Kashmir back then. Neither party was dying for it, so to speak.

## The shortened story of Kashmir

Let us start with the introduction of the spirit of Article 35A. The Kashmiri Pandits floated the protectionist idea of _Kashmir for Kashmiris_. They expressed to Maharaja Hari Singh that nobody other than Kashmiris should be able to use any of the resources in Kashmir, and that his government must protect the interests of the Kashmiris _from the British_.

Maharaja Hari Singh started enacting the legal provisions for this from 1912 (thirty-five years before independence). This went on for about a couple of decades, when the provisions took the form of more or less what Article 35A says today. But Article 35A is not the sole piece of document that guarantees these rights and privileges to the Kashmiris.

In the 1930s, the Maharaja had leased out the north-western region of Kashmir (known as Gilgit-Baltistan now) to the British. If I remember it right, this lease was for six decades. The British formed their Gilgit Scouts and remained there, even through the independence of India. Keep this in a corner of your mind for now.

Hari Singh, at the time of independence of India and Pakistan, wanted Kashmir to be on its own. He did not want to lose control of it to India. At the time, Sheikh Abdullah's and Jawaharlal Nehru's friendship was well-known, and Hari Singh felt that the Government of India would give Sheikh Abdullah the control of the state of Jammu and Kashmir. Being a Hindu himself, Hari Singh did not want to join Pakistan either.

In order that his state remained independent while he decided its future, he sent a _Standstill Agreement_ to both India and Pakistan, asking both the parties to not change the ongoing arrangements (and the status of his state). Pakistan agreed to it. Nehru, being a Kashmiri Pandit (conspiracy theories aside), did not want Kashmir to be part of Pakistan. India called Maharaja Hari Singh for "negotiating Standstill Agreement between Kashmir Government and Indian dominion."

Variables changed (as they do in politics) and Pakistan attacked Kashmir out of the blue. The Maharaja asked India for help. The Governor General of India said that it would be diplomatically, politically and strategically dangerous to send Indian troops to a state that was not part of India---a state that was neutral between India and Pakistan. The Government of India suggested accession. Maharaja Hari Singh signed the Instrument of Accession.

The Indian troops moved in and a battle ensued. There were some wins and some losses. Because both the nations were still infant nations, and when soldiers could not fight battles in the cold weather in the Valley (the technology in the late forties could not help with battles in such weather conditions---on both sides), the battle that started in 1947 ended in 1948. When the cold struck hard in 1947, the Indian troops took a break, and so did the Pakistani army. When the conditions became better, Pakistan had "cemented" the border (which is more or less the current Line of Control).

Meanwhile, since with the Independence, all the ties and agreements with the British had gotten severed, Maharaja Hari Singh sent his officials to the Gilgit region to take stock of the territory. the Gilgit Scouts stopped them, and said that they had sworn allegiance to Pakistan. Hari Singh did not accept this, nor did the Government of India, saying that all ties with the British were no more, and it did not matter whom the Brits of the Gilgit region swore allegiance to. Legally, we were right.

As tensions rose, the Government of India, under the leadership of Nehru, approached the United Nations to intervene in the matter. Anecdotes suggest that Sardar Patel and the higher officials within the Indian Army suggested continuing with the war and ending the dispute once and for all. But that Nehru ignored the suggestions, ignored the opposition from Sheikh Abdullah, and went on to the United Nations.

The United Nations asked Pakistan to withdraw its troops from the region and asked India to reduce "the bulk of" its troops from the region. Now, let us bring in the issue of religion. Jinnah maintained that Jammu and Kashmir should be part of Pakistan because of the Two-Nation Theory, based on religion. In other words, since the majority population of Jammu and Kashmir was Muslim, Pakistan said, the state should go to Pakistan. Jinnah also said that he was ready to let Junagadh (which had by now sworn allegiance to Pakistan) and Hyderabad join India, if India loosened its grip over Kashmir.

India said, religion did not matter to us. As long as the leader of the people of the state signed the Instrument of Accession with the Dominion of India, the state was part of India. The Nawab of Hyderabad signed for Hyderabad, and so did Junagadh. Maharaja Hari Singh signed for accession to India, and by that, Kashmir was part of India. (We could not have held this argument if we were a Hindu State.)

The United Nations asked the people of Jammu and Kashmir to decide their future through a referendum, once the countries demilitarised the region. India accepted the resolution, but Pakistan rejected it. Pakistan also said that the Azad Kashmir movement and the revolt of people against Maharaja Hari Singh had effectively stripped him of the authority to sign the Instrument of Accession in the first place. Neither country has demilitarised the region in the last seven decades, and as a result, we have never been able to hold a plebiscite.

The word, plebiscite, has become a sort of derogatory term these days. To clarify, a plebiscite is a direct poll. Think of it as asking the people themselves (and not their representatives) to voice their opinion on something; in this case, on whether Jammu and Kashmir would like to join India as per the Instrument of Accession, or Pakistan.

## Article 370 and its significance

Article 370 was a result of what happened in 1947--’48. The Indian Government said, 'All right, we understand that you need a special status given that you do not want to adopt the Constitution of India. We have also taken into account what the United Nations say. To that effect, we will add an article to the Constitution of India, which will give the State of Jammu and Kashmir a special status. This would be in effect _while the final status of Kashmir remains undecided_.'

Article 370 got added to the Constitution in 1952. Two systems were in effect now, in Jammu and Kashmir. The people of Jammu and Kashmir would follow what their constitution said. The Constitution of India adopted the Constitution of Jammu and Kashmir. Any amendments made to our constitution did not directly apply to the State of Jammu and Kashmir. The bill went to the Constituent Assembly of Jammu and Kashmir, and after the Assembly passed it there, their constitution received the amendment, and the bill came into effect after that.

In other words, Jammu and Kashmir operated as almost a separate country, which had sworn allegiance to India. They had their own flag and their own constitution. The people born in Jammu and Kashmir became citizens of Jammu and Kashmir first, and then, India; until 1954, Kashmiris did not even automatically become citizens of India. And until the sixties, Jammu and Kashmir would elect the Prime Minister of Jammu and Kashmir, not the Chief Minister; a practice that had stopped in the other provinces in 1950---after the formation of the _Republic of India_.

## Separatist movements

Some groups have asked for a separate Kashmir. Pakistan aids these groups via "moral support". This is also known as the insurgency.

While the people of Jammu and Kashmir were in favour of joining with India up until the sixties or seventies, later drastic changes to the state of Kashmir, setting up "dummy" governments there, and rigging elections and manipulating the assembly (Cough ... Indira Gandhi ... cough!) angered the Kashmiris; their anger directed towards India. These are signs of alienation.

## Transitioning forward

That is more or less the history part of the Jammu and Kashmir region. Remember, the Jammu and Kashmir that we have seen since birth is the map that the colonial cartographers left with India. This map is technically four parts: the Gilgit-Baltistan region in the north-west (which swore allegiance to Pakistan despite technical issues with it), the Aksai Chin region in the north-east (that China claims as own, the issue India woke up to in 1950), the Azad Kashmir region that Pakistan has occupied, and the India-administered Jammu and Kashmir---Siachen is now part of this region.

The rest, we will [see in the next post]({{< ref "kashmir-and-article-370-part-two" >}}).

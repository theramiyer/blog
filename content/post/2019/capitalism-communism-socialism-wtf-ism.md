---
date: "2019-02-19T00:00:00Z"
tags:
- opinion
- government
- mythbuster
- democracy
- politics
title: Capitalism, communism, socialism ... wtf-ism
---

Let's face it, this is the generation of isms. Everywhere you go, every channel you pick, every Facebook page that floods your newsfeed, have one or the other form of _-ism_ being talked about: whether it's a meme, or a debate video (where you barely hear a thing), or a long post like this one. Needless to say, we're all either tired, or are numb.

But still, we end up retaining a part of every incomplete or complete piece we gather, only to happily use it later on, whether the understanding or the context is right or wrong---of late, mostly the latter.

I'm not the kind who has learnt to keep quiet. Sometimes unfortunately, if I may say so. It is hard for me to not jump at fiction stated as fact. I've been growing less turbulent over the last couple of years, but I still sometimes do get fired up all right. (Sometimes I simply choose not to show it in the interest of self-preservation. Think: You're alone, surrounded by seven people who hold dear what you very naturally hate.)

Another important aspect is how quickly more and more information is becoming commonly accessible. Don't get me wrong; I don't mean to say that information should not be accessible---quite the contrary. The challenge is that not everyone is inclined to collect every available information. More importantly, we don't verify whether the information we've come across is legitimate. Worse, even if we do verify, we find it hard to find an authoritative source for such information. Therefore, we float in the sea of opinions, which we accept or reject, based on _whether we like the opinion or not_.

Enough prologue.

Given the current setup of our country (or even perhaps the world at large), we are quickly reducing ourselves to bipolarity. Call me an elite if you want, but if we had to reduce ourselves to such a state, we might as well not call ourselves humans. But that's the paradox, isn't it: either you're bipolar, or you're not? Oh, wow.

I started with that, because I feel, today, you either support the current ruling party, or you don't. You either like a country or hate it. There's no in-between, there's no partial agreement. That's just how it's become. Recently, I called someone a right-wing-ist (of course based on his ideas and beliefs, in general---I've known him for long enough to be able to make that statement). He was quick to call me a leftist. I asked him whether he knew what the two meant at all. 'Of course, that I support \<enter a three-letter party name here\> and you support \<enter another underachieving party's name here\>.' I told him that not only was he wrong, but that the latter underachieving party was not the left. This is all he had to say: 'It is. You wouldn't accept it, because it is too shameful.'

I was left wondering how to respond to that. How do you respond to blatant arrogance of ignorance? How do you get started on how appalling it is to get a roaring applause for, 'I don't know. And I don't care.'? No, your statements no more have to be supported by facts! Time to celebrate! Long live ignorance. Down, down science. Down, down facts. Long live anarchy. Down, down thought. Down, down knowledge. Long live hollowness. Down, down intellect.

The next time someone says knowledge is power, slap them. It's not. And the knowledge of _that_ is today's power.

But allow me to disarm you a little. Know the difference between a few most common isms. Of course, this has been simplified to be friendlier to understanding (at the risk of much more misunderstanding).

## Communism

In short, if I may quote Dictionary.com, it is "socialism gone wrong". Communism is considered extreme left. Communism is a system, wherein, all the resources are owned by a community, which is mostly people at large. And then, there is a single totalitarian entity, who runs the entire system. Typically, this can also mean advocating atheism, voiceless common man, zero liberties, and so on[^1]. Everyone's favourite example is how China even regulates the use of the Internet.

[^1]: It is funny that the right-wing entities that hate communism also, sometimes, seem to have the characteristics of zero tolerance.

## Socialism

This is a larger system, of which, communism forms a small (perhaps evil) part. This system relies on what is called _collective co-operation_. Think of this system as one that has the public, who have a government, and this government runs all the institutions for the public. Therefore, all the institutions that are part of this system are owned by the public. The public---every member of it---pays taxes, and these taxes in turn are invested in the well-being of the public.

Some of the northern European countries have excelled at this system.

## Left-wing-ism

This is a political (and not a philosophical) term. This political line of thought is supposed to resonate well with the working class in places where they are considered socially inferior. Left-wing politicians use social equality as their trump card. They do not believe in _one \<anything\>_. This is supposed to mean pluralism (having open arms to all forms of thought) and egalitarianism (everyone is equal, and should be given equal opportunities).

## Right-wing-ism

Another political term and thought process, ostensibly, very popular these days. This form of thought usually aligns with conservatism. Politicians who support this ideology, say, 'There is a system in place for a reason; there's a social hierarchy in place for a reason. You don't get to question what is existing, and is right for you.' This is also a form of _absolutism_, but from the right. To quote the Oxford dictionary, absolutism means:

> The holding of absolute principles in political, philosophical, or theological matters.

Typical examples of this would be how one choice of food is chosen to be superior to the other, and the consumption of the other is not allowed; how one form of belief is chosen as the one for all of humanity, while everything else is wrong; how one way of living is sinful and prohibited, even though the law as directed by the Constitution allows it, and so on.

## Which is better

Neither. As you see, extreme right-aligned mentality, as well as extreme left-aligned mentality lead to anarchy. If you go extreme left, you are looking at totalitarian control, the implementation of which will lead to anarchy---this is called _left anarchism_. If you go to the extreme right, you get the same situation, implemented in a different way. This is another form of anarchy, called _right anarchism_.

Moreover, from a socio-economic standpoint, both extremes can lead to an unbalanced state. For instance, by allowing capitalism to reign supreme, you would be killing the lives of the economically weak. Similarly, by allowing communism to reign, you would end up with a handful controlling a voiceless country of minions.

## What are we as a nation?

I would say, rather confused. Before someone jumps off the chair, draws their weapon, and threatens Amazon to shut this site down, here is what I mean.

Let us start with what India should be. To quote the [Preamble to the Constitution of India](https://en.wikipedia.org/wiki/Preamble_to_the_Constitution_of_India):

> WE, THE PEOPLE OF INDIA, having solemnly resolved to constitute India into a SOVEREIGN SOCIALIST SECULAR DEMOCRATIC REPUBLIC and to secure to all its citizens  
> JUSTICE, social, economic and political;  
> LIBERTY of thought, expression, belief, faith and worship;  
> EQUALITY of status and of opportunity; and to promote among them all  
> FRATERNITY assuring the dignity of the individual and the unity and integrity of the Nation;  
> IN OUR CONSTITUENT ASSEMBLY this  26th day of November, 1949, do HEREBY ADOPT, ENACT AND GIVE TO OURSELVES THIS CONSTITUTION.

Now, to quote Dr B.&thinsp;R.&thinsp;Ambedkar in the context:

> It was, indeed, a way of life, which recognizes liberty, equality, and fraternity as the principles of life and which cannot be divorced from each other: Liberty cannot be divorced from equality; equality cannot be divorced from liberty. Nor can liberty and equality be divorced from fraternity. Without equality, liberty would produce the supremacy of the few over the many. Equality without liberty would kill individual initiative. Without fraternity, liberty and equality could not become a natural course of things.

A very thoughtful, beautiful thing to say. But a Himalayan thing to execute. And at this time, what the preamble promises seems to be a far dream. Hopefully, we get there before it is too late. But before everything else, here is what a "socialist democratic republic" means.

## Democracy

Democracy is a system, created by the people, for the people and is composed of the people. Democratic socialism is a branch in socialism, where, a socialist government is chosen by means of democracy. This system, in other words, lets _people_ decide who will uphold the distribution of the resources to everybody. Or, the people decide who will run the nation and how.

## Republic

A republic is where people are the ultimate power. In a republic, the one who rules isn't above the people, nor are the people above the chosen head---because the head forms part of "people" as well.

## Socialist democratic republic

This is a little challenging to understand. In a vanilla democracy, capitalism reigns. The public, as such, does not own all of the resources in that system. In a socialist democracy, a good chunk of these resources are owned by the government, which is in turn, elected and run by people. Therefore, the public owns a good chunk of the resources in the system. Capitalism co-exists. In these systems, you have government-run as well as publicly-owned transport corporations, for example. Taxes are given back to people in the form of things that improve the lives of the people; people ultimately "own" these "things" in a sense.

Combine the features of a republic, wherein nobody is above the people, and people are above nobody, and it becomes the ideal system that India was dreamt to be (and is, even, in many ways).

The sad part is, we have let politics cloud our judgement of everything. Politicians oversimplify issues, principles and concepts, and feed us with what is favourable to them, and easy for us to chew. All this, in an attempt to get to "own the country" (because that's what ruling today is, apparently). Ruling a democracy, in my opinion, should not even be a thing---in a democracy, you _run_ a country. Representatives are appointed by people, who in turn, lead the progress of the said democracy. But what happens in our country?

You know the answer. But here is a story. There is a ship. This ship is full of people who know how to run it. Many are qualified captains. They all select one captain to run the ship at a time. They also choose one person each for a role to play in running the ship, of course, among the many that are qualified. The captain and the crew agree to work eight hours a day. Then, some other qualified person would pick up from where the incumbent left off. Eight hours later, the captain and the crew hand over the responsibilities to the newly-elect, and proceed to retire for the day. This goes on for several days. Then, one rogue captain starts work. His crew is as corrupt as he is. He decides, his son would guide the ship when he retires for the day, the son appoints his own son when he retires, for the job.

The captain wears a band across his head covering one eye, and starts to plunder everything he comes across _within the ship_, claiming those findings as his own property. He repaints the ship in the colour of his flag. He also pastes his face on the water bottles and doors and television sets to tell those on board that he is the _brand_ the ship is running under. Eventually, he rubs off another (qualified or otherwise) captain's nose in the process ... moronically, considers the ship to be a mere underwater wreckage that he came across when diving for pearls.

It's a sad story from there on. I don't want to scar you.

## On coalition

Every time coalition is mentioned, someone goes, "Haww, you're such an anti-national. And so against development! How can you be so stupid? Nobody will let anything happen in a coalition."

Partially true, considering the options of coalition that we have in our country today. Also, if recent history is any indication (case in point: Karnataka, 2018), coalition doesn't work as well as it should or is portrayed to. However, democracy dictates that people be left to choose what is right (this pun is perhaps what led to "left" and "right" being called, "left wing" and "right wing"). By electing the person for your constituency, you are vesting in them the power to choose what is right for the country. That essentially means that if the people so decide that there be a coalition government, that is what should be.

If a political party says that coalition is bad for the country, and majoritarian power is the best thing for the nation, it is simply their idea, perhaps in their own interest. We, as learned citizens should do what _we feel_ is right for the country.

Often, we hear that the state of no opposition is fatal to democracy. This is because a democratic system must have equal powers of all different kinds, across the spectrum. The people should choose the right candidate, who would work towards the betterment of the nation. And the other powers should guide the leading power towards a better country by means of dialogue and debates on the different policies and schemes---including amendments to the law---in the parliament. Doesn't this mean that a single party having absolute power in the parliament could possibly lend other parties powerless? That is certainly not to say that a clean majority is equivalent to dictatorship; our constitution allows a clean majority.

However, the tone used is of complete autonomy without "disturbances" from the opposition. That could potentially lead to dictatorship. And before you say, 'That's what India needs today', dictatorship may have its own merits, but we as India are a democracy. It's important we remember that. The opposition is as important to our country as the leading front. You vote for who will represent your constituency. And let the system work the rest of the way.

## A little peeve-point: Marxism

> 'Guess what, I learnt a new term today!'  
'Yeah? What is it?'  
‘ "You, Marxist!" ’  
'Okay, what does it mean?'  
'Everything I'm not.'  
'Couldn't get any more specific, but okay, how do you use it?'  
'As usual: willy-nilly.'

The right-wing in India, today, openly calls out anything that does not align to its thought process, the left-wing. And their favourite form is, "You, Marxist!". And I hear this being said to people more often than I would like. And to clarify, Marxist mentality is one that says, socialism is imperfect, and incomplete, and that communism is the ultimate goal of socialism.

Why is this important? This indicates that socialism is a state achieved while a certain system is moving from capitalism to communism---that the destination is communism, and that it is achieved when you go beyond socialism. It implies that socialism is half-baked. Now, it is easy to understand why those who are supposed to have an egalitarian mentality and believe in liberalism, resort to violence to overthrow the people's power (which is essentially the government in a democratic republic like India). That's an example of left anarchism. It is also now easier to understand Naxalism and Maoism. And that they have got little to do with democratic socialism. That is to say, just because you believe that the resources should be co-owned by people, you are not a communist. If anyone gave you that idea, they were wrong.

## Finally, a personal note (the disclaimer)

I'm not a political analyst; I'm as far from it as you can get. I have natural dislike towards politics. However, I remember my lessons in civics. What I've said is more from a citizen's standpoint in terms of civics, rather than politics. That is to not only say that I'm non-aligned to any of the parties, but also that I know what I should, when voting for the person to represent my constituency, and therefore, my country.

Also, being someone who understands that Left -- right is a spectrum, and not being bipolar (and that I can overcome compulsion) I agree _and disagree_ with the stands that political parties take. I am someone who can (and does) differentiate between political parties and the government. For instance, if I were in favour of ABC, but XYZ led the government, I would be in favour of the policies and schemes that XYZ brought in if they were favourable to the country, and be happy to oppose unfavourable policies that ABC advocates.

---
date: "2019-11-22T00:00:00Z"
tags:
- politics
- democracy
- protest
- social
- economic
- government
- socialism
- freedom
- education
- whatsapp university
title: Is the JNU protest justified
---

Despite hating to say it, it started with _WhatsApp University_. WhatsApp University is a term used for all of those streams of knowledge tidbits that you get from WhatsApp, that replace your beliefs, your thoughts, your rationale with what's now popular belief. There is a massive engine that drives this, and this engine includes people with the ability to write posts like this on their phones. This is amplified by people who have all the time in the world to forward them to the masses. These masses gain fresh knowledge of the world through the lens of these forwards.

When I blame the Baby Boomers of India (the generation of my parents and uncles and aunts) for ruining the country for us, it extends to this---these are the guys sitting in their couches sipping their coffees, with their phones in hand, being our teachers at WhatsApp University, still influencing the country. Relentless, I say.

{{< toc >}}

I received this message yesterday ([skip this](#the-premise) if you want to):

> JNU issue - there are a total of ~8000 students at JNU. Of this, the lion’s share of 57% students is of social sciences, language, literature & arts (4578 students) & International studies 15% ( 1210 students). Almost 55% of the total i.e. 4359 students are doing M.Phil. or Ph.D
>
> Cost of running such University: Just how much does government spends on JNU? Look at the Profit & Loss account of JNU for 2018. This is hidden somewhere in the middle of an 600 page annual report. It should be the first section on annual reports.
>
> So JNU spends about Rs 556 crores per year on its functioning.So what is the cost per student at JNU ? We can calculate. Rs 556 crore / 8000 students gives us Rs 6.95 lac per student annually.
>
> Research, publications & patents at JNU: Assumption is that such high subsidy on "good talent" would be producing great research outcomes, right? Well, hardly.  There are no concrete claims on research & publications, no names of journals or publications worth mentioning.
>
> You’ll find some mention of research in the placement brochure (yes they have one, although very superficial & doesn’t disclose any statistics about past placements). Here is what you’ll see in the name of research at JNU:
>
> With 4360 students in M.Phil. & Ph.D. courses, there are hardly 1000 research “articles” published in Journals. The university doesn’t name any notable journals while making such claims. This implies that there is just ONE “article” published for every 4.5 students each year.
>
> There is huge participation in international conferences. So about 2000 international conferences are attended every year. Again, not sure what materializes in those conferences. Certainly not the research papers or patents.
>
> But there should be some patents at-least ? All that we find for patents is 4 names of Mr. Bhatnagar, Ms. Dixit, Mr Kar & Mr. Mukherjee. No patents by any other faculty. Number of patents by students ??? Try searching…….I did not find any results. So much for 600 PH.Ds every yr
>
> So is the fee hike Justified ? Why punish the meritorious students? Firstly, in view of the data above, it doesn’t appear that there is much focus on output or getting laurels for the nation at JNU. Now, let’s look at the “fee” the students pay to study at the university.
>
> The numbers you are looking at are not in thousands or lacs. These are what they are in INR. So Rs 240 is the entire tuition fee paid by JNU student. Aside, they pay a generous Rs 6 for library & Rs 40 as refundable security deposit. Any research paper on how the fee is so low?
>
> IIT Delhi which is located close by charges about Rs 2.25 plus lac annually & IIMs charge about Rs 5 to 10 lac per year. We don’t see any strikes at those places. Students know they need to jump into the job market, start their earning life & also repay their student loans
>
> JNU is free of any such thoughts or troubles. Probably the reason why students have much time left to create new troubles of their own. Ideology or idle mind, you decide.
>
> I don’t want to conclude any political thoughts here. But the data from official sources of the university help us infer a lot of ideas. JNU is a perfect example of bad socialism. If you give something for free, people have no incentive to work & earn.
>
> Why would anyone at JNU be in a hurry to find jobs or even publish research. In a nation where we’re are hardly able to educate our children thru primary schools, JNU stands tall as an example of socialist elitism at its best.
>
> The students may have all the time for doctoral courses, the nation is certainly running out of money to pay for these scholars.Surprised that this social reform idea never came out from JNU that claims to be the study centre on social sciences? Irony died a quite death !! (sic)

## The Premise

To be honest, I had no idea why the JNU students were protesting. I was too busy with my own life to be worried about what happened over two thousand kilometres away. This is why we rely on the media, but we all know how useful it is. Trust them to ask the right questions, always. I don't blame people who rely on WhatsApp University for such information; when they cannot trust the media, they have to trust whom they trust, even though the trusted brush off all responsibility when asked about misinformation they forwarded; they're not journalists, after all. Did anyone talk about irony dying a "quite" death?

Through "the talks", I learned that the fee hike to ₹240/year was the issue. Not being a big, big fan of Indian socialism (yes, it is different from the plain vanilla socialism), I scoffed at this and left it there. But when I read this message, something irked me. Did it mean they paid ₹240/year before, and now it was hiked to something else? Now I had to find answers. But before that, I replied to the message with what I thought. (Of course, that would not be forwarded; neither I the sender, nor the recipient belong to any political party's IT cell.)

It was time for me to start for work, so, I left it there temporarily. And then, on the way to work, I found Akash Banerjee's video on the exact same topic. I went to the description, and found a set of links. This was what I was looking for. (A shout-out to The Deshbhakt[^daa5b947]!)

[^daa5b947]: [The Deshbhakt (YouTube)](https://www.youtube.com/user/akashban)

## The actual fees

It is evident that the students at JNU are protesting the fee hike. But what is the hike anyway? Is it ₹240?

No.

According to The Print[^8a948c6c], the fee hike being protested is of the hostel; not the tuition or the library like the message claims. Here is a little breakup for better understanding:

[^8a948c6c]: [After hostel fee hike, JNU will become India’s most expensive central university](https://theprint.in/india/education/after-hostel-fee-hike-jnu-will-become-indias-most-expensive-central-university/320897/)

> The fee hike — set to come into effect from the next academic year — will nearly double the annual fee for JNU students living in hostels from the current Rs 27,600-32,000 annually up to Rs 55,000-61,000.
>
> The new fee includes a mufti-fold (sic) hike in room rent — from Rs 10/20 per month to Rs 300/600 per month. A new service charge of Rs 1,700 per month has also been added — taking the monthly hostel fee up to Rs 2,000-2,300. Other charges, like establishment (Rs 2,200 per annum), mess (Rs 3,000 per month) and annual fees (Rs 300) remain the same.
>
> ---Kritika Sharma and Tarun Krishna, The Print

Not only does the ₹240 not figure anywhere (or it probably vaguely does), the premise of WhatsApp University could not be farther from the truth. In context, the fee hike has been up to 99.28%. Imagine joining a college that quoted the fee as 6 lac for the entire four-year course, and then, half a year later, they said the fee would be 10.47 lac instead.

## Story of the deficit

The "Profit and Loss account" is talked about in the forward. This did not sit well with me, given that JNU is a government-funded university and it is by design a not-for-profit institution given that India is a socialist State. Even if we were not a socialist State, I would argue that education should not be for profit, and that there is nothing wrong in the government spending on education. It is, after all, what is necessary in this world of information (more so in the world of misinformation, so that people don't have to resort to WhatsApp University).

I do not know where the details on 6.95 lac per student annually was obtained (partially because like all WhatsApp University students, I did not have the time or the patience to read the so-called 600-page annual report that mentions this), but let us look at it in perspective of what data we do have openly available. According to Budget 2019, only 3.37% of the budget was allotted towards education. When my aunt said that pension is a major chunk in government expenditure, I did not believe her. But pension is indeed the fourth-largest chunk of the budget, at 6.62% (education is the twelfth). Here is a gist[^81ddd482] of what I am talking about:

[^81ddd482]: [Budget 2019: A quick look at where the government spends](https://www.moneycontrol.com/news/business/economy/budget-2019-a-quick-look-at-where-the-government-spends-3489751.html)

![Where the government spends our money (pie chart)](https://static-news.moneycontrol.com/static-mcnews/2019/02/Budget-Expenses-600x436.png)

As a side, the Bullet Train dream would have made the Interest chunk bigger. But of course it is a higher priority than education; it's the pride of our nation. The money wasted in subsidies on the surplus (the surplus, mind you) rice that Punjab farmers produce forms part of the food subsidies---the third-largest chunk. Is money wasted on that better than spent on education? In an economy that thrives on skills and services, education ranking only the twelfth in the budget allocation is upsetting.

But, here is the cherry on the pie---the forward says:

> The students may have all the time for doctoral courses, the nation is certainly running out of money to pay for these scholars.

Are we? According to Business Today[^6b3548f1], the government _underspent_ on education in the first four years of NDA-1. This is from Union Budget 2019.

[^6b3548f1]: [Union Budget 2019: Govt underspends budget allocated for education in 4 out of last 5 years](https://www.businesstoday.in/union-budget-2019/budget-2019-govt-underspends-budget-allocated-for-education-in-4-out-of-last-5-years/story/361606.html)

Education is part of the social infrastructure. The whole purpose of paying taxes is to fund things like education. But if the government under-spends on education, and there is money remaining in the coffers, why are we complaining about deficits? Where is the deficit? There is only surplus. And that is a bad thing. In fact, between 2015 and 2016, the allocation itself dipped from about ₹83,000 crore to about ₹69,000 crore, and the expenditure also saw a slight dip from ₹69,000 crore to about ₹67,000 crore.

Twelve-digit numbers (five-digit numbers, in crores) are hard to digest, aren't they? Especially compared to the three digits such as ₹240 "in INR". But yes, unfortunately, I was also a victim of WhatsApp University this time for a moment.

But coming back, if the government does not mind spending ₹32.4 lac a year (or ₹2.7 lac a month[^3dc4b1ea]) on an MP who is too busy for people, why not spend a fifth of that on a student, if at all the numbers in the forward are true? And mind you, this expense on an incompetent MP is an expense, the expense on a good student is an investment.

[^3dc4b1ea]: [Government spends Rs. 2.7 lakh a month per MP](https://www.thehindu.com/data/government-spends-rs-27-lakh-a-month-per-mp/article7699415.ece)

> Travel reimbursements and daily allowances account for the biggest chunk of public spending on Indian MPs, or nearly half of all expenses, at Rs 83 crore for the year, the data shows (Numbers have been approximated)
>
> ---The Hindu

India spent a mere 2.7% of its GDP on education[^ccdece59], in 2017--2018. This is lower than Bhutan, Zimbabwe, Costa Rica, Kyrgyzstan, Palestine, Malaysia, Kenya and Mongolia spent more on education (and I have not counted the likes of the US, the UK, the Netherlands, Finland, Sweden, South Africa, etc.). Zimbabwe spends about 7.5% of its GDP on education.

[^ccdece59]: [Panel: Hike education spend to 20% of public expenditure in 10 years](https://timesofindia.indiatimes.com/india/panel-hike-edu-spend-to-20-of-public-expenditure-in-10-years/articleshow/69950757.cms)

Where do you even get started?

## Protesting the hikes

The forward claims this:

> IIT Delhi which is located close by charges about Rs 2.25 plus lac annually & IIMs charge about Rs 5 to 10 lac per year. We don’t see any strikes at those places.

Wrong.

Here are some examples:

> IIT aspirants stage protest against 900% MTech fee hike, demand rollback ([India Today](https://www.indiatoday.in/education-today/news/story/iit-students-protest-mtech-fee-hike-hrd-ministry-1606467-2019-10-05))

> Panjab University fee hike: SOI sits on hunger strike ([The Times of India](https://timesofindia.indiatimes.com/city/chandigarh/pu-fee-hike-soi-sits-on-hunger-strike/articleshow/70424771.cms))

> Bengaluru: National Law School students protest steep tuition fee hike ([The Indian Express](https://indianexpress.com/article/education/bengalurus-national-law-school-students-protest-steep-fee-hike-decide-not-to-pay-fee-5818726/))

You can find more. Go ahead.

In fact, in the case of IITs, the fee hike was not even for the existing students, as clarified by the government. In case of JNU, that is not the case.

## JNU is useless

The forward says that 4360 students are enrolled in MPhil and PhD courses, and only 1000 articles are published. Well, my college had roughly 1,200 students in 2010, only 270 students graduated. Does that make my college useless?

Here is why it doesn't. While there may have been 1,200 students enrolled, only 270 were in the final year. The maker of the message would have known this had s/he attended college (or school). Not even as hard as common sense.

Anyone who underwent research would know that research works differently from the "professional courses" that a mainstream Indian is used to. Explaining it is probably futile, but those interested can simply Google this. Or apply.

Now to something serious; I didn't know about this, but this trend of giving importance to engineering and management while giving second priority to everything else, is a national phenomenon. According to a report by The Print[^a0d9e8e], IITs and IIMs take up only 3% of the students among those in State-funded educational institutions. Yet, they get 50% of the funding. The rest 97% are stuck with the remaining half.

[^a0d9e8e]: [IITs, IIMs, NITs have just 3% of total students but get 50% of government funds](https://theprint.in/india/governance/iits-iims-nits-have-just-3-of-total-students-but-get-50-of-government-funds/89976/)

Of course, the IITs contribute immensely to the country in terms of the little research that happens here, but these students are also among the highest that go out of the country. You might justify this citing educational loans and what not, but as a taxpayer who is contributing to their education, I'm indirectly helping another country prosper, and I do not like it. When 97% of IITians and IIMites stay back in India after their education, work here, pay taxes here, and in general contribute to the progress of the nation, I'll stop complaining about this.

Despite not getting the "lion's share" of the higher education funding, JNU (and DU) managed to feature at least in the top 500 humanities universities in the world[^0b7ee19] (2020 list). IITs and IIMs frequently feature in the top universities' list, but being surprised about them would mean we do not think high of our students or well-endowed universities. Now if you think humanities is not a necessary study, you're part of the problem that you complain about---ethical policy-making, general human life, culture, philosophy, promotion of classical languages, etc form part of humanities. You can't complain about these not developing by complaining about funding these studies; you do either. Don't be the proverbial _Kalidasa_.

[^0b7ee19]: [JNU, DU among top 500 humanities varsities in the world: Times global rankings](https://m.hindustantimes.com/india-news/india-s-jnu-du-among-top-500-humanities-varsities-in-the-world-times-global-rankings/story-rHAOQIR41CJAZA7zj0ljAP.html)

Here is something the supposed 600-page annual report---conveniently---did not mention:

Given that ₹37,461.01 crore[^6b3548f1] (44.07% of budgetary allocation to education) is allocated to higher education, of which only 48.9% (₹18,318.43 crore) goes to the 865 institutes offering higher studies[^a0d9e8e], even if distributed evenly among these institutions, the total money that goes to JNU is about ₹21.18 crore (or 0.06% of the total allocation, and a far, far cry from the said ₹556 crore---nineteen-five-hundredth, to be exact, or roughly, one-twenty-fifth). Does JNU get that level of treatment---twenty-five times the calculated allocation? Really? The Hindustan Times article[^0b7ee19] makes me think, no effing way.

Now to some speculation (I am allowed some if the message maker is): Going by the forward, if 8000 students study in JNU, that translates to about ₹26,475 per student. If we factor in the actual spend of the government, at 84% of the total allocated budget, the amount spent on a student is ₹22,239.

In reality, of course, this is not an exact number because the number of students studying in central universities is unknown. When we learn to publish data transparently, we will know the exact numbers. Until then, I am going to call ₹6.95 lac per student bollocks.

## On socialism

> JNU is a perfect example of bad socialism.

Agreed. So are many, many things in our country. We have a lopsided view of finances. We do not understand farming[^22a27c9] or preservation[^85f0b5b]. Our expenditure on healthcare is questionable, not only because we do not spend enough, but because the spend is simply inadequate for the masses. Our schemes do indeed make people lazy, such as _unchecked_ farm loan waivers. These are all examples of bad socialism or failed socialism.

[^22a27c9]: [How Delhi’s air crisis can be resolved by curbing paddy cultivation in Punjab, Haryana](https://theprint.in/opinion/how-delhis-air-crisis-can-be-resolved-by-curbing-paddy-cultivation-in-punjab-haryana/315250/)

[^85f0b5b]: [NAFED wasted over 30,000 MT onions, more than half of its buffer stock, amid soaring prices](https://theprint.in/india/nafed-wasted-over-30000-mt-onions-more-than-half-of-its-buffer-stock-amid-soaring-prices/323545/)

Back to JNU, is JNU the best university in India? Largely, no. But does it have to be, in a socialist State, to justify the tax money spent on it? Where are those that question the quality of education in government schools when they take up over 55% of the allocation for education?

Do students in JNU not engage in social disturbances? They do, all the time. But do we smother this? Well, if you agree to smother ABVP's and other student bodies' "disturbances" as well. But even then, it is still not convincingly justified; students are an important part of a democracy; these are people who have just attained adulthood, trying their way to nudge the country in a direction that they feel is good. When this behaviour peaks, it is called dissent. If dissent itself is curbed, then we are no democracy. If students do not have a voice, then why does no one raise the voting age from 18 to any arbitrary higher value?

Yes, most JNU students have this far left sort of mentality in general, and that is probably dangerous in today's India. But does that mean we shut down the university? Had you asked me three days before writing this, I would have said yes. Today, looking outside of WhatsApp University and rooms where news breaks first, I would say, heck, no.

JNU says about 40% of its students come from backgrounds where the monthly income is below ₹12,000. Does that mean the rest 60% can afford the fees? ₹13,000 a month also falls under the 60%. Can they afford to spend ₹5,000 a month on their child's education? This steep increase is the beginning of a bad trend.

Also, the numbers we must be focusing on are elsewhere; not in the forward.
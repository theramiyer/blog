---
date: "2019-12-18T00:00:00Z"
episode:
  apple: 1000467989861
  spotify: 2UqKyMqXnOXG2x6J7TsMML
  google: MWVjYmU5MWEtMTAzMS00MmQyLTlmODktM2Y3NDVmODNlYjAw
tags:
- clutter
- India
- citizenship
- law
- constitution
- protests
- religion
- whatsapp university
title: The CAA is not about throwing out people
---

Over the last week, I saw protests everywhere. Most groups (not WhatsApp groups) that I am part of had a discussion about the _Citizenship (Amendment) Act_ going. I even surprised some that I was taking the stand I was, given my track record of having ideas contrary to the ruling party's. I find it difficult to explain to people that having rational thoughts is all I do.

Let me get to the point: The CAA is not about throwing people out. At all. The NRC is a different issue. Like always, you are free to disagree. Start a healthy discussion, though. If you know me personally, you have my number already.

{{< toc >}}

Buckle up for the ride. This is interesting.

{{< podcast >}}

## Citizens of India

This directly concerns the Constitution of India, and this time, I downloaded and read the related articles myself. Most of them, at least. If you would like you can also get yourself a free copy of [the Constitution of India (amendments until April 2019)](http://legislative.gov.in/sites/default/files/COI-updated.pdf). This is from the website of the Legislative Department, Ministry of Law, Government of India.

Part II of the Constitution defines who it considers a citizen of India.

The Constitution came into power on the 26th of January 1950. Read Article 5 of the Constitution of India. In plain English, anyone who took birth in India since the 26th of January 1950, anyone who was in India since the 26th of January 1945, or were biological children of Indian parent(s)---parent(s) who were either born in India or had lived in India for over five years by the 26th of January 1950, were citizens of India.

In 1955, the Parliament enacted the Citizenship Act, which defined who would be citizens of India after 1950---those who were not born by the first Republic Day. It also speaks about who can get citizenship of India, and how; it said that those who came to India should stay for eleven years before being able to apply for citizenship. (Until then, the tenure was five years.)

In 1987, the Parliament amended the Citizenship Act. This said, anybody born between 26 January 1950 and 1 July 1987 was a citizen of India (apart from those counted as citizens when the Constitution came into effect). Although now, to be a citizen of India, at least one of your parents had to be Indian. If your year of birth was 1990, you would be a citizen if one of your parents is Indian---not because you took birth in India. The Parliament made another amendment to this act in 2003[^5a5a8308], which said that both your parents must be Indian for you to be an Indian citizen.

[^5a5a8308]: [The Constitution (Eighty-ninth Amendment) Act, 2003](https://www.india.gov.in/my-government/constitution-india/amendments/constitution-india-eighty-ninth-amendment-act-2003)

But what happens if your parents are not Indian, and came to India, in, say, 1985? Are you an Indian citizen, then? Why does this matter?

## Immigration to India

Religious persecution is a word that we hear everyday given the situation. Say, if there was a country, called Gamma Land, and the people of Gamma Land worshipped a Lomanso Gomo, their religion being Gomoism[^44299239]. Imagine that an Ashok Kumar, a Buddhist, is a resident of that country; seven generations of his family have been. Today, there is religious extremism in that country, and the Gomos ill-treat Ashok Kumar and his family, by say, beating him up if he stepped out of the house, _because he is Buddhist_. This is an example of religious persecution.

[^44299239]: All names of people, places and religion in the paragraph are fictitious. Any resemblance to anything in actual existence is pure coincidence.

Imagine that Ashok Kumar managed to escape the country and come to India. He is an immigrant.

This way, Parsis from Iran have migrated to India. During the partition of India, millions of people went from the Dominion of India to the Dominion of Pakistan and vice versa. Even after the partition, people from East Pakistan kept coming to India because of religious persecution. Thousands of Sri Lankan Tamils came to India, Tibetans came to India (including the Dalai Lama). Similarly, Rohingyas from Myanmar came to India---this is recent. People from Afghanistan have also, at some point, come to India to escape religious persecution.

According to the 2001 Census, most migration to India took place from Bangladesh and Pakistan. Remember, it does not say Muslims alone migrated from there---an important point to note. These immigrants could be Hindus, Sikhs, or anyone, including Muslims.

These people who come to India are the immigrants. The Citizenship Act(1)(b)[^bb92a781] defines an _illegal immigrant_ as:

> “illegal migrant” means a foreigner who has entered into India―  
  (i)  without  a  valid  passport  or  other  travel  documents  and  such  other  document  or authority as may be prescribed by or under any law in that behalf; or  
  (ii) with a valid passport or other travel documents and such other document or authority as  may  be  prescribed  by  or  under  any  law  in  that  behalf  but  remains  therein  beyond  the permitted period of time;

[^bb92a781]: [Citizenship Act, 1955](http://legislative.gov.in/sites/default/files/A1955-57.pdf)

Again, in simple English, it means someone who does not have travel documents such as a visa, or has the documents but has stayed beyond the period specified in the document.

The takeaway so far should be that immigrants do not automatically become citizens of India. They must apply for citizenship, which when approved, will make them a citizen. It naturally follows that those with valid travel documents will be able to apply for Indian citizenship without much trouble; for illegal immigrants, it will be difficult.

Everything so far is fine. What follows is where the issue is.

## What the Citizenship (Amendment) Act 2019 does

Until this point, irrespective of the religion or country of origin, if anyone came into India without proper documents was an illegal immigrant. Those with proper documentation, if they wanted to become Indian, would have to stay in India for eleven years, and then apply for citizenship.

The Citizenship Amendment Act 2019[^bb982ab74] changes this. It says, it does not matter how someone came to India, but if they came in before the 31st of December 2014, from either Bangladesh, Pakistan or Afghanistan, and they belong to either of the Hindu, Sikh, Buddhist, Jain, Christian or Parsi communities, they can get citizenship.

[^bb982ab74]: [The Citizenship (Amendment) Act, 2019; The Gazette of India](http://egazette.nic.in/WriteReadData/2019/214646.pdf)

In other words, if you came in from one of the three countries mentioned, and you belong to one of the six communities, and you have come five years ago, you can apply for citizenship. (Understand, you do not _automatically_ become citizens. You still have to apply, you still have to establish that you came in before the 31st of December 2014.) Going forward, immigrants belonging to the six communities, coming from these six countries, will have to be in India for five years (instead of eleven) before applying for citizenship.

This is not your trigger. Do not jump yet.

## Article 14

Article 14 talks about _Equality before law_:

> The State shall not deny to any person equality before the law or the equal protection of the laws within the territory of India.

Protestors and TV debates kept mentioning Article 14. And they ask, why not include Myanmar and Sri Lanka (given that the Rohingya Muslims and the Sri Lankan Tamils are both minorities in the respective nations)? Is this not discrimination? This, has nothing to do with the Constitution. The Parliament should answer its people, but this in itself is not against the Constitution---this is instead a matter of policy. Read on (and feel free to verify this against your copy of the Constitution).

Article 14 provides for the so-called "reasonable classification"[^7cad2f65]. In plain English, "religious minorities of Pakistan, Bangladesh and Afghanistan, belonging to either of the Hindu, Christian, Sikh, Jain, Buddhist and Parsi communities" is a specific class of people, not meant to divide the subjects of legislation (as in, the current Indian citizens), into separate classes. Why does this not divide current Indian citizens? Because this does not talk about us in the first place---this talks about specific religious minorities in these three countries.

[^7cad2f65]: [Article 14 of the Constitution of India](https://en.wikipedia.org/wiki/Article_14_of_the_Constitution_of_India)

If anything, the CAA _relaxes_ the citizenship law, as rightly pointed out by experts. In reality, it says, Muslims coming from these three countries are the _same as any other immigrant_ (such as persecuted Tamils from Sri Lanka), but those from these six communities will have the law relaxed because the amendment presumes that they faced persecution as minorities in Muslim-majority countries.

If you are among those that ask if the government can do this, read Article 11 of the Constitution that lets the Parliament regulate the right of citizenship by law:

> Nothing in the foregoing provisions of this Part shall derogate from the power of Parliament to make any provision with respect to the acquisition and termination of citizenship and all other matters relating to citizenship.

In other words, the Parliament has all the rights to decide the right of citizenship by amending the Constitution. This gives the right to the Legislative body to bring in Acts such as that of 1955 and amendments such as those in 1987, 2003 and 2019.

At this point, it _does not_ look like the immigrants have to establish religious persecution; another important point to note. The Parliament discussed the matter of religious persecution, but it does not feature in the body of the bill. In other words, the Parliament seems to _assume_ that minorities in these three countries face religious persecution. Given that Article 14 presumes validity, if I were a Hindu in Pakistan who wants to come to India, those opposing my citizenship have to prove that I did not face religious persecution, but am coming to India because, say, I love the pollution in New Delhi. Whether leaving it that way is right or wrong is a policy debate, not a legal one, because by nature of our law, the accused does not have to prove innocence, but the accuser has to prove guilt of the accused. Legally, the Parliament can make a reasonable classification which it did, and can amend citizenship law, which also it did. Legally, no problem.

If religious persecution were a criteria of reasonable classification, this still means that according to the law, if you are a Hindu who migrated to India because of _economic reasons_, someone accusing you of foul play has to prove it to make you an immigrant who would need to spend eleven years in India before applying for citizenship.

The question remains that even if religious persecution were a criterion, whether the government will ask non-Muslims coming to India from these three countries on economic or other grounds (or potential "infiltrators"), to wait eleven years. Who will find foul play? Again, a policy issue, not a legal one. A question for Mr Shah's claims to answer, not for the Constitution. If you can legally prove that you are a religious minority, that is, belonging to one of the six communities, coming from the three countries, you have a relaxed citizenship process.

If all you cared about was validation of your claims, you can stop reading here and go on with your lives. But if you care about the spirit of India and about educating yourself, then read on to get some insights into the nuances.

## Why people are crying Unconstitutional

A valid question. At the start, I was of the idea that this Act went against the Constitution. And then I read documents, parts of the Constitution itself, spoke to people from both the sides, and listened to the claims on both the sides. WhatsApp University had a boatload of crap as usual, but I have reached a point where I can differentiate between fact and fiction.

I am among those that say while this is legal and technically constitutional, the act deviates from the principles of the Constitution.

This anxiety (and I say this about those that are anxious based on facts, not every protester out there) stems from the following aspects. We start with Assam:

## The Assam issue

Most of the protests happening today are in the North-east. Delhi, yes, but even in Delhi, the concentration is not as high as the North-east. And so, everyone talks about Assam and immigration of Bangladeshis to Assam and the other north-eastern regions. The Home Minister says, "_Infiltrators are the nation's problem_; because West Bengal is a border state, the problem is more acute there."

And then, we talk about the NRC exercise in Assam. That increases noise.

The reason for NRC in Assam was different. First of all, in Assam, the issue was not religion---it never was religion. What BJP says does not matter. The tribal people of the Assam region felt that because there was so much influx of people from Bangladesh---both Hindus and Muslims---that their ethnic identity and that of the land was getting eroded. It did not matter to them which religion the immigrants belonged to; all that mattered was _that the immigrants did not belong to Assam_.

The Assam issue has come up on a large scale at least twice. First, ever since the partition of India and Pakistan, the Hindus had been coming in from East Pakistan (now Bangladesh). These Hindu immigrants came to West Bengal, Tripura, Meghalaya and Assam; Assam had a problem that these immigrants were diluting the ethnicity of the place (even though the border shared between Bangladesh and Assam is smaller than that with West Bengal, Meghalaya or Tripura).

To put an end to immigration to Assam, India and Bangladesh signed an agreement. The agreement said---and I rephrase---that anybody that came into Assam from Bangladesh, irrespective of the religion, since the 25th of March 1971, must go back to Bangladesh.

Second was in 1978, when there had to be an election (a re-election, in fact), India found that the number of registered voters had shot up dramatically, compared to the previous elections. This brought the student unions in Assam to the streets in protest, and gave rise to the Assam Movement. To put an end to the Assam Movement, Rajiv Gandhi signed the _Assam Accord_ in 1985, which declared those that came into Assam after the 24th of March 1971, as illegal immigrants.

## The National Register of Citizens (Assam Edition)

The task that remained now was to identify those illegal immigrants, and thus comes the NRC (National Register of Citizens). Something that had not happened for three decades after the Assam Accord.

The Supreme Court carried out this exercise, and like Mr Shekhar Gupta says, the exercise "threw surprises". The process identified over 19 lac people in Assam as immigrants from Bangladesh. Perhaps not too big a number, but this is it. The surprise was that the majority of these were Hindus.

Panic!

Not the BJP alone, but everyone. Name one political party in 2019 has the "audacity" to throw out over ten lac Hindus from India, saying they are illegal immigrants? None. Not one political party would do that in 2019. Not Congress, not the Left, and most certainly not the BJP. If the BJP pitched something like that, I would look to the west in the morning to welcome the sun.

Now, if you understand what the CAA 2019 says, you will know that it goes directly against the Assam Accord of 1985, and the agreement between India and Bangladesh in 1971. It kills the purpose of the Assam Movement. Also, it renders the efforts of the government and the 1,200 crore rupees spent in the NRC wasted.

The CAA was to handle this issue like a hot knife on butter, but ended up being a pneumatic drill (a.k.a. jackhammer) on granite.

Again, not your trigger. Do not jump.

## What happens if I cannot prove my citizenship

At this point, answer to the question is unknown. All we can do is look at Assam, see what is happening and speculate. The government has made no official statement on what the criteria for NRC are, or what the process is. We would need to wait.

## Are people coming from Bangladesh

People came to India from Bangladesh for two reasons: religious persecution and economy. Ask anyone who came to India from Bangladesh with an idea to settle here, they will tell you one of the two reasons.

Let me address the Home Minister saying that Bangladesh is an theocratic Islamic State and implied that as a State, it does not respect other religions; he said that Hindus suffer torture there. Perhaps among the load of text he read in what he claimed to be the Constitution of Bangladesh, he missed the Preamble of the Constitution of Bangladesh[^204f7c64] that says:

> Pledging that the high ideals of nationalism, socialism, democracy and secularism, which inspired our heroic people to dedicate themselves to, and our brave martyrs to sacrifice their lives in, the national liberation struggle, shall be the fundamental principles of the Constitution;

[^204f7c64]: [The Constitution of the People‌‌‍’s Republic of Bangladesh](http://bdlaws.minlaw.gov.bd/act-367.html)

Did you see "secularism"? I did. Bangladesh, in 1972 declared itself a secular socialist democratic republic (India added the words "secular" and "socialist" to the Preamble in 1976 through the 42nd Amendment). To get this straight, Bangladesh is a State with Islam as its State Religion, but a secular state that treats other religions as equal. True, there was Martial Law in Bangladesh for some time and that Islamised Bangladesh, but the Supreme Court of Bangladesh later declared all laws passed under the Martial Law as void[^5c801b95]. Their supreme court also prohibited the use of the Sharia Law. That makes Bangladesh a NOT-a-theocratic State. If we say that a State enshrining secularism in their constitution does not make a them secular, we should perhaps look in the mirror before making baseless accusations.

[^5c801b95]: [Verdict paves way for secular democracy](https://www.thedailystar.net/news-detail-148678)

Second: economic refuge. India was once way better off than Bangladesh, but in 2019 Bangladesh is either equal to or better than India in a good number of economic indicators (Data: The Print[^9057cf45]). While we are at 4.5% GDP growth rate, Bangladesh is at 8.1%. India is a mere 17% higher in per-capita GDP. But Bangladesh's per-capita income is 60% higher than Assam and almost the same as West Bengal. Bangladesh's female labour participation is at 33%, while India's is 27.2%. Certainly, women from Bangladesh are not dying to come to India for jobs. That is another point for non-Islamism; Bangladesh treats its women better than average Islamic States. And in the end, the Global Hunger Index[^105a8707]: India is at rank 102 and Bangladesh is at 88---ahead of us by 14 positions.

[^9057cf45]: [CAB-NRC, India-Bangladesh ties, and breaking some popular myths about our friendliest neighbour](https://www.youtube.com/watch?v=gmOfSlxEPBc)

In other words, India is no more an economic eye-candy for people of Bangladesh. There may have been Bangladeshis coming in, in the past, but India is not attractive to them anymore.

Anyway, this was mainly to call out Mr Amit Shah for his statements on our friend nation. If you think I am putting down India, you are part of the problem; you are not facilitating diagnosis.

Of course, you may say that Bangladesh is a smaller country, and thus is easier to manage. That logic may fly for a second when trying to pitch two governments, but the numbers disagree with that people of Bangladesh are dying to come to India. And that is the point in question here.

[^105a8707]: [Global Hunger Index 2019: India ranked lower than Nepal, Pakistan, Bangladesh](https://www.thehindu.com/news/national/global-hunger-index-2019-india-ranked-lower-than-nepal-pakistan-bangladesh/article29714429.ece)

## Are Muslims persecuted in Pakistan

The ideology of Pakistan is that of the Sunni sect of Islam. Understanding the intricacies in Islam is beyond me at this point, but the fact is that the State in general cares little about the non-Sunni Muslims. Shias, Ahmediyas and other sects either get the second-class treatment or face persecution.

'So what? Why is it India's responsibility to give shelter to the Shias or the Ahmediyas if they face persecution in Pakistan? How is that India's problem?' Of course, legally, India has no obligation to absorb anyone into its population. Today, India says that members of the six religious communities from the three nations have a relaxed citizenship law; tomorrow, we could say that we extend the courtesy to Rohingya Muslims and no one else. Legally and constitutionally, we will be right in saying it.

Counter question: Why is it India's responsibility to give shelter to anybody coming from another country? What does it matter what religion they follow? Again, legally, India has no obligation to absorb anyone into its population.

But the answer to 'Why not Muslims' being 'Muslims do not face persecution in Pakistan' is factually incorrect.

Point two, given that most immigrants coming into India are from Pakistan and Bangladesh (Census 2001), India is refusing to relax the citizenship law for one part of the immigrants. Why?

Of course, policy decision, not legal discrimination, but why the discrimination from even the policy standpoint in a secular State?

## The sense of India

We are a secular State. India has no State Religion. India was not formed by the Partition---Pakistan was. If anyone told you otherwise, they are wrong. Mr Shah is no exception. Those that did not believe in India's secular philosophy went to Pakistan. The partition was not about Hindus and Muslims, but about a class of Muslims wanting a separate Islamic State on one side and everyone else on the other. The _everyone else_ included Muslims who did not want an Islamic State. That, is India, my dear friend. It did not matter what god you believed in (or whether you believed in a god), what you ate or wore, what language you spoke, what you did for a living, or anything as crude. All that mattered was that you wanted to be free, and be together, perhaps as a nation.

People mattered.

We Indians are not united by a religion. Not by a language. Not by an ideology. Not by culture. Not by ethnicity. Not by colour of the skin. We were not even a single nation to begin with!

> India, that is Bharat, shall be a Union of States.  
> ---Article 1(1), Constitution of India

What unites us is the idea of India. The sense of oneness is despite our differences. Beyond tolerance, _inclusion_ is the philosophy behind India.

This model of oneness is magnificent, but also rather fragile in a sense. And that sentence probably upsets more people than the essay makes happy. But my role as a writer is to show the truth.

A populist has to make the masses happy. And in this hunger to make the masses happy, comes the game of majority. To appeal to the majority, a populist taps into what the masses like. Gradually, "making the majority happy" becomes an obsession. As this obsession advances, the deeper the populist goes to the most basic of instincts. And in this race, we, along with the populist, lose sight of the sense of India, and alienate a section of the society. Then another. Then another. Until everyone gets alienated. Because no one can satiate this lust.

Take a step back and think: What we fought two centuries for, would not last one. History will repeat itself. One of my friends said India should have more nationalism to counter the division mode that we are in. He meant it in the sense of landmass. But what good is a country with an undivided landmass but divided people, resentment and Inner Lines?

## Was this amendment needed

No.

Read Article 11 again, and you will know that the government can declare anyone a citizen. The government can accord and revoke anyone's citizenship as it deems fit. Given that, what was the necessity to bring this amendment? Even as a matter of policy, why was this required---what was so pressing after all? The Sikhs and Christians and Hindus and Buddhists and Jains from other countries wait eleven years to get Indian citizenship, like everybody else, so what? You want people of these religions to "feel welcome", sure, but are others not?

## Is this act discriminatory

Constitutionally, no ([read above](#article-14)). Fundamentally, based on the spirit of India, yes:

>  In the Third Schedule to the principal Act, in clause (d), the following proviso shall be inserted, namely:—  
> 'Provided that for the person belonging to Hindu, Sikh, Buddhist, Jain, Parsi or Christian community in Afghanistan, Bangladesh or Pakistan, the aggregate period of residence or service of Government in India as required under this clause shall be read as "not less than five years" in place of "not less than eleven years".'.

## Nationwide NRC

This is what makes the Act malicious. The Home Minister says, 'Understand the chronology. First, the CAB will come [into existence]. Then, there will be the NRC [exercise].'

During the NRC exercise, based on our experience, everyone is a suspected non-citizen to begin with. The citizens then establish their citizenship and get included in the registry.

I would have no problem establishing my citizenship because I have airtight documentation proving my citizenship, right from my birth. I got educated, I know to work with the government functions, my parents and I have ensured to preserve the documents. I have a job in the regulated sector of the economy, I have a bank account with continuous transactions. I have nothing to worry about. But what number of citizens can do this? I do not know. Will a voter ID count as a valid document? My guess is as good as yours.

Back in the day, nobody worried about all this. Nobody had DigiLocker to store the documents. What would those that do not have the documents do? My mother can prove she is the wife of my father, who in turn has enough documentation to prove his citizenship---and this way, she is a still a citizen. But what about others? My mother went to school, got educated, and still, this is the situation. What about the uneducated? Or the illiterate? Perhaps they have the documents, perhaps not. Perhaps the authorities will accept the documentation they have, perhaps not.

All that we say about the NRC will be mere speculation; the government has not shared official word on the exercise.

Everyone says, 'Do not look at CAA with NRC---the two are separate.' I cannot help but look at them together, because Mr Shah said the latter will follow the former, and the goal is to expel "intruders" from the country. Should I read between the lines along with the current narratives? No? All right.

The Amendment, _if_ married to the NRC, goes against the _spirit of_ the Constitution of India, not the Constitution itself. This is beyond technicalities.

Do I support the NRC?

Not now. Not any time soon.

Does it mean I am in favour of illegal immigrants?

No. I am not in favour of anything illegal.

Look, the NRC is expensive. 12 billion rupees is no small amount for one state. To find what, 1.9 million immigrants. 6% of the population of Assam. This in a small state---3.09 crore people. The expense would be 52,000 crore if this exercise has to happen across India. (By simple arithmetic, not accounting for the speed of the process and the inflation, other operational difficulties, etc.) Can we afford it financially, more so _at this time_?

No. The government earns ₹97,637 crore in November 2019 from GST[^da06660d]. The average from the last eight months is ₹97,039.625 crore.

[^da06660d]: [GST revenue collection, Ministry of Finance](http://gstcouncil.gov.in/sites/default/files/Press-Dynamic/PIB1594406.pdf)

Is this the most pressing issue in 2019 -- 2020?

No.

Second, do we go tracking down every illegal immigrant and throw them into the detention centres? The Assam NRC identified 6% of those evaluated, as illegal immigrants. Assam is a border state. Say, in the rest of India, we find 3% as illegal immigrants. Are there enough detention centres for 4 crore people? Or are we going to build them?

How do we have funds to build detention centres but not healthcare and education? And are we willing to let the government use the tax money we citizens pay to care for illegal immigrants?

Or perhaps there will never be an NRC. Good, but still, what was the necessity of this amendment?

A friend asked:

> I want persecuted minorities to enter. And have a list of Indian citizens also. Give me a solution.

Persecuted minorities can still enter. _Even without the Amendment._

If at all you want to do an NRC exercise, wait; work on the infrastructure. I will give you an example: I cannot download my birth certificate online. Why? I have a physical Birth Certificate from Chennai Corporation; Chennai Corporation says everyone born since 1989 can get their birth certificate on their site. I cannot find mine. And I have tried to get in touch to fix it; it has not worked. I have applied for the voter ID, I see the status says approved, but it has been that way for years. I keep getting bounced between offices. No luck.

Nice of you to say that no Indian citizen will go through hardship because of NRC. Not all the citizens of India have documents to prove their citizenship. Start regulating the documentation. Make a set of documents mandatory and push citizens to get the documents. Make the process simple. Make the process understandable; understand that not everybody is a Shashi Tharoor. Reduce bureaucracy. Before jets can take off and land, there must be a runway. If the NRC gets introduced now, it will be another demomentisation.

Dear those running the government, think beyond a nice list of citizens. If you take away a citizen's citizenship, they lose their job because the law says so. The regular citizen does not have the time to go to the courts and the tribunals. We have a day job, our companies expect us to be in the office at least 9 hours. This is how we earn our bread and butter. We work shifts, we go through traffic; a regular Bangalorean spends 12 hours for work. If you ask 1.5 crore Bangaloreans to prove their citizenship, then get our documents rejected, go fix the documents and come back, get them rejected again, then approach a tribunal with an appeal ...

Now expand this to the rest of India.

Thought is hard, and it probably does not naturally come to you; you take pride in not being an "intellectual", because you never disagree with yourself. But thought is necessary. This is not about the last seventy years. This is not about who is "bold". This is not about power play. This is not a game.

And no, I did not say that this is somehow a political party's way of taking away the voting rights of a large, unfavourable chunk of the population, making the immigrant Muslims wait another six years---"like everybody else"---and expediting the citizenship of non-Muslim immigrants at the same time, in the Bengal region, before the elections, to enlarge the favourable vote bank. Saying so would be highly "unintellectual" of me; I leave such behaviour to those who can go on the stage and find out by the clothes of people what their agenda is.

I take responsibility for what I say, not for what _you think I mean_.

Long live Bhārat Gaṇarājya, the Sovereign Secular Democratic Republic of India.

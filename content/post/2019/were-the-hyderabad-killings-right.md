---
date: "2019-12-07T00:00:00Z"
tags:
- police
- law
- constitution
- state
- rape
- murder
title: Were the Hyderabad killings right
---

The first piece of text I read upon waking up yesterday was that the police had killed the alleged rapists of the veterinarian in Hyderabad[^abe13496]. It took me a moment to let it sink in. This was like the films, where film heroes go about threatening criminals, 'I will take you down and term it an encounter.'

[^abe13496]: [All four accused in Hyderabad vet rape and murder case shot dead](https://www.thehindu.com/news/cities/Hyderabad/four-accused-in-hyderabad-vet-rape-and-murder-case-shot-dead/article30202752.ece)

I am no "liberal community". This is important.

More important is that I respect the Constitution. I base my opinion on what our constitution says about how a citizen should be. I also base it on the textbook definitions of some of the important terms that we use on a daily basis. If you have guessed what my stand is (which is no rocket science), humour yourself, because this is probably not a perspective you have heard of.

{{< toc >}}

I have no words to describe the anger I have towards the perpetrators of the rape. As a man, it shames me that such men exist amongst us, making girls generally uncomfortable around all men. And I admit that I wished they received the greatest punishment there is in our law. But, is this how this should have ended?

Why?

## The republic way

(No pun intended. Notice the lowercase r.)

We are a democratic republic. This is different from all the stories we have heard from way back in the past when kings ruled, and the king's word was the final one. In thr Democratic Republic of India, we have a system called the Constitution of India, and accompanying it, are the Indian Penal Code and the Criminal Procedure Code. As a total, we call them the _Legal System_. The Constitution defines how the nation should run, what a citizen's life should be like, what are his/her rights and duties and so on.

The Indian Penal Code and the Criminal Procedure Code work in situations where the guidelines laid by the Constitution are not followed. The IPC defines the different offences, and the respective penalties that the convict must pay---in the form of fine or punishment or both. The CrPC, defines _how_ to handle the offences; how the law enforcement should run the investigation, what or how they should document what is necessary, which court to approach, what the courts' hierarchy is, etc.

Also important is the fact that an _accused_ is different from a _convict_. An accused is someone whom the complainant (the one who lodges a complaint) points the finger at, or the law enforcement strongly believes is the perpetrator of a crime. Because someone accuses someone, it does not mean that the accused was the perpetrator. You or I or the media or the policemen are not to decide who the convict is. The law enforcement agencies must record the complaint, conduct enquiries, collect evidences or confessions, and present them to the courts. The courts, based on the submitted content and the hearings and trials that happen in the courts, decide whether the accused is indeed a convict.

In other words, if the police charge someone under an IPC section, it does not mean the accused is automatically punishable. The courts ratify the charges and sentence the accused to punishment. This is when an accused becomes a convict, and gets punished.

## Justice

True justice is when the different wings of the system follow the due process, sentence the convict to a punishment, and the convict goes through the punishment. The police or the people do not have the right to announce a punishment, much less take such an action. I am not the one saying it. This is what we gave ourselves on the {{< raw >}}26<sup>th</sup>{{< /raw >}} of January, 1950.

Popular beliefs and fan-worthy movies are merely that; they do not dictate how we function as a republic (again, lowercase r). When someone (or a law-enforcement agency) take the law in their own hands, the act is revenge, not justice. Of course, one may be comfortable with us calling this as an act of revenge. 'So what?', they may say. But that is not as per law. What happens when you commit such an act? It could be murder. And a murder is punishable in the court of law.

In other words, now the policemen who were responsible for the death of the alleged criminals would come under the lens (and they must, in a civil society that runs by its constitution) on whether the killings were justifiable as per the law.

Vigilante justice and encounters are nice for a film or a novel. In real life in a democratic republic, such an act is condemnable.

## What should have happened

As a citizen, this is what I would expect from the police, the so-called friends of the society:

When the victim's family went to the police with their worry, instead of being callous about the situation of the family, the policemen should have been sensitive. Saying, 'Well, she would have gone to meet someone.' is not acceptable from the mouth of a serious police officer, no matter what his capacity is. If the family did not think there was a reason to go the police, they would not have gone to the police. The fact that someone came to the police station means that the situation was more serious than usual.

The police should have begun their investigation by contacting the police station in the jurisdiction of the toll gate, because the sister must have told the police about the whereabouts of where her sister had parked her bike.

Having noted down the victim's phone number, the police should have called the cab aggregators to find out the last bookings from the said phone number. They could have constructed a timeline of her whereabouts, such as which vehicle she had taken to what place, whom the victim had gone to meet, whether she had travelled back to the place where she had parked her bike and so on. Having collected the details, the police should have started the investigation to find out where the girl was last seen, and by whom. Who knows, this could have led them to the girl before the rapists had burnt her.

All this, instead of wasting two hours under an _assumption_ that she was out with someone.

## The motive

Now, even though the people might be in favour of the policemen who killed the four men accused, they are potential criminals. There will be an official enquiry into the encounter. What will come of it, is unknown for now.

To me, though, this is what it looks like: The policemen at the police station did not bother to record a complaint, or file an FIR. Given that the family had gone to the police station to give a complaint, and a crime had happened as the family had anticipated, the next leg of enquiry would be on why the policemen did not record a complaint and begin investigation. There will be public outrage on this. There will be pressure from superiors that the policemen face punishment---within the department or under the law. There will be an enquiry on the station head, on the other members of the station present at the time, and so on. There would be uncomfortable questions to answer.

Instead, what if the police did what they could to avoid public outrage? Doesn't this story of the encounter sound too convenient? Ten policemen and four criminals. Armed policemen, unarmed criminals. Then someone said the criminals were so adept that two of them snatched the weapons of two policemen. Yet, the so-capable criminals could not shoot one policeman. But all the four of them are dead. Deep down, most of us are thinking this. And some of us are a little more intuitive. We have come to our conclusions. The intuitive ones know the conclusions are spot on.

But the intuitive ones are intuitive enough to also not demand an encounter of the well-to-do ones on whom there are rape allegations. Oh, did I hear a  Sengar somewhere? No? Oh, did someone say Chinmayanand? No?

Umm ... why?

## 'But the legal system is slow'

No argument here.

The funny thing about our incumbent government is not recording uncomfortable data at all. If there is no data, there can be no request for it, and if there is no data to request or share, the government can call all our grievances against it as "unsubstantiated". Not going by numbers[^03d4e17a] is in the vogue.

[^03d4e17a]: [Maths never helped Einstein discover gravity: Piyush Goyal](https://www.thehindu.com/news/national/maths-never-helped-einstein-discover-gravity-piyush-goyal/article29399249.ece)

Anyway, I managed to find the 2017 report from the National Crime Records Bureau[^26317ed7]. According to Page 222 of the report, about 1.7 lac cases against women were _pending investigation for over a year_. The heroic police fail to file charge-sheets in ~40% of the cases. Why?

Now to the judiciary. In the fast track courts, trials of about 3400 cases took over _ten years_ to complete (Page 1119). I repeat: ten years in fast-track courts. Over 15,000 of the 38,000 cases ran for over three years. The fast-track courts closed a mere 1,000 cases within a month. If you consider a case completing in a month as fast-tracked, the success of the fast-track courts is about 2.6%. If you make it a year, the success rate is 30%.

[^26317ed7]: [Crime in India 2017: Statistics](http://ncrb.gov.in/StatPublications/CII/CII2017/pdfs/CII2017-Full.pdf)

As the cherry on top, fast-track courts have about six lac cases pending[^5bc15b7f].

[^5bc15b7f]: [Justice delayed: Over 6 lakh cases pending in fast track courts; this state tops the list](https://www.financialexpress.com/india-news/parliament-session-lok-sabha-justice-delayed-6-lakh-pending-cases-in-fast-track-courts/1619917/)

All true. But do we take law into our hands? Or do we let the police take law into their own hands? When did the police become synonymous with the law? Let me remind you, we are a republic (with a lowercase r---I'm getting tired already). The moment you throw away the law, whatever little restraint that exists today---on the unlawful or the powerful---will vanish. Lawlessness is much worse than delayed justice.

Look the judiciary does not have magical powers. The judiciary must abide by the law. The law dictates that the proof of crime should be beyond reasonable doubt. This is the law's way of protecting the innocents, in whose warmth the criminals manage to bask. The investigating agencies must submit valid proof which the courts can base their judgements on. In most cases, the judge knows that the accused is a criminal, but he can do nothing until the prosecution produces valid evidence. The investigating agencies must gather the evidence. A major reason for the failure of the judiciary is the investigating agencies.

If the laws are inadequate, the lawmakers are to answer. And who are the lawmakers? The ones we elect. Another part of the failure of these cases is the lawmakers. And the judiciary is to blame as well. Why do public prosecutors not appear in courts during hearings? Who gives the right to the courts to be indifferent to cases? No doubt, there is incompetency in the judiciary as well, as everywhere else.

## Precedents and looking forward

The legal system must be a system. The legal system (or more specifically, the judiciary) is independent, and is answerable to The Book, and the book alone. This is why the people do not elect the judiciary---people cannot directly pressurise the judiciary.

The police are answerable to the judiciary. Our constitution protects our rights (which includes the right to life), the judiciary protects the constitution. The judiciary can question the police, and hence, the police have to go by the constitution. Given that the constitution gives us the right to life and to protection of life, the police have no authority to take it away, no matter how grave the alleged crime is, irrespective of who the criminals are.

After due judicial process, the judiciary decides on what punishment the proven criminals should undergo. In this case, the police had not proven anything yet. As per the judiciary, the prosecution should prove the crime, the accused do not have to prove innocence. It may be hard to objectively look at this situation, but that is what the law says.

'But what if the police could not prove the crime?'

That is precisely the issue. It takes significant effort to prove a rape. In a case where the victim was down to ashes, the case becomes harder. In the meanwhile, human rights groups would come, try to save the offenders from capital punishment and what not. In a blink, the alleged criminals would become "humans". And why not, unless proven as perpetrators of such a ghastly crime?

But, if this method becomes an illegal precedent and every other law enforcement agency starts doing this, eliminating the judiciary, we will become a police state. Those with _lathis_ and guns will have the ultimate power. They will not be answerable to a neutral agency. If there is no judicial process, a policeman could pick me up someday, accuse me of a crime I did not commit, and then in broad daylight, shoot me down claiming they have slain the culprit.

Take a step back and understand: this is not me saying this is what happened in Disha's case. I am saying, this _could_ happen tomorrow to you or me. If there is the judiciary, the police will have to submit me to the courts, which will try me, and can acquit me when they find me innocent. I have a chance to live.

## The larger issue

Justice or punishment are not about revenge. They should be what Fat Boy and the Little Man are to war. They are not merely about paying back. The dead is dead. She will never see (in)justice anymore. Afterlife is not something I subscribe to, but even if you do, what does it matter whether the four men are in jail or are dead, to the dead doctor? Perhaps her family could say, 'Well, four less men for our other daughter to worry about.' but that is the end of it.

In that context, will a death sentence (or an "encounter" for that matter) fix it all? If it could, there should have been no rapes happening after the Nirbhaya case. Conversely, the numbers have increased. Yes, an argument is that the number of cases reported has increased, but the number of rapes happening has reduced compared to before. (As an example, suppose the number of rapes happening before the Nirbhaya case were 15,000 a year, a mere 25 of them got reported; now, the rape cases had gone down to 10,000 a year but about 500 were getting reported.)

While this is a possibility, I find it hard to accept. First, I think the _Werther Effect_ is more powerful than the general population considers it to be. Second, I think the rapists today are comfortable in the knowledge that the police are not interested in pursuing rape cases, unless there is pressure from the likes of celebrities and higher officials. The reason is, rape cases are hard to prove, and every station already has a pile of cases pending investigation. Third, capital punishment is no more a deterrent; the attitude of the criminals is such. Among public figures downplaying the gravity of rape[^a59d842d] to films promoting toxic masculinity in an already patriarchal society, the bigger point gets lost behind obnoxious remarks and high-decibel narratives.

[^a59d842d]: [14 Times Powerful Indians Made Outrageous Remarks on Rape](https://www.news18.com/news/politics/14-times-powerful-indians-made-outrageous-remarks-on-rape-1484799.html)

Today, because rapes find such a high level of coverage in the media, the rapist (dormant or otherwise) _thinks of_ the coverage as a public approval of the crime---Werther Effect. The "elders" and some patriarchal public figures absolving the men of the crime, and victim-shaming, add to the fire. This leads the rapist to commit the crime with less remorse and more confidence.

But can you not report a rape? Can you not report the shameful statistics of the agencies that should be protecting us? How do you continue to maintain pressure on these agencies if statistics are not published, and the agencies are not questioned based on the statistics?

Education has not helped either, know that. Learned members of our society make statements that you cannot stand. A couple of days ago, I read someone's statements on how girls should carry condoms to protect themselves, and not resist rape[^846d42f1]. Once a public figure had said that most rapes were consensual[^a59d842d], making me run for the dictionary, thinking I had either gotten the meaning of rape, or that of consensual wrong.

[^846d42f1]: [Filmmaker Daniel Shravan asks women to carry condoms to avoid rape](https://www.newindianexpress.com/entertainment/telugu/2019/dec/04/filmmaker-daniel-shravan-asks-women-to-carry-condoms-to-avoid-rape-netizens-rip-into-him--2071440.html)

This issue is deep-rooted. The fault is in the mindset of the public in general. Band-Aid solutions such as hanging the culprits and shooting them down does not help curb rapes. That does not mean we stop punishing the rapists; like I said before, they deserve the toughest-possible punishment---by the book of law---such that it becomes a deterrent. Although _that alone_ will not fix it.

In reality, the problem is with the organ between the man's ears.

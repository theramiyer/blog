---
date: "2019-03-08T00:00:00Z"
subtitle: And what to do instead
tags:
- rationality
- celebration
- mythbuster
title: How to not celebrate Women's Day
---

When Ustraa sent me an "offers" SMS in November, "Gear up for No-shave November with Ustraa ... 20% discount on all products", I realised how meanings were being lost rapidly. No-shave November is a relatively new "observance", and most of us Indians don't know the meaning of it. (So, Ustraa is forgiven for now.) This post is not about No-shave November, but about a relatively old concept of _Women's Day_. Did we lose sight of its meaning because it's a pre-Millennial concept?

Every Women's Day, our news feeds, newspapers, messages, Instagram posts ... everything around us is filled with "Empowerment" and "Power to you, Woman" and an entire glossary of terms and phrases including "Beautiful ladies". It has become second nature: Come March 8, wish people on their birthdays, and then, go on about how women "make life beautiful" by celebrating those who "achieved" something for us.

I am not a feminist. You are probably not, either. So let's talk: human to human.

Women's Day is a rather old concept. A concept that was started by socialist groups in the US and Russia, primarily. Later on, China and other countries pitched in, and today, it is an official holiday in many countries including Afghanistan. Many movements that involved women (including movements for voting rights and the end of WWI), in the end, led to Women's Day being observed on the 8th of March every year. No matter what different reasons it was for initially, it later on turned towards equality of women. In 2019, the theme is:

> Think equal, build smart, innovate for change.

But I am biased against the Women's Day of today, and it has nothing to do with misogyny: Women's Day of today comes off as a rhetoric.

My friend, [Shruthi EN](https://shruthien.wordpress.com/), [summed it up rather beautifully](https://www.facebook.com/photo.php?fbid=10218848811600794):

> This Women's Day,  
> Don't wish me.  
> Don't thank me.  
> Don't tell me how special I am.  
> Don't tell me that I am a good this-and-that.  
> Don't tell me about the sacrifices I make or seem to need to make.  
> Don't tell me that I make your life beautiful.  
> Don't bring me flowers,  
> Or gifts or shopping vouchers or kitchenware.  
> Don't talk to me about empowerment.
>
> This Women's Day,  
> Let it not be about me.  
> Let it be about you.  
> Because I know this is not easy for you either.
>
> Can you take instructions from a female boss?  
> Are you unbiased when a lady colleague brings her ideas to the table?  
> Can you take "No" from a woman?  
> Can you let a woman decide finances?  
> Do you agree, to yourself, when she is better than you at the job---any job?  
> Can you take care of your house, not "to help her" manage, but for the sake of the house itself?  
>
> Let's do this celebration thing when you rise. When you are ready.

Many out there are going to see this (the "celebration thing when you rise" part) as a challenge. It may even "hurt" some "sentiments". But take this the right way. Of course my explanation is going to dilute the impact of the succinct piece, but here goes:

Women's Day is not about contributions women have to make to earn the respect that men are given without asking.

Women's Day is not an occasion to make women happy. Women's Day is not about the "gender role" of shopping, or taking care of the house, or about making (or having to make) sacrifices for men. Women's Day is not about appreciating women---every human is good in at least one aspect; it's no big deal. Women's Day is not about making someone "feel" special. It's not about bringing them gifts and goodies. (And for God's sake, Women's Day is not about marketing!) It is not about "gracefully" "empowering" a half of humanity.

It is about the mentality. It is about perception. (She also acknowledges that it is hard for men as well. I remembered an old post of mine, [On being a man in a patriarchal world]({{< ref "on-being-a-man-in-a-patriarchal-world" >}}).) It is about if you can, in reality, perceive women as your equal. It is about not mockingly calling your wife the "boss" or the "Home Minister". It is about whether you can accept the fact that she is the same as you.

When I was in the final year of college, my friend contested for the post of the president of the Forum. During a conversation with me, she said, 'I would be the first girl president of our college, if I do win, that is ... Ram, no matter what anyone says, our guys won't like having a girl president.' She won, of course, but that message speaks a lot about us as a society.

Taking "No" from women is an important issue today. Watch the film, _Pink_,  to know more. The _Pink_ "No" is just one among the several. Taking the "No" in the right way is about anything where the woman says no, and you respect her decision. You do not question it just because it was the woman who decided, especially when the decision rejects your thought, idea or proposal. It is not only about accepting the rejection, but _also about not having resentment towards them_ because of their rejection.

It is about not coming in the way of her due right to decide what is good (including finances), it is about being able to accept---without bringing in gender roles---facts about a woman being better at your job than you are. That also means, not saying things like 'Women are better at being teachers (but not engineers or drivers or astrophysicists).'

I really liked the '⁠... “to help her” manage' part. I have heard a lot of my friends say, 'Well, she cooks, but I also help her.' Basically, this means, the "she" runs the house, but the "he" helps her. It means that "she" has "accepted" the house chores as hers---which are hers to do because of gender roles---but "he" is kind enough to "help" her. This is bad.

She says, Women's Day can be celebrated, when we (humans) all rise above all this dirty prejudice and the rhetoric.

And to me, Women's Day is successful _when there's no Women's Day_; when that ideological distinction between men and women doesn't exist.

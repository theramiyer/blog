---
date: "2019-08-13T00:00:00Z"
subtitle: The nuances and the way forward
tags:
- kashmir
- India
- politics
- diplomacy
- geo-politics
- insurgency
- accession
- history
- civics
- constitution
title: 'Kashmir and Article 370: Part Three'
---

In the last two posts, we understood the technical aspects of the _Jammu and Kashmir Reorganisation Act, 2019_. To repeat for the benefit of those who landed here directly, this is the third part of the three-part series on the Act. [Part One of the series]({{< ref "kashmir-and-article-370" >}}) is about the background of the formation of the Union of India and accession of Jammu and Kashmir to it. [Part Two of the series]({{< ref "kashmir-and-article-370-part-two" >}}) is about the current situation, along with the legal and geo-political view of the change.

Reading the series partially will make you judgemental, and will not do justice to reality. This part is more of a nuanced view of the situation, and is not for those with low emotional intelligence, but an important part nonetheless. I have warned you. This article is available as a [typeset printable PDF](https://www.scribd.com/document/422074646/Kashmir-and-Article-370) as well.

Disclaimer: I have never been to Kashmir, and hence, I have never had first-hand interactions with Kashmiris in Kashmir. What I know, is from interacting with Kashmiris here in Bangalore, and reading. But this reading has rejected boundaries. I used a non-profiling search engine, over a non-regionally-identifying browser, because I did not want the search engine to tailor the results around "Hindu searcher from India". As an Indian, I am subconsciously biased towards India, and I understand that. I have hence had to put conscious effort to make this as impartial as I can---there is no substitute for that.

Here are the nuances:

{{< toc >}}

## Breaking down the regions

We read about the break-up of the colonial cartographers' version of the map of the state of Jammu and Kashmir, as handed over to India, in the first part of the series. Here are the regions (in a little more detail):

- India-administered Kashmir
- Azad Kashmir
- Gilgit-Baltistan (a.k.a. the Northern Areas)
- Aksai Chin
- Trans-Karakoram Tract

The United States Library of Congress has a pictorial representation of the map. (The region shaded with green lines is the Trans-Karakoram Tract.)

![Map of the Kashmir Region: United States Library of Congress \| Wikipedia](https://upload.wikimedia.org/wikipedia/commons/8/84/Kashmir_map_big.jpg)

## Separatism in Kashmir

The restrictions in Article 370 limited the democratic reforms in Kashmir for about three decades. By the late 1980s, the Government of India had even reversed some of the reforms. Along with this, during Indira Gandhi's tenure as the Prime Minister, the government stopped tolerating the expression of discontent. Some of these channels were even closed.

This gave rise to anger.

Now, to the political side of the story. I don't know how to apolitically put this. Let us start with Pakistan. Pakistan controls Azad Kashmir. This part of Kashmir is at the core of the insurgency. Most militant groups have started from here. The most-talked-about districts of this region, in the context, include Kotli, Muzaffarabad and Bhimber. Pakistan claims that Azad Kashmir is on its own, except for Pakistan controlling its defence and foreign affairs. Pakistan, through militants, also deter people from participating in elections there. That the military establishment of Pakistan runs the country is well known.

The people of the Gilgit-Baltistan region do not have even the basic democratic rights in Pakistan, and have never participated in elections.

On the Indian side, we have been shuttling between different stands. First, Sheikh Abdullah won the elections and led the Constituent Assembly of Jammu and Kashmir. The United Nations said that these State elections are not a substitute for the plebiscite. The Government of India said that there will be a separate plebiscite, which will decide the future of Jammu and Kashmir. Sheikh Abdullah was in favour of accession of the state to India. Later, his thoughts and stand changed over a period of time.

Nehru, on his part, also kept changing his stand on the plebiscite.

As a reaction to Abdullah's changing stands, the Government of India arrested him and the then _Sadr-i-Riyasat_, Karan Singh, appointed Bakshi Ghulam Mohammed as the Prime Minister of the State. The Constituent Assembly under him, ratified the Instrument of Accession. Jammu and Kashmir officially became part of India in 1954.

At the time, the Kashmiri sentiment was still for accession to India. Although, remember, while the Kashmiris were in favour of accession to India, they may not have been in favour of the abrogation of Article 370. The Constituent Assembly never spoke a word about the abrogation, nor was the plebiscite held. In other words, Kashmiris themselves never said, 'We want to be like any other non-Kashmiri Indian.'

The rigging of elections, during the years of Congress regime that followed, with the intent to ensure that either the National Conference or the Congress (and nobody else) were in power in Jammu and Kashmir, further angered the Kashmiris. No matter which candidate stood, none of them stood a chance to win these elections. Gradually, this, along with the retraction of democratic reforms and silencing of the voices in the late eighties, led to the wave of insurgency.

In the nineties, when the insurgency threatened the internal security of India, the government imposed President's Rule in the state.

In the mid-nineties, the Kashmiris' situation deteriorated further, feeding their anger. In May, the parliamentary elections were due. On the one side, the separatist militant groups threatened the voters of harassment if they voted, while on the other, the Indian armed forces forced the voters to vote. Not to mention the booth-capturing and other usual election malpractices.

Remember, the two countries had signed the Shimla Agreement by then, and Jammu and Kashmir was an "internal matter". The world had no say in it. Also, the Constituent Assembly elected in the State had ratified the accession in 1954. That meant that Pakistan was illegally trying to claim Indian territory. That part is true. But that the Kashmiris wanted to side with India was, technically, Schrödinger's Cat.

Sheikh Abdullah (and his family) have shifted their opinions every now and then; populism has never spared any politician. Shimla Agreement is (democratically) questionable because it does not take into account what the _people_ want---whether the people themselves endorse Sheikh Abdullah's (and Bakshi Ghulam Mohammed's) actions on the accession or not---and touches on the legal aspects alone.

## Angry Kashmiris

The anger of Kashmiris is that the two countries, India and Pakistan, have come to conclusions on their own, without _ever_ consulting them. That they have not been able to exercise their right to self-determination. What if they voted for Abdullah solely because there was no better whom they could bring to power? That does not mean they accept every action of his. And the United Nations had said (and India had accepted) that the elections are not a substitute for the plebiscite; that the plebiscite was separate, and was in no way related to the elections.

The Kashmiris feel, India and Pakistan no more see Kashmir as a people, but as a piece of land; that the countries are more concerned about the territory. That in view of their disputes, the two nations have lost sight of the soul of the land under dispute: the people. This separates the Kashmiris into three groups: ones that are still in support for Kashmir's accession to India, ones that are angry with India and want to join Pakistan instead, and the third, who want nothing to do with either country. The third category is perhaps the most common today, the extreme layers of which is the militants. In other words, while majority Kashmiris don't want to have anything to do with either country, militants are but a minuscule part of them.

But then, how do we know who is a militant, and who is not?

Militancy has been drastically growing in the state over the last decade. Militant groups target regular (Muslim) homes that have male children. The groups force families to let one of their children join a militant group. The militants threaten to annihilate the family if they don't send a child to the group. Some families relent, while others get killed. Those that relent become a target of the forces, and the forces begin surveillance, and harassment in the name of investigation. But, how can we, as a country, allow militancy? If a family has sent a boy to the group, a good way of being able to reach the militant group is by "keeping an eye" on the family. Yes, but then, isn't self-preservation a basic human right?

At the same time, is militancy the way? To what end?

But, imagine if all this happened to _your_ family. What would you do as a mother, as a father, as a son or a daughter or a sister? Let the militant group annihilate your entire family of twelve for a "landmass" that no more sees you as people but as property? Or lose one member while save the rest of your flock? What are you paying the price for? If you choose annihilation, do you think your annihilation will stop militancy in your region? Militants are like cattle. One dies, the shepherd breeds another.

## The undemocratic side

Legally, the move was straight-forward: The Legislative Assembly of the state of Jammu and Kashmir had dissolved as the state prepared for elections. The elections did not happen, and President's Rule continued. Since Article 370 and the move of the Constituent Assembly of Jammu and Kashmir in 1954 made Jammu and Kashmir part of India, and keeping in view the Shimla Agreement of 1972, the Parliament had the rights to revoke the special status of Jammu and Kashmir.

But remember that the government house-arrested the prominent political leaders of the region. The government also prohibited public gatherings. The central government stripped the local security authorities of their weapons. Thousands of troops entered the region, and the region was under a security lock-down.

Which part of that was democratic? How would you see this as a Kashmiri?

But then, as the Government of India, how else would you abrogate Article 370 without causing a massacre of innocent people---by militants, by secessionists, by political extremists, by religious extremists, or other anti-social elements?

But why abrogate Article 370 at all, you ask? [Go back to the previous article.]({{< ref "kashmir-and-article-370-part-two" >}}#but-what-changes-now)

A bigger question emerges in case of independent Kashmir: Will Pakistan and its militants let Kashmir last one month as a separate country? What then?

## The Two-Nation Theory

Pakistan uses the Two-Nation Theory to promote the secession of Kashmir from India. Pakistan says, 'You are a Muslim majority state. We are an Islamic republic. Your religious beliefs are our religious beliefs. You are no different from us. You are safer with us than being a minority in India.'

The Constitution of India says, 'We are a secular republic. It does not matter what religion you follow.' Kashmir is "spoilt with choices"!

But given the skirmishes in the last two to three decades (and specifically in the last five years), the sentiment is that the secular model of India is failing, notably so with Muslims. That Kashmiri Muslims are not seen as "loyal" Indians. 'Then why even bother?' I heard this sentiment first hand from a Kashmiri Muslim who was a colleague. He said he wanted Kashmir to join India, but the government authorities see his people at home with an eye of suspicion all the time, that they are Pakistan-friendly (solely because of their religion).

The narratives of some of the right-winged Hindutva-promoting political parties is that India is a Hindu nation. They make statements like, 'Barring Buddhists, Jains and Sikhs, we will throw the followers of all non-Hindu religions out of India.' This is the textbook definition of "ethnic cleansing". These incidents have handed Pakistan and Kashmir secessionists their trump card on a silver platter, of which, as expected, [Pakistan is taking advantage](https://pbs.twimg.com/media/EBnxRKDXsAAyNpQ.jpg:large).

The solution?

## Sabka Vishwas

Prime Minister Modi, in his election campaign, added the clause, "sabkā vishwās" or "trust of everyone" to the original "Sabkā sāth, sabkā vikās", or, "With support from everyone, [there will be] development for everyone".

This is a critical pivot point that, if followed, will solve a good number of trust issues in our country. Specifically, communal ones. What the right-aligned pro-Hindutva politicians fail to see is the word "secular" in the Constitution (while the others abuse this word in their own different ways). The politicians, because of their own agendas, have all forgotten the spirit of India.

And some moronic comments by the citizens on social media have added fuel to the fire.

Coming back, the point remains: the non-Kashmiri population of India must win the trust of the Kashmiris.

## Addressing the alienation

If there has to be a change on ground zero, the government must look at the issue of alienation. Merely saying 'You are no different than the other Indians' is not enough; giving defensive speeches is not enough. We must live up to that promise.

What our government does next will tell us if this move was akin to what late Mrs Gandhi did, thereby angering the Kashmiris further, or this was indeed a genuine attempt at opening arms and accepting Kashmir as a people. The next four weeks will give us a whiff of what the direction is, and the next four years will tell us for sure, what will happen later.

And all this while, Pakistan will not remain silent. The country leeches on such issues. If Kashmir becomes part of India and all dispute vanishes, the Pakistani Establishment will face an existential crisis. Will the Establishment let that happen? What about the hundreds of nuclear weapons?

## Wrapping up

To wrap up a rather long post, let us start with a question: was this at all necessary? To answer that, consider the aspects mentioned in the post and these counter-questions: Was Kashmir having a good life? We tried different incremental steps in the last seven decades. India as a country moved to become one of the most talked-about nations in the world, in the last seven decades. We are an important cog in the world machinery. Remove India from the equation of the world, and the world is no more the world we know.

Yes, what we did to Kashmir is perhaps the least democratic action for the world's largest democracy, but, in the interest of our fellow citizens in the region, was this move such a bad one? Did we not owe it to rationality to scrap a method that did not work, and try something new? What other options did we have? What other options did Kashmir have? I understand that it sounds like 'Kashmir was helpless'. It could sound rather arrogant if I said that India was the best chance that Kashmir had, but the reality is, Kashmir's other neighbours will never let Kashmir progress in its spirit.

India is the most democratic, among our neighbours around Kashmir, at least. Perhaps the people of Kashmir should give this a chance. Yes, there are religious issues in some parts of the country. Yes, there are linguistic fanatics, communalists, political propagandists, lynchers, corrupt officials and every other antisocial element on mainland India that every other regular country has. But the life of a regular free citizen on mainland India is better than the best life a Kashmiri has had in decades. India is democratic enough that I can legally post all this on a public platform. Yes, this government has made blunders. But again, no more than any other government in any part of the world. From an idealistic point-of-view, what happened to Article 370 is not great. But, what was the special status doing anyone any good in 2019? Is that not something to think about?

Personally, I don't see any other way the government could have handled the situation, given the current geopolitical factors and factors internal to Kashmir. Also, we had to avoid unnecessary engagement in unfruitful or wasteful activities that could potentially sap our growth and make us lose sight of our larger, global goals. We could not let other countries use us as a stage for negotiations. Nor could we be a pawn. We have our interests, our goals, and a long way to go.

Likewise, this is not the end of story like some politicians may have you believe. This post does not absolve the government of its actions. What happened is a twist, and we don't know what will come of it on ground zero. We cannot blame Kashmir for feeling alienated. The actions of a handful of corrupt agencies and officials has brought about this situation. Like how we should disregard the all-gloomy-and-dark picture that Arundhati Roy paints, we should disregard the sunny picture that the ruling party shows us, as well. Those are two extremes, and we call ground zero as "zero" for a reason (hint: integers).

Vajpayee, during his tenure as the Prime Minister, did care about a handful wounds of the Kashmiris. I don't know if adopting that spirit will suffice, or if the government would need to go an extra mile given the current temperament in Kashmir.

I hope we both manage to accept each other as fellow Indians disregarding geographical, religious and ideological differences, as the designers of the Constitution of India envisaged; the Indian spirit is to _celebrate diversity_, not merely _unite despite diversity_. The former is acceptance, and the latter, mere tolerance. Acceptance is beyond tolerance; acceptance is when you are a family with each member being different in thought and actions. Kashmir is that new member in the family who has not yet shared the roof with us. Merely opening the door, making them seated, and offering them water does not end the story. Again, we did not merely open the door; we ripped the wall on the other side of which the member lived, and said, 'Welcome home!'

Are we both capable of unconditionally accepting each other and moving forward as one? Time will tell.

> I welcome constructive criticism. If you find any issues with the technical aspects of this post, do [let me know](https://twitter.com/iamramiyer). Please attach evidence, and I will make corrections accordingly. I will ignore baseless arguments, propaganda, accusations, name-calling, tangential arguments, conspiracy theories, content overload and any other form of destructive engagement.

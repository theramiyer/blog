---
date: "2019-12-08T00:00:00Z"
tags:
- police
- law
- constitution
- state
- rape
- murder
- justice
- anarchy
title: The other side of the Hyderabad encounter
---

I met a friend of mine today, and heard an interesting perspective. A light discussion began on the Hyderabad killings, from her saying, 'I have started feeling that they could have been innocent.' But in no time, one sentence led to another, and we were on the opposite sides---neither of us believing that the four were fully innocent, but at the same time, me standing by the Constitution and her standing by the police.

{{< toc >}}

## The justification

She said the police were right in killing the accused, because otherwise the four would have walked as free men (because proving the rape was hard when the killers had burned her). Her stand was, there was no other way the police could ensure that these four men faced some punishment.

The discussion went on to justification of encounters in _such cases_, while I kept saying this is a bad precedent. I think this is where most would disagree with the stand I took on the [previous post]({{< ref "were-the-hyderabad-killings-right" >}}). 'Ram, I want these people to face some sort of consequence. If the police are unable to prove rape, the accused will walk free because the law protects them.' The idea was, that the judiciary has failed. And I don't fully deny it---the Nirbhaya case convicts are still in jail, alive. Most of them anyway. And a lighter punishment for the juvenile in the Nirbhaya case was even atrocious.

Like I said in the previous post, punishments to such crimes should act as a deterrent. No part of what happened to the Nirbhaya rape convicts, according to me, is a good-enough deterrent.

## Indoctrinated

Given that, the next statement that I got was, 'Well, Ram, this is the problem with you all. That boy in Haryana that said that women should not come out of their houses and should handle household chores and know their place, that man in ISIS in Afghanistan, and you---are no different. You are all indoctrinated. You all pick your book of ideas and say it's unquestionable. (To one, it's an unwritten societal book; to another, it's a religious book; to you, it's the Constitution.)

She was partially right.

Superficially, a valid point. After all, that is what it looks like. But here is where that idea is wrong:

## The Constitution is different how

The first difference is that the Constitution is subject to amendments. The moment I pointed this out, she said, 'So are the religious books. They have also evolved over time.'

Wrong. First of all, while most of these texts have indeed evolved to a certain extent over time, they all stopped evolving centuries ago. Worse, no one can bring meaningful changes to them anymore.

Second, following from the first, there is bias in these books---in one way or another---against some sections of the society, and given the way the world works today, the books are at least partially irrelevant.

Thirdly, the proponents of the _Shariat_ (or the _Manu Smriti_ or any other religious book for that matter) will not let it change. If at all someone agrees for a change, there is _no method defined_ to change these books, and this person will face the question on his authority to change what the book says.

We can go into further smaller details, but you get the point.

Contrast that with the combination of our constitution, the Indian Penal Code and the Criminal Procedure Code, which constitute a more decentralised system. The one thing that cannot be _easily_ changed in the Constitution is its basic structure.

Moreover, I can be unhappy with the Constitution, question the Constitution, challenge the Constitution---there is a due process for it, but the point is, I can---as a citizen; as a regular citizen. Nobody will cry blasphemy, nor will anyone persecute me.

## On anarchy

Yet, the answer to her question on whether the legal system had managed to deter rapists from raping, is in the negative. Taking a dig at an earlier comment of mine on people of her profession, she said, 'This is the problem with you engineers; you look from the macroscopic perspective. I'm talking from a microscopic standpoint---does it [the judicial system] serve justice in any way?'

A valid question. Apart from the system failing the victims of rape, it has also failed to deter criminals from committing the crime.

But, is that a justification to do away with the judiciary (and hence the Constitution)? Wouldn't it lead to anarchy?

'What else is it now, Ram? That's what I've been trying to tell you!'

I get it. She is right from her stand. Who is punishing the powerful? Who is protecting the common citizens? And the book alone is not to blame. The system comprises of the agencies that must ensure the enforcement of the law. Incompetent members at each level of the system are killing it. If people had faith in the judicial system, what reason did they have to applaud at the encounter killing of these four men?

I found (at least) two flaws, though; one in the statement that anarchy is fine, and one in that the encounter killing was right.

First, on anarchy. This friend of mine belongs in the "educated" category. She's an intellectual. She has a personal moral compass that is good enough to make people feel safe in her presence. But here is the point: _she_ doesn't need a religion or a constitution to guide her, or protect people from her. To my knowledge, she doesn't hit people, she doesn't kill people, she doesn't rape people, she doesn't rob anyone, probably pays her taxes, and in general, promotes a civil life. She understands people's rights, she understands her duties.

If everyone in our country was like her, we would probably not need the Constitution, and anarchy would have been fine. The limitation to being microscopic is that you must take a sizeable number of samples to get to a conclusion. Microscopically analysing one cell from a tissue leaves it to probability on how accurate you are about the characteristics of the tissue, based on your observing that cell. The higher the homogeneity, the higher the probability of your analysis being accurate. But are we a homogenous population of people like her?

The truth is, the general population (globally) needs a guideline on how to be a civil citizen. In other words, we need either religions or constitutions---or like in our case, both: religions are better-penetrating, and the Constitution is more relevant.

## The precedent

Now to why encounter killings are not the answer. First, an encounter is not a deterrent. Rapes haven't stopped. Those who say, 'Rapes will not stop overnight, but as more encounter killings happen, they will stop', are either delusional or do not understand psychology.

The first issue is that there is nothing to define how the police deduce who the rapist is and whether to kill the accused. This makes the situation anarchical. The basis of the action is an accusation and not a conviction. A rather questionable way of justice, leaning more towards revenge. That is emotional brownie points, and nothing more. Revenge does not serve a larger purpose, justice does.

Second---more dangerous---public approval to encounter killings will set a new (bad) precedent. The policemen will not see this approval as that for the case, but for the act of encounter killing itself. Also, based on what I learnt last night, given the lack of members in the forces and the piling up of cases, this shortcut would become a new norm unless checked. Why are judges so careful in giving out judgements? Because an exception given in a certain tough case will become a precedent, and all cases of similar characteristics will point to that exception tomorrow, and ask for it in their case as well. If you start branching out that way, you'll lose track of the branches in no time, and your system will become meaningless.

One way to handle these Hyderabad killings would be for the public to let go of this encounter killing, and not praise it or shower flowers; let the system take its course, the internal enquiries happen and there be an outcome. Publicly appreciating the encounter is asking for more of it. If you do not see a problem there, read above. Such myopia is dangerous.

The discussion had to stop there, because we were both getting late. But the point remains:

An independent, neutral system like the Judiciary is necessary. It's in our own good to keep it relevant.

## What now

But what about rape happening? What is the way forward?

This has to be a two-pronged approach. Beyond being a legal issue, rape is also an ideological issue in today's India. Changing an ideology takes decades. But amending the Constitution doesn't. Of course, if lawmakers themselves subscribe to a certain ideology, changing the law becomes difficult, but with the right amount of pressure at the right place, change is possible.

Amending the Constitution alone is one part of the story. The system must improve. We need more policemen, better police facilities, quicker courts, etc., but these are also achievable faster than a change in the mentality and ideological narratives of the society at large.

And as learned citizens, these changes are what we should work towards. Taking convenient shortcuts based on bias is suicide.

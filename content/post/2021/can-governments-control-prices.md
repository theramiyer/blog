---
title: "Can Governments Control Prices"
subtitle: "differentiating reality from eyewash"
date: 2021-03-31T19:22:39+05:30
description: |
  As Tamilnadu goes to poll on the 6th of April 2021, we take a look at one of the promises made by one of the contesting parties: control price rise. Can governments live up to such a promise made in political manifestos?
tags:
- government
- politics
- economy
- market
---

Elections are going on for five of our State Assemblies: Tamilnadu, Kerala, Assam, West Bengal and Puducherry (पुदुच्चेरि, not पुडुचेर्रि). I have not followed the elections in West Bengal, Assam or Puducherry, but have been watching the progress in Tamilnadu and Kerala. And this post is about Tamilnadu.

One of the claims that one of the contesting parties is making is about the price rise. They say that the prices have shot up, and that their party---if it comes to power---would work to regulate the prices. And I thought I would share my comments about it. I will keep it short.

{{< toc >}}

Before we proceed, let me make it clear that this post is _not political_. I limit my public discussions to governance; I do not discuss politics in public. I do not align with a specific political ideology. I do not support political parties. As such, read this post for information. Do not waste your time looking for political undertones here (there is none).

## What is price rise

Let us take a simple example. Assume that a country has 100 blocks of silver (the weight is immaterial to this example). Assume that the imaginary country also has a central bank, akin to the Reserve Bank of India (RBI). Assume that you are a citizen of this country.

The central bank of the country says that the total amount of money in the system is 100 rupees, out of which you have ten rupees (you are a rich person in this country; owning 10% of the total money). The central bank says that the total money in circulation is worth 100 blocks of silver. You are quick to calculate that the money you have is worth ten blocks of silver.

Years pass by. The bank decides to print another 100 rupees (let us not worry about the why for now). The total money in circulation is 200 rupees. But the number of silver blocks has not changed. With the money you have, now, you can buy no more than five blocks of silver.

From your perspective, the price of silver has doubled. But in reality, the _value_ of the money in your hand has halved.

What the politicians are referring to here is _inflation_, and what you saw above is an example of one of the forms of inflation. In general, inflation is more about money in your hands losing its value than commodities becoming more expensive. In other words, inflation is the reduction in purchasing power.

Of course, let me also state that this is an _oversimplified explanation_---for the sake of easier understanding---and experts will find holes in it. This explanation is not for the experts, but for the layperson like me. This is about the general principle, not technical accuracy.

## How prices work

Simple: If the demand for something is more than the supply, the prices go up. If the supply for something is more than the demand, the prices go down. One example is the global drop in crude oil prices in the last year. It became so bad that some oil-rich countries were _paying_ the buyers to buy oil. (The price of oil was in the negative.)

But does this have anything to do with inflation? Not in the short term. Let us look at it in this way; inflation increases when:

1. The cost of commodities required to make another commodity goes up. For example, if the prices of vegetables went up, the cost of food in restaurants may increase. If fuel prices went up, the cost of everything that gets transported may go up.
2. The prices go up when people are buying more than what we produce. This increases the demand, and because of short supply, the prices go up. What is interesting is that increase in people's purchasing power also causes the demand to increase, but that is healthy inflation.
3. Governments sometimes print money to increase the money in circulation or give the bottommost layer of the economy a small boost by giving money in their hands to spend. When this happens, consumption increases, and that gives a stimulus to production, which in turn needs more people to work to meet the demand, which then increases people's income, and it snowballs from there (in a good way). But this printing of money also dilutes the value of money that people have, like we saw above.

## What prices governments can control

The RBI can cut its repo rate, which may make loans cheaper.{{< sidenote "repo-rate" >}}Banks primarily lend using the depositors’ money, before deciding to borrow from the RBI, which means that a repo rate cut will not lead to a drastic reduction in the bank interest rates.{{< /sidenote >}} This could drive more loans, which can serve as a small stimulus for businesses. This adds a slight nudge to businesses, which leads to better employment and thus, money in the hands of people who need it the most. This increase in economic activity could have a good effect like increase in the production of goods and services, which increases supply. This effect can drive down the prices.

Secondly, the government can print more money, and give money in the hands of people, or the RBI can lend to banks. This will have a similar effect as above.

The government can manipulate taxes. Reducing taxes on goods or services could bring down prices, and have the opposite effect as cost push inflation.

Fourth, the government can increase its expenditure, such as starting new projects under programmes like MNREGA, which will increase employment, and sale of products and services to the government. This again, stimulates the economy.

Having said all this, none of these is as easy as it sounds, and governments are wary of taking such steps (mainly those that obsess over inflation control).

## Can the Tamilnadu Government control prices

So far, we spoke about governments in general. India has a federal system: we have State governments and the Central government.

The RBI works at the Central level. Repo rate cuts and printing money are not in the control of the State governments.

When it comes to taxes, we have the GST, which the Central government controls. State governments can propose tax cuts, but whatever they propose will affect the entire country, and so, the Central government will need to take other states into consideration when tweaking tax rates.

State governments, though, have a good control over taxes on petroleum and alcoholic drinks, since those do not come under the purview of the GST. But given that the Central government does not have a good track record of paying the State governments their due on time, will the State governments play around with what can essentially be their safety net?

Fourth, the State governments do have the ability to start projects which result in some amount of government spending. But again, that will depend on how much the State governments have with them to spend.

Historically, we have seen governments complain about the previous governments overspending, 'We do not have enough to spend at present; our primary focus will be to channelise our resources into low-expense-high-yield projects that need immediate attention.'

Also, we are more of a free market economy than a purely socialistic economy. Governments do not enjoy the control over the market that they used to, say, three decades ago. Governments can now nudge the economy, not fully control it.

## Do you vote for the same party that runs the Central government

A federal system is about co-operation between the State and the Central governments. Politicians make it a power play. This attitude of uniformity being the criteria for co-operation is detrimental to federalism. Balance of power is important to prevent concentration of power.

In other words, no, do not make uniformity _the_ criteria for your selecting one over the others. Governments need to learn to co-operate with each other despite their differences. I know, that sounds idealistic, but do not promote the attitude of 'uniformity for co-operation'; such a practice will come to bite us in the rear.

And always remember that you can question the government---State or Central. A citizen's duty does not end with casting a vote once in four or five years. One needs to constantly remain in touch with those in power, to ensure that the people's interests get priority over political agenda.

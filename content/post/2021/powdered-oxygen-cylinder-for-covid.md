---
title: "Powdered oxygen cylinder for COVID"
subtitle: from WhatsApp University, with love
aliases:
- covid-and-high-orac-foods
date: 2021-04-10T18:36:47+05:30
episode:
    spotify: "5EL0rBj5T7WdNI35OTgPe5"
    apple: "1000516677363"
    google: "MTFhMWQwY2QtODc3OS00YzE1LTllMTYtODg4MzgxMzZkZGVh"
description: |
    A message on WhatsApp University claims that adding some spices to your tea increases your body's oxygen absorption to reverse the ill effects of COVID. Let us look at it scientifically.
image: https://blogfiles.ramiyer.me/2021/images/tea-and-masala.jpg
tags:
- whatsapp university
- food
- health
- science
- covid-19
- mythbuster
---

WhatsApp is on fire these days. Another forward popped up a couple of days ago, which was a Tamil message. I have added a translation along with the message:

> \- BST HEALTH
>
> உடலில் ஆக்சிஜன் அளவு 98 - 100 க்குள் இருக்க வேண்டும் என்று சொல்லுகிறார்கள்; 43 க்கு கீழ் ஆக்சிஜன் சென்றுவிட்டால், ஆக்சிஜன் சிலிண்டர் தேவை;
>
> ORAC-Oxygen Radical Absorption Capacity என்று ஒரு கணக்கீடு உள்ளது; இதன்படி இந்த அளவுகோலில் ஆக்சிஜன் அதிகம் உள்ள பொருட்களை அவ்வப்போது நாம் சாப்பிட வேண்டும்.
>
> 1. கிராம்பு.   314446 ORAC  
> 2. பட்டை. .... 267537 ORA  
> 3. மஞ்சள்.......102700 ORA  
> 4. சீரகம்........... 76800 ORA  
> 5. துளசி..........67553 ORAC  
> 6. இஞ்சி..........28811 ORAC
>
> சரி, இவைகளைத் தினமும் எடுத்துக்கொள்ள ஏதாவது சுருக்கு வழி உள்ளதா?... அதற்கு ஒரு ரெசிபி உள்ளது! அதனை குறித்து வைத்துக்கொண்டு பின்பற்ற முயற்சி செய்யுங்கள்;
>
> 1. ஓமம் ........100 கிராம்
> 2. சோம்பு .......50 கி.
> 3. கிராம்பு ........5 கி.
> 4. பட்டை ......... 5 கி
> 5. சுக்கு ............10 கி
> 6. ஏலக்காய் .....10 கி.
>
> இவைகளை எண்ணெய் ஊற்றாமல் லேசாக வறுத்து பொடி செய்து ஒரு பாட்டிலில் அடைத்துக் கொண்டு காலை மாலை டீ போடும்போது இரண்டு பேருக்கு ஒரு ஸ்பூன் வீதம் கலந்து சாப்பிட்டால் டீ மசாலா டீ ஆக மாறும் ; 
>
> Corona வேகம் அதிகரித்துக் கொண்டே உள்ளது. நுரையீரலை சென்று தாக்கி ஆக்சிஜன் அளவை குறைத்து மரணம் அடையச் செய்யும் ஒரு கிருமி. 
>
> தயவு செய்து உங்கள் உறவுகளுக்கும் / நண்பர்களுக்கும் தவறாமல் ஷேர் செய்யுங்கள் ! 
>
> கொரோனா தொற்று வந்து செலவிடும் விரயச் செலவிற்குப் பதில் எப்போதும் வழக்கமாக உபயோகிக்கும் பொருள்களான ஓமம், சோம்பு, கிராம்பு, லவங்கப்பட்டை, சுக்கு மற்றும் ஏலக்காய், தேவையான அளவு சேர்த்து மேலே சொன்னது போல் தயார் படுத்தி காலையில் அருந்தும் டீ யில் கலந்து பருகினால் நம் உடலுக்கு தேவையான Oxygen கிடைத்து விடுமே. வரும் முன் காப்போம் கொள்கை ரீதியாக நாம் கடைபிடிக்கலாமே...
>
> இது ஒர் விழிப்புணர்வு பதிவு...

Here is the English translation:

> \- BST HEALTH
>
> They say that the level of oxygen in our body must be 98 – 100, and that you need oxygen support if it drops below 43.
>
> There is a measurement called ORAC, or, Oxygen Radical Absorption Capacity. We should consume foods high in oxygen level according to this scale:
>
> | Substance  | ORAC Score   |
> | :--------- | -----------: |
> | Cloves     |       314446 |
> | Cinnamon   |       267537 |
> | Turmeric   |       102700 |
> | Cumin      |        76800 |
> | Holy basil |        67553 |
> | Ginger     |        28811 |
>
> Okay, is there a shortcut to incorporate these into our diet? Here is the recipe! Please write it down and follow it:
>
> - 100 g of carom seeds
> - 50 g of fennel seeds
> - 5 g of cloves
> - 5 g of cinnamon
> - 10 g of dry ginger
> - 10 g of cardamom
>
> Dry roast these ingredients, powder them and store them in a glass container. Adding one teaspoon per two cups of your morning and evening tea would make your regular tea, masala tea.
>
> The rate of spread of COVID-19 is increasing. The virus spreads to the lungs, reduces its oxygen absorption capacity and kills.
>
> Please share this with your friends and family.
>
> Instead of spending a lot of money in an instant (for the treatment), if we followed the recipe above---which uses substances that we use on a regular basis---our bodies will get the necessary oxygen in our morning tea. Why not follow 'Prevention is better than cure'?
>
> This message is for awareness.

{{< apple >}}

{{< podcast >}}

Let me start by saying that like most "health tips" on WhatsApp, this is perhaps well-intended. But ORAC stands for Oxygen Radical Absorbance (not Absorption) Capacity, and this has _nothing_ to do with how much oxygen our bodies can absorb.

The premise of this forward is like linking 'irony' to 'ironing' because both the words begin with "iron".

{{< figure src="https://blogfiles.ramiyer.me/2021/images/tea-and-masala.jpg" alt="Teapot and spices" caption="Courtesy: [Aditya Joshi](https://unsplash.com/@adijoshi11)" >}}

Okay, if ORAC is not about the oxygen absorption capacity of our body, then what is it? Before we begin to understand this, we must know what metabolism, oxidation and free radicals are.

Metabolism is a process by which our body sustains life: in simple terms, our body converts sugar and fat molecules into energy, it converts food to fundamental blocks like nucleic acid, and throws away wastes generated from such processes.

Chemical processes combine reactants to make products. Some reactions produce by-products as well. Free radicals are, in simple terms, short-lived by-products. (Short-lived, because they are unstable.) In oversimplified terms, these free radicals could be "incomplete" atoms or molecules---these are looking for electrons to become stable. Free radicals could be of different types. The most common ones in human beings are _oxygen_ free radicals. These free radicals remain "free" for a fraction of a second, but their sheer number makes them dangerous. When looking for other atoms or molecules to bind with, they may end up altering our DNA, which leads to cell damage or mutation.

Our body, by design, can handle free radicals using its own enzyme systems. These are our own antioxidants. These natural antioxidants that our body creates balances the oxidant free radicals.

Wait, what does this have to do with oxygen?

That is where this gets interesting. In chemistry, we have oxidation reactions and reduction reactions. Oxidation reactions involve loss of electrons, while reduction reactions involve gain of electrons.

You see how this is going farther and farther away from coronavirus. But hang in there and read on.

In other words, an oxidant is a chemical molecule that "steals" an electron, leading to an oxidation reaction. In chemistry, we call this process an increase in the oxidation state (or “gaining of oxygen”).

For example, rusting of iron is an oxidation reaction, and spraying WD-40 on rusted iron is a reduction reaction.

Now, coming back to free radicals. When the free radicals outnumber the antioxidants in our body, our body goes into a state of _oxidative stress_. In this state, you have a much higher probability of cell damage, and at times, this leads to certain types of cancers.

Wait! I know I said cancer, but let us not conclude anything yet. _Picture abhī bākī hai._ We are not done.

Right, so, we need antioxidants in our body to balance out the free radicals. This is where antioxidant-rich foods come into the picture.

To measure the antioxidant capacity of foods, scientists developed a system of finding out the in-vitro (or, "in the glass"---in other words, "in the lab") antioxidant activity of a given food item. The measure of this activity is Oxygen Radical Absorbance Capacity. In the lab, they "challenge" a material vulnerable to oxidation (in the presence of a food item), with an oxidising agent, and see how well the food item resists oxidation of the vulnerable substance. As a simple example, let us say that you kept a thin, little sheet of iron in a test tube, placed some cloves and blew in steam mixed with atmospheric air. You then measured how well the cloves resisted the oxidation of the iron sheet. Of course, this is an oversimplified experiment---the actual experiments could be much more elaborate and use a more sophisticated method.

One important point to remember, though, is that this is a lab test. It does not necessarily translate directly to the antioxidant activity in the body (or, "in-vivo"). Measuring their in-vivo activity is not possible with the current technology (which was the reason the scientists developed the in-vitro system of measurement). Superfoodly keeps an updated list of these food items with their ORAC values.{{< sidenote superfoodly >}}ORAC Values: Antioxidant Values of Foods & Beverages ([Superfoodly](https://www.superfoodly.com/orac-values/)){{< /sidenote >}} Also, last I checked, this score is for 100 g of these substances. Which means, you would need to convert these values to your typical serving.

Upon converting the ORAC values for the quantity of these items as mentioned in the recipe (as per the data on Superfoodly, as on the date of writing this post), this is what you would get:

| Substance    | Quantity |         ORAC score | Resultant ORAC value |
| ------------ | -------- | -----------------: | -------------------: |
| Carom seeds  | 100 g     |            unknown |              unknown |
| Fennel seeds | 50 g      |            unknown |              unknwon |
| Cloves       | 5 g       |             290283 |             14514.15 |
| Cinnamon     | 5 g       |             131420 |                 6571 |
| Dry ginger   | 10 g      |              39041 |               3904.1 |
| Cardamom     | 10 g      |               2764 |                276.4 |

This gives us 25265.65 per 180 g of this powder. Assuming that the roasting of these do not vaporise the antioxidants present in the aromatic compounds of these spices, and going by that we would use 5 g of these per day, the ORAC score of the amount of this powder, per day, would be 701.824. That is the same as 100 g of raw carrots.

Or 50 g of whole grain bread.

Ironically, these foods absorb off oxygen free radicals to _prevent_ oxidation, while the primary purpose of our lungs is to _oxidise_ the haemoglobin in our blood. Am I saying that taking in antioxidant-rich foods will reduce our body's capacity to oxidise blood?

No, I am saying that the two processes are not related in that way at all. The antioxidant properties of these spices have nothing to do with our blood's ability to absorb oxygen.

> … டீ யில் கலந்து பருகினால் நம் உடலுக்கு தேவையான Oxygen கிடைத்து விடுமே.

or

> … our body will get the necessary oxygen in … tea.

No, we do not get our oxygen supply from tea. We get our oxygen supply by breathing air. A healthy level of haemoglobin in the blood will ensure availability of oxygen for cell activity. Cleaner lungs will ensure availability of larger surface areas to aid the transfer of oxygen to your red blood cells. Of course, another WhatsApp forward to vacuum clean lungs will not surprise me, but your falling for it will. Because the fact that you are reading this post means you are smarter than that.

The lungs of a person hit by COVID may get inflamed because of the infection and the body's reaction to it. This could impair the air sacs’ ability to transfer the oxygen to the blood. Or, some parts of the lungs may get filled with fluid, which will block the air from entering the air sacs, which, again, will impair the ability of the body to absorb oxygen. These, and other such factors lead to a drop in the oxygen saturation in the blood. Eating antioxidant-rich foods do not help by absorbing oxygen as a proxy. That is not how the human body works.

Also notice how the forward conveniently forgets to add % to 98 and 100, and builds the narrative that taking in spices with six-digit ORAC values will fulfil our body's oxygen needs, which is merely 98 to 100. "Oxygen cylinder in powdered form!" Flawed calculation and logic.

Am I saying that these spices will not help? No. Please make this mixture if you like, add it to your tea and enjoy it by all means. But do not let this message lead to complacence. Following COVID-appropriate behaviour, keeping an eye on symptoms, and consulting with a doctor when you feel ill, will increase your chances of survival.

Do not rely plainly on such "சுருக்கு வழிகள்" (shortcuts).

Also, share this post with your friends and family, so that they are aware. (Use the buttons below for ease.) If you have any WhatsApp messages that you would like me to validate, let me know on Twitter at [@iamramiyer](https://twitter.com/iamramiyer).

And most importantly, take care.

---
title: "COVID Vaccines"
subtitle: "in simple terms"
date: 2021-04-22T08:02:35+05:30
description: |
    Vaccines have become highly politicised. Saying anything in favour of them or against them is bound to unleash the wrath of those of the opposite pole. But this does not have to be about one or another. And in this post, we will look at it from all aspects.
tags:
- health
- science
- covid-19
- coronavirus
---

India is among the world leaders in vaccination. Also called the _Pharmacy of the World_, India is the largest manufacturer of vaccines. The public has been no different; you will find most of us wanting to get vaccinated. Polio vaccines are an example, where all children born in India get vaccinated through the extensive vaccination programmes that the government runs.

The aim of this post is to help you decide if you should go for vaccination or not. I am no one to tell you one way or another. I will tell you facts in the simplest terms possible, and you decide whether you want to get vaccinated or not.

{{< toc >}}

## The types of vaccines

Our immunity neutralises a virus using different mechanisms, which you can read about in a [previous post]({{< ref "covid-and-credibility#the-case-of-asymptomatic-transmission" >}}). A COVID-19 vaccine behaves like the COVID-19 virus in your body to trigger an immune response like the real infection would, but without causing damage to the body like the infection would. What the vaccine does defines its type:

1. A cold virus vaccine uses a weakened form of the virus to trigger an immune response. The virus either has its genetic material destroyed, or weakened so that it can replicate but not cause illness.
2. A protein vaccine uses a fragment of the virus’ protein substructure to trigger an immune response from the body. But this is often a weak response, and booster shots are more common in protein vaccines than the others.
3. Nucleic vaccines use the virus’ DNA or RNA to work. Vaccines developed by Pfizer and Moderna use a smaller strand of the RNA, called Messenger-RNA (or m-RNA) to dictate the production of antigens, which in turn triggers an immune response.
4. A viral vector also dictates the production of antigens, but uses a harmless variant of the target virus---for instance, a virus that affects pigs and not humans---but has a similar structure and triggers similar antigen production as the target. This behaviour also triggers an immune response, even though the virus cannot harm humans.

Covaxin has a full SARS-CoV-2 but with inactivated genes which disables the replication of the virus while keeping the spike proteins intact. The spike proteins try to bind with our cells and that triggers an immune response. [This New York Times article](https://www.nytimes.com/interactive/2021/health/bharat-biotech-COVID-19-vaccine.html) has a beautiful pictorial representation of the working of Covaxin.

Covisheild is the viral vector variant. This vaccine uses an adenovirus (a virus that causes flu-like illness) that affects chimpanzees, which has the same spike protein structure as the SARS-CoV-2. Covishield shares similarities with Covaxin in using the spike protein to trigger an immune response. Our immunity can then use the same identification-and-response mechanism to tackle SARS-CoV-2.

## Vaccine trials

At the end of the day, trials are a game of numbers. Clearing vaccines has serious consequences. Without going into technical details, here are the stages:

* Before anything, the scientists test the vaccine in the lab (without involving human subjects) to establish the safety of the vaccines. Without this safety data, the vaccine will not get a clearance for human trials.
* Stage 1 human trials involve about a hundred human participants, after the vaccine gets its clearance for human trials.
* Once we establish that the vacine does not cause serious harm or kill people, and that it shows immunoresponse (such as release of antibodies), a larger number of healthy participants get the dose. The primary goal at this stage is to establish safety, while continuing to test the immunoresponse. This data later aids in efficacy rating.
* This is where unhealthy participants get their shots, after we make sure the vaccines will not harm people with health issues. The number of participants is in tens or hundreds of thousands at this stage.
* Beyond this, scientists and vaccine makers may continue to test and improve the vaccines as more data becomes available.

At all these stages, scientists may use placebos, (something that looks like a vaccine but is not).

Let us now move on to answering some common questions and statements.

## Can you get infected after getting inoculated

Yes. You can also give the disease to the others if you catch the infection after you get the vaccine.

But will the vaccines make you ill? They may, but within safe limits. That said, they will not give you COVID-19.

The vaccine does not create a shield around you that prevents the virus from entering. The vaccine (or any vaccine) trains your immunity. How? Read above to know the workings of the vaccines. The Union Ministry of Health and Family Welfare claims the post-vaccination breakthrough infection rate to be around 0.02% – 0.04% (2 to 4 per 10,000).

{{< figure src="https://blogfiles.ramiyer.me/2021/images/vaccine-breakthrough-infection-india.webp" alt="Post-vaccination Breakthrough Infection (India)—Source: Ministry of Health and Family Welfare, GoI; courtesy: Press Information Bureau, GoI" caption="Post-vaccination Breakthrough Infection (India)—Source: Ministry of Health and Family Welfare, GoI; courtesy: [Press Information Bureau, GoI](https://youtu.be/xNEZllFuxWI?t=1207)">}}

When you do get infected, you are unlikely to have serious symptoms, or for your immunity overreact to the invasion.

## What about the blood clots

First of all, every vaccine has a risk associated with it, like every human activity (or inactivity). 1.5 lac people died of road accidents in 2019.{{< sidenote road-accidents >}}[India had most deaths in road accidents in 2019: Report](https://www.hindustantimes.com/mumbai-news/india-had-most-deaths-in-road-accidents-in-2019-report/story-pikRXxsS4hptNVvf6J2g9O.html){{< /sidenote >}} Considering a population of 130 crore, your chances of dying in a road accident are 112.77 in a million (or 1.13 in 10,000).

To compare, the odds of your dying by a blood clot because of a COVID-19 vaccine are utmost 4 in a million. In other words, your chances of dying of a road accident are at least 28 times more than dying by a vaccine-induced blood clot. Also, the basis of the number of blood clot cases is from the UK, not India; as of 11 April, 2021---basis a March 23 report---India had reported _no such adverse events_.{{< sidenote aiims-clot >}}"Very Rare...": AIIMS Chief On AstraZeneca Vaccine-Linked Clotting ([NDTV](https://www.ndtv.com/india-news/astrazeneca-vaccine-blood-clot-very-rare-aiims-chief-dr-randeep-guleria-on-astrazeneca-vaccine-linked-clotting-2411225)){{< /sidenote >}}

## Actor Vivek died one day after taking the vaccine

A handful of my friends asked about this when I put up a status message asking those above 45 to get vaccinated. Let us look at two aspects of this situation. First, the psychological. Celebrity deaths get widely reported, and so do speculations. In cases where the government pushes through vaccine approvals, unmindful of the legal grey areas, the effect gets amplified, because all deaths even remotely connected to the vaccination get reported. While necessary to make the government answer, this act is a double-edged sword:

Humans have a bias called _Availability Heuristic_. The brain, when trying to take a decision, takes a shortcut to immediate examples, and bases the conclusion on them. The second bias we have is the _Negative Bias_, where our memory retains negative feelings, expressions and memories for longer than the positive ones. When you combine the two, our brains take the shortest route available to the negative examples.

Second, the biological. What Vivek had was a 100% block in one of the vessels of his coronary artery. An angioplasty helps in most such cases. But as the blockage progresses, the chances of a stent helping reduce. A case of a 100% block was serious.

These blocks do not develop in a day; they develop over decades. The Health Secretary of Tamilnadu made a similar statement.{{< sidenote vivek-death >}}Actor Vivek’s cardiac arrest not linked with COVID-19 vaccine, says TN Health Secy ([The News Minute](https://www.thenewsminute.com/article/actor-vivek-s-cardiac-arrest-not-linked-covid-19-vaccine-says-tn-health-secy-147278)){{< /sidenote >}}

Talk to your doctor about your apprehension. They can clear your queries and guide you.

## Should you take Covaxin or Covishield

Before getting my parents vaccinated, I got an opinion from two doctors (the second, to convince my mother that my father would continue to be fine after the vaccination)---one from a DNB in General Medicine and Medical Oncology, and the second a DM in Cardiology. They both asked us to go with either. They both take different routes, but arrive at the same destination. They are both highly efficacious and safe.

## Should you wait for the m-RNA vaccines

I was of the impression that an m-RNA vaccine is safer than the dead-virus ones.{{< sidenote dead-virus >}}I realise that "dead virus" is perhaps a misnomer, given that viruses are non-living.{{< /sidenote >}} But having read about the vaccines and listened to eminent medical professionals talk about them, to me, it does not look like the m-RNA vaccines are better enough  to be worth waiting for.

The m-RNA vaccine technology is new. We do not know the long-term effects of these. Not that these vaccines are bad in any way, but there is a lot of unknown. Healthy, young individuals, who are ready to cope with potential long-term adverse effects should choose these for humanity, but as of now, nothing can conclusively say that these are better or worse than the dead virus vaccines.

## Go, get your vaccine

When is a good time to get vaccinated? NOW. (Or _yesterday_, like my American colleagues sometimes say.) I am a sceptic myself. But I am pro-vaccine, because I am a sceptic in the scientific sense. I do not believe in hyper-anything. I detest the hyper-nationalism shown in case of vaccines. I was sceptic of the government not answering valid questions by medical professionals---like how I would go to a lawyer and not a dramatist for legal advice, I listen to medicos and not bureaucrats or politicians on medical topics. "Trusting" political office-bearers to be right with these is a matter of probability.

The basis of science is evidence, not faith.

And science says that the vaccines are safe, and that they work. The caseload of today has overwhelmed our health infrastructure. And this is not merely the government hospitals, but all hospitals, pharma and health instrument manufacturers and suppliers, and everyone in the medical system, including the health insurance providers. To add to it, people are behaving so irresponsibly that despite wearing masks and maintaining distance, you have a chance of catching the virus because of others’ carelessness and indifference.

A vaccine is your best shot at safety and survival in case of an infection.

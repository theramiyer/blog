---
title: "Is COVID-19 Winning"
subtitle: "The question should be, are we letting it?"
date: 2021-05-16T09:48:27+05:30
description: |
    Here is this week's rundown of my learnings and some thoughts on what we need. This post is an apolitical take on the need of the hour. Along with information about the pandemic, this we also look at the new learnings of this week.
tags:
- covid-19
- health
- governance
- India
---

Today is Sunday, and time for me to share what I have learnt about the disease. Disclaimer: I am an engineer, not a medical professional. What I am sharing here is for informational purposes, not health or medical advice. If you have specific queries, please talk to your (or a) doctor. And do not trust WhatsApp University.

{{< toc >}}

## A moment for my uncle

The virus hit too close to home. I lost my uncle; my father lost a brother. This is the third COVID-related death in the family. The news devastated us. We could not go to Ahmedabad to hold our cousins as they broke down. We could not be there to find medicines and other supplies for the treatment. My father did not even get to see his brother's mortal remains. Not even virtually.

The brothers and their families are mourning Uncle's passing in their respective homes, sharing consoling words over digitised lines. I understand his family's anxiety, their struggle to find what was necessary, and their frantic calls for help, most of which echoed within their skulls, without a voice.

Of course, this makes me angry. At the "system" led by the Executive, which we have magnanimously accepted and nurtured over the decades. What has changed today is that we now defend it.

We need to change this situation. We need to win over the pandemic. And for that, we must start by looking in the mirror.

Let us look at the good and the bad about our handling of the pandemic, more specifically, this second wave. Let us start by looking at some of the aspects of how some of our States have handled the pandemic.

Before going any further, know that this is not political, because:

I do not care about politics.

When I say a State handled the situation better, the statement is plainly that. Does it mean that one State is better than the others? No. Does it mean that the States that I did not mention failed? No. (Who am I to certify, anyway?) Does it mean that the Central government is not doing nearly enough? I will never imply anything; I will state it, because:

I do not care about politics.

If criticising a leader---any leader---disturbs you, stop reading this right now. I have warned you.

## On "alternative" treatments

We have been in this situation for well over a year. If we still do not understand that real, replicable, observable, provable science works, the lessons will get harder.

Nothing is wrong with using alternative treatments, provided we put them through the same scrutiny as conventional medicine. The problem with _Patanjali_ was that they took shortcut{{< sidenote coronil-1 >}}Just 45 healthy cases, interim results, no peer review ⁠— how Patanjali found ‘Covid cure’ ([ThePrint](https://theprint.in/health/just-45-healthy-cases-interim-results-no-peer-review-%E2%81%A0-how-patanjali-found-covid-cure/447540/)){{< /sidenote >}} after shortcut,{{< sidenote patanjali-2 >}}WHO clarifies it hasn't reviewed or certified Coronil for Covid-19 treatment ([The Hindu BusinessLine](https://www.thehindubusinessline.com/news/world/who-clarifies-it-hasnt-reviewed-or-certified-coronil-for-covid-19-treatment/article33893901.ece)){{< /sidenote >}} exploited people's sentiments, and were generous about spewing misinformation in the name of science to sell goods worth ₹250 crore ($34.12 million) _in four months_.{{< sidenote patanjali-revenue >}}Patanjali sold 25 lakh Coronil kits worth Rs 250 crore in 4 months ([India Today](https://www.indiatoday.in/india/story/patanjali-sold-25-lakh-coronil-kits-worth-rs-250-crore-in-4-months-1737229-2020-11-02)){{< /sidenote >}} Does it mean that their product does not work? No. It means that there is not enough evidence that the product works. In the world of medical science, a trial involves more than a hundred participants and takes into account hundreds of variables.

I work for a company one of whose businesses is pharmaceuticals. The GMP certificate _Patanjali_ talks about, is in reality, a set of provisions, aimed to ensure the safety of food, medicines, supplements, etc. A GMP certificate says nothing about the efficacy of a drug---it merely says that the said product has been manufactured in a safe environment based on a set of criteria.

Lesson: When someone tells you that their product has received X certification, look in the details.

The company ran a flawed drug study; the sample size was too small, thereby not representing a diverse human population.

States like Tamilnadu are using _Siddha_ to treat slightly symptomatic patients. The treatment may have worked for some, may not have worked for others. The point is, none of these treatments have been _clinically_ shown to kill the virus. And "clinically" does not mean they must follow the same techniques as the "allopathic" school; all it means is that you must run controlled experiments that can prove beyond reasonable doubt that your solution works.

But what about the hundreds of thousands of people whose disease these products cured? Well, the recovery rate of COVID-19 is over 98%. Did the treatment methods show a better recovery rate? Or at least, did they make the disease less symptomatic? No. Or let us say that their claims did not stand scientific scrutiny. Also, if you are someone that says, 'Don't go by science; science did not help Mr X discover the sun', then you probably do not even need Coronil. My suggestion: Do not waste your money.

The point is, the recovery rate is better with conventional medicine  than without. The percentage of people getting a serious disease is about 15%. Without hospitalisation and medical treatment, a large chunk of them would not survive. If an alternative medicine proves itself better than conventional medicine, go for it. Else, choose conventional medicine.

{{< figure src="https://blogfiles.ramiyer.me/2021/images/covid-19-recovery-india-2021-05-15.webp" alt="COVID-19 recovery rate in India (15 May 2021) \| Source: Worldometer" caption="COVID-19 recovery rate in India (15 May 2021) \| Source: Worldometer" >}}

Remember: asking for proof is not rejection of claim. Emotional handling of such situations helps no one in the long run.

## On shortages

I do not know about Uttar Pradesh and Delhi, but Bengaluru has been facing blow after blow over the last month. We have a shortage of beds, medicines and oxygen cylinders. I do not mean that those in need of bed are not getting a bed, but that they face long delays; some of them do not make it. What saddens me also is that people are having to look for oxygen cylinders on their own. I personally called about sixty suppliers for a friend's mother-in-law; not one had a positive response. If Bengaluru is having such a crisis, I find it hard to imagine the state of places where medical facilities are not even as good as Bengaluru.

What helps here is a social justice system, like how we have in Tamilnadu and Kerala. I have friends and family in both the States, and they can tell you about how well the States manage public health. Are they perfect? Far from it—in fact, they are also faltering at the moment. Are they better at healthcare than most other States in the country? Yes.

Secondly, the problem of oxygen is primarily logistical.{{< sidenote o2 >}}Medical oxygen: Why is it vital and does India produce enough? ([WION](https://www.wionews.com/india-news/medical-oxygen-why-it-is-lacking-and-how-it-is-made-381323)){{< /sidenote >}} We have States that produce surplus oxygen, we have refineries, the metal extraction industry and what not, who have the capacity to generate medical oxygen. How we get it to the needy is the challenge. In general, most States and the Centre have done abysmal work in managing the actual crisis; they instead resorted to managing the headlines.

I get their point: They do not want to sap the morale of people by letting them see the grim picture, but does that mean we go into denial? A little bit of humility and a lot of honesty would have worked much better. Accept it: one did nothing for "seventy years"; another's promise of fixing everything has not worked in the last seven years. Our medical infrastructure is nowhere near enough. Ask any globally renowned public health expert. Cut the bull crap of the entire world conspiring against us already.

We need to learn true leadership. The first chapter in my book says, "Stop hiding behind shiny headlines." I see a redacted piece that reads something like "Grow a pair of █████."

The second issue with denying shortages is that the countries that want to help us, get the wrong message. And people are not stupid; when they look at the large number of friend nations sending help (which, ironically, the same media outlets that claim 'no shortage' run news shows about), they will add one and one. The whole concept of this narrative building has serious flaws:

Citizens would ask, 'If we have no shortage, why are friend nations sending in so much aid?' The friend nations would ask, 'If India does not have shortages, why are we sending aid to India? Can we redirect them to other countries that need them?'

People who say that India has never sought help from any other nation since 2005 are right. But we are in an unprecedented situation. If friend nations do not help each other, how will we survive as humanity? Do we want to go down as that forgotten nation that perished because of pride? That is not an example a _Viśvagurū_ sets.

India is more than capable of helping other friend nations. At the moment, though, we need to tend our wounds. The sooner we do it, the sooner we will stand back up on our feet.

I get it: China is flexing its muscles and showing to other friend nations that India is going through a rough patch (to put it nicely). But at this moment, denying the issue will make China's narrative stronger, and make people question India's integrity. In time, when we are back on our feet, we can counter this narrative without much effort, because India has substance. We must trust our capability and do the right thing.

## Need of the hour

Sanity.

But that will take time. To get there, we need to handle the following:

1. Treat the ill
2. Stop the spread
3. Vaccinate

### Treat the ill

The first two points mentioned above are of the topmost priority today. First, those who are ill need treatment. They need to recover, get back home and reunite with their families. We need medicines, oxygen, beds, healthcare professionals, and medical supplies.

We as a nation have not concentrated enough on public health. Some States are better than the others, but we are nowhere near the mark.

Once those in need of treatment get medical help, they will be back to being productive, and happier.

### Stop the spread

Perhaps before we do the previous, we must also stop the spread. Here are the reasons we want to concentrate on that:

- If the number of new infections does not decrease, the health system will continue to operate under stress. This does a lot of harm than good: the professionals get fatigued, because seeing people suffer and die is taxing. Doctors and nurses who have seen people recover and go home smiling are now seeing deaths and darkness. They break down. They may not show it on camera or on your face, but when they catch a moment of solitude, they break down.
- Every infection is an opportunity for millions of mutations. Natural selection does control the process, but to do that is lead to, say, one successful, significant mutation.

How can we stop the spread? By understanding how the infection spreads. It spreads when two people or more, at least one of them infected, meet in close physical proximity or a closed environment, without a mask. The infected exhale, talk, sing, sneeze or cough, releasing viable viral aerosol. The healthy inhale this and get infected.

Everybody showing symptoms has the disease, vaccines it does not mean everyone who has the disease shows symptoms. The point of measuring temperature at entrances of malls, cinemas and hospitals is beyond my grasp—this does not account for the pre-symptomatic individuals. For all we know, millions of pre-symptomatic people are going around assuming that they are fine, all the while he the infection around:

What do we do about this?

Wear a mask. Specifically, those who are spreading the virus must wear masks, so that the virus does not escape their person to infect others.

We must all assume that we are all infected. All the time. That is the best way to cut the spread.

Avoid super enough-spreaders right now like rallies, religious gatherings, and other mass gatherings. No matter how protected you are, the sheer density of the aerosol viral particles is enough to infect hundreds of thousands. No amount of masking and wearing shields will help you.

Unless you have access to a spacesuit, avoid these gatherings.

### Vaccinate

I had arguments with friends about whether I was right in suggesting people to get vaccinated. Here is why I was right: Scientific evidence says that the vaccines we have today are safe and efficacious. The vaccines are safe enough for non-medical professionals to suggest going for it. If you have queries, you now know enough to ask your doctor about it.

Vaccine hesitency is bad for you, and for others. Vaccination helps us get to herd immunity faster. A burnout also leads to herd immunity, but at the rate of even 1% fatality, the total fatality we would have seen would be much more than 1.3 crore people. We are not willing to pay that price as a population. The best way to herd immunity is through vaccination, because vaccines are much safer than getting the actual disease.

Burnout
: The state of a communicable disease when it has infected an entire population, to the point where it does not find a new healthy individual who is vulnerable.

But vaccines are in short supply. Which brings us to two aspects:

1. Importing vaccines
2. Relaxing Intellectual Property limitations

We will talk about them below.

Also, yes, I was hesitant about Covaxin, when the government pushed through to approve the vaccine without the third stage of trials. And I still stand by that the government should have been transparent about the vaccine. The vaccine still does not have enough data. But based on the data available now, we know that Covaxin is safe, and highly efficacious.

## On testing and lockdowns

Do lockdowns help? In short term, yes; long term, no. The reason it does not help in the long run is because it puts a lot of pressure on the economy. But helps us take proactive steps abstract term for most of us. What is tangible is how much we spend versus how much we earn. If our earnings seem insufficient even though a year ago we found it and is And lockdowns increase inflation. How? Because the supply goes down, while the demand either stays the same or goes up.

But lockdowns are the way go in our country, because we need someone to impose restrictions, and then walk around with a stick and smack us to make us comply. I had "checkpost duty" the last year; despite a complete lockdown, we found people giving reasons as ridiculous as 'Going to a friend's place to give him some sweets that Mum made.'

But for how long can lockdowns go?

Relaxing lockdowns is tricky. Experts look at two parameters: the number of positive cases and the test positivity rate.

_Test Positivity Rate_ is the percentage of tests detecting the virus versus the total number of tests done. In India, the test positivity rate is about 17%. This means that one in six tests is positive. Test positivity rate is a indicator because it shows a high value when a high number of cases are positive, as well as when way the total number of tests done is low.

{{< figure src="https://blogfiles.ramiyer.me/2021/images/india-tpr-15-may-2021.png" alt="Test Positivity Rate, India \| Source: covid19india.org" caption="Test Positivity Rate, India \| Source: covid19india.org" >}}

A high Test Positivity Rate means that we are missing out a lot of asymptomatic infections, which we would have known had we tested more. It says that the number of actual positive cases is more than we are seeing.

In general, a good time to relax a lockdown is when the available medical resources can handle the case load without stretching itself, and the positivity rate is low. The ideal is zero, of course.

## On medicines that work

Again, this is not medical advice. But here is what I have gathered from professionals:

1. You do not need medication in most cases of the symptomatic disease, other than, say, Paracetamol. But, the doctors (at least in Bengaluru) have a protocol which includes a kit of medicines that are safe, which they give to all symptomatic individuals. If your doctor has prescribed a set of medicines, take them.
2. Remdesivir works in mild infections. Again, your doctor decides what is mild and what is severe. If your doctor has prescribed Remdesivir, the hospital will give it to you as an injection. Otherwise, do not buy it to administer it to yourself.
3. Steroids work in severe cases. Again, I cannot stress this enough: The In professionals will decide which steroid to give to you and how much. From personal experience, steroids change too much in your body, including inducing temporary or permanent diabetes. They cause a serious imbalance of physical and emotional parameters. Your doctor knows enough to weigh the risks and benefits. If your doctor prescribes a steroid, you will get it under a medical setting. Do not take it on your own, or it could lead to complications that could be even life-threatening.
4. Ivermectin, Hydroxychloroquine, etc. have shown nearly no evidence of working, and most medical bodies do not prescribe it as part of the treatment protocol.{{< sidenote ivermectin >}}Ivermectin for COVID-19 treatment: WHO recommends against use of the drug ([TimesNow News](https://www.timesnownews.com/india/article/ivermectin-for-covid-19-treatment-who-recommends-against-use-of-the-drug/755560)){{< /sidenote >}}{{< sidenote ivermecting-tli >}}Fact Check: Did ICMR Suggest 'Ivermectin' Against COVID-19? ([The Logical Indian](https://thelogicalindian.com/fact-check/indian-council-of-medical-research-icmr-guidelines-ivermectin-drug-covid-19-23705)){{< /sidenote >}} Experts are working on the validity of the claims that such medicines work. Some doctors may decide to give you these medicines, if they see any potential benefit. Leave it to the doctor treating you.
5. Antibiotics do not work against the virus. If not already obvious, antivirals work this is not the end work against bacteria. (And not all antivirals work against all viruses.) Doctors prescribe antibiotics in case of symptomatic COVID-19 for a reason, which has nothing to do with treating COVID-19 itself. If your doctor prescribes an antibiotic, take it; do not take one on your own.
6. None of the food items "increase" oxygen. I saw a message circulating about high ORAC foods, which builds on a flawed premise, and a misreading and misunderstanding of the term, _Oxygen Radical Absorbance Capacity_. [I wrote a post about it]({{< ref "powdered-oxygen-cylinder-for-covid" >}}), which you can read for more details. You need an oxygen cylinder or an oxygen concentrator to deliver medical oxygen to your lungs. Ignore the self-styled _bābās_ who say your lungs are your oxygen cylinders and that the atmosphere has trillions of tonnes of oxygen which you can inhale if you know to breathe properly. They are, at best, insensitive to, and ignorant about the real issue. And I will not comment on their agendas.
7. Remember, the Internet is not your doctor; it cannot give you _contextual_ medical advice.

## On mutations

Virologists say a virus that has RNA as its genetic material makes a lot of "mistakes" when replicating (unlike the more stable replications in case of the DNA). Mutations happen when the genetic material results in the virus gaining or losing properties or abilities. This is what makes variants.

Natural selection lets the variants better suited to the environment survive, while the others perish. Think of this as the nature's trial-and-error path to "betterment".

The more times the virus tries different variations, the more the better-adapted variants will emerge. Mutation can bring out deadlier or more infectious variants of the virus. The bigger challenge is when the changes to the virus helps it evade immunity.

Like we read before, _every infection_ gives the virus billions of opportunities to mutate. Reducing the number of chances it gets to mutate is important. One way to do that is to reduce the number of infections.

By preventing each infection, we reduce billions of chances for the virus to mutate. If you ask me, this should be our top priority.

## On relaxation of intellectual property restrictions

We must ramp up vaccination now, so that we flatten the curve further, in the following months. At the same time, we are running short of vaccines. One of the claims made was that removing the restriction on the intellectual property will help us produce more vaccines. Bill Gates made a statement that invited ire of the general public.

But he has a point:

Relaxing intellectual property restrictions will free up the information available to produce vaccines. But making vaccines is not like making bread. Vaccine production is a complicated process. Availability of information is merely one of the requirements. We need the machines, raw materials and the production capacity---including the factories, trained human resources, etc.

The best shot we have at getting enough vaccines right now is importing them. I know, that goes directly against _Ātmanirbhar Bhārat_. But we do not want to face [what we faced with ventilators]({{< ref "transparency-on-ventilator" >}}).

The relaxation we need is in the regulations for the medical supplies coming in from other countries.{{< sidenote customs >}}India's faltering vaccination drive and supply hiccups in fight against Covid ([ThePrint](https://www.youtube.com/watch?v=viuB_i7ss5k&t=292s)){{< /sidenote >}} We need to speed up the process of reviewing data about vaccines, consulting with experts and see if the vaccines can get clearance for use.

## Investment on health

I have long argued that we do not want freebies from the government; [we want the necessary infrastructure]({{< ref "are-government-freebies-bad" >}}). The Indian government has failed in the health infrastructure area.

1. We do not have enough medical and paramedical professionals.{{< sidenote dpratio >}}Doctor-patient ratio in India less than WHO-prescribed norm of 1:1000: Govt ([The Economic Times](https://health.economictimes.indiatimes.com/news/industry/doctor-patient-ratio-in-india-less-than-who-prescribed-norm-of-11000-govt/72135237)){{< /sidenote >}}
2. We do not have enough of medical resources.{{< sidenote med-res >}}India’s public health system in crisis: Too many patients, not enough doctors ([Hindustan Times](https://www.hindustantimes.com/india-news/public-health-system-in-crisis-too-many-patients-not-enough-doctors/story-39XAtFSWGfO0e4qRKcd8fO.html)){{< /sidenote >}}
3. Healthcare workers have had to work without the right protective gear, some still are.
4. People hare having to hunt for medical oxygen and drugs like Remdesivir. The visuals of people dying of lack of oxygen and reading about how doctors are having to lie about oxygen supply without an option is disturbing.

I am in favour of India having sent vaccines to other countries that needed them. But what I did not like was that India had not placed enough orders in time to help our own people when in need. Had the government not foreseen this issue? Were we not listening to the health experts, or were the health experts quiet?

## What more do we need

First of all, experts say that we are not doing enough genomic sequencing. This process is about looking at the signature of the virus’ genetic makeup. How does this help? It helps us take proactive steps. When you look at the genomic sequence of the samples we collect for RAT and RT-PCR by sending them to labs that have these machines, you get to know if the virus has changed and how.

We get to know which variants are spreading where. We get to know if we are seeing any new variants. We can learn about the traits of the new variants and take necessary actions. More importantly, we will be able to see if our vaccines will be effective against these strains. We will know about the higher ability to spread, or higher mortality, etc. These are critical pieces of information that will help us stay on top of the issue.

Next is ramping up vaccination. As of yesterday (15 May 2021), we had administered 18.15 crore doses of vaccine. A total of 4.04 crore people have received both the doses of vaccine.{{< sidenote covid-vaccine-rate >}}India COVID-19 Vaccine Tracker ([Geographic Insights, Harvard University](https://geographicinsights.iq.harvard.edu/IndiaVaccine)){{< /sidenote >}} It took us precisely four months to get here. Of course, the rate is not linear, but even then, it would take us months to get to a point where everyone has received at least their first dose of vaccine.

We need to ramp this up because the more we delay, the more people will get infected. The more the infections, the higher the chances of mutation. The higher the chances of mutation, the higher the chances of getting a variant that evades the vaccination, like how the South African variant evades the AstraZenica vaccine (Covishield in India). Oh, that does not mean it does not evade Covaxin; we do not have enough data about Covaxin to conclude one way or the other.

## The second wave (and the waves)

Typically, a "wave" refers to the increase and decrease in the number of cases in a pandemic, over a period of time. What causes these waves is unclear. But we know about some of the contributing factors.

Mutation cause newer waves. One of the possible explanations for the wave in India is the B.1.617 variant. Other factors include human interactions, interactions with animals where the animals are the vectors for the disease, seasonal changes, etc.

{{< figure src="https://blogfiles.ramiyer.me/2021/images/covid-waves-india.png" alt="COVID-19 waves in India \| Source: covid19india.org" caption="COVID-19 waves in India \| Source: covid19india.org" >}}

In our case, events like weddings and other gatherings helped spread the disease. The major events were the _Kumbh Mela_ and the massive election rallies. People from across the country went for the _Kumbh Mela_, got infected and took the disease back to their respective villages.{{< sidenote kumbh >}}In India’s surge, a religious gathering attended by millions helped the virus spread ([The Washington Post](https://www.washingtonpost.com/world/2021/05/08/india-coronavirus-kumbh-mela/)){{< /sidenote >}} The elections were the other super-spreaders; the Election Commission of India attracted a fair share of criticism from high courts in the country.

In general, scientists have accused the Central Government of not heeding the warning of the second wave.{{< sidenote second-wave >}}Lancet Editorial Slams Modi Government for Ignoring Second Wave Warnings ([The Wire Science](https://science.thewire.in/health/lancet-editorial-slams-modi-government-for-ignoring-second-wave-warnings/)){{< /sidenote >}}

Now, the experts warn of a third wave.

Where we went wrong was in prematurely declaring victory over the pandemic. When we did that, and allowed these super-spreader events to happen, we gave a general idea to the public that all was well. All was not well. Virus spread is exponential, determined by the R-number. Even if you think that one person infects 2 people, the infection would go from 2 to 4 to 8 to 16 to 32 to 64 to 128 to 256 to 512 to 1024 to 2048---in other words, over 1000 times in ten steps.

Instead, when our case count was low, we should have concentrated on vaccination. We should have started putting together the necessary infrastructure in place, like Mumbai did.{{< sidenote mumbai-model >}}Mumbai civic body installs 15 LMO tanks in 40 days ([The Hindustan Times](https://www.hindustantimes.com/cities/mumbai-news/mumbai-civic-body-installs-15-lmo-tanks-in-40-days-101620327293482.html)){{< /sidenote >}} We are late, but this is not the end. All we need is for political parties to stop this blame-game, get together, sit down and fight this pandemic (like Tamilnadu is doing)---more importantly, not be a hindrance to the work that _is_ happening. They have all the time in the world to fight for seats and popularity once the pandemic is behind us.

And for goodness’ sake, please stop saying that the coronavirus has the right to live; it does not. A virus is technically non-living. Read some science before making public statements.

## In summary

While we are in a bad situation, we as people of India have potential. Following three simple rules---masking right, maintaining distance and avoiding crowds, and maintaining personal hygiene---will take us a long way towards defeating the pandemic.

- Lockdowns work, but not for long.
- Vaccination is the long-term solution. We need our governments to ramp up the drive.
- Alternative treatments have not shown conclusive evidence of working. If you are ill, talk to your doctor; get medical help.
- We are not testing enough; do not let the numbers fool you. Our actual case count could be much higher than we know.
- Do not self-medicate. Self-medication without professional medical care is dangerous. You might do yourself more harm than good.
- Mutations could be dangerous. The key is to control the spread so the virus gets less chance to mutate.
- Producing vaccines will take time. We cannot be fully self-reliant right now. We will need to import vaccines. Relaxing intellectual property restrictions on vaccines will not magically increase their availability.
- Our governments must learn to build a robust health infrastructure. Such crises bring out the faults in the system. Our governments---both State and Central---are responsible for this. While the Central Government can say that Health is a State subject, invoking the Disaster Management Act makes Health the Centre's responsibility.
- We must observe the virus’ mutations and variants, see which need immediate attention. Observing the trends also help us project what we will need soon, and help us prevent a catastrophe.

Having said all this, remember to trust your body to handle the infection if you get it. Also remember to be responsible and not give the infection to others. Like we keep saying, not everyone will be able to handle the disease like you probably can.

Take care.

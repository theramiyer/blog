---
title: "The Organic Utopia (Part IV)"
subtitle: "getting a perspective"
date: 2021-03-09
description: |
  Organic farming prohibits methods like food irradiation and genetic modification. Why? Or if it were not prohibited, why would we use these methods? We look at some of those points in this piece.
tags:
- food
- farming
- science
- development
---

Previously, in this series, we addressed the merits of organic farming (and looked at how much merit they carry in reality). But there also are some scientific methods that help with storing, preserving and serving food to the end consumer. We look at those in this piece.

This is the fourth of the five-part series on organic food. To jump to any topic on the subject, use the list below:

- [The traditional farming methods]({{< ref "the-organic-utopia-i#the-traditional-farming-methods" >}})
- [How food works]({{< ref "the-organic-utopia-i#how-food-works" >}})
- [The case of reduced nutrition]({{< ref "the-organic-utopia-i#the-case-of-reduced-nutrition" >}})
- [Enter: Organic Farming]({{< ref "the-organic-utopia-i#enter-organic-farming" >}})
- [The sustainability argument]({{< ref "the-organic-utopia-i#the-sustainability-argument" >}})
- [Customer benefits]({{< ref "the-organic-utopia-i#customer-benefits" >}})
- [It isn't as simple]({{< ref "the-organic-utopia-i#it-isn-t-as-simple" >}})
- [Organic tastes better]({{< ref "the-organic-utopia-ii#organic-tastes-better" >}})
- [Organic has lower pesticide residue]({{< ref "the-organic-utopia-ii#organic-has-lower-pesticide-residue" >}})
- [Organic has higher nutritional value]({{< ref "the-organic-utopia-ii#organic-has-higher-nutritional-value" >}})
- [Organic is more eco-friendly]({{< ref "the-organic-utopia-iii#organic-is-more-eco-friendly" >}})
- [Genetic Modification](#genetic-modification)
- [Food irradiation](#food-irradiation)
- [Does organic have no real merits](#does-organic-have-no-real-merits)
- [What does it mean for us, the consumers](#what-does-it-mean-for-us-the-consumers)
- [Way forward in sustainability]({{< ref "the-organic-utopia-v#way-forward-in-sustainability" >}})
- [When to buy organic]({{< ref "the-organic-utopia-v#when-to-buy-organic" >}})
- [When not to worry about organic]({{< ref "the-organic-utopia-v#when-not-to-worry-about-organic" >}})
- [What about the reduced nutrition]({{< ref "the-organic-utopia-v#what-about-reduced-nutrition" >}})
- [Summing up]({{< ref "the-organic-utopia-v#summing-up" >}})

To jump to the parts themselves, use the links below:

- [Part I: Making a case for organic food]({{< ref "the-organic-utopia-i" >}})
- [Part II: Reviewing the claims]({{< ref "the-organic-utopia-ii" >}})
- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

## Genetic modification

When we talk genetic modification or food irradiation, we think of giving plants superpowers. What if, some day, these powers came to bite us?

The reality is a little less dramatic, though. Genetic modification is not about giving wings to plants, but something more basic, like making them capable of protecting themselves. To be clear genetic modification in one form or another has been part of agriculture for hundreds of years if not thousands. One of the processes has been artificial selection, which we have been doing to crops like corn and banana for centuries. We have other processes like cross breeding of plants, which also we’ve done for a long time. And organic does not specifically disallow those practices. What it does disallow is anything done in a lab.

In reality, genetic modification is sometimes a better alternative to pesticides. How? Because we can genetically engineer seeds to have genes that make the plant pest resistant, for example. This way, you would not have to spray pesticides on the crop. If you do not spray pesticides—targeted or otherwise—you are protecting the biodiversity in that area, removing chances of pesticides residue and ultimately, making the food less dangerous.

Does genetic engineering mean adding properties of, say, soap within the genes of, say, rice? Far from it. Genetic modification, like one of the videos I watched on the subject said, is perhaps a misnomer. This is not about making rice soapy. Genetic modification identifies a specific gene from an organism (a gene that gives it a strength). The gene is then---in oversimplified terms---placed within the cells of a specific seed. The seed then gets this strength.

But this process is not as simple as it sounds. The seed would need to go through cycles of growth: grow into a plant, make the fruit, and thus the seeds, and then repeat the process over and over. The scientists test the seeds after every stage to look for changes in its genes, and ensure it gives the intended results and nothing more. This process often takes years of testing and development.

From an ecological standpoint, this drastically reduces the amount of toxins that go into the environment.

## Food irradiation

We will slightly brush upon this topic, because while this is interesting, its implications are not all that good in the end. And no, this has nothing to do with human beings turning Hulk.

Food irradiation is a regular practice today. The scientists use Gamma rays from elements like Cobalt-60 and Caesium-137, or electron beams or X-rays to irradiate food. This process kills pathogens in the food and helps preserve the food better, while avoiding diseases. There have been cases of spices containing high levels of pathogen like Salmonella and E. Coli. Irradiation would kill these pathogen.

These are ionising radiations (well within limits tolerable to humans), applied to food, which destroy the DNA of the pathogen and neutralise them.

Does the radiation stay in the food? No. Does it make the food radioactive? No. The intensity is too low to be able to do anything like that. The FDA, the WHO, the CDC have all evaluated the safety of irradiation and endorse it. In India, our own BARC (Bhabha Atomic Research Centre, not the TV rating agency) endorses food irradiation, and the FSSAI has guidelines for it.

But this has its downsides. Irradiation would certainly remove pathogens from the food, but in the act, increase its shelf life longer than the food takes to lose its nutrient value. It also would mask any unsanitary conditions in which the food processing happened.

## Does organic have no real merits

Organic farming does have its benefits. For instance, the concept of cover crops helps with soil erosion. We have all learnt that the release of nitrogen from manure is a slow process. This means that the nitrogen run-off from fields would be rather low, which would not harm water bodies with algal blooms.

Antibiotic-resistant bacteria is another major concern, and organic wins here hands down.

Also, if you look at crop rotation from a broader perspective, it does not allow any particular pest to become used to the environment. No single pest would stay on for the presence of food, because it will not find a perennial supply---the crop keeps changing.

But as we saw above, organic has its downsides as well. It cannot, given the state of agriculture today, support the scale.

## What does it mean for us, the consumers

I agree with “Sadhguru” on this: There is no such thing as "organic" farming. But not in the way he probably meant it. Organic or no organic is unnecessary polarisation; a dirty one at that, because it appeals to the basic instincts,{{< sidenote bas-ins >}}Nothing inherently wrong with basic instincts themselves, but I would be wary of anyone appealing to them, because they make manipulation almost effortless.{{< /sidenote >}} like buying organic "feels" right. Science does not work like that. Science works on evidence, facts and figures.

The point should not be "organic or not organic", but "whatever works best". The problem is, how do you define "best"? One way forward is analysing the cost-to-benefit ratio. And cost does not mean economic cost alone, but ecological, ethical, and so on.

Take for example the use of fertilisers. In conventional cultivation, farmers have the option of "spoon feeding" nutrients, wherein they add water-soluble fertilisers in the irrigation water on an as-needed basis. Doing this in the organic system is expensive.

Another example could be of the environmental cost of running a greenhouse farm. While it makes some fruits and vegetables available off season, it consumes enormous amounts of energy, which happens to be fossil-fuel-generated. This is not what ecological ethics stand for.

Organic has its ethical benefits, but also has ethical pitfalls. For example, the system allows a farmer to treat his cattle with antibiotic if the animals fall ill. But once treated with antibiotics, the farmer cannot stamp the milk produced by the treated animal as organic. What would the farmer do, forgo his organic label or let the animal suffer? Or worse, lose his integrity, treat the animal with antibiotic and continue to label its milk as organic, until someone finds out?

This whole "organic" scene also introduces complacence. For example, I hear people in my own extended family make statements like, 'Oh, don't give him sugar, he's diabetic. Give him jaggery instead.' Why? Jaggery is natural, perhaps even branded organic. But, jaggery or sugar, in the end, they are both chemically sugar, which will break down into glucose and show up in the blood. Jaggery may have its health benefits in the form of micronutrients, but is as harmful to a diabetic as refined sugar. Also, in case it still is unclear, organic sugar is as bad as regular sugar. Organic does not magically make anything unhealthy healthy.

The tendency also is to think that organic is natural. In most cases, it may be. In reality, "organic" is a marketing term. Natural means that the food does not contain artificial colours or flavours, or preservatives, even. But the materials used to make the product may not be of organic origin. The same is true the other way around as well.

Another example for why organic does not equate to ethical is a point I made in [_Food and Sustainability_]({{< ref "food-and-sustainability" >}}): eating organic meat. Organic meat is as harsh on the environment as regular meat, if not more.{{< sidenote eco-columbia >}}_Is Organic Food Really Better for the Environment?_ ([Earth Institute, Columbia University](https://blogs.ei.columbia.edu/2019/10/22/organic-food-better-environment/)){{< /sidenote >}} Again, no, I am not promoting vegetarianism (read the post, and you will know), but merely pointing out the wrong equations people draw.

What do we do next? Which one do we pick?

Read further:

- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

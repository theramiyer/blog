---
title: "On Petrol Prices"
subtitle: "Going beyond WhatsApp University"
date: 2021-06-20T08:58:44+05:30
description: |
    Fuel prices are at an all-time high. Why? Is the government to blame? Can we not do so much for the country? Or are these fuel prices indeed bad? In this post, we look at some of the contributing factors, the logic behind the hike, and along with that, address some claims by WhatsApp University.
tags:
- economy
- whatsapp university
- government
- India
- resources
- market
---

As soon as someone complains about the fuel prices, we see some coming out in support of the hike, calling it an economy booster. I am no exception; I saw those forwards as well, for as long as I was on WhatsApp over the last year. In this post, we look at the good and the bad of such fuel price hikes, look at whether the government does indeed control the prices, what its implications are, and what we can do about it.

This is not a political post. Please do not waste your time looking for political undertones in it; none exists. What matters is that the government did what it did, not who was (or is) leading it. Left, Right or Centre, fuel prices affect us all, and we must care about it.

{{< toc >}}

To begin the discussion, let us look at what the prices have been over the last eighteen years. (Eighteen years, because that is how far back the data I could find goes.)

{{< raw >}}
<div class="flourish-embed flourish-chart" data-src="visualisation/6479858"><script src="https://public.flourish.studio/resources/embed.js"></script></div>
{{< /raw >}}

You would notice two sharp climbs: the first after 2010 and the second after 2020. You would also see a sharp climb in the diesel prices since 2018. Let us go into the past to get the reason for it.

## The history of petroleum price regulation

Fuel is among essential goods. Petroleum is not something we can avoid as people. Fossil fuels are a lifeline of our economy.

Imports meet over 80%{{< sidenote 84import >}}India's oil import dependence jumps to multi-year high of 84% in 2018-19 ([Business Standard](https://www.business-standard.com/article/pti-stories/india-s-oil-import-dependence-jumps-to-84-pc-119050500133_1.html)){{< /sidenote >}} of our petroleum needs. And the international oil prices fluctuate. The government, to ensure that the economy functioned without friction, controlled the fuel prices. While the global fuel prices changed every day, the government kept the prices limited within a small window. You see this as a gradual, almost linear climb in the chart.

In 2010, the government began the process of deregulation of petrol prices. This deregulation let the oil marketing companies (IOCL, BPCL, HPCL, etc.) decide the prices, which reduced the burden on the government, along with ensuring that the oil companies got compensated on time. The first jump you see in the chart above is because of this.

The government, back then, decided to tweak the prices on the first and the sixteenth of every month, according to the global market prices. In 2017, the government fully deregulated the prices of fuel, so that the prices could change every day as per the global market rate. This is the second jump you see. Notice that the prices of diesel also jumped this time around.

This is what the government means when it says that it has no control over the prices.

But that is not the whole picture.

During the pandemic, when the entire world went into a lockdown, the demand for petroleum dropped across the world. This resulted in a steep drop in the price of the crude oil. But this was also the time that our government realised that it would face a hit in the GST collection.

When the prices dropped, the government saw this as a window of opportunity. The government decided to keep the prices at around ₹70. The logic was that the public could afford this; the process had been in this range for some time, and there were little complaints. But how could the government do this when it did not control the prices any more? Of course, by tweaking the taxes.

When the economy came to a grinding stop because of the pandemic, one way to kickstart it, was to give money in the hands of people, so that they began the spend cycle. Such an act leads to more demand, then more production, and more consumption. But where would the government get the money to give to people when its tax revenues get hit?

Exploiting the drop in crude prices was a good way to make some quick money. Nobody had anything to lose.

But this is not the whole story either.

The government increased the taxes when the prices dropped. But did not reduce them when the prices rebounded. This led to a situation like this:

{{< raw >}}
<div class="flourish-embed flourish-chart" data-src="visualisation/6479135"><script src="https://public.flourish.studio/resources/embed.js"></script></div>
{{< /raw >}}

This morning, I spoke to [Ajit Paul](https://twitter.com/ajitgpaul1), a Business Transformation Advisor, someone who follows the economy, and a member of the book club that my friend is in. He shared his insights about the subject along with some pictorial representations of global fuel prices, taxes on them, etc. which he had collected from news stories.

While the base price of a litre of petrol is ₹37.29, the governments (central and state) charge ₹55.21 as tax. The tax is more than the price of the commodity itself---about 1½ times the base price.

{{< figure src="https://blogfiles.ramiyer.me/2021/images/price-buildup-petrol.webp" alt="Price buildup of petrol by state" caption="Price buildup of petrol by state, as of Feb 2021 (Courtesy: [India Today](https://www.indiatoday.in/) and Ajit Paul)" >}}

Today, the price of a barrel of crude oil is about USD 73.75.{{< sidenote bz-f >}}Brent Crude Oil Last Day Financ (BZ=F) ([Yahoo Finance](https://in.finance.yahoo.com/quote/BZ%3DF/history/)){{< /sidenote >}} If the taxes remain where they are, the fuel prices will go further up as the crude prices increase. And the crude prices will increase in the future---the prices had dropped because the demand had gone down. When the demand increases as travel opens up, the prices will catch up.

{{< figure src="https://blogfiles.ramiyer.me/2021/images/oil-prices-rebound.webp" alt="Crude prices (average) over the last four decades" caption="Crude prices over four decades, as of Feb 2021 (Courtesy: [India Today](https://www.indiatoday.in/) and Ajit Paul)" >}}

## I do not use a vehicle

Not having a vehicle does not make you immune to this effect. Everything needs fuel, starting from the transportation of the grain you eat, irrigation and what not, to perhaps even the water you use. Every commodity needs fuel at every stage.

A hike in fuel prices leads to _cost push inflation_. As a result of this, in fact, we are already seeing a retail inflation of 6.3%{{< sidenote retail-inflation >}}Retail inflation soars to 6.3%, a 6-month high ([The Hindu](https://www.thehindu.com/business/Economy/consumer-inflation-quickens-to-63/article34816205.ece)){{< /sidenote >}}. The inflation in the _fuel and light_ category was 11.58% across India, while that of urban India was 14.24%. Food related inflation shot up to 5.1%. Fuel prices are one of the reasons for these.

An important point to keep in mind is that inflation is also an indicator of growth in GDP. But, like Ajit said, inflation is a complex issue. In general, inflation is a part of growth, and about 3–4% inflation is, in fact, healthy. But anything beyond that should raise a red flag.

## Can we not even do this for the country

If you care about the country and the economy, then you must not accept this increase in tax on fuel. The reason is simple: if the fuel prices go up, it will push inflation. A higher inflation means lower value of money in people's hands. Lower value of money in people's hands would lead to less spending. Reduced spending will lead to reduced demand, which will in turn lead to less money earned by those producing the goods. This will further reduce the money in people's hands, starting a vicious cycle, which is detrimental to the economy and the nation.

## Petrol prices are not in the government’s control

The _crude prices_ are not in the government's control, but the prices of the finished products are---through taxes. Back in 2014, the tax on petrol was ₹9.48, which now stands at ₹32.98.{{< sidenote petrol-tax-21 >}}At 69%, India’s taxes on petrol, diesel highest in world ([The Economic Times](https://auto.economictimes.indiatimes.com/news/oil-and-lubes/government-hikes-excise-duty-on-petrol-by-record-rs-10-diesel-by-rs-13/75571493)){{< /sidenote >}} The story is worse with diesel; the tax was ₹3.56 a litre, which has now jumped to ₹31.83. The states also levy a large amount of VAT, as you see above. Talking about exact numbers for the state is difficult because each state has its own VAT rate (anywhere between 6% and 28%{{< sidenote vat-rate >}}Actual rates of State taxes ([PPAC](http://ppac.org.in/WriteReadData/userfiles/file/PP_3_SalesTax.xls)){{< /sidenote >}} as of writing this post).

{{< figure src="https://blogfiles.ramiyer.me/2021/images/fuel-tax-hikes.webp" alt="Fuel tax hike (2014 vs 2021)" caption="Fuel tax hike (2014 vs 2021), as of Feb 2021 (Courtesy: [India Today](https://www.indiatoday.in/) and Ajit Paul)" >}}

And remember, VAT is over the _sum of_ the base price, the freight and the Excise Duty.{{< sidenote buildup-bpcl >}}Price Build-up of Petrol at Delhi at BPCL Retail Pump Outlets ([Bharat Petroleum](https://www.bharatpetroleum.com/pdf/MS_Webupload_16.06.2021.pdf)){{< /sidenote >}} We are paying tax on tax. When the state governments reduced the tax on petrol and diesel, people saw it as an act of generosity, but no; the states could afford to do it because the increase of ₹10–₹13 per litre on the Excise Duty led to a drastic increase in the tax collected by the state governments. They could reduce the tax a little to appease the public, but technically, that came at no added cost to them.

The price of petrol went up from ₹69.87 to over ₹100 in a span of fifteen months; this is unacceptable. The damage the governments are doing by increasing the taxes to this extent is much more than the benefit that they aim to get. Do the governments not see this? Your guess is as good as mine. The official statement from the union petroleum minister is that the government is not looking at a reduction in taxes on fuel because the government is saving money for welfare schemes.{{< sidenote tax-welfare-schemes >}}‘We’re saving money for welfare schemes’: Dharmendra Pradhan on high fuel prices ([The Indian Express](https://indianexpress.com/article/india/dharmendra-pradhan-petrol-diesel-prices-welfare-schemes-7357174/)){{< /sidenote >}}

## Why is the government doing this

Let us look at some of the arguments I came across (through WhatsApp University and otherwise).

### Fuel imports

Apart from making some money for welfare schemes, the government is also doing this to reduce dependency on fuel import. The idea is that the higher tax rates would serve as a discouragement to buying vehicles that run on fossil fuel. Our domestic production has not gone up in the last decade, and the demand has outpaced the domestic production.

Switching to an alternative at this scale is a long process. Vehicles running on biofuels, vehicles using hydrogen as fuel, etc. are not yet a practical reality. The alternative that is a reality is electric vehicles. The problem, though, is that we do not have the charging infrastructure necessary to cater to millions of electric vehicles.

{{< youtube pLcqJ2DclEg >}}

But our automakers are working on making electric vehicles more accessible, like introducing swappable batteries, charging lounges, longer run per charge, etc. Remember, though, that electric vehicles are not all that eco-friendly; they become eco-friendly when the source of power is renewable (solar energy, for example) or less polluting (like nuclear power).

Regardless, the process of switching will take a long time. Perhaps half a decade. Perhaps more. In today’s call, Ajit pointed out how mobile phones became a reality in India. But can we go on with these tax rates, knowing fully well that as the world opens up to more travel and the demand for fuel increases, the prices of crude oil will also increase?

Is such discouragement to using or importing fossil fuels healthy, when we have no accessible alternatives? We are paying in multiples of the crude price, as this table shows:

{{< figure src="https://blogfiles.ramiyer.me/2021/images/no-parity.webp" alt="Average petrol price as multiples of crude prices" caption="Average petrol price as multiples of crude prices, as of Feb 2021 (Courtesy: [India Today](https://www.indiatoday.in/) and Ajit Paul)" >}}

### Domestic oil exploration

To meet the increased demand, we must explore and mine more oil. For this, we must improve our infrastructure. Oil and Natural Gas Corporation (ONGC) is our state-owned oil exploration company. The company, having bought stake in HPCL and GSPC (Gujarat State Petroleum Corporation) is now short on funds,{{< sidenote ongc-shortfunded >}}Cash reserves sliding, ONGC trims exploration and development works ([The Indian Express](https://indianexpress.com/article/business/cash-reserves-sliding-ongc-trims-exploration-and-development-works-7188884/)){{< /sidenote >}} and cannot expand the way it could before these purchases.

That our domestic oil companies are nowhere near meeting our domestic demand{{< sidenote short-domestic-supply >}}The story of India's failure to drill its own oil in 9 charts ([Business Standard](https://www.business-standard.com/article/economy-policy/the-story-of-india-s-failure-to-drill-its-own-oil-in-9-charts-118052800153_1.html)){{< /sidenote >}} is a well-known fact. Irrespective of the claims, we are still at the mercy of the Organization of the Petroleum Exporting Countries (OPEC).

### Exchange rates

Another interesting aspect that most of us do not think of is the value of our currency. We buy fuel in dollars. If the value of the rupee against the dollar falls, it shows in the fuel prices.

And the value of the Indian Rupee has been falling against the US Dollar.

### The Oil Bond situation

Time and again, WhatsApp University goes abuzz with claims that the previous governments gave oil bonds to the oil marketing companies in return for selling oil at government-regulated prices and making a loss in the process.

Back when the government decided the price of petroleum products, the oil marketing companies were to get their losses covered by the government. The government, instead of paying the companies in cash, gave them bonds, thereby kicking the can down the road. The government set a maturity date and agreed to pay interest for the duration of the bond.

The WhatsApp University claim is that the government has not lowered the taxes to be able to pay the oil marketing companies. As Vivek Kaul explains in his post,{{< sidenote vivek-kaul >}}Petrol and Diesel Prices are High Due to Lower Corporate Taxes, Not Because of Oil Bonds ([Vivek Kaul](https://vivekkaul.com/2021/06/17/petrol-and-diesel-prices-are-high-due-to-lower-corporate-taxes-not-because-of-oil-bonds/)){{< /sidenote >}} this claim is false, because a mere 7.5% of the bonds are nearing maturity. Also, while the bonds are worth about ₹1.3 lac crore in all, the government makes over twice that money in taxes _in a year_.

The truth, Mr Kaul says, is that the government is trying to offset the loss in revenue because of its 2019 decision to reduce corporate taxes, and the further drop in revenue because of other decisions by the government, to which, the pandemic added fuel.

### The pause during election months

During the election period in March–April 2021, the petrol prices remained constant.{{< sidenote elections-prices >}}Election season is here, so is a freeze on petrol and diesel price hike ([India Today](https://www.indiatoday.in/diu/story/india-petrol-diesel-fuel-prices-hike-rise-elections-crude-oil-1777422-2021-03-09)){{< /sidenote >}} While the oil marketing companies change the base price of petroleum products based on the global market rates, they chose not to, during the election window. Nothing else can explain the zero changes made to oil prices, while the global market rates did change during the time. Also, the changes to the fuel price made afterwards are disproportionate to the increase in the crude prices in May 2021, which indicates that the oil companies were trying to cover their losses.

## Show me the light

The truth is that the Monetary Policy Committee (MPC) of the RBI has urged the central and the state governments to act in co-ordination to control the fuel prices, to prevent inflation,{{< sidenote rbi-inflation >}}Cut petrol, diesel taxes to ease inflation, RBI's monetary policy panel tells Modi govt, states  ([ThePrint](https://theprint.in/economy/cut-petrol-diesel-taxes-to-ease-inflation-rbis-monetary-policy-panel-tells-modi-govt-states/671718/)){{< /sidenote >}} which would hamper the fragile economic growth of our country.

> The rising trajectory of international commodity prices, especially of crude, together with logistics costs, pose upside risks to the inflation outlook. Excise duties, cess and taxes imposed by the Centre and States need to be adjusted in a coordinated manner to contain input cost pressures emanating from petrol and diesel prices.  
> ---RBI MPC

While the RBI has not put its foot down, it does seem that the governments may reduce the taxes, because like Ajit said, major elections are coming up the next year. A fallen economy, no matter what, will hit the people, and no party can afford to take the economy for granted. Of course, this is not straight-forward as it seems, given that the government would need to keep its fiscal deficit in mind as well.

Having said that, there is no denying that the government must rethink fuel taxes. Corporates getting a tax cut does not benefit the common man as much as a reduction in fuel prices would. And corporates having to pay a higher tax will not hit the common man as increase in fuel prices would. This model of fuel taxes is unsustainable.

We must all understand that increase in fuel prices is not merely the rich man’s problem. Taxing essentials like fuel the right way is in national interest. What the government took was a short-sighted shortcut.

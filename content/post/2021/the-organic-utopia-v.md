---
title: "The Organic Utopia (Part V)"
subtitle: "the way forward"
date: 2021-03-10
description: |
  When it comes to sustainability, how do we proceed? And in simple terms, if we had to, how do we pick organic or the conventional?
tags:
- food
- farming
- science
- development
---

So far, in this series, we have addressed most of the claims and merits of both the methods of agriculture. We even addressed the polarisation: organic vs conventional. But how do we proceed keeping sustainability in mind? And in simple terms, if we had to, how do we pick one over the other?

This is the fifth of the five-part series on organic food. To jump to any topic on the subject, use the list below:

- [The traditional farming methods]({{< ref "the-organic-utopia-i#the-traditional-farming-methods" >}})
- [How food works]({{< ref "the-organic-utopia-i#how-food-works" >}})
- [The case of reduced nutrition]({{< ref "the-organic-utopia-i#the-case-of-reduced-nutrition" >}})
- [Enter: Organic Farming]({{< ref "the-organic-utopia-i#enter-organic-farming" >}})
- [The sustainability argument]({{< ref "the-organic-utopia-i#the-sustainability-argument" >}})
- [Customer benefits]({{< ref "the-organic-utopia-i#customer-benefits" >}})
- [It isn't as simple]({{< ref "the-organic-utopia-i#it-isn-t-as-simple" >}})
- [Organic tastes better]({{< ref "the-organic-utopia-ii#organic-tastes-better" >}})
- [Organic has lower pesticide residue]({{< ref "the-organic-utopia-ii#organic-has-lower-pesticide-residue" >}})
- [Organic has higher nutritional value]({{< ref "the-organic-utopia-ii#organic-has-higher-nutritional-value" >}})
- [Organic is more eco-friendly]({{< ref "the-organic-utopia-iii#organic-is-more-eco-friendly" >}})
- [Genetic Modification]({{< ref "the-organic-utopia-iv#genetic-modification" >}})
- [Food irradiation]({{< ref "the-organic-utopia-iv#food-irradiation" >}})
- [Does organic have no real merits]({{< ref "the-organic-utopia-iv#does-organic-have-no-real-merits" >}})
- [What does it mean for us, the consumers]({{< ref "the-organic-utopia-iv#what-does-it-mean-for-us-the-consumers" >}})
- [Way forward in sustainability](#way-forward-in-sustainability)
- [When to buy organic](#when-to-buy-organic)
- [When not to worry about organic](#when-not-to-worry-about-organic)
- [What about the reduced nutrition](#what-about-reduced-nutrition)
- [Summing up](#summing-up)

To jump to the parts themselves, use the links below:

- [Part I: Making a case for organic food]({{< ref "the-organic-utopia-i" >}})
- [Part II: Reviewing the claims]({{< ref "the-organic-utopia-ii" >}})
- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})

## Way forward in sustainability

Carrying on from the last point, we want to be able to grow more food with the existing land infrastructure. We do not want to fell more trees and deforest land so we can grow more grain. Rather than look at "organic" or "non-organic", go for what is good for humanity _and_ our environment. For example, in one case, an analysis in a research found that some organic hay crops surpassed conventional yields in the US.{{< sidenote organic-hay >}}_Commercial Crop Yields Reveal Strengths and Weaknesses for Organic Agriculture in the United States_ ([PLOS ONE](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0161673)){{< /sidenote >}} While we may not be able to fully replicate it here in India (because of varying climatic, soil and other conditions), this is a good point: Organic farming, with some of its ecological benefits, has surpassed conventional farming in yield in one documented case. Go for it!

India has the largest number of organic farmers in the world, and is one of the largest global contributors to organic produce. Our standards are high enough that the consumers respect it. We have our organic certification programmes; two of them: One under the National Programme for Organic Production (NPOP) and Participatory Guarantee System (PGS). You can find out more by visiting [Jaivik Bharat](https://jaivikbharat.fssai.gov.in/), FSSAI.

But let us also look at the other side of the story.

Sikkim is the first state to go all organic, but has their experience been great since? No.{{< sidenote sikkim100 >}}_Sikkim is 100% organic! Take a second look_ ([DownToEarth](https://www.downtoearth.org.in/news/agriculture/organic-trial-57517)){{< /sidenote >}}

> When chemicals were allowed, I could grow 280 to 300 kg of pulses and now, after 4 years, I barely manage to grow 80 to 85 kg. This year, I am expecting a slight improvement with a yield of around 100 kg.
> --- Revathy Sharma, Farmer, Sikkim

Of course, this is not to blame the organic methods themselves. The transition from conventional to organic needs a lot of support, including from the government. The officials, though, have not shown will in supporting these measures:

> Kapil Shah, founder of Jatan Trust in Gujarat ... rues that the problem lies in the mindset of the bureaucracy as most people in the agriculture department come from a conventional agriculture background and do not understand organic farming.

This is the other pole. Again, unhealthy and unsustainable. Also, the organic farmers in Sikkim receive little support from the government when it comes to regulations. For example, the State does not restrict food that comes from outside, because it says it needs more food than is locally produced, to cater to the tourists that come to Sikkim. This kills the market for the local farmers, because even the locals buy the conventionally grown food that comes from West Bengal, which is cheaper than the locally grown organic food.

This begs the question, is it better to buy local, no matter whether organic or not? Because one reality is that some of the standards set by FSSAI even for conventionally grown food is so high, that it does not matter if the food is "organic" or not. My take on this would be go for locally made food, whatever is in season. This achieves three purposes:

1. You do not use energy to run a greenhouse
2. You support local farmers and the produce
3. You get fresh produce

If the local farmers see enough support from the local buyer community and establish a regular cycle, they would not need to preserve their produce for longer, which would mean reduction in the use of preservatives.

## When to buy organic

For completeness, let us also address this question: When do you buy organic?

We now have a general picture of how conventional farming works. Conventionally grown food has a higher chance (chance, not necessarily quantity) of pesticide residue, no question there. In that case, you have a higher chance of pesticide exposure in case of _concentrates_. When you buy fruit concentrates, condensed vegetables or baby food, go for organic.

A non-profit organic research organisation called the Environmental Working Group creates a list called _[The Dirty Dozen](https://www.ewg.org/foodnews/dirty-dozen.php)_. Follow the list to know which food items have a high pesticide residue. Some of this residue does not get washed away, in which case, it makes sense to go organic for them. Although, remember that there is a serious conflict of interests here, because the EWG gets funded by large organic farms. The EWG, therefore, have a vested interest in pushing for organic products. The EWG also has a long record of pushing anti-science false narratives. Personally, I do not care about the _Dirty Dozen_ (or the _Clean Fifteen_, for that matter).

If antibiotic-resistant bacteria concerns you (it should), go for organic poultry and meat---for merely that reason. Remember from above that organic red meat does not make it a healthier alternative to conventional red meat. Organic milk in India is expensive; for example, 500 ml of Akshayakalpa milk in Bangalore costs ₹35, as opposed to Nandini, which costs ₹22. If organic milk fits your budget, go for it.

## When not to worry about organic

I would say, in most cases. But specifically, do not worry about pesticide residue in case of thick-skinned vegetalbes and fruits, and those vegetables and fruits whose outer layer you peel off, like cabbage and lettuce. Which also means, you need not worry about pesticide residue in bananas, papaya, watermelon, and so on, because they have a thick skin, which you peel off anyway. The idea of not worrying about pesticide residue in thick-skinned produce is that by peeling off the skin, you get rid of most of the residue. Also, the thick outer layer does not let the pesticides seep into the food within.

In any case, thoroughly wash all vegetables and fruits in water. Even though the pesticide residue has been undetectable, the enforcement of regulation in locally sold produce has been spotty, which makes one question the "undetectable" claim in India. Does that mean that organic is by default better? Again, who is vouching for the non-spottiness of the enforcement of the regulation of unorganised local sale of organic produce?

## What about the reduced nutrition

Ah, almost forgot. Rather than I explain, watch Derek Muller explain it:

{{< youtube Yl_K2Ata6XY >}}

## Summing up

Is organic environmentally, humanly, or economically ethical? You should have an answer to that question by now.

But let me re-state: The question is not whether organic or conventional. The point is to look at the "organic manifesto" and know what it stands for. It stands for ecological sustainability, biodiversity, ethical farming, food safety, and all those great ideas. But "Organic" the marketing system, is---sorry to say this---a leech on all those great ideas, because by taking simplistic approaches of perceived legacy, it does more harm than good.

The way forward is to look at the best available methods to achieve the goals that the organic vision paints. This means, using technology such as fuel cells that make ammonia to get processable nitrogen into the soil, use of targeted pesticides (or genetic engineering) that do not harm the biodiversity, not using unnecessary antibiotics or growth hormones on animals, reducing land use by intensive farming of rotational crops, preventing deforestation, income support for farmers, and so on.

What is the point of going around making green symbols on paper produced by felling trees?

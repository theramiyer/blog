---
title: "Is EdTech a Scam"
subtitle: "Looking at the “one-sided” narrative."
aliases:
- "is-whitehat-jr-a-scam"
date: 2021-05-17T22:53:22+05:30
description: |
    A TechEd startup has been in the news for some time now. Are they a scam? Are they ripping off people? Are coding lessons indeed available for free?
tags:
- business
- learning
- education
- technology
---

About half a year ago, an EdTech (Education Technology) start-up came into the limelight. The company had found its way into serious controversy, including claims that the company had misled parents and children, ripped them off of millions of rupees and were home to some questionable conversations.

The subject came up tonight and I did not know how to conclude one way or another. I thought I could try to untangle it using a post.

---

Update: In a curious coincidence perhaps, someone seems to have gotten Department of Telecommunications to block my domain:

{{< tweet user="iamramiyer" id="1396654910641360898" >}}

While I stand by calling out those that do not play by the rules, I also believe that people must know what is right for them. I choose the latter this time; people are smart enough to form their opinions and see what leads to what. Unlike the "intelligent" bots that scour the web. Feel free to judge, but I find the timing of this "blocking" strange.

---

## The reason for the controversy

I think it all began when Mr Pradeep Poonia blew the whistle saying the company had teachers who did not know what they were teaching, or had gotten trained to teach a specific set of subjects and topics to children (implying that they did not have in-depth knowledge of the subjects they were teaching). His video claimed that the teachers he spoke to did not know the difference between Java and JavaScript and that they said they wrote Java in C++; such teachers could not possibly teach children, let alone make them expert coders.

Soon, him and others were picking out advertisements put out by the company, one of which was:

> 9 year old Wolf Gupta learnt computers to earn Rs 150 cr from Google Video while other kids his age were surfing You-Tube videos. [sic.]  
> ([Source: Karthik](https://twitter.com/beastoftraal/status/1331074275181174784/photo/1))

And these advertisements did come out. I ignored them on my Facebook newsfeed and elsewhere, dismissing them as stupid. But I see what impact these could have had on people that did not know how the world of software development worked.

The primary question raised was on the ethicality of these advertisements.

Also, if one went by the logic that coding could make you rich, India would probably have been the richest country given the millions of coders that we have. But making people understand this, in a society where people think software engineers make millions in the first year of their career, is hard.

But this is merely one facet.

## EdTech and India

EdTech---or Educational Technology---is a booming industry. If you have been anywhere near the Internet in the last couple of years, you will have seen at least a hundred advertisements of MasterClass, SkillShare, Brilliant, and what not. Byju's and the likes run advertisements on TV; you are unlikely to have missed them.

India is no stranger to EdTech. And the IT world has used EdTech for well over a decade to train employees. Using technology-aided education was easy for the tech giants because they had the infrastructure needed for it, even a decade ago. Why, my M.Tech. classes were all online---the lectures would happen in Chennai and we would virtually join live from Bangalore. Now, with the advancement in technology, EdTech has become accessible to everyone. In fact, thanks to it, our students have had at least a little learning in the last fourteen months.

We now know that EdTech is here to stay. One company cashed in on it with their aggressive marketing and sales.

## Learning programming from the Web

But wait, if EdTech has been around for over a decade in the IT industry, then are there no online platforms that teach coding? Or is it restricted to reputed universities that have the capacity to run their classes online? If not, is Pradeep Poonia right in saying that you can find IT-related learning resources for free?

Coursera has been running free courses, including in Computer Science subjects, for a long time now. Secondly, the IT community is vibrant. You have freeCodeCamp, code.org, Codecademy, edX, etc., that have programmes in which you can learn to code for free. Apart from that, most techies blog about their learning, and share solutions. [Here is mine.](https://ramiyer.io/)

You can even get them books that teach them basics of coding---like {{< amazon "this one that teaches the basics using Python" "https://amzn.to/3vbXTPf" >}}, or this {{< amazon "one published by the Cambridge University Press" "https://amzn.to/3b58pAT" >}}, or if you would like to use the same platform that one of the most known EdTech platform uses, {{< amazon "buy this book for ages 5–9" "https://amzn.to/3v7DTNu" >}}, or {{< amazon "this one for ages 7–11" "https://amzn.to/3vcjEy5" >}}.

But my view is that merely saying, 'Well, here, you have an ocean of resources! Go, enjoy yourself!' is utterly useless. Unless you have a direction, you cannot possibly pick up a line and have any success in learning. Even today, nothing beats someone sitting you down and teaching you something. Yes, you can learn from guided tutorials, but that also tires your brain.

Call me old school, but I have had video-based learning, online tutorials, interactive learning, and live session learning. Live session learning has always worked best. I have trained people as well, and got the same from them: live learning works best.

Am I saying that these petabytes worth of online learning resources are useless? No. All I am saying is that a novice will find live learning much more effective.

The 1:1 learning model of the platform is a win.

The question then is: How competent are their teachers?

## Employment

One of the claims that someone made on the Web was that the majority of the teachers on the platform were women, and that the moment someone tried to question the teacher too much, they would receive a call warning them of a harassment complaint, reminding them to ensure good behaviour with the women.

What the founder says is that their platform has ensured giving jobs to women who were not part of the workforce before (for reasons like being the primary caregiver to their children).

Opinion, of course, but this seems a little too convenient.

Let me explain. First, not all parents who want to enrol their children know to code. Not all can judge whether a teacher is capable, based on a trial class. If the teacher and the parent get into an argument, the teacher transfers the call to another team, which asks the teacher to drop off the call and close the trial session. Yes, harassment is bad. But is there a legitimate way in which the platform actually allows checking the competence of a teacher?

Refunds are not the solution to this.

I get it, you cannot do that in schools either. But one must realise that comparing a school to platforms like WhiteHat Jr is like comparing a tablet to a PC.

Schooling is a regulated system. We have standards in place. Schools undergo external audits by regulatory bodies. Teachers need to have a certain level of teaching-specific qualification before they can start teaching children. Those in the leadership in schools have a teaching background, and ensure quality. Schooling is not perfect, but we know what we are getting into and what to expect out of it. The system restricts the narrative to practical claims. Yes, the company never said that they guarantee such a job, but the message they are giving out is in the grey area.

Also, schools work in the "open market". You have options available at your disposal. Schools maintain a track record of kinds, and have healthy open competition. A platform cannot trivialise the schooling system by showing some resemblance with teaching.

I am not saying that the company should not do business. They are free to. And who knows, a couple of decades later, they might be a leader in the space. All that is beyond the point. We cannot apply the 'Do not ask how they got their first million' formula here.

The subject of ethics is perhaps too complicated for words.

## The reactions to criticism

While we are comparing these platforms to schools: You are free to criticise a school, their teaching methods, the student's score, everything.

This has not been the case with some:

1. WhiteHat Jr founder files Rs 20 cr defamation suit against critic ([The News Minute](https://www.thenewsminute.com/article/whitehat-jr-founder-files-rs-20-cr-defamation-suit-against-critic-138221))
2. WhiteHat Jr and the curious case of disappearing dissent ([Forbes](https://www.forbesindia.com/article/take-one-big-story-of-the-day/whitehat-jr-and-the-curious-case-of-disappearing-dissent/63627/1))
3. WhiteHat Jr’s online crackdown: How the EdTech startup went after its critics ([The News Minute](https://www.thenewsminute.com/article/whitehat-jr-s-online-crackdown-how-edtech-startup-went-after-its-critics-138601))

These are three of tens of such articles you will find online, from reputed news outlets.

Of course, the company said that they were the victims, and that people were oversimplifying these issues, but in the end, once all the acquisitions and other processes were over, the company has, as of today, withdrawn the ₹20 crore defamation case against Pradeep Poonia.{{< sidenote poonia >}}WhiteHat Jr withdraws defamation case against software engineer ([The Indian Express](https://indianexpress.com/article/business/companies/whitehat-jr-withdraws-defamation-case-against-software-engineer-7303550/)){{< /sidenote >}} I refuse to buy the idea that the company alone has the truth, while everyone else fails to see it. I am far from saying that the company is bad, but I do not think the company is a misunderstood underdog.

## The "misleading" advertisements

This is something I have not been able to make peace with.

The first red flag was when the company started claiming that a 9-year-old child got a ₹150 cr job at Google Video (did they mean Google Videos?). The first point is, no (reputed) company (such as Google) would hire a 9-year-old. Second, yes, I found it ridiculous and dismissed it as a joke, but the world does not think like I do. The company did argue that the child was a work of fiction, but that does not make it right. And I am not talking of the legal aspect of it; not everything that is wrong is illegal.

Second, they started posting advertisements that had quotes of people like Bill Gates and Sundar Pichai, along with their pictures. When Faye D'Souza asked Karan Bajaj if Bill Gates and Sundar Pichai had approved the use of their pictures, Mr Bajaj said, 'Their photos and details along with their quotes were a source of inspiration.' He downplayed the action saying something like there were hundreds of creative pieces, and that he could not personally review each of them before approval.

I found it too convenient: the most controversial ads of theirs which were also the most effective, had somehow slipped executive review. His saying, 'I accept responsibility' is too little, too late.

He claimed that they were running with a barebones marketing team; the startup was in the zero-to-one phase then, and that they had to put long hours (because of the amount of work they had). He said that they had not been able to set up a conventional marketing team with the necessary hierarchies in place. He said that if he were to sit down and review each creative piece, he would be talking a full-time marketing job, which was apparently a luxury then.

Too convenient. This sets a wrong precedent.

Going by this logic, I could start up with a barebones-everything team, hire a bunch of overenthusiastic college-goers as marketing interns and give them freedom to make any claims as they please. I could approve every unsubstantiated claim that they make in their campaigns, and when someone asks me, I could say, 'Well, what do you want me to do? Work forty hours a day?'

Not done.

Yes, the company has now changed their pitch by getting celebrities as ambassadors, and doing conventional advertisements showing how coding is good for kids. Never mind the ad that shows people fighting with each other to be the first to enter a kid's house to invest in his app.

## The legal boundaries

Some of these matters are sub judice (meaning, the company has filed some cases, which are under judicial consideration; we cannot discuss such cases in public). I will steer clear of the specifics, but companies have this tendency to file defamation suits against people, which has what we call a _chilling effect_. Such an action stifles all criticism because people get scared of saying anything unfavourable to the company in question. 'Who wants to get into all these legal troubles and go to the courts whenever the courts issue a summon? Better keep quiet.'

People like Pradeep Poonia have also claimed that the company has people who scour the Web for criticism and either report such statements asking the social media platforms to take down the posts, or change the questions on platforms like Quora (yes, you can do that on Quora) to something that has no relation to the subject. And surprise, surprise: none of this is illegal. Hello, Terms and Conditions.

The platform also has a clause that forbids you from taking a trial class if you do not intend to enrol your child into the platform. Yes, such a clause is necessary to protect the legitimate interests of a company. But because this is in their terms and conditions, the moment you take a class to verify anything including the competence of the teachers, you are in violation of it. You are free to form your opinion about such a clause. I think discussing that would create a tangent that will not lead to a conclusion related to the platform itself.

I believe that we need reform in our laws. I think this double-negative of "not illegal" tips the balance against the consumer.

The company's mission statement is great. But we must ask, 'Do the ends justify the means?'

## What this should make us think

To me, this is a wake-up call. The premise that these EdTech startups build on is that they are teaching children the concepts in a fun way. Take for example this image that I received from someone (redrawn for better resolution):

{{< figure src="https://blogfiles.ramiyer.me/2021/images/blue-angle-puzzle.svg" alt="Puzzle: Find the blue angle" caption="Puzzle: Find the blue angle" >}}

### Abstract concepts

Did the puzzle feel like a little challenge? To me, this was a mental arithmetic challenge: solve in 30 seconds.

Our schools do not have the luxury of making _everything_ fun because they have to complete the syllabus. Our schools continue to teach us abstract concepts in abstract ways. But this approach needs rethinking because it then leads us to rote learning.

### Teachers and their lives

Second, I have seen my teachers go through a lot of trouble to handle their responsibilities in school as well as at home. We see teachers work for six hours a day, but their actual work hours are much longer. Today, they are having to teach online despite not being tech-savvy. They continue to exemplify adaptability.

Our schooling system needs reform.

At the same time, we must appreciate the aim of some platforms like to help those who cannot work a fixed schedule, find work. In a society like ours, this is a source of self-esteem.

As long as those teaching have the necessary qualifications (I am not talking merely about certificates), we should have no problem.

Fees, again, are a different story.

### Should children learn to code

Well, that is a large topic in itself. But I would say that coding is merely a means of communicating with the computer. Like there is no point in going for a debate competition by memorising a dictionary and a textbook of grammar, learning to code without developing logical skills is useless.

When I started to interact with computers, I started with a language called Logo. We learned to control a "turtle" around the screen to draw beautiful patterns using instructions. We learnt how a program is merely a set of instructions executed in a certain order.

Today, we spend the majority of our time staring at the screen for the "Progammer's Inspiration", or attending meetings with different teams to get the requirements, doodling on virtual boards while planning features, thinking about how a novice would use our solution, etc. Coding is a small part of our work day.

And "coding is a universal language like dance is" cracks me up. Coding is as multi-versal as it gets.

Make your children develop their logical thinking skills. Leave coding for later; coding is not that critical. And do not fall for languages; ten years from now, developers may not even touch the most popular language of today. Who knows, in a couple of decades from now, computers would be developing software, and you would not need a large developer workforce.

Rest assured, your kid is not falling behind if s/he does not learn to code. Coding is not difficult, coding is not the ultimate. Ask any software engineer and s/he will agree with me.

I will make a post about how one can start learning to code sometime. I also plan to start a free video series about the fundamental concepts of coding. You can try them out and decide for yourself if coding is as great as people make it out to be.

Meanwhile, chill. Let your kid run around, play with others, bruise her knees, build sand castles, get into arguments with his friends, challenge your authority, and paint the walls. Each of these is an important life skill. Coding can wait.

Meanwhile, if you would like your kid to strengthen their logic, get them some of {{< amazon "these logic-building games" "https://amzn.to/3RXX2uX" >}}.

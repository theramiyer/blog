---
title: "The console Google does not want you to use"
subtitle: "because your privacy kills their business"
date: 2021-04-14T08:41:15+05:30
description: |
    Google, Facebook and other ad companies need every bit of information about you to show you relevant ads. But what if data gets breached? Or sold? You can have slightly better control of your data. We start with Google.
image: "https://blogfiles.ramiyer.me/2021/images/kashmir-google-maps-india.webp"
tags:
- privacy
- internet
- google
- social
---

Facebook was a target at the beginning of this year, when they decided to tweak their privacy policy. It turned out to be a disastrous PR move. Although they reversed it, we are already seeing a better-worded version of the update. I wrote a couple of posts: one about [how the change affects your privacy]({{< ref "privacy-and-you" >}}), and another, about [whether WhatsApp chats are at all private]({{< ref "are-whatsapp-chats-private" >}}).

Facebook may have a different data collection strategy than its peers, but is not alone in the game---the other big name is Google. But before we look at Google’s strategy, let us go back to a statement I had made:

> This is not about showing a company its place. This is about taking control of your data and data about you. Weigh the value you receive in return for your data. If the deal is not good enough, walk out. Do not hold on to toxic relationships.

Google gives you a lot of value in return. You have Google services ranging from a fantastic search engine to the most popular mobile operating system. Google aces almost every space it steps into.{{< sidenote google-ace >}}... and actively develops: which excludes products like Blogger, Orkut, Google Plus, Feedburner, etc.{{< /sidenote >}} I mean:

1. Google Search
2. YouTube
3. Android
4. Gmail
5. Google Chrome
6. Google Docs
7. Google Maps
8. Google Photos
9. Google Analytics
10. Google Keep

These top-ten products are ubiquitous! In fact, when I started de-Googling my life, I found it hard to move away from these products. I have almost succeeded, except I still use YouTube (and do not see myself moving away from it). I am sure you will be hard-pressed to find alternatives to these products (which is why I listed out the [alternatives that I use]({{< ref "privacy-and-you#in-conclusion" >}})).

I feel, Facebook, as a family of products, is not as much value. Yes, everybody has WhatsApp. And Facebook does help us interact with a lot of those whom we lose contact with over time. But are virtual interactions valuable enough? I mean, such a product is indeed useful, but is it enough value compared to what you pay as data?

That is not to say that Google products are worth your data, but Google products are much better value than Facebook products. That said, you must take control of your data on Google as well.

Companies like Google and Facebook collect your data so that they can show you advertisements that are most relevant to you. Why? Because businesses pay these companies, and expect return of investment. For example, imagine that you sell cell phones that cost over ₹60,000. Would it make sense for you advertise them to a high school student, who is a child in a family whose monthly family income is ₹50,000? Would it make sense to sell a dating service to a 90-year-old? (Not that a 90-year-old cannot date, but what is the probability of a 90-year-old dating?) The money paid by the dating service to an advertiser to show that ad to the 90-year-old went to waste. No business wants that---you would not want that as a business.

But why is this a problem? They find out whether you would like a certain product or service, and show you an advertisement for a similar product or service. You see a relevant ad, which is useful to you; the service showing you a relevant ad fulfils its responsibility; the business that paid for the ad got business. Is that not a win-win-win? Yes, but this is the 'Golden Path' (or the 'Happy Path' that people at my workplace like to call it).

What we need to understand is that companies like Facebook want you to spend as much time as possible on their platform, so that they can show you ads. For that, you must see relevant content as well. The first part of the story is them tracking you across the Web, so that they understand what is relevant to you (and what is irrelevant to you). The second part is tuning your content so that you do not get to see anything that differs from your views. This locks you into a bubble of content you agree with, protecting you from content you disagree with.

This altering of the information you receive alters your general personality, which changes your thought process, and ultimately, makes you behave a certain way.

Google is not immune to this either. Google locks you in filter bubbles in Search. You will find uncountable examples for this on the Web. {{< amazon "People have even written books on it." "https://amzn.to/3PS3rGw" >}}

Why does Google filter search results despite being one of the two largest advertising platforms on the Web? Their AdSense is everywhere---conservative sites, liberal sites, economic right-wing sites, economic left-wing sites, entertainment sites, gossip sites, conspiracy theory sites---everywhere. Why does it matter if Google shows "agreeable" results or not? I have yet to find an answer to that. Perhaps conflicting signals (getting an economic right-wing ad on an economic left wing site)? But that does not change the fact that Google filters search results---based on demographics, your personal profile based on your interests, etc.

But the problem that this filtering creates is bigger. By attempting to exclude content you may not like, Google does not give you the entire picture.

For example, when you google "Kashmir" on Google Maps from India, Google may show you the border a certain way, compared to googling the same phrase from, say, England. Look at the example below: the image on the left is from India, and the one on the right is from the US (thanks, VPN). Move the slider and watch the International Border.

{{< before-after "https://blogfiles.ramiyer.me/2021/images/kashmir-google-maps-india.webp" "https://blogfiles.ramiyer.me/2021/images/kashmir-google-maps-us.webp" "Kashmir as seen on Google Maps from India" "Kashmir as seen on Google Maps from the US" "Kashmir as seen from India and the US" >}}

Seeing a map of India that excluded the Pakistan-occupied Kashmir and China-occupied Kashmir shook me. I had, thus far, lived in the blissful assumption that while China and Pakistan laid claims on the state, India still held control. And Google continued to confirm my view.

To make Google give you the complete picture, you need to look for specific information (which is precisely the problem---how do you specifically look for something you do not know?). Try searching for "northernmost Indian administered point", and the results may surprise you. National disagreements aside, I am sure the case is the same in Pakistan.

To me, this means limited information. The knowledge of this area being officially recognised as "disputed" does (and will continue to) disturb me, but that does not mean I do not get to know about it.

You may have noticed that Facebook, YouTube, Twitter, etc., constantly show you what you agree with. Content that you disagree with does not appear in your feed. This is why people of a certain community see a certain other community attacking them all the time, and vice versa.

These networks create an echo chamber, where you do not get the whole picture, and at the same time, your biases get reinforced.

Why? Because seeing something you disagree with---or get disturbed by---will make you put away your phone or navigate away from the platform (unless you are the confrontational kind). That reduces your presence on the platform, which in turn hits the probability of your seeing an ad.

This is why platforms collect all possible data about you. Whom you talk to the most, what you talk about, what kinds of posts you engage with the most, what you do, where you go, how much you spend on what, what your likes and dislikes are, how much money you make, what your political views are, what your virtues and vices are, and so on. The more these platforms collect, the better for them.

Why do you think Google gives you a timeline of where you have been? Why do you think Google asks you about your experience at a certain store the moment you step out of it? Why do you think it asks you to rate the prediction of "You may like this video" in your YouTube feed? Why do you think Google let you upload high quality photos for free until May 2021?

In fact, Google entering into the CCTV business would not surprise me at all, given all the facial data it has.

You would have noticed that the biggest names in Web Analytics are the biggest players in the online ad world. When companies give away free analytics, website owners add them to see how their website performs, and the analytics providers collect data in the background to profile you. Google and Facebook are the leaders in this area. (I use analytics, but through [a privacy-respecting platform](https://plausible.io/), whom I pay. No other trackers are present on this site except in case of posts with [social embeds](/privacy/)).)

But the situation is not all bad. You can control what data these companies have about you. And I am not even talking about using services that help you wipe your data off of the Internet. Step one is following these basic rules:

1. Share what is mandatory, _leave all else blank_. (Does the phone number field not have a red asterisk? Do not enter your phone number.)
2. Read before ticking boxes---some boxes are about subscribing to newsletters and sharing email addresses with third parties; if you do not want it, do not tick it.
3. Do not click 'Agree and close' on cookie consent pop-ups. They often have advertising and other data collection ticked. When you have "More options", open the options and deselect what you do not want.
4. Use incognito/private mode whenever possible. The issue with always using private windows is that you need to enter your password over and over. Use a password manager instead---[Bitwarden](https://bitwarden.com/), for example.
5. Do not use Chrome.{{< sidenote chrome >}}Why You Shouldn’t Use Google Chrome After New Privacy Disclosure ([Forbes](https://www.forbes.com/sites/zakdoffman/2021/03/20/stop-using-google-chrome-on-apple-iphone-12-pro-max-ipad-and-macbook-pro/?sh=289b29df4d08)){{< /sidenote >}} (Ooh, big one.)

But, what about all the data that Google already has?

First of all, the data since the time you created your Google account is useless to you. My Google account is over 12 years old. What I was twelve years ago has nothing to do with what I am today---as in, my interests, tastes, circles, attitudes, everything was different back then. I was a teen. _Everything_ about me was stupid. In fact, I was so stupid even a couple of years later, that I let my Xperia automatically back up my text messages to Gmail, in plain text. But Google is happy to store all that data. It gets a nice profile of how I have grown, and that means a lot to one of the most powerful profiling engines.

Our goal here is not to stop Google from collecting data. That is impractical. De-Googling is not for everyone, either. What we can do is limit the data Google has about you. Google claims it gives you that power, but the level of truth in it may differ from what it says, because the platform is opaque. But despite that no one can guarantee that Google will purge all the historical data, you have nothing to lose by asking for deletion:

1. Go to [Data & personalisation](https://myaccount.google.com/data-and-personalization) under your [Google Account](https://myaccount.google.com/).
2. Under Activity controls, select Web & App Activity.
3. Set your preferences for what you want Google to record.
4. Go to Auto-delete, and select the data retention period.
5. Repeat the steps above for other items such as Location History and YouTube History.
6. Click on [Other Google activity](https://myactivity.google.com/more-activity) under [My Activity](https://myactivity.google.com/myactivity).
7. Go into each category and delete what you do not want Google to remember. (The Delete button merely _looks_ grey---click on it, and you will get an overlay pop-up to clear data.)

Data older than 12 months is perhaps not relevant to you at all (you should actually consider setting a shorter duration). For example, the difference between what I used to do a year ago and what I do today is drastic. The way I spend my time is different, my interests have changed, how I interact with people has changed, etc.

'But wait, is it not good that Google has irrelevant data about me?'

It depends. If all you want to do is give irrelevant data to Google, you have other ways to do that. Letting stale personal data remain on the platform is not a good strategy.

If some day a data breach occurs, someone might get access to your old data, and we do not know what they can do with it. Something that is irrelevant to you today might be relevant to someone else.

Businesses say that your data is secure, but when breaches occur, they make a statements like: The attacker merely took "superficial" or "public" data.{{< sidenote clubhouse-breach >}}Clubhouse data breach: 1.3 million users have info leaked online ([TechRadar](https://www.techradar.com/news/clubhouse-data-breach-13-million-users-have-info-leaked-online)){{< /sidenote >}} (And sometimes, they do not even disclose breaches.){{< sidenote breach >}}Facebook will not notify more than 530m users exposed in 2019 breach ([The Guardian](https://www.theguardian.com/technology/2021/apr/08/facebook-2019-breach-users)){{< /sidenote >}}

But is this control enough? No. As of now, this control remains limited. For example, what about the health data from Google Fit? But though limited, this ability to delete your data is a start.

Go on and set your preferences under Activity Controls. Digital housekeeping will take you a long way in taking control of your data privacy.

Like they say, everyone knows what we do in our bathrooms, but does it mean we keep the door open?

Do not stop here. Share this piece with your friends and family.

---
title: "The Organic Utopia (Part I)"
subtitle: "making a case for organic food"
date: 2021-03-06
description: |
    Amidst the noise around organic food, cleaning supplies and what not, we try to make sense of why—or if—one should go for organic food.
tags:
- food
- farming
- science
- development
---

Over the last decade or so, we have seen the buzz about organic food amplify. Hundreds of stores have sprung up in our urban neighbourhoods, which tout the traditionally grown food that is more nutritious, apart from being safer than the "conventional" food. But is organic food more nutritious, tastier and safer than conventionally grown food?

As usual, the answer is not binary.

A friend of mine texted one evening about the craze about organic food. This is a friend that gets nuance, open to discussion. I thought this was a good topic to write about; I hope this answers her questions, and also helps others who are looking for information about organic food.

This is the first of the five-part series on organic food. To jump to any topic on the subject, use the list below:

- [The traditional farming methods](#the-traditional-farming-methods)
- [How food works](#how-food-works)
- [The case of reduced nutrition](#the-case-of-reduced-nutrition)
- [Enter: Organic Farming](#enter-organic-farming)
- [The sustainability argument](#the-sustainability-argument)
- [Customer benefits](#customer-benefits)
- [It isn't as simple](#it-isn-t-as-simple)
- [Organic tastes better]({{< ref "the-organic-utopia-ii#organic-tastes-better" >}})
- [Organic has lower pesticide residue]({{< ref "the-organic-utopia-ii#organic-has-lower-pesticide-residue" >}})
- [Organic has higher nutritional value]({{< ref "the-organic-utopia-ii#organic-has-higher-nutritional-value" >}})
- [Organic is more eco-friendly]({{< ref "the-organic-utopia-iii#organic-is-more-eco-friendly" >}})
- [Genetic Modification]({{< ref "the-organic-utopia-iv#genetic-modification" >}})
- [Food irradiation]({{< ref "the-organic-utopia-iv#food-irradiation" >}})
- [Does organic have no real merits]({{< ref "the-organic-utopia-iv#does-organic-have-no-real-merits" >}})
- [What does it mean for us, the consumers]({{< ref "the-organic-utopia-iv#what-does-it-mean-for-us-the-consumers" >}})
- [Way forward in sustainability]({{< ref "the-organic-utopia-v#way-forward-in-sustainability" >}})
- [When to buy organic]({{< ref "the-organic-utopia-v#when-to-buy-organic" >}})
- [When not to worry about organic]({{< ref "the-organic-utopia-v#when-not-to-worry-about-organic" >}})
- [What about the reduced nutrition]({{< ref "the-organic-utopia-v#what-about-reduced-nutrition" >}})
- [Summing up]({{< ref "the-organic-utopia-v#summing-up" >}})

To jump to the parts themselves, use the links below:

- [Part II: Reviewing the claims]({{< ref "the-organic-utopia-ii" >}})
- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

## The traditional farming methods

The concept of organic farming is not new to India, for obvious reasons. We were an agrarian family ourselves, until my grandfather's generation. I have heard of the methods that they used, to get better yields, ranging from what they fed to the cattle to how they made natural fertiliser out of plant remains, compost, etc.

And then the methods changed, although gradually.

And, they are changing again.

## How food works

But before we get into the depths of the rabbit hole, let us set the fundamentals on the table.

Food is a medium of energy transfer. In the end, what we get is the sun's energy. Plants, in the presence of sunlight, combine carbon dioxide in the atmosphere and water in the soil to make hydrocarbons. (Let us keep the other nutrients aside for the moment.) They package this into food that we eat---be it fruits, vegetables, grains, pulses or anything. We consume these hydrocarbons (or the animals that consume these hydrocarbons), and separate them into carbon dioxide and water (which we release back into the atmosphere). This process releases the energy from the sun that had gotten trapped within these hydrocarbon molecules, which our bodies use.

Of course, this is an oversimplified version of the process, but this is the general principle behind food. Remember this for later in this post, because here lies a twist.

## The case of reduced nutrition

We hear all around us that the general nutrition in our food has gone down. If you are an Indian, there are chances that you heard people talk about your grandma's generation leading a much healthier life. You would wonder how come the numbers such as longevity and other parameters disagree with this, and yet, you see _Maturists_ (those born before 1945) living a life of much less complications. For instance, my maternal grandmother lived for 91 years, without hypertension or diabetes or any chronic ailment for that matter. Her reasons for hospitalisation were rather silly, like dropped levels of sodium because of her fasting habits (which she changed after she turned 80, of course).

But have the nutrition levels dropped in our food in the first place?

Over five or six years ago, I read somewhere, that a university conducted a study about the levels of nutrition in our foods, over time.{{< sidenote nutri-drop >}}_Dirt Poor: Have Fruits and Vegetables Become Less Nutritious?_ ([Scientific Amercian](https://www.scientificamerican.com/article/soil-depletion-aND-NUTRITION-LOSS/)){{< /sidenote >}} The argument here is that the newer methods of farming that we adopted for shorter cycle times, did not give the plants enough time to absorb enough nutrition from the soil. The other argument is that the use of chemicals in farming drain the soil of nutrients, giving us a double-punch.

## Enter: organic farming

The general idea behind organic farming is the use of traditional methods, which means longer cycle times, no presence of chemicals like synthetic fertilisers or pesticides in the food, and so, a healthier produce.{{< sidenote org-farm-intro >}}_Organic Production/Organic Food: Information Access Tools_, [USDA](https://www.nal.usda.gov/afsic/organic-productionorganic-food-information-access-tools){{< /sidenote >}} Another talking point in the organic world is the disregard for genetically modified food.

And the law in general is that as well. No GMO, no synthetic fertilisers, no synthetic pesticides or herbicides, no antibiotics or hormones, no irradiation, no sewage sludge. By related definitions, the process is also about ethically farming, which means, use of practices that do not harm the biodiversity of the area.

In India, the [National Standards for Organic Production](http://apeda.gov.in/apedawebsite/organic/ORGANIC_CONTENTS/National_Programme_for_Organic_Production.htm) contain the specifics to follow, for organic certification. They, too, prohibit genetically engineered seeds, transgenic plants or plant material; ask organic farmers to use "certified organic" seed and plant material when available, and use chemically untreated seed and plant material when "certified organic" is unavailable. Our standards have a strict prohibition on mineral fertilisers containing nitrogen. Human excrement is not allowed, but restricted use of "Sewage sludge and urban composts from separated sources ... monitored for contamination" is.

## The sustainability argument

One of the sustainability issues that we have today is monoculture, wherein a farmer grows the same crop over and over. He may have his reasons: better profits, better suited growth model, the local demand, etc. But every plant species has its specific nutrient needs. If you cultivate the same plant over and over, after a finite number of cycles of cultivation, the soil would have lost all the nutrients that the crop needs. The next cycle would not fetch you good produce. This is where fertilisers help. Fertilisers artificially add the nutrients into the soil, which the plants can absorb. But the more natural way to get this effect is crop rotation---and growing crops that complement these nutrient needs. For example, legumes help with "Nitrogen Fixing".{{< sidenote nitro-fix >}}_How Legumes 'Fix' Nitrogen in Your Soil_ ([Tilth Alliance](http://www.tilthalliance.org/learn/resources-1/almanac/october/octobermngg)){{< /sidenote >}}

Using chemical fertilisers---specifically the nitrogen fertilisers---causes what we call "algal blooms". When we add fertilisers to the soil, the plants absorb some of it, and the rest remains in the soil. When water runs through the field, it would dissolve some of the nitrogen compounds and carry it along to a nearby water body. This run-off benefits some of the plants growing in the water, which leads to overgrowth of such plants in the water body, which can throw the ecosystem off balance. And then you have people consuming that water, leading to health issues in infants, and conversion of these compounds into greenhouse gases by bacteria, which is hundreds of times more potent than carbon dioxide.{{< sidenote nitro-fert >}}_Do industrial agricultural methods actually yield more food per acre than organic ones?_ ([Grist](https://grist.org/food/do-industrial-agricultural-methods-actually-yield-more-food-per-acre-than-organic-ones/)){{< /sidenote >}}

Also, synthetic nitrogen fertilisers are energy expensive. Often, this energy comes from fossil fuels.

## Customer benefits

The big one, and perhaps one that interests you the most.

The foremost benefit of going organic is pesticide residue. Over the recent years, the presence of pesticides and chemicals in food has come into spotlight. From talks to films, we have a solid---despite the risk of the negative connotation the word may have---narrative about the harmful effects of the chemicals in our food, ranging from pesticide residues, to presence of preservatives and heavy metals.

Organic farming does not allow use of chemical pesticides. Instead, it relies on other bio-pesticides. One of the methods is use of birds and animals that prey on pests. Those switching to organic also mention that organic food tastes better than conventional food. And everywhere we look today, we see the 'better taste' banner wrapped around anything organic.

As I said, organic farming is not new to India. In fact, a fraction of Indian farmers still use traditional methods in their farming, even though they may not call their produce as organic (although I do not see why they should not). From hand-pounded rice to jaggery, we have organic variants available all around us.

Our households trust these products, because they use ethical practices, which does not harm the soil---we culturally hold the soil dear---or the surroundings. They do not kill birds or animals in the process, including the earthworms. Organic farmers do not give antibiotics to their cattle. In general, the belief is that these natural products have a high nutritional value.

And as most of us maintain, the natural way is better, closer to nature.

'Then, why are organic products more expensive?'

'Well, more work, better quality ingredients, and so on.'

'Right, meaning, if the prices were the same as conventionally grown produce, would there be no reason not to switch to organic?'

{{< figure src="https://blogfiles.ramiyer.me/2021/images/organic-price.webp" caption="Difference in price between 'conventional' and organic food" >}}

## It isn't as simple

Of course, the simplest explanation is the most popular. But is not necessarily accurate. Which means, we would need to look at other aspects before we come to a conclusion. Polarisation using simplistic arguments have seldom (if at all) helped humanity in the long run.

Read further:

- [Part II: Reviewing the claims]({{< ref "the-organic-utopia-ii" >}})
- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

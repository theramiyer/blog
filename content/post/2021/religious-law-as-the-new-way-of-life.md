---
title: "Religious Law as the New Way of Life"
subtitle: "and the possibility of its inclusion in contemporary law"
image: "https://blogfiles.ramiyer.me/2021/covers/taliban-explainer-nytimes.webp"
date: 2021-08-18T08:23:01+05:30
description: |
    Ever since the Taliban takeover of Afghanistan, conversations about the implications of incorporating religious laws into the workings of a modern nation have gained momentum. In this post, we look at the validity of these claims by looking into the history, and talking about secularism.
tags:
- religion
- governance
- law
- democracy
---

The past weeks have been eventful in the international space. The state of Afghanistan---our neighbour (we share a small bit of our international border with Afghanistan at the north end of Kashmir)---worries us Indians. The Taliban does sound like a formidable force, and we constantly wait for our government to put out statements that say, 'We are doing fine.' And then we hope that the government is being truthful.

{{< figure src="https://blogfiles.ramiyer.me/2021/covers/taliban-explainer-nytimes.webp" alt="Taliban members in an area controlled by the group last year in Laghman, Afghanistan. Credit: Jim Huylebroek for The New York Times" caption="Taliban members in an area controlled by the group last year in Laghman, Afghanistan. Source: [The New York Times](https://www.nytimes.com/article/who-are-the-taliban.html)" >}}

Until a week ago or so, the word, Taliban, sounded evil to me; I associated it with the "evil" work that we have known them to do over the past decades, ever since I was a child. And then, a short documentary gave me the meaning of the word: Students.

And for a moment, it did not sound all that evil. That confused me.

{{< toc >}}

When I started looking further, I found another article---within seconds---by the BBC, which says that the Taliban promised to enforce the _Sharia Law_.{{< sidenote bbc-sharia >}}Who are the Taliban? ([BBC News](https://www.bbc.com/news/world-south-asia-11451718)){{< /sidenote >}} For those who do not know, Sharia is Islamic Law, which form part of the _Quran_ and the _hadith_.

While I have not read it, and it would perhaps be wrong to go into the technical details or the correctness of the interpretation of it, this post looks at religious laws in general, not the law of a single religion.

## Religion and the law

For some time, religion has been the basis of law. From _Dharmaśāstras_ to _Halakha_ to _Sharia_.

Religious laws exist for a reason: humans freely accept what the religious scholars say is the word of the God (or gods). Again, this post is not to question the existence of the God, or the "God's Word"; we are more concerned about the implications of these laws.

One of the hypotheses on the need for religion is that in nature, humans lived in small groups. This tendency is clear from our social connections of today: we can handle a couple hundred meaningful relationships at the most, at a time.{{< sidenote dunbar >}}Dunbar's number: Why we can only maintain 150 relationships ([BBC Future](https://www.bbc.com/future/article/20191001-dunbars-number-why-we-can-only-maintain-150-relationships)){{< /sidenote >}} (There is debate about the why, but the why is not in question here.) This means that the rules in a small group would work in that small group, but as the group expanded, the control on the group reduced. The model of group-based rules was not "scalable".

But for collective existence and to create a society, we had to grow these small groups into larger groups. These groups are perhaps what grew into communities, congregations, and over time, nations.

We humans understand the importance of laws. We have had them for millennia now. But perhaps at some point, the ultimate authority had to be omnipotent. Whether such a power exists or not, we all understood the importance of it. These omnipotent beings, for good reason, wanted us all to have a sense of morality, a sense which these laws guided.

In a way, the irony aside, religions tried to give us this moral compass.

## Going beyond morality

Before we begin this section, I must state that I have not read any of these laws or treatises myself, but I know the common interpretations of these laws and treatises. (We will talk more about this problem at a later point.) Some of these treatises and laws influence our present lawmaking process as well---some for good reason, others not.

_Annadānaṃ_, for example, is a good principle. Not harming sentient beings for food is a noble idea. Sharing a part of your privilege with the underprivileged is a noble act. Respecting every human is a good way of living. All these ideas and principles are part of our religious traditions.

In general, religious teachings form a basis of morality in our society (or at least they should). They call for a collective coexistence. Tolerance (or acceptance), patience, understanding, thinking about others’ good, etc. find a place in our religions.

At some point, our scholars perhaps thought of writing these down to share with the others, so that good thoughts, ideas and practice could go around and stop us from harming each other.

> Ā nō bhadrāḥ kratavō yantu viśvataḥ.  
> (May noble thoughts come to me from all around.)  
> ---Rigveda (1.89.1)

## The Third Millennium

Humans write the laws (the Divine may or may not have made them). Regardless of whether a human being receives ideas from the God or looks around to judge what is right or wrong on their own, the human relies heavily on their perception. Whether divine intervention or human thought, we can all agree that the humans who wrote the laws had their own limitations. Even if God had come and told someone something, what we have today is what that someone understood and passed on.

If we consider that they had Divine blessing to understand everything that the Divine said, the limitations of human language has caused change of meaning as the ideas got passed down.

This bring us to interpretation.

The reason we have such a large number of sects in every major religion is testimony to that each teacher interpreted something a certain way, and thought they were right (and worse, that the others were wrong). And what started off as something aimed to bring peace among humans has now become a matter of contention.

Secondly, the human society changes with time. A hypothesis in the Indian subcontinent is that we had no laws _written down_ until Pāṇinī, because the scholars, until then, believed that anything written down will become etched in stone and would lock societies in outdated ideas, hindering intellectual growth. The hypothesis goes on to claim this as the reason for not having a script for Sanskrit, and keeping the teachings alive in oral tradition.

If the hypothesis that the scholars did not want written documents for this reason is accurate, I could not agree more with the thought behind it. One reason for our religions of today being in shambles is lack of evolution (pun unintended).

## The immoral atheists

The popular belief is that godless atheists are evil people. That the Devil is working through them to break down social order and cause chaos. This may have been true for the atheists of centuries or millennia ago, but statistics (even if taken with a grain of salt) suggest that the idea about today's atheists could be wrong.{{< sidenote atheists-prison >}}Atheists in Prison ([A Scientific Enquiry](https://scientificinquiry.blogspot.com/2007/01/atheists-in-prison.html)){{< /sidenote >}}

If the God could come down today, may be They would rip apart the religious scriptures that we have now and ask them redone in the contemporary sense. Because today, the role of humanity in this society has changed. The world has changed. The human needs have changed. The Divine would like an upgrade:

'My word should propel society, not retard its growth.'

In the modern world, one leader does not lead the pack. Today's sense of leadership is collective contribution. This is what scales. Like how we went from groups to a congregation, we must lift ourselves up to a more collective mass: humankind. And for that, we cannot get stuck with what someone three (or five or fifteen) millennia ago, thought was the Divine Word.

Today, we have better instruments to understand the philosophy of our existence. We have a better sense of the effects of our actions. As our knowledge expands, we must do away with what was once the apex of human thought and perception. We have outgrown those ideas.

## The Constitution of India

The Constitution of India, much like the constitutions of other democratic nations, understands the idea of collective leadership. Democracy has its flaws, but this is the best we have today. The system has a lot of room for growth, but who knows, we might forge a new, better path while Millennials like me are still alive.

What is good about the constitutions is that they accept the idea that the society changes, and has systems in place to amend the law to accommodate the growth of the human society. Of course, the system has its flaws, otherwise, we would all be living in a world much more beautiful.

Also, the modern laws of today cannot be arbitrary. Every piece of document that the Legislature incorporates into our legal documentation must have a basis, must define each of the players accurately, and must lay out the consequences of breaches. This quality is a result of maturity of human thought and understanding of human nature over time.

Religious laws, in general, do not have these qualities.

We should not follow Sharia or include it in our constitution because of the same reason that we should not follow the Manusmṛti or include it in our constitution: Our society has outgrown those ideas and views.

## Personal law

I hear from some of the followers of Islam in India that all they ask is for the system to let them follow Sharia in their local communities. Something like approaching an Islamic scholar for a civil land dispute within the family, so that s/he can settle it for them.

Perhaps there is nothing wrong with small cases, provided the parties have access to legal recourse when they want or need Judicial intervention. Because we have such a system any way in the form of _Pancāyats_; I do not think all the leaders in a village are experts of the law. When someone brings an issue to them, they do try to settle it without the matter having to go to the courts. But the law limits such powers to specific types of cases, and there is always an option of legal remedy on the table.

But including Sharia into the constitutional framework is something I am personally against. (Read above.) Sharia (or any religious law) must not be part of, or above the Constitution of India, the IPC and the CrPC.

And this, dear reader, is the meaning of a secular state. Religion---any religion---is personal; it cannot interfere with the law or how we run our nation. Everything has its place.

And India is a secular state. We are better off that way, as we will soon see, when Afghanistan starts mixing religion with running the state. Pakistan already does this. Bangladesh broke away from that; we can see how it has surpassed Pakistan in all social indicators today.

I also have some Hindu friends who romanticise enforcement of the Manusmṛti. And I tell them the same: No. It may all sound good, but we have evolved to be much better human beings. Again, of course, I say this based on the current interpretation of the treatise.

'Why do you do that? How does it matter how some people interpret it? Noble people wrote the scriptures after much thought. The documents themselves are good; the interpretations may be wrong.'

Fair enough. And this probably holds good for all religious laws. But this is precisely the problem: how the powerful today interpret it would be how they will write the laws, and that may have no alignment with what the religious laws meant to say.

In contrast, our constitutional system has checks and balances in place. And despite that, the powerful are able to misuse the laws. Imagine what happens when religious laws, with no checks and balances whatsoever other than an intangible power, that has---sorry to say---so far failed miserably to "punish the evil", do when the powerful misuse it.

I for one, am against inclusion of religious laws in the modern legal framework. One's religion must not leave the four walls of one's home. And a state religion is a recipe for disaster in the long run.

Also, I am in favour of most aspects of the Uniform Civil Code; are you? That is a topic for a separate post---going beyond the political narrative that is prevalent today.

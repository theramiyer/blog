---
title: "Mid-brain Activation"
subtitle: "The miracle of the 21st Century"
date: 2021-09-13T08:36:20+05:30
description: |
  About seven years ago, midbrain activation was all the rage. Thousands of children enrolled for these programmes to gain unbelievable abilities like reading blindfolded, sensing colours by a mere touch. What is the science behind midbrain activation? What we thought was dead half a decade ago, seems to be coming back. Regardless of how long it will last, as a parent, knowing if the midbrain activation programme is good for your kids is important. And for that, you need to understand the science behind midbrain activation. (If any, that is.)
tags:
- education
- learning
- science
- scam
---

Remember those days in school when you had people come over and show you how to remember lists of words, calculate complex arithmetic in seconds, etc.? We had such people visiting our school a couple of times as well. A lot of my schoolmates had enrolled themselves for these "workshops", in hopes of achieving better scores.

None of them worked; had anything so groundbreaking happened, our school would have organised these workshops every year, and we would have aced all state- and national-level competitive exams.

The tree continues to give: it takes on different names, reaches the remotest of places (like my school), and gets students enrolled for thousands of rupees. The claims have not changed. Millennial parents who did not enrol for these workshops in their childhood are now enrolling their children---after all, no parent wants to make mistakes their parents made.

About a week ago, I came across something along these lines: Mid-brain Activation.

These programmes for kids continue to fascinate me, which is why I [wrote about a company]({{< ref "is-edtech-a-scam" >}}) whose name rhymes with _Flight-cat Goonier_. But something has changed: these programmes have now graduated from being suggestive to being passive-aggressive.

Today, I thought I would talk about the programme, having watched tens of videos of kids performing all sorts of unbelievable feats.

Of course, I have not met a mid-brain activation specialist. Perhaps I never will. (The programme is losing traction after some magician and free-thinker communities came down hard on it.{{< sidenote nilambur >}}"Mud Brain" Exposed @ Nilambur (Malayalam) By Magician R K Malayath ([Kerala Freethinkers Forum](https://www.youtube.com/watch?v=ZVDIEls9NxA)){{< /sidenote >}} Why magician communities? You will know in a moment.) I have never met a kid with an "activated" mid-brain. Again, perhaps I never will---spoiler alert: you cannot "activate" the midbrain.

Have I given it all away already? No. Read on to never get scammed by mid-brain activation specialists. For that, you must understand:

## What is the midbrain

The impression some proponents of midbrain activation give is making you think this is the "middle" between the left hemisphere and the right hemisphere of the brain. Think: left brain, right brain and mid-brain. The hyphenation adds further confusion and enables evasion when someone points out that the midbrain has nothing to do with processing (and they go, 'Oh, no. You are talking about a different part. We are talking about the middle of the two hemispheres, not the mesencephalon.' thereby seeming more technically sound.)

"Brain-balancing" is another term proponents of this idea use, to make you think that the program enables communication between the two hemispheres of the brain by "decalcifying" the Pineal gland, to bring the right balance between the logical-thinking left hemisphere and the intuitive right hemisphere of your brain. (No proof exists for correlation between left- or right-sidedness of the brain and personalities.{{< sidenote lr-brain >}}An Evaluation of the Left-Brain vs. Right-Brain Hypothesis with Resting State Functional Connectivity Magnetic Resonance Imaging ([PLOS](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0071275)){{< /sidenote >}})

In the biological sense, the midbrain is the part of the brain located right below your cerebral cortex, at the top of the brain stem. In more common, non-neuroscience terminology, this is the part where the spinal cord attaches to the main brain body.

But here is what is interesting: they are technically correct in saying that the midbrain is responsible for a lot of the conduction of sensory signals. In fact, the midbrain is responsible for most of the sensory signals like the auditory, visual, touch and pressure signals.

But the midbrain is not responsible for any of the _processing_ of these signals (except some of the reflexes---reflexes are like pre-programmed shortcuts, not conscious, logical processing). In a way, the midbrain is more of a router than the CPU. Which means that a midbrain-_deactivated_ person cannot get any of their visual or auditory signals processed, because the signals will not get transmitted in the first place.

In other words, if you are reading this, your midbrain is active.

## The hypernatural claims

I do not want to use "supernatural" here, because that would be inaccurate. But the claims are far from natural. Hence hypernatural, even though the term may not exist.

The claim made is that because of the kids’ ability to balance brain activity, they gain unusual abilities, like sensing colours through the touch, ability to see through visual barriers, etc.

{{< youtube tRP9Bgk790g >}}

This video shows the ability to read blindfolded. In other places, children rub the cards on their arms and tell its colour. Or bring the card close to their ear, tap on the card and tell the colour and the number.

Amazing, isn't it?

## The "debunk"

Most sceptics are quick to dismiss this as fraud. The counter-claim here is that the blindfolds they use are not real blindfolds, but those used by magicians, which have a way to, sort of, "open the blinds" within. This opening of the blinds allows the light to pass through the blindfold.

Others claim that the kids use the gaps between their noses and cheeks to read when blindfolded.

Someone even used an ophthalmologist's surgical patch to demonstrate that the midbrain-activated children fail to read or see when the patch is used instead of the blindfold. The reason is that the ophthalmologist's patch is opaque and a much tighter barrier around the eyes.

And to counter this claim, a franchisee came out and said that midbrain activation is not about reading blindfolded, but a mere demonstration of the fact that midbrain activation unlocks a lot of sensory abilities of children, of which blindfolded reading is one. He even applied an ophthalmologist's patch on the kid's eye, and the kid could read---the patch was even bigger and "more comfortable", though (ahem).

This is a rabbit-hole. Claims, counterclaims and claims that counter the counterclaims. Thankfully, this process seems to have ended in the mainstream, but still active in some pockets.

Regardless, what is important is the science behind something as such.

## The science

The most critical point here is that vision is a function of light (meaning, without light, there is no vision). Second, colours are a function of the wavelength of the visible spectrum of electromagnetic radiation (how complex and cool does that sound?).

In simple terms:

- we call electromagnetic radiation in the visible spectrum as "light";
- the frequency of the radiation determines the colour of the light.

When you see a green-coloured light, it means that the waves of the light you are seeing are within the range of frequency that makes it _seem_ green. And when you see an object as being green, it means that the object is absorbing all the light that is falling on it, _except_ the green range; the green range bounces back out. We, in turn, catch this bounced light and perceive the object as a "green object".

And do you know which part of our body receives light in all its glory to process it in the way that helps us _see_?

## The third eye

The eye receives light in all its glory to help us see. But then, some of the cleverer franchisees talk about there being a "third eye" in humans. They say that with the midbrain activated, people get the ability to see using their pineal gland, which is why they can read blindfolded. Some even cite a [PubMed Central article](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3398033/#:~:text=The%20pineal%20gland%2C%20which%20contains,1%5D%2C%20%5B2%5D.&text=The%20pineal%20photoreceptor%20cells%20function%20autonomously%20with%20strong%20rhythmic%20patterns.) to add credibility to their claim.

They also say that with the midbrain activated, their sensory pathways get "cleared", enabling them to sense colours and text using touch. My guess is that they imply that the lemnisci get "supercharged", which is what enables the sense of "super"-touch in them.

But is this true? Can humans indeed see using the pineal gland?

## The eye

Let us first understand how the eye works, in simple terms. An eye is much more than a white ball with a dark dot at the center. Here are the parts to note in this context:

{{< figure src="https://blogfiles.ramiyer.me/2021/images/eye-diagram.svg" alt="A 3D diagram of the eye (Source: Wikimedia Commons)" caption="A 3D diagram of the eye (Source: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Eye-diagram_bg.svg))" >}}

To describe how it works:

- Light falls on the cornea (the transparent part that covers the dark part of the eye).
- The iris (the pigmented muscular part of the eye right behind the cornea) flexes to contract or dilate the pupils (the "hole") to control the amount of light that goes into the eye. (The midbrain actually controls this function, by the way.)
- The lens converges the light to shrink the image enough to fit in the small area at the back of the eye.
- The retina receives this converged image:
  - The "rod cells" sense the intensity of the light (how bright / dark).
  - The "cone cells" react to the wavelength / frequency of the light (what colour).

Here are a couple of points that the advocates of the idea of seeing using the pineal gland forget:

1. The pineal gland is not exposed to the light outside; in reality, it sits within an opaque bone casing.
2. The pineal gland cannot "see" anything, let alone get the colours and focus images to be able to read. The focusing happens in the human eye because of the lens within; where is the lens in our pineal gland?

The term for the process of merely sensing the presence of light is "non-visual photosensitivity". An example of this process in the non-living world is your smartphone's photo-sensor that adjusts the brightness of the screen according to the light around. Or the sensor in your television that adjusts the brightness, or the sensor in your musical clock that adjusts the volume based on the ambient light. Or the sensor in the automatic tap in your bathroom.

The term for sensing the light along with forming an image is "visual photosensitivity". This is akin to the camera on your phone.

If you had a smartphone whose cameras did not work, it would still be able to adjust the screen brightness according to the ambient light. This is because the non-visual photosensor is different from the camera's sensor.

In the human body, the eyes are the camera---the visual photosensors. Our eyes form proper images apart from merely sensing the light (which is why this screen does not appear to you as a blotch of light, but as readable text).

The pineal gland in higher vertebrates (like humans) is not photosensitive.{{< sidenote "pineal-physiology" >}}Physiology of the Pineal Gland and Melatonin ([NCBI](https://www.ncbi.nlm.nih.gov/books/NBK550972/)){{< /sidenote >}} Even if it were, all photosensitivity is not visual ability.

The claim that humans can read visual text without using their eyes is false. If we could read visual text without eyes, we could ditch the Braille script for good.

"Then what about the skin sensing light?"

The skin can sense light; it has rhodopsin.{{< sidenote ultraviolet >}}Our Skin Cells Can 'See' UV Rays ([Livescience](https://www.livescience.com/16859-skin-senses-ultraviolet-light.html)){{< /sidenote >}}

"Okay, then we are good, right? The skin can sense light, which so far, we thought only our eyes could."

No. Sensing light is different from forming an image. Refer to the light sensor analogy above. Also, the skin can sense certain wavelengths of ultraviolet light; not visible light. And, think about it: when a kid rubs the card on his or her skin, no light falls on the side of the card touching the skin (because the card is face down). Photoreceptors cannot sense the colour of the pigment in the absence of light.

Those who claim that they can activate the midbrain of your kids tie all these concepts together like: Your midbrain is a the hub of audio-visual sensory activity (true). The pineal gland has been found to be photosensitive in many vertebrates like the zebrafish (true). Humans are vertebrates (true). Your midbrain contains connections that work with sensing touch and pressure (true). Your child has a lot of potential (true). Training her midbrain would give her just enough push; superpowers to make her learn quickly, including reading without seeing and reading an entire book in seconds (false).

The next time someone tells you something like this, tell them that zebrafish and humans are not the same class of vertebrates. And ask them to activate their own midbrains enough to be able to understand basic biological concepts.

## What if we don't know yet

A valid question. What if we do not know for sure that midbrain does not have these capabilities? What if we simply have not understood our midbrains enough?

In fact, for so long, we thought that no human organ other than our eyes had rhodopsin. Now we know the skin also has it. Is it possible that we can indeed activate the midbrain and gain such abilities as reading entire books in seconds without even having to open them? I think that would be a useful ability to have.

Yes, it is possible that we have not understood our midbrains enough.

But the claim that we can read books without even opening them has problems. Since we depend on light to read, seeing through books would be a difficult task, because, a thick book blocks all visible light (a book is opaque). We can read a book because light falls on the opaque (say, white) sheet of paper, which reflects most of the light. The (say, black) ink used to print is a pigment that absorbs most of the light, which means that hardly any light bounces off. This contrast enables us to read. If the book absorbed or reflected all light, no light would pass through it to reach our eyes. Even if we assumed that somehow some light passed through the white pages and got blocked by the black pigment ... well, try printing five sheets of transparency, stack them up, and try to read all the pages together. Vision does not work that way.

If they said that you can read the book by flipping through it in a couple of seconds, I think that is also a great ability to have. Except, that also has problems. We know that no part other than our eyes form a visual image. The chemicals in the retina of our eye react to the light based on the intensity and the wavelength to create signals that pass through the optic nerve to reach our brain, which processes this information. This reaction is reversible. And it takes the chemicals about a sixteenth of a second to return to the previous state (under normal conditions), to be able to react again. The midbrain has nothing to do with the rate of reaction of the chemicals in the photoreceptor cells of the retina.

A human being cannot read a 500-page book in a second and process it. Our vision depends on the reaction time of our muscles and the chemicals in our retina. Even if we assumed that a supercharged midbrain can process signals at rates faster than the speed of light, apart from Einstein having an issue with it, the other parts of the visual system will not co-operate---because they cannot.

Regardless, if "midbrain activation" is indeed a phenomenon, there must be a scientific explanation to it. Those who activate children's midbrains should approach the scientific community---not restrict themselves to non-peer-reviewed whitepapers---and demonstrate this phenomenon in the open. The scientific community can help find an explanation for it; after all, that is what we have been doing for thousands of years now. If midbrain activation is real, it could do a great good to humanity, if only we could rise above petty issues like children's score in schools.

But scientific analysis of anything involves testing---scientifically controlled, repeatable, falsifiable tests to show that something is true given a specific set of conditions. For example, we must be able to temporarily disable a person's eyes’ sense of light and see if the midbrain alone can "see". This means that the phenomenon of reading blindfolded should work irrespective of what blindfold one uses. Be it a cotton cloth, an eye mask, or an ophthalmologist's patch. Further, we must prove that the skull can indeed conduct light. That the skin and the forehead muscles can conduct light. That the brain that encases the pineal gland can conduct light.

If the vision does not depend on light, then, can the kids with their midbrains activated read in complete darkness?

Saying something like "it just happens" is unscientific.

What brings you to the conclusion that the midbrain can process something? How do you establish the idea of "brain-balanced" kids performing better in tests? What is your control?

Saying something is "beyond science" makes it unscientific. Such statements are for the mystics. Oh, some do say that we have been activating the midbrain since the ancient times. What is this testable phenomenon that people thousands of years ago understood but we cannot? Is it not a matter of shame that we have been able to explain every ancient scientific phenomenon except this? Let us test this out already!

If you are a parent, do not let these claims make you think that your kid will get superpowers. Holistic learning is not about using tricks to solve differential equations. Learning is not about memorising entire books.

The midbrain is not responsible for learning. "Midbrain activation" is a gullible person's lousy explanation to the simple tricks played by kids at best, or more realistically, a manipulator's trick to make kids think they have gotten superpowers, or worse, a scammer's way to teach kids to lie---and in the process, make easy money.

Also, great scores have only taken us so far.

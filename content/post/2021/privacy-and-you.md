---
title: "Privacy and You"
subtitle: "Finding a balance"
date: 2021-01-09T08:40:53+05:30
description: |
    Ever since Facebook decided to change its terms and conditions of the use of WhatsApp, regular users---those who did not care much about privacy until then---said they have had enough, and started switching to alternatives. Here is a little primer on how it all works.
tags:
- privacy
- tracking
- advertising
- internet
- open source
---

Online privacy is like civilisation. It takes time, but when it happens, it happens. The process is slow and arduous. While we do not push ourselves to such drastic changes, some events do push us. One such was that of Facebook deciding to force WhatsApp users to allow sharing data between WhatsApp and Facebook.

{{< toc >}}

## I have nothing to hide

Bold statement. Great.

Now, let us talk sense, yes? The statement above is a product of confusing privacy with secrecy. You see, little of your life is a secret. But a lot more is private. You do not walk around with a t-shirt proclaiming your PAN. You close the doors of your home and choose whom to let in. Why? Because your home---leased or bought---is your _private_ property.

Private information is something like that. Your tax receipts, your bank account statement, your phone number; these are not secrets, but they are private. Without a certain degree of protection, people can use these pieces of information against you.

## My way is convenient

Convenience and security are inversely proportional. The higher the security, the lower the convenience. What the masses use is often more convenient and less secure. This principle applies to privacy as well.

Businesses are here to make money. In the Information Age, data is money. Companies use data about you to serve relevant ads, and that is what the general understanding is. But it goes beyond that. If you see, companies like Google, Microsoft and Facebook have an eye on companies for acquisition. Fitbit is among the most recent ones to come under the wings of Google.

You may think, well, what could Google possibly gain from this?

Over time, you may have seen the shift from mere activity tracking to health tracking.{{< sidenote health-track >}}_Google Buys Fitbit For $2.1 Billion: Here’s What It Means_ ([Forbes](https://www.forbes.com/sites/davidphelan/2019/11/01/google-buys-fitbit-for-21-billion-heres-what-it-means/?sh=5e6d0240732f)){{< /sidenote >}} Wait, are they not the same? No. A health tracker can track far more: your blood pressure, mood shifts, stress levels, sleep and even menstrual cycle. Activity trackers do none of that. They measure how (and how much) you move, and perhaps how your heart responds to the movements. In a sense, activity trackers are "dumb"; they give the makers little useful data; the data they give is useful to _you_ because no one other than you can put it in context. Health trackers add the context as well.

But that is convenient. You know how well your latest morning walk route works for your weight and blood pressure. And you get all that information under one icon. Maintaining an Excel worksheet is so 2010.

But this has a flip side. Assume that the company that collects this kind of data (Google, Apple or anyone) either allows third parties access this data, or someone breaches their security fortress and lets out all the data. Imagine your insurance provider gets this data, sees a trend in your weight and stress levels, correlates that data with your past hospital visits, and increases your premium. It has never happened so far, but there is no saying it will never happen.

## What is the big deal

This post is about data sharing between Facebook and WhatsApp, which going forward, you have no say in. The question that I got asked when I said I was going away from WhatsApp was, 'Well, what is the big deal? WhatsApp encrypts messages end-to-end!'

Think again. If Facebook saw no big deal in a stupid app that lets you catch up with friends, sends and receives encrypted messages and displays no ads while charging nothing, why did it buy the app for 19 billion{{< sidenote fb-wa-acq >}}_Facebook to Acquire WhatsApp_ ([Facebook](https://about.fb.com/news/2014/02/facebook-to-acquire-whatsapp/)){{< /sidenote >}} dollars? Nineteen _billion_ dollars. Let that sink in.

Simple: WhatsApp aims to replace the good old SMS.

You would have services that send you messages, such as order updates, over WhatsApp. This is one of the reasons Facebook bought WhatsApp. In reality, SMS is government-regulated space, at least in countries like India. Telecom Regulator Authority of India has always had a say in this, including setting limits on the number of SMS messages that one can send in a day. Companies looking to use this have to clear regulatory requirements before getting the access to send bulk messages. Moreover, how do you buy SMS?

At most, you can create an SMS app, like [Messages](https://play.google.com/store/apps/details?id=com.google.android.apps.messaging) by Google or [SMS Organizer](https://play.google.com/store/apps/details?id=com.microsoft.android.smsorganizer) by Microsoft. I think Microsoft aces this space, and to make it sweeter, says that all processing happens on the phone, no link to the cloud.{{< sidenote sms-org-cloud >}}The app has been around for some time now, and Microsoft has not changed the no-cloud strategy. But we do not know how long they will keep it that way.{{< /sidenote >}} Facebook did indeed try to push [Messenger](https://play.google.com/store/apps/details?id=com.facebook.orca) as the SMS app, but nobody I know uses it for SMS. Also, all this is on Android, because, Apple does not let any third party touch SMS, citing privacy and security.

Facebook wants to be in this space, because transactional messages are critical in determining the user's financial profile and behaviour. This is why Google tracks your purchases via Gmail, even though (it claims that) it does not show you ads based on them.

A step in this direction is WhatsApp Business. Businesses can sign up for the platform and, if more than one individual are interacting with customers or businesses want automated messaging, they could go for the paid package where the businesses get access to the [WhatsApp Business API](https://www.whatsapp.com/business/api), using which they can automate messaging.

This is crucial information for Facebook, because it can tell who is buying what from whom, how frequently they have interactions, etc.

Also, WhatsApp Business is not restricted to businesses. Political organisations, activists, anyone with need to interact with hundreds, thousands or millions of users can use the platform. The _Cambridge Analytica_ scandal is a classic example of manipulation of users on the fence. All this power vested in a single large entity is dangerous. And Facebook is an empire.

And, do not forget the payment integration. Using UPI apps such as PhonePe and Google Pay we (users and businesses) give loads of financial data to the makers of these apps. If you combined messaging with payments (which Google and PhonePe have not excelled at yet because nobody sees the two as communication apps), user-base and data collection could skyrocket. It would be a business _ecosystem_ that nobody would want to leave.

## Facebook already knows me

Like a friend pointed out, Facebook already knows perhaps too much about each of us. Going by what we browse, which ads we watch without scrolling away and what products interest us is half the story. Where the real money goes is the more important half. We all have our #FoundItOnAmazon lists and those lists that have products we may never buy, but wish for nonetheless. No store makes real money from window-shoppers. For now, other than products that you buy on Facebook itself, Facebook knows you probably like something; it does not know what you actually buy, unless you review the product on the platform, claiming to have bought it.

This may change in the future. In other words, we will confirm Facebook's doubts. This will make for a more accurate profile.

Second is your contacts and whom you interact with. This information can help Facebook create a cohort. And anyone who has used any form of analytics can tell you how valuable cohorts are. These are microcosms of similar thoughts and behaviours. In short, this is valuable data.

---

Update (13/01/2021): WhatsApp put out a tweet stating that it _does not_ log whom you interact with, or share your contacts with Facebook. To be fair, it may be true that they don’t share this with Facebook ... yet. But as per the privacy policy, you allow WhatsApp to collect your contacts (of course, without this, the app will show messages against phone numbers instead of names, and information like “this person is not in your contacts” would be inaccurate) and data about whom you interact with.

First, communication log:

> Usage And Log Information. We collect information about your activity on our Services, like service-related, diagnostic, and performance information. This includes information about your activity (including how you use our Services, your Services settings, how you interact with others using our Services (including when you interact with a business), and the time, frequency, and duration of your activities and interactions), log files, and diagnostic, crash, website, and performance logs and reports.
>
> ---WhatsApp Privacy Policy

Next, contact upload:

> Your Connections. You can use the contact upload feature and provide us, if permitted by applicable laws, with the phone numbers in your address book on a regular basis, including those of users of our Services and your other contacts. If any of your contacts aren’t yet using our Services, we’ll manage this information for you in a way that ensures those contacts cannot be identified by us.
>
> —WhatsApp Privacy Policy

---

Does Facebook not already know whom you interact with the most? It has Messenger, it knows what you post, who likes your posts the most, whose posts you like the most and so on. It knows with whom you agree and disagree.

In reality, these are different worlds. I need not explain this; I will ask a simple question: Are those that interact with your posts or those whose posts you interact with the same people that you message on a regular basis? To me, that is a no. I have a set of close friends some of who do not even have Facebook accounts. Even if they did, some seldom log onto Facebook, others are those who see my posts and text me.

## WhatsApp was sharing data with Facebook anyway

WhatsApp has indeed been sharing data with Facebook all this while. Ever since the change in their terms some four or five years ago, WhatsApp has shared data with Facebook. But it worked a little differently. Until now, users had a say in whether they want their data shared with Facebook or not. We even had thirty days to change our minds when they introduced this new policy. I for one declined data sharing with Facebook because I saw no good coming of it.

New sign-ups to WhatsApp got a thirty-day window as well, to decide whether they wanted WhatsApp to share data with Facebook.

Up until now, we could say, 'No'.

Now, Facebook says, 'Well, too bad. If you want to continue using WhatsApp, you _must_ allow it to share data with Facebook.'

> If you use our payments services, or use our Services meant for purchases or other financial transactions, we process additional information about you, including payment account and transaction information. Payment account and transaction information includes information needed to complete the transaction (for example, information about your payment method, shipping details and transaction amount).  
> ---Facebook (via ThePrint)

While WhatsApp shared data with Facebook since 2016 or so, this is different in that:

1. WhatsApp will now share financial data with Facebook, including the transaction amount.
2. Users cannot opt out of the data sharing.

## Is it not easier to delete my Facebook account

At this point, that may be an alternative. But remember that WhatsApp collects a lot of 'data linked to you'. We do not know how the platform will use this data yet. Given the practices of the parent company, I for one, am not holding my breath.

Whether you have a Facebook account or not, the point now is that WhatsApp will use the Facebook infrastructure. For now, it will share data and perhaps do nothing else with the platform. But you still have your name and phone number stored with WhatsApp, and as per the new agreement, Facebook will track your financial information, including your shipping address. Facebook can tie all this into a single identifier. Without a Facebook account, Facebook now has your name, phone number and your address, along with what you buy, how frequently you buy and whom you buy from. Congratulations, you now have a Facebook account (but you cannot log into it to manage it).

To me, that is a losing proposition and a bigger concern. I would much rather have an account that I can manage (in whatever little way possible).

And what I cannot manage is a WhatsApp account, because it offers no data controls---not yet, anyway. With your WhatsApp and Facebook account linked, you _may_ be able to manage _some_ of your data.

That said, there is no guarantee that Facebook does indeed delete data you ask it to. Which means, yes, deleting your Facebook account may help, but not much because Facebook will get other important data from WhatsApp which it can use to serve ads and do anything else that it does.

The ideal way is to not have either account.

## In conclusion

As software engineers, we look for avenues to improve people's lives. As advocates of free (as in speech) and open software, we do not like this "take it or leave it" attitude; we always choose the latter because we know we can do better.

Secondly, users’ data is no company's commodity. The user must have a right to choose how someone handles their data or data about them---more so when the product or service comes with 'no guarantee whatsoever'.

This is not about showing a company its place. This is about taking control of your data and data about you. Weigh the value you receive in return for your data. If the deal is not good enough, walk out. Do not hold on to toxic relationships.

We have a handful of alternatives for such services out there. Privacy does sometimes come at a cost. Living in the open, exposed to the elements does not need any money; you need the ability to survive. If you need clothes and a shelter to protect your privacy and what you value most, you must pay for it. Experience tells us that we can trust no single business with what we value, because businesses are here for business, and their priorities may be different from yours. This is why I support Free/Libre and Open Source Software (FLOSS). The community drives them. Most of these run on donations and grants. Here are some privacy-friendly alternatives I use on a regular basis:

1. [Pop!_OS](https://pop.system76.com/) as my notebook OS (open source)
2. [ProtonMail](https://protonmail.com/) for email (open source)
3. [Signal](https://www.signal.org/) for messaging (open source)
4. [Firefox](https://www.mozilla.org/en-GB/firefox/browsers/) browser (open source)
5. [ProtonCalendar](https://calendar.protonmail.com/)
6. [Cryptee](https://crypt.ee/) for notes (open source)
7. [DuckDuckGo](https://duckduckgo.com/) and [Qwant](https://www.qwant.com/) for search
8. [Plausible](https://plausible.io/) for analytics (open source)
9. [Wire](https://wire.com/en/) for video calls and conferencing (open source), planning to try [Jami](https://jami.net/) (also open source, and peer-to-peer)
10. [Hugo](https://gohugo.io/) for blogging (open source)
11. [Home Assistant](https://www.home-assistant.io/) for home automation (open source, I use it without cloud, on the local network)
12. [Zoho](https://www.zoho.com/) for email on my domains

If you do try any of these out, and like them, please also consider supporting the open source ones either financially or through contribution to code or documentation.

Like [I have been saying]({{< ref "better-online-privacy-part-ii.md" >}}), it does not make sense to get rid of the Big Five (GAFAM), but we sure can reduce their control over the data about us. Also, none of the above asked me or paid me anything to list them out.

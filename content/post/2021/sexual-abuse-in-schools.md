---
title: "Sexual Abuse in Schools"
subtitle: "looking beyond mud-slinging and finger-pointing"
image: "https://blogfiles.ramiyer.me/2021/covers/sexual-abuse-in-schools.jpg"
date: 2021-05-26T20:13:53+05:30
description: |
    News broke out about sexual harassment of children in PSBB and other schools in Chennai after an alumnus started an Instagram reach-out. Soon, the issue got cluttered. We look at the different aspects of the issue and the way forward.
tags:
- social
- society
- psychology
- justice
---

Yesterday, I woke up to the news that students of a top school in Chennai---_Padma Seshadri Bala Bhavan_ (or PSBB)---had alleged that a teacher had sexually abused them.{{< sidenote psbb-news >}}PSBB teacher accused of child sexual harassment, school accused of inaction ([The News Minute](https://www.thenewsminute.com/article/psbb-teacher-accused-child-sexual-harassment-school-accused-inaction-149418)){{< /sidenote >}} The piece of news shook me because my cousin studied in the school, and I could personally relate to it. The teacher also taught at the same branch that she studied in. But as usual, the issue received a polarised public response.

But let us take it piece by piece. The first was that a political group had somehow made this a "thing". In a matter of hours, this issue became more cluttered than what is conductive to a sane investigation.

Let us handle these one at a time.

{{< toc >}}

## Those in support of the school

When I say this, I mean mothers and fathers of the students. From what I heard, some went into denial. The parents who went into denial need to understand that this issue is not about the school per se; this issue is about a teacher. You have no obligation (real or imagined) towards the teacher, to deny allegations by the students.

Also, alleging a teacher sexually harassed someone does not mean the school did. Please stop connecting people with institutions. A teacher is not the same as a school, a priest is not the same as the church, a prime minister is not the same as a nation. The teacher, the priest and the prime minister are positions of responsibility; the institutions are different.

And when you understand the difference between a position of responsibility and an institution, also know that a person is a person, s/he does not represent a position of responsibility. For example, Mr A R Rahman does not represent music composers, Mr Narendra Modi does not represent prime ministers, Mrs Y G Parthasarathy does not represent deans.

On that note, this specific allegation of sexual abuse is against Mr Rajagopalan. Not PSBB or the teachers of PSBB.

## Sexual abuse in general

What surprised me was one of the alleged messages by a student/alumnus about another teacher in another school of the same group:

> And [another teacher] used to deal with kids from age 10-14 than older ones. Imagine … What sucks is we didn't even know it was wrong at that point because abusive/toxic/patriarchal behaviour was something even the female teachers practised. Slapping, body shaming, slut shaming for having male friends was normalised.

I suppose this person is talking about non-sexual abusive behaviour when they say they did not know such an act was wrong. Because my sister---when she was in the first grade---learnt about "good touch and bad touch" from the school. While this does not absolve the school of its responsibilities, saying that the students _did not know_ what was wrong seems odd.

Now, does that put the blame on the children? No. Read on.

## The environment

Children are children. No matter how much a parent tells their child to treat them as a friend, or how much a teacher tells their student to treat them as a confidant, children will not feel comfortable sharing such incidents until they feel comfortable. I have had friends tell me about how someone abused them on the way back home from school, or on the bus; they well up. Sharing details of such incidents is hard.

And predators know this well.

What matters is that we---as a parent, sibling, friend, teacher, spouse, whoever---be supportive of those who share such experiences. What happens in our society is the opposite:

{{< youtube HQgcGgDoefw 712 816 >}}

The video above got widely circulated after the incident. For those who do not know the world of Tamil cinema, the woman talking is Sowcar Janaki, a well-known actor. The man is Y G Mahendran, also an actor, son of Y G Parthasarathy and Mrs Y G Parthasarathy (founder of PSBB).

If Tamil is not among the languages you understand, here is what the video says:

> YGM: You said you did not lose your individuality. This is what inspires the artistes of today, 'Be like Sowcar Amma.'  
> SJ: Really, do they think so?  
> YGM: Yes, of course. That poise. "Hold your own in a man's world." What is your advice to them? Because, you see, there is a lot of talk, 'We are being harassed.' You see a lot in the papers.  
> SJ: That is another, that "Me Too!" is something which is bothering me, I tell you. Such a lowly act for publicity. Something that happened some day, or did not happen, or should have happened---  
> YGM: [Chuckles]  
> SJ: Something that you agreed to, but turned out differently. Is it necessary to bring it all out? It suited you then, you kept quiet. Now someone said "Me Too" in Hollywood or Bollywood, isn't it disgusting? Whom does it hurt? It hurts your family; if you have a husband, it hurts him; if you have children, it hurts them. Life should be under wraps. You are hurting everyone's life---  
> YGM: Beautifully put.  
> SJ: It's bad for everybody. Ever since this Me Too business, I stopped watching TV or reading print news. What is this kind of woman today? What is the big deal, what do you aim to prove? Someone stalked you, caught you by your arm, groped you on the bus … what happens to your reputation as an individual after you bring this out in public? What are you gaining from it today? Something that happened twenty years ago---  
> YGM: Exactly.  
> SJ: I stand for women. I am a feminist. But this I will not accept.  
> YGM: Beautiful.  
> SJ: This is rubbish. It is crap.

I do not mean to show the video in connection with the issue (barring the coincidence that the man is the son of the founder of the group of schools). The point here is the attitude of the society.

Also, ma'am, with all due respect, you may be (or have been) a women's rights activist, but going by what you are saying, you are not a feminist (or you have a serious misunderstanding of what feminism stands for). Also, you are a lousy women's rights activist. No, I am not a feminist; I talk from the technical standpoint of the definition of feminism. You will know where you are wrong when you get the nuances. Read a little.

## Community preference

An allegation made on the school is that they prefer students of a specific community. This is a red herring. I am going to steer clear of this for a simple reason that this dilutes the issue at hand, and distracts us from it. That topic is for some other time.

To recap, the issue is sexual harassment of students by teachers.

## Complaint and Cognisance

The first statement that the school management made was that they were unaware. In their second statement, they said that no one had given a written complaint about this. The difference is subtle, but significant. Should a school that has "zero tolerance" towards such issues wait for a written complaint? How could the school take "suo motu" cognisance of this issue now, without getting a written complaint?

Or was it indeed suo motu, given that the alumni had written to the management about this after the social media blow-up? Or does it take a social media blow-up for the school to take suo motu cognisance of such issues? Something does not add up.

_Padma Seshadri_ is not alone. Sexual abuse of children is a global problem. Schools are a place where children spend a quarter of their day, which makes schools a haven for predators. Singling out one school and putting pressure on them alone does not help. In the days that followed, students of other prominent schools, such as Chettinad Vidyashram, Kendriya Vidyalaya (CLRI), Sushil Hari School, Sastra University, etc., brought out the issues in their schools and colleges. If this caught on the way #MeToo did, we would see more students coming out and sharing their experiences.

Sure, a handful of these may be "overreactions" or "misunderstandings" or "misplaced retaliation", but what about the rest? Are they all fake?

Paedophiles exist. They have existed for centuries, perhaps. Our society may be realising it now, but other societies---specifically the western ones---are serious about it, and are taking steps in that regard.

'Okay, how do you separate fact from fiction? How do you know which of these allegations are indeed true?'

Well, we do not have a well-defined path to finding that yet. But we for sure are forging one. This has been a grave issue buried under the carpet for too long; now that we are starting to see this monster emerge, we are finding ways to deal with it. Other social issues like education, discrimination based on caste, etc., are old ones on which work has been going on for ages, and yet, the system is imperfect. Child sexual assault (or sexual assault other than forced intercourse) is something we have started to recognise in the recent past. Forming a framework will take time. We cannot leave it alone merely because the framework is imperfect.

In fact, rather than saying "imperfect", we should go with "maturing".

## Why now

One question that riles me up when such issues come out is, 'Why now, after all these years?' Singer Chinmayi faced it, and so did other wo/men who had to suffer in silence.{{< sidenote chinmayi-why-now >}}Shame, Guilt, Mudslinging: Survivors of Sexual Assault are Hounded by Fears before They Come Out ([News18](https://www.news18.com/news/buzz/shame-guilt-mudslinging-survivors-of-sexual-assault-are-hounded-by-fears-before-they-come-out-3529544.html)){{< /sidenote >}} I did not have a concrete answer to this other than 'How does it matter?', and 'The court said it.'

Allow me to explain. Sexual assault is a complicated issue. The perpetrator hits so close to your core that you get shaken to say the least. You blank out, whether you are a woman or a man that faced abuse. Women get shaken because the world (the real world) trivialises these issues to a great extent. The police refuse to take complaints; the society in general thinks that the victims are somehow inferior to the other "goddesses" out there; virginity, for some reason, gets a sacred status (accepting that someone made sexual advances on you, undermines this "sanctity"), etc. Men get shaken because the society does not understand that men also face sexual abuse. The society thinks this somehow makes a man less manly, or that men always enjoy sexual advances made by women.

This discourages victims from coming out about their experience.

Movements such as #MeToo give the victims a voice. The movements help them realise that they are not alone. When they see thousands of victims coming out, they feel they could share their experience as well.

I asked a friend what she thought about this question, and she said:

> I think there's 3 reasons why someone who's been a victim of sexual assault doesn't come forward in most cases.
>
> 1. For women, it's because the justice system in India (you'd know from the news) and abroad (ref Chanel Miller case), the emotional cost of pursuing legal action is too heavy. Victim blaming and Red Tape abound.
>
> 2. Socially, the victim becomes a pariah and their job prospects are affected too, more so if they happen to be in the entertainment industry. This is gender agnostic too - even a man claiming assault is unlikely to be taken seriously and is shamed for it.
>
> 3. The culture is rigged to favour men. Especially cis-het white men. Even if the victim wins the case, they've already lost time, reputation,  and will have to deal with the trauma for the rest of their lives. Fighting for justice is an exercise in futility unfortunately.

She then went on to say:

> Bringing it up then would not have changed anything. The ecosystem was crueler, victims were alone. There was a severe lack of social security then. And trauma is complicated, some people don't feel ready to talk about it for years.
>
> Bringing it up now might [change something, because], there's large numbers, and less fear of persecution.

I do not think she knew about this case. But she pointed out a pattern:

> Now, when there's a movement like #MeToo … the victims probably feel like it'll tip the scales. But here's the thing, you can't fight centuries worth of patriarchal conditioning with one / two movements. Once a movement like this goes from becoming a niche thing to mainstream media, politicians and other powerful actors use it for their gain.

And so comes:

## The political angle

Soon, Kanimozhi Karunanidhi joined the movement. And some parents alleged that the Dravida Munnetra Kazhagam (DMK) government was retaliating against the school's opposition to a change in syllabus almost a decade ago that the then TN government (led by M Karunanidhi) had proposed. The present allegation was that the DMK government (now led by M K Stalin), was harassing the school. The other allegation was that this was part---in my friend's words, "of a larger scheme"---of spreading a narrative against a certain community. Who better to tell us than Dr Subramamanian Swamy:

{{< tweet user="Swamy39" id="1397355696094543873" >}}

Is there a political agenda? Perhaps; perhaps not. But does that mean we leave this matter alone? No. Is the school responsible for what happened? One must question their negligence, but the school did not _cause_ this per se. Should the school spring into action (other than merely suspending the teacher) and do the right thing? Yes; why is that even a question?

Any action taken on paedophiles like Mr Rajagopalan must deter other predators from harassing more children. The schools must act. These paedophiles have been harassing children for decades.

And irrespective of the identity (religion, caste, etc.), the graphic accounts shared by the students and alumni are disturbing beyond expression. My ability to write this piece in a sane language, despite reading all about this, is shocking to me. Perhaps this is an ability we gain over time. Perhaps my friends’ experiences have desensitised me. Perhaps we chug on, looking at the bright side, telling ourselves to be positive, as each scar makes us stronger. Perhaps we think that we will be better parents.

But what about the children of today? They are going through unspeakable pain, doubt and oppression in the hands of such people who personify perversion. The predators understand the relationship that the children share with their parents.

While the fact that students are now bringing this out in the open seems encouraging, those like Kripali having to hide the identities of those confiding in them speaks a lot about the attitude of our society towards such issues. Children are nowhere near safe.

## How predators work

I thought it important to throw some light on how no one other than "that one child" in a class gets treated differently.

Not all predators are the same. Some may be open about their actions, like some "Vulgar Varadarajan" of Sastra University and the sports coach at Chettinad Vidyashram. Most predators are selective. They have a pattern. They pick people of a certain kind, and this "kind" is as arbitrary as it gets. This means that out of fifty children in a class, the predator may pick one; out of seven hundred students, s/he may pick one.

The child will first feel nothing odd; s/he would feel that s/he is getting some special attention. The paedophiles establish a level of trust, after which, their behaviour changes. But it would be too late for the child to see the oddity. The predatory behaviour would start to appear, which would initially seem normal to the child. But once the child realises what is going on, s/he would feel overwhelmed. They would not understand what they feel, except something being "terribly wrong". Then the phase of self-doubt would begin, and they would feel that they are facing this alone. This is partly because the predator would not behave the same way with others, and others would not believe the victim's claims.

The child would then try to confide in their parent. But as seen in the cases that did come out, the parents either do not trust their child, call it all "imaginary", do not have the strength to take on the predator, or feel that their child would lose face in the society if this came out.

Ask any psychologist and they will tell you the same. (Of course, they will have much more insight into the subject.)

"I called (insert name here)’s parents, and spoke to the children also. They said they saw nothing odd about the teacher." Of course they said that. This sort of single-call-investigation does not cut it. We are talking about thousands of children. Paedophilia is everywhere, hidden in plain sight.

## The legality

Two bodies can work with such complaints: The Internal Committee of the school and the Law Enforcement. The Internal Committee comes under Prevention of Sexual Harassment (POSH). This is to ensure protection against sexual harassment, for those above 18 years of age; not children.

The Protection of Children from Sexual Offences (POCSO) comes into play in case of children. This Act came into being in 2012, and takes into consideration any sexual offence against children starting when it got enacted. Offences such as those happening at PSBB come under POCSO.

Anyone---including the alumni of the school---can bring such issues to the notice of the management. The management must then file a police complaint in this regard, and start the internal as well as legal proceedings.

## The image

Most organisations do not want such issues to escape their walls. Organisations feel that acknowledging the presence of a perpetrator is a sign that the organisation is an unsafe place, and is "bad PR". The opposite is true, though. Organisations making something like this a serious issue will deter perpetrators from trying anything funny.

When an organisation tries to protect such perpetrators, they become bolder and continue their harassment.

Writing words like 'zero tolerance policy' does nothing to stop these predators. A policy that is on paper but not in action is dead policy.

## The way forward

Organisations nowadays hire behavioural experts to analyse someone's behaviour before they join the organisation. I have met people who run organisations that specialise in behavioural psychology, that profile candidates who have applied to join the organisation by conducting interviews. These interviews bring out traits that one would not see in a person in general. Perhaps having applicants go through such interviews is a good starting point.

Second, organisations need to mention a sexual offence as a sexual offence. I know that organisations let go of a proven sexual predator under the garb of underperformance, as Chinmayi rightly pointed out in one of her tweets. Mentioning a sexual offence as one in the termination order would deter people from making such advances. They would know that their career as they know it, would end. Otherwise, the person gets laid off as an under-performer, finds a job elsewhere, and starts harassing people in the new organisation. Of course, this will not stop all sexual crimes. Nothing is airtight. But any deterrence is good enough for now.

Third, we need a culture of transparency. Parents should know what is going on in the school in which their child studies. The schools should be open about issues that they are facing. Rest assured that no school is perfect. Every school is going through every problem that every other school is going through. Schools must get together to solve such issues. These issues are beyond a school's vision; this is about the future of humanity.

Fourth, parents must know that making their child suffer in silence is the apex of selfishness---that their name does not get tarnished while getting their child justice (I acknowledge that "justice" is a rather broad term here). Those who understand your child's situation will support them. The others are not worthy of being part of your child's life. End of story.

---
title: "Are We an Independent State"
subtitle: "and if so, when did we attain independence?"
date: 2021-11-14T08:17:44+05:30
description: |
    The last couple of weeks were busy with two women of our country questioning our independence and the struggle that led to it. One questioned our independence itself, claiming that we were on a 99-year lease from the British Crown, while the other questioned the freedom struggle, calling it "bheekh" (alms), and that we gained freedom in 2014. In this post, we look at the facts that these two fellow citizens seem to have misunderstood or maliciously ignored, depending on how you see this.
tags:
    - democracy
    - law
    - religion
    - history
    - constitution
    - India
    - whatsapp university
    - politics
    - propaganda
    - nationalism
    - fact-check
    - freedom
    - accession
    - diplomacy
    - geo-politics
---

Political fads come and go. More so when the elections are around. National level fads peak at the time of the Lok Sabha elections. Having said that, they are not confined to those times.

Before we begin, let me state that this is not a political post, even though it starts with the word "political". This post is about a political phenomenon. Also, this post does not state the obvious for most people, because history from a couple of centuries ago has taken a back seat in our heads (as is evident by some of the responses that I saw on social media).

{{< toc >}}

This post is about something deeply disturbing, because, one, it insults people's intellect, and two, it feels like an attempt to create a larger, malicious narrative. That is crossing the proverbial line, and one must check it. All is not fair in politics. Like Himmat Singh says in _Special Ops 1.5_, 'It hurts when the country suffers because of politics.'

## What happened

I talk about two instances in this post. First, what Ms Ruchi Pathak said about India being on a 99-year lease from the British Crown. Second, about what Ms Kangana Ranaut said about the Indian Independence. (Which begs a larger question, what is with attacking the Indian Independence these days?)

Let us add some information to work with this narrative, because if I may state my personal view on this, the direction that this narrative is taking is worrisome.

### "You took back India on a 99-year lease from the British Crown"

Here is a good picture (beyond the memes) of what the actual issue is. _The Lallantop_ organises a programme called _Aḍḍā_. The video of this programme got published on the 26th of October, 2021. In this video, someone called Ruchi Pathak, during a discussion about privatisation, appears to have gotten mixed up in a rebuttal by a Congress activist (?), to which, she responds, 'You (Indian National Congress) did not get complete freedom for India from the British. You merely took it on a 99-year lease. [Translated and paraphrased.]' Here is a short clip of the statement she made:

{{< youtube fhdQBggGKrY 42 72 >}}

Of course, social media erupted afterwards. People started creating memes and trolling the woman for what she said. Trolling is not the way to go. I thought I must look for the context. And in a fair conversation, one must get the chance to explain themselves.

Another YouTube channel, _Suno Sarkar_, interviewed Ms Pathak, asking her if she had an explanation to what she said. And here is that video:

{{< youtube QZIz571aAg4 124  >}}

For those who do not speak or know Hindi, she says, 'I said that Nehru did not get us independence, in that he was not solely responsible for it.' She then goes on to say that someone put up an RTI application, asking for the following:

> Subject: To get information under the RTI
> 
> Kindly provide the following information:
> 
> - When did India become free?
> - From whom did India become free?
> - Who governed the country on 14 August, 1947?
> - Who governed the country on 15 August, 1947?
> - Who governed the country on 25 January, 1950?
> - Who governed the country on 27 January, 1950?
> - When did the British rule begin?
> - Who declared the British the rulers of India?
> - On what day did the British rule end in India?
> - Do the (current) British or their heirs have any authority to rule or govern India? If yes, then what authorities do they have? And on what basis?

She then says that nobody responded to the RTI application. (In reality, as you can see in the video, the different departments were shuttling the application, as is usual with the government machinery.) She then brings out another application (perhaps) requesting a response for the original application. And then, she talks about another letter by the Ministry of External Affairs, Government of India, which says the following:

> Government of India,  
> Ministry of External Affairs.
> 
> New Delhi, the 10th November, 1953
> 
> To
> 
> Shri Ram Narayan,  
> Rohtas Printing Press,  
> Rohtak.
> 
> Subject: Status of Indian citizens under British Nationality Act, 1948
> 
> Dear Sir,
> 
> With reference to your letter dated the 5 September, 1953, on the above subject, I am directed to say that under Section 1(1) of the British Nationality Act of 1948, a citizen of India is also a British subject for the purpose of British law. This position has remained unchanged even after India became a Republic.
> 
> Yours faithfully,  
> \[Signature\]

After which she says,

> देखिए, मैं तो यही चाहती हूँ कि ऐसा कुछ नहीं है, तो government राजपत्र पर या अपने letter pad पर लिख कर दे दे, कि यह जो भ्रम फ़ैला हुआ है आम जनता में, यह दूर हो जाएगी, ‘भाई, हम completely आज़ाद हैं, हम कोई British subject नहीं हैं, न ही उनके rules हमारे ऊपर apply होते हैं’, तो ये सारी चीज़ें clear हो जएँगी, और यह RTI का जवाब दे दे; इनके जवाब क्यों नहीं दे रही है government?

When the interviewer points out that none of these documents say anything about the lease, she responds (notice the discomfort on her face), 'But they do not say that we are fully independent either.' The interviewer nods without explicit rebuttal (although he later says that he does not agree with her on that we are not fully free).

### "Independence without a war"

Moving on, a couple of days ago, Ms Kangana Ranaut, during a programme called the _Times Now Summit_, stated that we got our Independence in alms. She asked:

> आज़ादी अगर भीख में मिले, तो क्या वह आज़ादी है?

She later made a long post on her Instagram story,{{< sidenote kr-story >}}Kangana Ranaut defends her '_Independence was bheek_' remark, claims will return Padma Award if proven wrong! ([Zee News](https://zeenews.india.com/people/kangana-ranaut-defends-her-independence-was-bheek-remark-claims-will-return-padma-award-if-proven-wrong-2410119.html)){{< /sidenote >}} explaining her stand. Here is what I think she means:

1. We did not fight a war with the British to take back power, but instead---she claims---"begged" for it.
2. We were not fully free (in the emotional sense) because we were (apparently) ashamed to follow our own principles.

Now, let us try to take these loaded statements piece by piece. If you have the patience to read, I admire your inquisitive attitude; if not, I am no one to judge you. We are a free nation, and you have the right to your opinion.

## On a lease?

First, here is what the Preamble of the Constitution of India{{< sidenote constitution >}}Constitution of India (Source: [Legislative Department, Government of India](https://legislative.gov.in/constitution-of-india)){{< /sidenote >}} says:

> W{{< smallcaps "E, THE PEOPLE OF INDIA" >}}, having solemnly resolved to constitute India into a {{< smallcaps "SOVEREIGN SOCIALIST SECULAR DEMOCRATIC REPUBLIC" >}} and to secure to all its citizens:
>
> JUSTICE, social, economic and political;
>
> LIBERTY of thought, expression, belief, faith and worship;
>
> EQUALITY of status and of opportunity;
>
> and to promote among them all
>
> FRATERNITY assuring the dignity of the individual and the unity and integrity of the Nation;
>
> {{< smallcaps "IN  OUR  CONSTITUENT  ASSEMBLY" >}}  this  twenty-sixth  day  of  November,  1949, do {{< smallcaps "HEREBY ADOPT, ENACT AND GIVE TO OURSELVES THIS CONSTITUTION" >}}.

Of course, before the Forty-second Amendment, the first line of the Preamble said, "We the people of India, having solemnly resolved to constitute India into a sovereign democratic republic …"

And I want to bring to your attention the word "sovereign". That one word is enough to quash the claim that Ms Pathak makes in her statement.

"But, we gave ourselves our constitution in 1950, and the government did not send a response to the RTI. Does this inaction by the otherwise hyper-active, efficient government not nullify the Constitution? Surely this silence is a sign of hiding the truth from the people." Someone please call Mr String. This is a conspiracy right here! It needs uncovering.

Here are some excerpts from the Indian Independence Act of 1947{{< sidenote iia1947 >}}Indian Independence Act 1947, as originally enacted ([UK Legislation](https://www.legislation.gov.uk/ukpga/Geo6/10-11/30/enacted/data.pdf)){{< /sidenote >}}. This Act _substitutes_ provisions made in the Government of India Act of 1935, "to provide for other matters consequential on or connected with the setting up of those Dominions".

And to quote:

> 6 Legislation for the new Dominions
>
> (1) The Legislature of each of the new Dominions would have full power to make laws for that Dominion, including laws having extra-territorial operation.
>
> …
>
> (4) No Act of Parliament of the United Kingdom passed on or after the appointed day shall extend, or be deemed to extend, to either of the new Dominions as part of the law of that Dominion unless it is extended thereto by a law of the Legislature of the Dominion.

As per the definition in the Act, "appointed day" here means the 15th of August, 1947.

In simple English, this means that no law passed by the British Parliament will apply to the Dominions unless the Dominions themselves decide to accept it. That section also goes on to say, later, that no law passed by the Legislatures of the two Dominions would stand null if the law contradicts with the law passed by the British Parliament, starting the 15th of August, 1947. In other words, this explicitly severs all ties with the British Parliament, and the British Law.

Moving on to Section 18 of the Indian Independence Act of 1947, it says:

> (4) It is hereby declared that the Instruments of Instructions issued before the passing of this Act by His Majesty to the Governor-General and the Governors of Provinces _lapse as from the appointed day_, and nothing in this Act shall be construed as continuing in force any provision of the Government of India Act, 1935, relating to such Instruments of Instructions. [Emphasis mine.]
>
> (5) As from the appointed day, so much of any enactment as requires the approval of His Majesty in Council to any rules of court shall not apply to any court in either of the new Dominions.

Now, coming to the Constitution of India, let us focus on one word:

> Sovereign
> : possessing supreme or ultimate power.

If you read the entire Preamble of the Constitution of India, you will understand that we the people of India have ultimate power over ourselves, and nobody---nobody---has the power to dictate how we operate within our own territory. This is precisely why we are able to call something an "internal matter" and stop others from interfering. This is why even the United Nations cannot tell us what to do with matters such as the Kashmir conflict. This is why India and Pakistan can both say, "We got this; please do not interfere in what is our internal matter. We will sort this out among ourselves."

Also, please read Section 395{{< sidenote constitution-395 >}}Constitution of India (Source: [Legislative Department, Government of India](https://legislative.gov.in/constitution-of-india)){{< /sidenote >}} of the Constitution of India:

> 395. Repeals.—The Indian Independence Act, 1947, and the Government of India Act, 1935, together with all  enactments  amending  or  supplementing  the  latter Act,  but  not including  the Abolition  of  Privy  Council Jurisdiction Act, 1949, are hereby repealed.

Someone put it beautifully: What is more free for an erstwhile colony than being able to constitute a Legislature which repeals a law that the British Parliament enacted?

Let us revisit this. First, the British Parliament enacts the Government of India Act of 1935. Then, the British Parliament enacts the Indian Independence Act of 1947, making provisions for the consequences of the independence of India (and Pakistan), in which they say that the British Law then in effect in (British) India will continue to apply to the two Dominions until the respective Dominions form their own Constituent Assemblies and set up their own Law. The Constituent Assembly gets formed in 1946 and works on drafting the Constitution of India; Article 372 of the Constitution of India contains the provision to continue the British Law in effect at the time, until the commencement of the Constitution of India (so that the Law mentioned in the previous sentence allows for continuation of the law until required). In 1950, we the people of India (whom the Constituent Assembly represented) enact and adopt the Constitution of India, which has a provision to repeal all the laws enacted by the British Parliament and take over full control of ourselves and our nation. This way, there are no gaps which can create a conflict. For example, no one can come into India between 1947 and 1950 and claim, "The British Law ceased to exist in 1947, and you do not have your Law until 1950. Meanwhile, I take over."

In effect, these two Sections freeze the British Law as on the 15th of August, 1947, and in 1950, we repeal it and take over control. This way, there is no funny business between 1947 and 1950.

## But what about the Queen of England not travelling with a passport

Simple: In case of England, the people of England get a passport in the name of the Queen of England. What sense does it make for the Queen of England to give herself the passport and travel in her own name? Why go through all the trouble?

This is what the Ministry of External Affairs means when it quotes the official website of the British Monarchy.{{< sidenote queen-passport >}}Passports \| [The Royal Family](https://www.royal.uk/passports){{< /sidenote >}} The Queen of England does not have a passport; she can travel the entire world without a passport.

If Nithyananda manages to create _Kailasa_ as a sovereign state with all powers resting in him, he would not need a Kailasa passport to travel to other countries.

Heads of State of India need a passport to travel because India is a republic; here, the people of India are the ultimate power, and the passport gets issued in the name of the _Republic of India_. Everybody in India---except nobody---needs a passport to travel out of India, unless the destination country says Indians need not carry an Indian passport when entering its territory.

## What about the "British subject" issue

First of all, the Ministry of External Affairs sent this letter in 1953. The Citizenship Act came into effect in 1955 (two years after the quoted letter). Besides, a citizen of India is a British subject "for the purpose of British law", but the British law does not apply to us (read above).

To understand what this means, you must understand the concept of Continuity of Law. Although that is a subject out of the scope of this article, the gist is that laws must continue to apply through parliaments and through years. We can amend and repeal laws, but it does not make sense to ask another sovereign state to amend all its laws, all the wordings, as a consequence of a change such as independence from Colonialism---when unnecessary. That is not how the law works. Regardless, it does not matter what a country's law says about another sovereign state---it does not matter what the British law thinks of Indian citizens, because India is a sovereign state.

Also, because of that, we still need a passport to travel to England. My friends and relatives, who are Indian citizens, have lived (or live) there. All with an Indian passport. None of them is a "British citizen".

And why does the government of India have to issue a notice on their letterhead saying we are not British subjects? The Constitution of India already does that. Read the Constitution of India to reassure yourself. If you are lazy, read its Preamble.

As for the other RTI application demanding answers, I think I can do that myself, as much as anyone who has studied Social Sciences in school:

_When did India become free?_

Assuming the question is about the Colonial rule, India became free on the 15th of August, 1947. For more information, refer above.

_From whom did India become free?_

India became free from the British rule on the 15th of August, 1947.

_Who governed the country on the 14 August, 1947?_

The Interim Government,{{< sidenote interim-govt >}}India's first government was formed today: All you need to know ([India Today](https://www.indiatoday.in/education-today/gk-current-affairs/story/interim-government-260823-2015-09-02)){{< /sidenote >}} formed in 1946 was in effect the "government in power", although, the head of state was the Monarch of Britain.

_Who governed the country on the 15 August, 1947?_

The Indian Dominion was an independent Dominion on the 15th of August 1947. The Interim Government governed the country. This Interim Government had the Viceroy's Executive Council, which was the "Executive" in the government. The Constituent Assembly existed at the time, which was the "Legislative" wing of the Interim Government. Since we were not yet a republic, the Monarch of Britain still acted as the head of state. His appointee, the Governor-General of India, exercised the powers of the Monarch. British law applied to the Dominion of India as of 15th of August, 1947 (read above for the explanation on continuity of law).

_Who governed the country on the 25 January, 1950?_

Same answer as above, except that C. Rajagopalachari had replaced Louis Mountbatten as the Governor-General of India in June 1948. This means that all the members of the Viceroy's Executive Council were Indians.

_Who governed the country on the 27 January, 1950?_

Dr Rajendra Prasad took over office as the head of state of the Republic of India on the 26th of January, 1950. Meanwhile, the Constituent Assembly became the (provisional) Parliament of India on the first Republic Day (26 January, 1950). The Provisional Parliament continued until the first elections happened in 1951–52.

_When did the British rule begin?_

Technically, the British Raj began in India after the _Rebellion of 1857_. Until then, the East India Company directly ruled some parts of India, and exercised power over princely states.

_Who declared the British the rulers of India?_

Nobody did. The British took over power.

_On what day did the British rule end in India?_

On the 15th of August, 1947. But one cannot draw a hard line in this case, because there was the _transition period_. The transition happened in stages. But per the law, the British rule ended on the 15th of August, 1947---this is the day we celebrate as the Independence Day of India. For more information, read above.

_Do the (current) British or their heirs have any authority to rule or govern India? If yes, then what authorities do they have? And on what basis?_

The present-day British or their heirs have no authority to rule or govern India. For more information, read above about the Independence Act of 1947 (enacted by the British Parliament) and Section 395 of the Constitution of India.

## Which war did we fight for Independence

This is a big one.

But an analogy first. At work, we build a solution that builds servers. This solution is some tens of thousands of lines of code. And when something breaks, we get alerted of failures.

When we have to look into an issue in such a large code base, we start by focusing on one aspect at a time.

The history of modern India is also prohibitively complex (which explains why so little of us know enough about it). To top it off, we have political narratives overshadowing it from all sides. At one point, we get confused: where do we even start when this is so overwhelming?!

## The East India Company becomes greedy

We all know that the Europeans came to India in search of spices, primarily. This is how the East India Company came in. They came to India in the 1600s. This was the time the Mughals ruled most of the present-day India and Pakistan. Marathas were emerging. By about 1700, the British had established ports along the coast of India. In the first couple decades of the 18th Century, the Mughal Empire had started shrinking. By mid-18th-Century, the Marathas had taken over most of the present-day Maharashtra, Madhya Pradesh, Gujarat, Rajasthan, some of the present-day West Bengal, and the entire Hindi belt. Here is a nice video about this period:

{{< youtube QN41DJLQmPk 611 643 >}}

1757 is when the East India Company acquired territory.{{< sidenote plassey >}}Memoirs of the Revolution in Bengal, Anno Domini 1757 ([World Digital Library](https://www.wdl.org/en/item/2384/)){{< /sidenote >}} The Battle of Plassey was the turning point. The East India Company's army defeated the Nawab of Bengal, and captured the territory. By 1801, the British EIC had established the Madras Presidency. By 1818, the British EIC had captured the entire modern-day India, except the territory controlled by the Sikh Empire, the territory of the Nawab of Sindh and the territory under the Durrani Empire. The Sikh Empire captured Jammu by 1823, which they lost to the British EIC by 1846. By 1849, the EIC controlled the entire Indian subcontinent.

## The Sepoy Mutiny

Mr V. D. Savarkar describe the 1857 Sepoy Mutiny as the "First War of Independence". Karl Marx called it a national revolt. Ms Ranaut calls the mutiny a nationalist movement, says the right wing arose. But what right wing? Who was the right wing? We had no wings at the time.

Here is a gist of who participated in the rebellion:

{{< figure src="https://blogfiles.ramiyer.me/2021/images/Indian-revolt-1857-states-map.svg" alt="Map: The 1857 Sepoy Mutiny; Source: Wikimedia Commons" caption="Map: The 1857 Sepoy Mutiny; Source: Wikimedia Commons" >}}

The image shows seven spots where the mutiny gained serious ground. In itself, this is a large topic, and I suggest you check out this book called {{< amazon "_From Plassey to Partition_" "https://amzn.to/3OqbwkB" >}} by Sekhara Bandyopadhyay. The episode is crucial to the history of India, but not in the way Ms Ranaut may have us think.

One of the reasons I disagree that this was a "decisive" fight for freedom is because the different actors in the mutiny had different goals. Some fought against the "uniform military culture", to ensure their liberties within the army continued. {{< amazon "Rani Lakshmibai" "https://amzn.to/3crebNo" >}} joined the rebellion because she wanted to fight the East India Company's Doctrine of Lapse to ensure her adopted son could inherit the kingdom. The soldiers started the rebellion because most of them were either Hindus or Muslims, and a "rumour" about the cartridges containing cow and pig fat had reached their ears, and they felt offended. Also, there was this larger topic of declining state of the peasants in the present-day UP–Bihar region. But nobody---none of these actors---showed any interest in uniting under a single umbrella. They may have had localised nationalism towards their respective kingdoms or minor collaborations, but none of them was even thinking about creating the state of India. In other words, they were each fighting either for what they thought were their rights, or against the annexation.

Of course, apparently some of the rebels did end up declaring Bahadur Shah II as the Emperor of Hindustan. But not everybody shared the wish. The British recaptured Delhi in about three months and took Bahadur Shah II prisoner. The mutiny took until almost the beginning of 1859 to fully die down.

As Bandyopadhyay puts it:

> The extremely localised nature of the uprisings helped the British to tackle them one at a time.

I repeat: This was not an all-encompassing nationalist movement in the sense of throwing out the British. The effect of this rebellion got confined to the present-day Punjab, Haryana, Rajasthan, Delhi, Uttar Pradesh, Uttarakhand, some parts of Madhya Pradesh and some parts of western Bihar. (See the brown area above.)

Moreover, as you can see in the image above, most states (the blue areas) supported the British---including the Rajputs and the Sikhs.

But this was a pivot point in the Indian history. This mutiny ended the rule of the East India Company and placed India directly under the British Crown. The Secretary of State of India now was member of the cabinet. This did change the game, even though it may not have been all that evident on the ground yet. Bandyopadhyay says that this mutiny led to Indians becoming a suspect in general in the British eyes, and the British thought "that Indians were beyond reform", thereby denying "the aspirations of the educated Indians for sharing power". This frustrated the educated middle class gave rise to modern nationalism over the next four decades, making the empire more vulnerable (and paving way for independence the way we got it).

Nationalism did not die a "sudden death". Its flavour changed. Ironically, the mutiny that Ms Ranaut celebrates led to this new flavour of nationalism that she does not appear to agree with.

Also, does she realise that the _Kākorī Conspiracy_ happened after 1857? That the bombing in the Central Legislative Assembly happened after 1857? That the Quit India Movement happened after 1857? How many more should we count? What is with this reductionist baseless sweeping statements? Perhaps she limited her research to the life of Rani Lakshmibai, which is why I think she thinks the history of Indian nationalism ended with the mutiny. And that some magic happened, and then someone begged, and the Brits rolled their eyes and said, 'Fine, we will give you India. Enjoy.'

## Wars, battles and "no-man's land"

In the interview, Ms Ranaut says that we did not get our independence through a fight, even though we lost to the British in battles.

The different kingdoms and empires in India lost their power to the British East India Company. In some cases, the Company had to fight battles, and in others, they got away with gaining control without a fight.

But did a war for independence take place?

To understand this, we must understand that a battle in most cases involves two or more parties fighting over a short span of time. A war is much different. A war could span years (or decades, or centuries). Armed battles may or may not be part of wars. For example, there was no armed battle involved in the US trade war with China. The Cold War spanned decades. Similarly, the war of Independence also spanned centuries. The war was slow and expensive. Starting from little mutinies to pivotal mutinies like that of 1857, to political pressure and ultimately, independence.

Ms Ranaut seems to have a narrow view of wars and a state. She goes back to the times when there used to be a king and his state, the state had a state religion which everyone in the state had to follow, states would fight (with arms) for territory and annex the territory won in a battle or a war. This model is from the medieval times.

India is a much more progressive state. We do not link religion with governance. This is what we mean by "secular". Secularism does not make a piece of land a no-man's land. The reason that countries are adopting secularism is that a country's governance and laws need to be fluid, to adapt to the global climate (not as in weather patterns). They need to evolve over time. Tying religion to governance makes the laws rigid---which is precisely what we hate about the Taliban, who use Islam as the source of law, and curb the liberty of people. Islam is not alone to have this problem, though; every religion potentially has the problem.

While religions may have formed as a way to give people a good life, human society has evolved since. As the society evolves, we amend our laws to suit the needs of the people at that time.

Traditionally, "amendments" have not been smooth with religions.

Secularism does not prohibit the practice of a religion, but merely separates religion from policymaking. In other words, secularism means that our religions do not dictate how we run the state. An example closest to a secular state in the sense of absolute separation of religion from policy is France. India is different, though. And for the why, we circle back to the Mutiny of 1857. When the British Crown took over direct control of India after the 1857 mutiny, Queen Victoria issued a Proclamation promising religious tolerance (given that hurt of religious sentiments was one of the reactants in the mutiny). This has continued since, because we all learned that we Indians do not like messing with religious sentiments.

The Indian flavour of secularism is about giving everyone, irrespective of their religion, equality before the Law (Article 14). We have Personal Laws based on the religion, that apply to the persons following that specific religion. These laws govern issues that the no other statutory law addresses.

Of course, we have had instances of politicising this for appeasement of one section of the society or another, but they do not make secularism itself bad. We must call out the politicians and political parties that indulge in such activities instead. This idea of each religion having to have a state (or vice-versa) harks back to the medieval ages.

But going beyond this would make this a political discussion. I would like to stay away from it.

Meanwhile, about independence: Not all battles need arms. Some, we fight using diplomacy. India is a fine example of an open, modern, diplomatic state (as Prime Minister Modi's visits to nations around the world in his initial days as prime minister would tell you). The fight for _Swarāj_ was a mixture of radical and diplomatic kinds. We won independence using more than one ways. Who says wars must involve blood (and that wars that do not involve blood are not wars)?

The War of Independence was not a Sudoku puzzle for it to have no more than one solution.

The likes of Netaji Bose {{< amazon "fought with blood," "https://amzn.to/3cCzICO" >}} while those like Gandhi and Nehru fought with {{< amazon non-violence "https://amzn.to/3BaeXc4" >}} and {{< amazon "diplomacy." "https://amzn.to/3PuNNAU" >}} Swami Vivekanand {{< amazon "fought spiritually." "https://amzn.to/3RXR2SZ" >}} Sardar Bhagat Singh {{< amazon "inspired the youth." "https://amzn.to/3RZZ79K" >}} Sardar Patel instilled the idea that {{< amazon "united we stand." "https://amzn.to/3OKkXLX" >}} Each had their own expertise and tools. Each fought in their own way. Who is to judge which way got us freedom? Never place all eggs in a single basket. To break a boulder, you go at it in different angles using different techniques; we have traditionally even used water and wood to split large rocks. Soft wood and water.

## Why did a "White man" draw the line of partition?

_Radcliffe Line_; that is the name of the line that divides India and Pakistan. The answer to this question is simple. The British were transferring power to the Dominions of India and Pakistan. Demarcating the territories was the responsibility of the British because the two dominions were their colony. When transferring power, they had to define the territories.

To get some context to this, one would need to read the Indian Independence Act of 1947.

Cyril Radcliffe did not do this on his own, though. He set up two councils---one for the partition of Bengal and the other for the partition of Punjab. Each council had two members each from the Indian National Congress and the Muslim League (double "each" intended). The eight members---four from INC and four from the Muslime League---were all Indian judges of British India.

Of course, the process was hasty. And if we were to believe the sources, Radcliffe was indifferent. But nobody at the time had the power to do this other than a British official for a simple reason that we were then a British Colony.

## Why did Indians kill each other instead of celebrating freedom

Oh, I don't know, ask the religious extremists, perhaps? Hint: The Two-nation Theory. Funny, the one thing that has not changed over the past centuries is our sense of separation based on religion, and political powers exploiting it to their benefit.

## Other rants

She then rants about Netaji Bose not getting a chance to fight the British with the Indian National Army. This is something I do not know much about. I will read about it and update, though. I do not have the liberty of a being a loudmouth who can get away with saying anything about anyone. On the personal front, I do not agree with head-on violence; I do not agree with complete non-violence either. The claim that we do not read much about Netaji Subhash Chandra Bose has been true in my case; I do not know much about him and his work. Of late, there has been a lot of noise about him, which in itself has been a deterrent.

Having said that, Netaji is an important cog in the machinery that got us independence, and we ought to know about him.

At the same time, I also believe that a modern state does not choose war as its first path to attaining what it needs. Diplomacy is a much finer tool given the present geopolitical framework. And no matter who says what, no matter who forms the government, India chooses diplomacy first. This has been the way in the 20th and the 21st centuries. And thankfully so.

As for the letter from Nehru to appoint Rajaji as the Governor-General of India, he wrote the letter in 1948, when India was still an independent Dominion with the British monarch as the head of state. The language is diplomatic, as per the protocol. This is how, say, the Prime Minister of India would write to the President of India. India became a sovereign democratic republic in 1950---about a year and nine months after the letter. Regardless, using phrases like His Majesty or His Excellency is common in diplomatic communication;{{< sidenote excellency >}}Political row over Modi's pitch for Partition Horrors Remembrance Day ([Deccan Herald](https://www.deccanherald.com/national/national-politics/political-row-over-modis-pitch-for-partition-horrors-remembrance-day-1019771.html)){{< /sidenote >}} it has no hidden meaning whatsoever. In no way does it translate to "begging" or someone being lower than the other. This claim of hers is ignorant at best.

Also, transfer of power is not "begging". The civilised world calls it transition. This way, you ensure order while something as drastic as transfer of power happens from an empire to two dominions created afresh during the transition. When you think of the enormity of the issue, you would realise that "fighting and taking" something does not always work. In 1947, the world was much more civilised than in 1757. You did not show up at someone's door with an army, beat the brains out of them and take over control of their house. The Second World War had ended, and countries were realising the scale of the wasted resources. No war is truly worth it; the world had realised it by now.

Let me put it this way: how many battles have we had with Pakistan? And in the last seventy-four years, how much territory have we each transferred to the other? Physical battles are an inefficient way to transfer territory or power in this world. We are much more sophisticated than we were in the 1700s. Battles and wars look good on comic books and screens, not in real life in 2021. One may romanticise it in films and theatre, but no civilised society today likes bloodshed.

## In summary

Discrediting the Indian Independence Movement is apparently in vogue today. But such a narrative not only questions our freedom struggle, but also diminishes our stature in the globalised world. Some sections of our society that criticise closed societies that base themselves on religion and brand them "medieval minded" societies, seem to be subconsciously drawn to that very model.

Identity-based religion and politics blind us and harm us. Religious disturbances hinder economic growth by influencing factors including foreign investments. In other words, for example, our religious disturbances impede our vision of "Make in India, Make for the World".

Also, when on a public stage, it becomes one's responsibility to ensure they are speaking facts, and keep in mind the consequence of their words on the general public. A programme being telecast to millions of people is not a opportunity to regurgitate half-baked knowledge gathered from questionable sources. One must under no circumstance reduce the struggles of our freedom fighters merely because they have a stage and are free to express what they feel. If one does not root these statements in facts, their personal quest for fame and fandom may succeed, but to the detriment of the nation. Ms Ranaut's putting out statements such as "some of the answers I am seeking please help me find answers [sic]" or "can someone please help me understand [sic]" imply that she does not know enough, in which case, silence about the subject on a public stage would have been the way to go. Saying 'I said what I said, because I wanted to know' is a lousy excuse. Such "nationalists" are not true nationalists; they are merely self-centred individuals interested in publicity, indifferent towards the nation that they claim to put before all else.

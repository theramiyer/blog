---
title: "Enjoy Your Life With Covid 19"
subtitle: "because WhatsApp University has your back"
date: 2021-05-08T16:38:17+05:30
description: |
    Dr Faheem Younus asks people to forget about the disease and go outside, live free. Or does he? We rip apart a WaatsApp University circular to gauge the substance in it.
tags:
- covid-19
- whatsapp university
- mythbuster
- health
image: "https://blogfiles.ramiyer.me/2021/images/street-food.jpg"
---

There was a time when I was a favourite student of WhatsApp University. When I realised the ultimate quality of education there, I was so envious of it that I quit. After all, those of us who have had the privilege to attend the mainstream universities would be jealous of the free-and-open system. This is why I dropped out of the university (and I made a video about it as well):

{{< youtube "LMK13EgxHNs" >}}

A couple of days ago, I read a circular from the university that reminded me of Solzhenitsyn:

> Hastiness and superficiality are the psychic diseases of the twentieth century.

(Of course, I did not remember his name---thanks, Goodreads! By the way, he is right about two centuries.)

Here goes what I read:

> Head of Infectious Diseases Clinic, University of Maryland, USA says:
>
> 1. We may have to live with C19 for months or years. Don’t deny or panic. Don’t make your life a misery. Let’s learn to live with this reality.
>
> 2. You can’t destroy the C19 virus that has penetrated the cell walls, by drinking liters of hot water – you’ll just be going to the bathroom more often.
>
> 3. Washing your hands and maintaining a physical distance is your best method of protection.
>
> 4. If you do not have a C19 patient at home, there is no need to disinfect your home.
>
> 5. Grocery bags / plastic bags, gas stations, shopping carts and ATMs do not cause infection.Wash your hands, live your life as usual.
>
> 6. C19 is not a food infection. It is associated with infectious droplets such as flu. There is no demonstrated risk that C19 is transmitted by ordering food.
>
> 7. You can lose your sense of smell by using a lot of anti-allergies and viral infections. These are only non-specific symptoms of C19.
>
> 8. Once you get home, you don’t need to change clothes immediately and take a shower!
> Cleanliness is a virtue but not paranoia!
>
> 9. The C19 virus does not fly through the air. This is a respiratory drop infection that requires close contact.
>
> 10. The air is clean, you can walk to parks and public places (just keep your physical distance for protection)
>
> 11. Just use regular soap against C19, no need for anti-bacterial soap. These are viruses, not bacteria.
>
> 12. You don’t have to worry about your food orders. But you can heat everything in the microwave, if you want.
>
> 13. The possibility of bringing the C19 home in your shoes is like being struck by lightning twice a day. I’ve worked against viruses for 20 years – drop infections don’t spread like that!
>
> 14. You can’t be protected from viruses by consuming vinegar, sugarcane juice and ginger! It’s just for immunity not medicine.
>
> 15. Wearing a mask for a long time interferes with your breathing and oxygen levels. Wear it only in crowded places.
>
> 16. Wearing gloves is also a bad idea; the virus can accumulate in gloves and is easily transmitted if you touch your face. Better to just wash your hands regularly.
>
> 17. Immune system gets very weak by always living in a sterile environment. Even if we are taking immune-boosting supplements / medicines, please regularly leave your house to the park / beach or anywhere else.
> Immunity is increased by EXPOSURE TO PATHOGENS, not by sitting at home and consuming fried/spicy/sweet foods & fizzy drinks.
>
> Original article:- https://theazb.com/we-will-live-with-covid19-for-months-lets-not-deny-it-o nor-panic- dr-faheem-younus /
>
> Very good article, must be read by everyone.

First, the disclaimer: I’m not a medical or paramedical professional. What you are about to read is _not_ medical advice. You will see a collection of tweets from an expert in the field—the tweets are advice from a professional. This post is for the purpose of general information and awareness.

Before we talk about this message, I want to state that _some of the points_ mentioned in it are true. Second, we have still not fully understood this virus---we know a lot, but not everything. (It does not matter which great sage claims what, or whether someone predicted this thousands, hundreds or tens of years ago.) This is a "new" virus, and we learn something new about it every month if not every week.

Dr Faheem Younus (who happens to be from our neighbouring country) is a respectable figure in his field. He actively tweets about COVID-19. And I collected these tweets from his timeline. I think the creator of the message paraphrased the doctor, and in my view, that is where the problem lies.

Dr Younus has been vocal about not being paranoid. He advocates use of masks, and other COVID-appropriate behaviour. What this forwarded message does is take it a couple of notches up and talk about complete liberalisation of the protocol in closing.

That is not the way to go.

Working with such messages is tricky, because they have some truth in them. Let us take one point at a time and see what makes sense and what does not.

{{< toc >}}

## Do not deny or panic

> We may have to live with C19 for months or years. Don’t deny or panic. Don’t make your life a misery. Let’s learn to live with this reality.

C19: what a cool way to say COVID-19! In general, this is true. We have no reason to panic, nor should you deny the fact that there is the pandemic. All those who publicly showed the virus their arrogance suffered from the infection (figuratively speaking). Some even succumbed to it.

Having said that, let me also say that a friend of mine ([whom you can find on Instagram here](https://www.instagram.com/chinmay_nadig/)) has been volunteering here in Bangalore since April 2020, and has not caught the virus yet. He gets himself tested frequently given the possibility of exposure; he has always tested negative so far, nor does he have antibodies against the virus, which could mean he’s never had the infection. Thirteen months of close contact with COVID-positive patients, and he has not caught the infection, for a simple reason: He wears a mask when out and about, and wears PPE when handling patients and mortal remains over hours. That is not to say that he will never get infected (I hope he never does). The point is that personal protection has protected him for thirteen months, despite the constant, close contact with the infected.

In other words, he has learnt to live with this reality.

## On hot water and the virus

> You can’t destroy the C19 virus that has penetrated the cell walls, by drinking liters of hot water – you’ll just be going to the bathroom more often.

True. Those who claim that steam and hot water can _kill the virus_ are wrong. Steam may help alleviate some symptoms, but it does nothing to prevent or kill the virus.

## On the best method of protection

> Washing your hands and maintaining a physical distance is your best method of protection.

True in that washing hands is a best practice. Until we are all vaccinated, personal protection is the way to go. In fact, Dr Younus says:

{{< tweet user="FaheemYounus" id="1324362475245109253" >}}

## On disinfecting our homes

> If you do not have a C19 patient at home, there is no need to disinfect your home.

Here is what he had tweeted:

{{< tweet user="FaheemYounus" id="1239710943568973827" >}}

## On shopping bags

> Grocery bags / plastic bags, gas stations, shopping carts and ATMs do not cause infection. Wash your hands, live your life as usual.

This is something that the doctor has been saying for a long time. I also found this tweet (rather, thread):

{{< tweet user="FaheemYounus" id="1241812326019485698" >}}

## On take-out food

> C19 is not a food infection. It is associated with infectious droplets such as flu. There is no demonstrated risk that C19 is transmitted by ordering food.

And I found this tweet.

{{< tweet user="FaheemYounus" id="1241812327151910914" >}}

My folks were paranoid about ordering food from outside. But being a Bangalorean, avoiding street food is impossible for me. Once, we ordered some _chāt_ while working on our volunteering report at my friend's place:

{{< figure src="https://blogfiles.ramiyer.me/2021/images/street-food.jpg" alt="My friends and I religiously hogging some chāt." caption="My friends and I religiously hogging some chāt. (We had removed our masks for a little while because we _knew_ that the terrace of my friend's house was safe.)" >}}

I was in self-quarantine at the time to prevent transmitting a potential infection to my family. My being fine at the end of the quarantine convinced them that ordering food was safe. We have been ordering food since. At home. All good. The disease has not spread through food so far.

## On the sense of smell

> You can lose your sense of smell by using a lot of anti-allergies and viral infections. These are only non-specific symptoms of C19.

Well, the last part is true; I found an almost-verbatim tweet:

{{< tweet user="FaheemYounus" id="1241812329404211202" >}}

I do not know what "anti-allergies" are. But non-specific here means that losing smell does not _necessarily_ mean that you have COVID-19. Meaning, anosmia also has other causes. You may want to get tested, though:

{{< tweet user="FaheemYounus" id="1268189928942440450" >}}

The idea here is to not panic. (Also, in case of India, the percentages are different. For example, majority of the cases in Bangalore have been asymptomatic.)

## On showering

> Once you get home, you don’t need to change clothes immediately and take a shower!

Again, I do not shower every time I get back home---from, say, the store---these days. I used to do that back in April and May 2020, but our understanding of the virus has evolved since.

{{< tweet user="FaheemYounus" id="1241812333359398915" >}}

## Airborne or not

> The C19 virus does not fly through the air. This is a respiratory drop infection that requires close contact.

_The virus is airborne._ Experts have said this before, and the US Centres for Disease Control (CDC){{< sidenote cdc-airborne >}}The virus is an airborne threat, the C.D.C. acknowledges. ([The New York Times](https://www.nytimes.com/2021/05/07/health/coronavirus-airborne-threat.html)){{< /sidenote >}} and the WHO{{< sidenote who-airborne >}}WHO Finally Admits Coronavirus Is Airborne. It’s Too Late ([Forbes](https://www.forbes.com/sites/jvchamary/2021/05/04/who-coronavirus-airborne/?sh=75d18a764472)){{< /sidenote >}} agree. But “airborne” here does not mean “getting carried around by the air for kilometres”. The scientific language is a little unusual for most of us, but in closed environments, the virus tends to travel more than a couple of metres and _hang_ in the air for a long period of time.

The best defence against this is wearing a mask, and wearing it properly---fully covering your nose and mouth. Wearing a mask will _reduce the chances_ of your catching the virus. The viral load matters in case of COVID-19. The higher the amount of the virus you inhale, the higher are your chances of getting a severe disease.

A study done about the virus’ airborne nature in close environments said, in fact, that the virus can hang in the air in apartment buildings for long. Doctors also advice us to be careful when using public toilets. In such small enclosures, the virus can hang in the air for hours. Watch this video:

{{< youtube "xFvC8HMMgm0" "1396" "1795" >}}

The easiest way to prevent infection and protecting others apart from ourselves is wearing a mask.

Remember, this is science. Science works on evidence. And observations and protocols may change based on new evidence. What the doctor means by “not airborne” is that the virus does not get carried around to long distances and remain viable. If you hear Dr Rakesh Mishra in the video above, he says that those performing the experiment could “detect the virus as far as 20 feet from the subject in a closed room”. Does this mean the virus is airborne? Technically, yes. Practically, it depends on the environment.

Take the doctor’s more recent advice instead:

{{< tweet user="FaheemYounus" id="1383718885040410628" >}}


## On taking a walk in the park

> The air is clean, you can walk to parks and public places (just keep your physical distance for protection)

I did find a tweet related to this from the doctor:

{{< tweet user="FaheemYounus" id="1244145852773515264" >}}

But the tweet is from March 2020. According to newer findings, _the virus is airborne_ (read above). Although, based on what I understand, taking a walk in the park is still safe, given conditions like a small number of people, great air circulation, people wearing masks, etc.

Caution is necessary. Talk to your doctor to know more. Based on what I have understood from the doctors I have spoken to, transmission while walking in the park is highly unlikely, provided you handle your personal protection.

Again, watch the video clip above, you will understand.

## Antibacterial soap

> Just use regular soap against C19, no need for anti-bacterial soap. These are viruses, not bacteria.

No matter what, this is not going to change.

{{< tweet user="FaheemYounus" id="1244147624913010689" >}}

And soaps are better than sanitizers.

{{< tweet user="FaheemYounus" id="1239710939630448647" >}}

## Microwave food

> You don’t have to worry about your food orders. But you can heat everything in the microwave, if you want.

Well, not those words (not "everything" anyway), but:

{{< tweet user="FaheemYounus" id="1244149254442098688" >}}

## Virus in shoes

> The possibility of bringing the C19 home in your shoes is like being struck by lightning twice a day. I’ve worked against viruses for 20 years – drop infections don’t spread like that!

Here is an almost-verbatim tweet by him:

{{< tweet user="FaheemYounus" id="1244150646846173184" >}}

## Protection using sugarcane

> You can’t be protected from viruses by consuming vinegar, sugarcane juice and ginger! It’s just for immunity not medicine.

Nope. I do not think he said this; I do not think he ever suggested ginger for immunity, because:

{{< tweet user="FaheemYounus" id="1291307520603172864" >}}

{{< tweet user="FaheemYounus" id="1268174722883366912" >}}

## Mask interferes with breathing

> Wearing a mask for a long time interferes with your breathing and oxygen levels. Wear it only in crowded places.

Wrong.

Impossible that a doctor of his calibre would say this. Also, no, wearing a mask DOES NOT interfere with your breathing or oxygen levels.{{< sidenote mask-workout >}}If you are looking to do any cardiovascular exercises ("cardio") or anything that requires heavy breathing, talk to your doctor about what you should do.{{< /sidenote >}} I wore a mask for _28 hours_ straight (even while sleeping) when I had to spend a day in the hospital. I had a machine monitoring my vitals. Oxygen saturation in my blood was between 98% and 100% throughout.

Do not believe me? Here, this is what Dr Younus has to say:

{{< tweet user="FaheemYounus" id="1276225846609666049" >}}

And no, I did not find a tweet of his that says that wearing a mask interferes with oxygen levels. One never will.

Also, I have never had to remove my mask while climbing stairs. (I tried five floors at a stretch.) Sure, everyone is different. If you feel breathless, stop to take rest; do not remove your mask. Talk to your doctor if you suffer from breathing abnormalities.

## Wearing gloves

> Wearing gloves is also a bad idea; the virus can accumulate in gloves and is easily transmitted if you touch your face. Better to just wash your hands regularly.

Yes, I could find this tweet. As expected.

{{< tweet user="FaheemYounus" id="1244157447725559808" >}}

## Exposure to pathogen for immunity

> Immune system gets very weak by always living in a sterile environment. Even if we are taking immune-boosting supplements / medicines, please regularly leave your house to the park / beach or anywhere else.
> Immunity is increased by EXPOSURE TO PATHOGENS, not by sitting at home and consuming fried/spicy/sweet foods & fizzy drinks.

This statement is reckless. And of course, I could not find this tweet. But I found something interesting:

{{< tweet user="FaheemYounus" id="1269332316540801024" >}}

He talks about _this WhatsApp forward_ in this tweet. Also, I found [the same message, attributed to a different doctor.](https://www.richmond-news.com/local-news/fake-list-of-covid-19-wisdom-attributed-to-dr-bonnie-henry-making-rounds-online-3125096) The piece also links to the right AZB article (yes, the one which you did not bother to click).

Anyway, exposure to pathogens is one way to better immunity (which is also the popular go-to advice we get from our uncles). But I do not buy the "increasing immunity" story. To me---based on my conversations with doctors---the question is about healthy and unhealthy immunity.

If you are in haste, remember that _"boosted" immunity can kill you_, skip the rest, and go back to WhatsApp University. If you dislike half-baked knowledge, read on:

Healthy immunity is when your body is able to detect a pathogen, react aptly, and clean up with zero to mild discomfort, without causing harm to you. Unhealthy immunity can be of two kinds, broadly: deficient immunity and untrained immunity.

Your body needs nutrients to function. Immunity is part of your bodily functions, and that needs adequate nutrition. If you are deficient in, say, vitamins or minerals, your immunity gets hit as well. This leads to a higher probability of your suffering from or succumbing to a disease. "Boosting immunity" may work in this case, by giving your body the right dosage of the right nutrient (talk to a doctor). If your body does not have any deficiency, taking multivitamin pills will not help you. At best, your body will throw out the excess. Otherwise, this seemingly harmless "immunity boosting" could lead to complications.{{< sidenote methi >}}Too much turmeric, methi, vitamin D — Doctors fight new emergencies driven by Covid fear ([ThePrint](https://theprint.in/health/too-much-turmeric-methi-vitamin-d-doctors-fight-new-emergencies-driven-by-covid-fear/495557/)){{< /sidenote >}}

Untrained immunity is the second broad category in unhealthy immunity. This is like equipping children with guns, and making them guard something. In this case, your body has all the nutrients it needs, it has the capacity to produce antibodies, but the detection system is haywire, or the trigger works wrongly, etc. Auto-immunity is an example of this.

Auto-immunity causes your immunity to detect phantom infections, or to trigger unnecessarily, flooding the circulatory system with proteins and other chemicals meant to fight an infection, which could end up damaging your organs when uncontrolled.

Yes, exposure to pathogens does train your immunity over time. And yes, living in a sterile environment may cause your immunity memory to "forget" the signatures of some of the pathogens, but that does not mean you voluntarily expose yourself to pathogen, because, it could lead to unexpected results (ahem), tire you and harm your body in general.

The best, scientifically proven way to train your immunity is vaccination.

## Summing up

Our knowledge of the virus continues to evolve as time passes. Follow the updates from the [WHO](https://twitter.com/WHO), reputed doctors like [Dr Younus](https://twitter.com/FaheemYounus/), [Eric Topol](https://twitter.com/erictopol); official sources like the [Ministry of Health (GoI)](https://twitter.com/MoHFW_INDIA), [Indian Council of Medical Research (ICMR)](https://twitter.com/ICMRDELHI); reliable media outlets like [ThePrint](https://theprint.in/coronavirus-full-coverage/), [The Hindu](https://www.thehindu.com/topic/coronavirus/), [Faye D'Souza](https://twitter.com/fayedsouza), and [Twitter's list of COVID resources](https://twitter.com/i/events/1385596085192691712) among other sources.

Keep yourselves updated.

If you cannot do that, follow these simple rules that are unlikely to change for COVID-19:

1. Wear a mask; wear it the right way.
2. Avoid crowds, maintain physical distance.
3. Wash or sanitize your hands at regular intervals.
4. [Get vaccinated at your turn.]({{< ref "covid-vaccines" >}})

And share this post in your circles.

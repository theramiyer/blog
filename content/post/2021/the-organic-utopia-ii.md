---
title: "The Organic Utopia (Part II)"
subtitle: "reviewing the claims"
date: 2021-03-07
description: |
  Those "in favour of" organic food make important claims about organic food. Most of them have to do with taste and nutritional value. We look at those aspects in this article.
tags:
- food
- farming
- science
- development
---

Previously in this series, we looked at the basics of how food works, and what we look for in food. We get an overview of the claims that organic proponents make. In this piece, we look at three of the claims in more depth, and test their validity based on the evidence we have.

This is the second of the five-part series on organic food. To jump to any topic on the subject, use the list below:

- [The traditional farming methods]({{< ref "the-organic-utopia-i#the-traditional-farming-methods" >}})
- [How food works]({{< ref "the-organic-utopia-i#how-food-works" >}})
- [The case of reduced nutrition]({{< ref "the-organic-utopia-i#the-case-of-reduced-nutrition" >}})
- [Enter: Organic Farming]({{< ref "the-organic-utopia-i#enter-organic-farming" >}})
- [The sustainability argument]({{< ref "the-organic-utopia-i#the-sustainability-argument" >}})
- [Customer benefits]({{< ref "the-organic-utopia-i#customer-benefits" >}})
- [It isn't as simple]({{< ref "the-organic-utopia-i#it-isn-t-as-simple" >}})
- [Organic tastes better](organic-tastes-better)
- [Organic has lower pesticide residue](organic-has-lower-pesticide-residue)
- [Organic has higher nutritional value](organic-has-higher-nutritional-value)
- [Organic is more eco-friendly]({{< ref "the-organic-utopia-iii#organic-is-more-eco-friendly" >}})
- [Genetic Modification]({{< ref "the-organic-utopia-iv#genetic-modification" >}})
- [Food irradiation]({{< ref "the-organic-utopia-iv#food-irradiation" >}})
- [Does organic have no real merits]({{< ref "the-organic-utopia-iv#does-organic-have-no-real-merits" >}})
- [What does it mean for us, the consumers]({{< ref "the-organic-utopia-iv#what-does-it-mean-for-us-the-consumers" >}})
- [Way forward in sustainability]({{< ref "the-organic-utopia-v#way-forward-in-sustainability" >}})
- [When to buy organic]({{< ref "the-organic-utopia-v#when-to-buy-organic" >}})
- [When not to worry about organic]({{< ref "the-organic-utopia-v#when-not-to-worry-about-organic" >}})
- [What about the reduced nutrition]({{< ref "the-organic-utopia-v#what-about-reduced-nutrition" >}})
- [Summing up]({{< ref "the-organic-utopia-v#summing-up" >}})

To jump to the parts themselves, use the links below:

- [Part I: Making a case for organic food]({{< ref "the-organic-utopia-i" >}})
- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

## Organic tastes better

While we hear that organic food tastes better, no scientific study could conclusively claim this to be true. While that does not mean that the claim is false, the chances of it being true are close to none.

When we conduct a study, we blind the participants (not in the literal sense). Imagine you have two apples placed on your plate. They look similar, their size is similar, they smell similar---the difference is that one is organic and one is not. You do not know which one is which. You taste the two apples and point at the tastiest one.

To add a twist to this, we give you two apples, again, you cannot tell the difference by looking or picking them up in your hands. We say one of them _may_ be organic, and ask you to pick the tastier one. You pick one.

Scientists conducted studies as simple as this, and more complex ones, but could not find any difference in taste between organic and conventional produce.

One of the references to taste was something I found on National Geographic,{{< sidenote org-taste >}}_Organic Foods Are Tastier and Healthier, Study Finds_ ([National Geographic](https://www.nationalgeographic.com/culture/article/organic-foods-are-tastier-and-healthier-study-finds)){{< /sidenote >}} which cites a study published in the British Journal for Nutrition. But I think the study has conflict of interests written all over it.{{< sidenote benbrook >}}Benbrook ... had 100% of his research cited in this meta-review financed by the organic industry. ([Genetic Literacy Project](https://gmo.geneticliteracyproject.org/FAQ/are-organic-foods-healthier-than-conventional-foods/)){{< /sidenote >}} Now, there is nothing inherently wrong in a lobby on organic food funding the research, but if you have the key members of the study taking up critical positions in the most prominent lobby group, one cannot help but look for an independent analysis. And in cases as simple as this, the absence of evidence despite more than one independent analyses done, does say something.

One of the arguments{{< sidenote better-taste >}}Should You Buy Organic? ([LiveScience](https://www.livescience.com/22966-should-you-buy-organic.html)){{< /sidenote >}} for the possibility that consumers claim organic food tastes better is that organic farmers tend to get their produce to the consumers sooner. One of the reasons to reach the food to the consumer sooner is the high probability of spoilage. In all, though, this is good for the consumer.

## Organic has lower pesticide residue

No debate here. While organic is not pesticide-free, studies found the presence of pesticide residue in 38% of conventionally grown food. The number for organic food was 7%.

Regardless, though, how much is what matters here. Like Rachel Carson said:

> The ultimate answer is to use less toxic chemicals so that the public hazard from their misuse is greatly reduced.

Our governments regulate the amount of dangerous chemicals that go into our food. In India, we have FSSAI doing this. While organic and conventional food have pesticide residues, they are well below the standards set. Bioaccumulating pesticides get special attention, because these, over time, can cause serious illness by depositing themselves in our bodies.

Regardless, the levels found in food are a fraction of the upper safety limits. In other words, we do not have to worry much about them. Toxicity depends on concentration.

Another issue is with complacence. People tend to think that "natural" pesticides are not harmful---because they are natural after all; how can anything natural be harmful? The reality is different.

Organic food protects itself from pests by natural chemical warfare. The plants produce chemicals to ward off pests, most of which are antioxidants. Organic food items have shown much higher levels of antioxidants. But is that enough to call them a _health benefit_? We are not sure.

For instance, if I told you that a certain organic variety of watermelon contained 2000 milligrams of dihydrogen monoxide---a chemical critical for human survival and bodily functions---per kilogram, what would you think of it?

The statement merely means that the specific organic variety of watermelon had 2 grams of water more than conventional watermelon, per kilogram of the watermelon. Is that a substantive health benefit?

These natural pesticides could also make you ill.{{< sidenote nat-pest >}}_Mythbusting 101: Organic Farming > Conventional Agriculture_ ([Scientific American](https://blogs.scientificamerican.com/science-sushi/httpblogsscientificamericancomscience-sushi20110718mythbusting-101-organic-farming-conventional-agriculture/)){{< /sidenote >}} For example, potatoes produce solanine as they turn green, which is a natural pesticide. Another issue is the use of, say, petroleum distillates, copper or sulphur as pesticides. Organic farming allows use of these, but we all know how toxic they can be. Copper accumulates in soil over time, which increases the toxic load on the environment.{{< sidenote sulphur-salts >}}_Is Organic Farming Better for the Environment?_ ([Genetic Literacy Project](https://geneticliteracyproject.org/2017/02/16/organic-farming-better-environment/)){{< /sidenote >}} Copper also bio-accumulates, which means, over time, our body collects these molecules and stores them long term. This is dangerous.

From the biodiversity standpoint, these compounds could smother even the friendlier insects on the farm, because they do not specifically target a species. Also, given the liberal use of them, these will add to the toxicity of the surroundings.

While scientists debate the probability of presence of pathogens in organic and conventional food, there has been one study{{< sidenote ecoli-cont >}}_Preharvest evaluation of coliforms, Escherichia coli, Salmonella, and Escherichia coli O157:H7 in organic and conventional produce grown by Minnesota farmers_ ([PubMed](https://pubmed.ncbi.nlm.nih.gov/15151224/)){{< /sidenote >}} that did find presence of E. coli _five times_ as high in organic farm samples, compared to conventional ones.

Having said that, I would echo Rachel Carlson in that pesticide use is good, provided the concentration is low. Because some of the pesticides such as organophosphates may have harmful effects on the brain, not for the consumers alone, but also for the farm workers (because they get exposed to much higher concentration). Pesticides have shown to affect infants and pregnant women.

## Organic has higher nutritional value

One of the forerunners in this claim are some organic milk producers in India. Globally, the claim among organic advocates has been the higher levels of Omega-3 fatty acids. But the scientific community criticised this as flawed.{{< sidenote omega-critic >}}_Got (Healthier) Milk? How Activist Scientists And Journalists Bungled Report On Organic Foods_ ([Forbes](https://www.forbes.com/sites/jonentine/2013/12/19/got-healthier-milk-how-activist-scientists-and-journalists-bungled-a-report-on-organic-foods/?sh=638a8065574c)){{< /sidenote >}} Also, to note, this is a Benbrook study (yes, the same one you read about before).

Another study that found wide circulation in the organic community was a 2018 study published in the Journal of the American Medical Association, which concluded:

> A higher frequency of organic food consumption was associated with a reduced risk of cancer.

The scientific community challenged this as well, specifically the sample selection. Apart from that, they said, this study shows lowered risk of merely two cancers.{{< sidenote prev-cancer >}}_Don't believe the hype, organic food doesn't prevent cancer_ ([The Guardian](https://www.theguardian.com/commentisfree/2018/oct/26/dont-believe-the-hype-organic-food-doesnt-prevent-cancer)){{< /sidenote >}}

The one nutrient whose concentration was of significance was phosphorous. Although, the deficiency of phosphorous is rather rare, and is not related to food intake.{{< sidenote phosphorous >}}_Phosphorous_ ([National Institutes of Health](https://ods.od.nih.gov/factsheets/Phosphorus-HealthProfessional/#h5)){{< /sidenote >}}

In all, the evidence on concentration of nutrient is not conclusive in any sense. For instance, a review of 162 studies{{< sidenote 162-studies >}}_Nutritional quality of organic foods: a systematic review_ ([The American Journal of Clinical Nutrition](https://academic.oup.com/ajcn/article/90/3/680/4597089)){{< /sidenote >}} concluded, saying:

> On the basis of a systematic review of studies of satisfactory quality, there is no evidence of a difference in nutrient quality between organically and conventionally produced foodstuffs. The small differences in nutrient content detected are biologically plausible and mostly relate to differences in production methods.

Another important claim we hear is that organic farming is eco-friendly. How true is that?

Read further:

- [Part III: Addressing the elephant in the room]({{< ref "the-organic-utopia-iii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

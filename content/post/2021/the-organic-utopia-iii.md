---
title: "The Organic Utopia (Part III)"
subtitle: "addressing the elephant in the room"
date: 2021-03-08
description: |
  We consider organic farming as a more eco-friendly variant of farming. The simple reason is that the methods are from back in time when the air was cleaner and the planet greener. But do we have the full picture? We address the question in this piece.
tags:
- food
- farming
- science
- development
---

So far in this series, we have seen why one chooses to buy organic, and reviewed some of the claims that the proponents of organic food make. But this picture is incomplete without perhaps the most important aspect of the practice, from a broader point of view.

This is the third of the five-part series on organic food. To jump to any topic on the subject, use the list below:

- [The traditional farming methods]({{< ref "the-organic-utopia-i#the-traditional-farming-methods" >}})
- [How food works]({{< ref "the-organic-utopia-i#how-food-works" >}})
- [The case of reduced nutrition]({{< ref "the-organic-utopia-i#the-case-of-reduced-nutrition" >}})
- [Enter: Organic Farming]({{< ref "the-organic-utopia-i#enter-organic-farming" >}})
- [The sustainability argument]({{< ref "the-organic-utopia-i#the-sustainability-argument" >}})
- [Customer benefits]({{< ref "the-organic-utopia-i#customer-benefits" >}})
- [It isn't as simple]({{< ref "the-organic-utopia-i#it-isn-t-as-simple" >}})
- [Organic tastes better]({{< ref "the-organic-utopia-ii#organic-tastes-better" >}})
- [Organic has lower pesticide residue]({{< ref "the-organic-utopia-ii#organic-has-lower-pesticide-residue" >}})
- [Organic has higher nutritional value]({{< ref "the-organic-utopia-ii#organic-has-higher-nutritional-value" >}})
- [Organic is more eco-friendly](#organic-is-more-eco-friendly)
- [Genetic Modification]({{< ref "the-organic-utopia-iv#genetic-modification" >}})
- [Food irradiation]({{< ref "the-organic-utopia-iv#food-irradiation" >}})
- [Does organic have no real merits]({{< ref "the-organic-utopia-iv#does-organic-have-no-real-merits" >}})
- [What does it mean for us, the consumers]({{< ref "the-organic-utopia-iv#what-does-it-mean-for-us-the-consumers" >}})
- [Way forward in sustainability]({{< ref "the-organic-utopia-v#way-forward-in-sustainability" >}})
- [When to buy organic]({{< ref "the-organic-utopia-v#when-to-buy-organic" >}})
- [When not to worry about organic]({{< ref "the-organic-utopia-v#when-not-to-worry-about-organic" >}})
- [What about the reduced nutrition]({{< ref "the-organic-utopia-v#what-about-reduced-nutrition" >}})
- [Summing up]({{< ref "the-organic-utopia-v#summing-up" >}})

To jump to the parts themselves, use the links below:

- [Part I: Making a case for organic food]({{< ref "the-organic-utopia-i" >}})
- [Part II: Reviewing the claims]({{< ref "the-organic-utopia-ii" >}})
- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

## Organic is more eco-friendly

The sustainability part! Let us first look at how farming affects our environment. In a [previous article]({{< ref "food-and-sustainability" >}}), I talk about the environmental impact of our food. In this article, let us concentrate on the growing of food alone.

Here are the major actors in the environmental impact in this context:

1. Pesticides
2. Fertilisers
3. Water use
4. Greenhouse gas emissions

We have already covered pesticides in detail. Let us jump to fertilisers.

Fertilisers mainly supplement nitrogen, phosphorous and potassium in the soil. Of course, we have other variants of fertilisers that have specific use, but the most commonly used ones are NPK fertilisers. Plants need these for amino acids, creating the cell membrane and even the DNA. Among these elements, nitrogen plays the most important role.

Why do we need fertilisers?

We have a large population (of humans and animals) to feed. The needs are specific: staples, fruits and vegetables, along with other food items. For reasons mentioned in [_Food and Sustainability_]({{< ref "food-and-sustainability" >}}) and [_Farmer Protests_]({{< ref "farmer-protests" >}}), we have resorted to monoculture. This means that the soil loses its nutrients, which makes us add fertilisers to it, so that the crop can get the necessary nutrients.

Even otherwise, the soil does not have enough of these nutrients to enable us to grow crops.

When we go the organic way, we cannot use chemical fertilisers. We must resort to methods such as:

- Using manure to enrich the soil with nutrients
- Going for crop rotation for nutrient fixing
- Growing cover crops, off season

The little issue here is that farmers are not allowed to use biosolids, because that is not "organic". Why? The argument is that urban sewage contains domestic and industrial waste. Which means it "may contain chemicals". Another point is that some people may have ethical issues with using human waste (even though the waste is heavily treated). Sally Brown, a soil scientist at the University of Washington who has been studying biosolids for over a decade, says:{{< sidenote sally >}}_Whole Foods Bans Produce Grown With Sludge. But Who Wins?_ ([The Salt, NPR](https://www.npr.org/sections/thesalt/2014/01/17/263370333/whole-foods-bans-produce-grown-with-sludge-but-who-wins)){{< /sidenote >}}

> People have been taught that poop is dangerous and it makes you sick, and so they're suspicious of it. And municipalities have done a terrible job of communicating what they do and what wastewater treatment really is.

The problem with organic farming has been the yield. Yield reduces in organic farming for reasons including:

1. Lower soil nutrition
2. Lower resistance to pests
3. Slower growth (no chemicals/growth hormones)

But is that all that bad? No. But the sentiment perhaps is slightly misplaced. Growth hormones used on cattle, for example, gets degraded by our stomach acid.{{< sidenote bovine-horm >}}_Are organic foods really healthier? Two pediatricians break it down_ ([UC Davis Health](https://health.ucdavis.edu/good-food/blog/are-organic-foods-really-healthier.html)){{< /sidenote >}} Given the lower resistance to pests, the produce suffers higher wastage.

Lower yield would mean larger pieces of land needed for the same output as conventional farming. Again, a couple hundred years ago, this might not have been a problem, but today, the world has changed. What worked then may not work today.

But let us first address the fertiliser problem. Fertilisers, like we saw a moment ago, is primarily nitrogen. And our air is 78% nitrogen. Problem solved.

No.

Plants cannot use pure nitrogen molecules because they cannot break the triple covalent bond that nitrogen atoms share. They need nitrogen compounds, which they can then separate and recombine to form compounds necessary for them. Two processes can do this:

1. Bacteria in the soil that decompose organic matter such as animal and plant waste
2. The Haber--Bosch process

Organic farming uses the former and conventional, the latter. Does that make organic farming a better practice on that front? To answer that, let us look at the amount of forest land we would need to clear so that we can produce enough to feed the entire world. Does organic farming look like a better practice on that front now?

At the same time, does that make conventional farming better? Look at the amount of fossil fuels we need to burn to make nitrogen fertilisers. Does it look better? Perhaps a little, as of now. But remember the impact that will have on climate change, which in turn will lead to lower yields, which may then need more land. A vicious cycle.

Wait, at least organic processes like composting are not releasing any green house gases. In theory, we could convert all our terraces to organic farming grounds and make the whole world go organic. All green, right? Wrong. Composting releases its share of green house gases.{{< sidenote shock-ghg >}}_The Shocking Carbon Footprint of Compost_ ([Applied Mythology](https://appliedmythology.blogspot.com/2013/01/the-shocking-carbon-footprint-of-compost.html)){{< /sidenote >}} The carbon footprint from that is so large that we cannot call it "green" from any perspective.

What then?

Fuel cells. These are one of the solutions proposed, and research is in progress. The aim is to produce ammonia using renewable sources of energy like wind{{< sidenote ammonia-puck >}}_Ammonia—a renewable fuel made from sun, air, and water—could power the globe without carbon_ ([ScienceMag](https://www.sciencemag.org/news/2018/07/ammonia-renewable-fuel-made-sun-air-and-water-could-power-globe-without-carbon)){{< /sidenote >}} which we can then use as fertiliser.

Another observation was that over 80% of the nitrogen we put into the soil does not get consumed by people.{{< sidenote nitro-waste >}}A Sustainable Agriculture? ([Dædalus, September 28, 2015](https://www.mitpressjournals.org/doi/10.1162/DAED_a_00355#fn49)){{< /sidenote >}} Is this something that needs a review? Can we optimise nitrogen use---organic or otherwise?

Lastly, use of biosolids.

The organic proponents oppose the idea of using chemically treated waste, or suspect the presence of chemicals in these treated wastes. While we cannot make these chemical free, biosolids are:

1. different from sludge
2. not laden with harmful chemicals

Biosolids are the product of treated sewage sludge. The sludge goes through a series of mechanical and chemical processes, in treatment plants, after which the organisational bodies that run the plants call it safe for use. Not using biosolids is as good as wasting it. Human excrement or otherwise, once treated, the sludge is no more sludge. And no more dangerous. We have certification levels for them, and we heavily regulate them. India has a set of standards for treated biosolids, and allows their restricted use in organic farming.

The use is "restricted" because every piece of land and its ecosystem is different. We must analyse the soil to establish a nutrient baseline and apply biosolids accordingly, based on the nutrient concentration in them. Also, we have regulations on how far from a water body the land should be, what the usage windows are (for example, no cattle rearing for thirty days since application, no application within thirty days before harvest, etc.) to ensure safety.

When done right, biosolids improve nutrient retention, soil tilth, friability, water infiltration and retention, soil structure, ion exchange capacity and biodiversity. A major plus biosolids have is the reduction in carbon emissions. Think of how well they would offset the carbon footprint.

Can a farmer collect human waste, process it themselves and apply it in their fields? No. The pathogen present in human excrement are unsafe, in general. Waste processing centres handle these with their biochemical or biomechanical processes to bring the levels within safe limits.

Of course, this does not give us a full picture of this discussion. For completeness, we must also look at a couple of prohibitions in the organic method.

Read further:

- [Part IV: Getting a perspective]({{< ref "the-organic-utopia-iv" >}})
- [Part V: The way forward]({{< ref "the-organic-utopia-v" >}})

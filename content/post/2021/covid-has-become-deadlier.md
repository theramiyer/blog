---
title: "COVID has become deadlier"
subtitle: "a WhatsApp University circular claims"
date: 2021-04-06T23:47:37+05:30
episode:
    spotify: "1BO4AhbFLEpkGmbB5mxrhY"
    apple: "1000516644531"
    google: "ODZjNDUyOWQtM2MzOC00Y2MzLWJhNjUtNWY2ZTVjMGViMTc2"
description: |
    A new message claiming to be from the CMC Vellor Doctors’ group is doing the rounds on WhatsApp. Is it another sensational message or a sensible one? Is the newer variant of COVID-19 more fatal?
tags:
- covid-19
- whatsapp university
- mythbuster
- health
aliases:
- '/a-deadlier-covid/'
---

It saddens me that we all fall prey to sensational WhatsApp forwards. And the fact that people I love and respect send this---and may take offence at my fact-checking the claims---worsens the feeling. This evening, I saw the following message in two of the groups that I am member of:

> From CMC Vellore Doctors' Group
>
> The virus is back, this time with more energy, tactics and camouflage.
>
> We don't cough
>
> No fever, it's joint pain, weakness,
>
> Loss of appetite and Covid pneumonia!
>
> Of course, the death rate is higher, it takes less time to get to the extreme. Sometimes no symptoms ... let's be careful ...
>
> The strain is not domiciled in our nasopharyngeal region!
>
> It directly affects the lungs, which means window periods are shortened.
>
> I have seen a number of patients without fever, but an x-ray report shows moderate chest pneumonia!
>
> The nasal swab is often negative for COVID!
>
> There are more and more false pharyngeal nasal tests "COVID") ... which means that the virus spreads directly to the lungs causing acute respiratory distress due to viral pneumonia! This explains why it has become acute and more fatal !!!
>
>
> Be careful, avoid crowded places, wear a face mask, wash our hands often.
>
>
> WAVE more deadly than the first. So we have to be very careful and take every precaution.
>
> Please become an alert communicator among friends and family ...
>
>
>  __________
>
> DO NOT keep this information to yourself, share it with your family and friends.
>
> Please take care and stay safe !

{{< apple >}}

{{< podcast >}}

I am going to ignore the language in that message, and look at the content. I will tell you what I agree with: we must continue to take precautions, wear a mask, avoid crowded places, wash hands, etc. This point is not even debatable. Ten on ten for that.

But that is where the useful information in the message ends. Let us even keep aside the fact that we have no way of verifying if the message did indeed come from the CMC Vellore Doctors’ group (I hope it did not). At best, this message looks like a word salad of terms like "domiciled", "nasopharyngeal" and "respiratory distress".

Second, the virus is not "back"; the virus has been here for over a year. Of course, it has undergone mutations. Viruses undergoing mutations should never come as a surprise. It happens all the time, which is why we have such a large number of mutations (or "variants" or "strains") of, say, the influenza virus. 

The part about joint pains and pneumonia in this case will not surprise a good doctor, because s/he understands the concept of inflammation. Also, inflammation has been common in case of COVID-19 (pneumonia is inflammation of the cells in the airway). Weakness, loss of appetite, fatigue, cough, fever, all are common symptoms of COVID-19.

No cough, no fever is no surprise. That is how the disease has been in our subcontinent (more so in the southern region). I had written about asymptomatic transmission in a [previous post]({{< ref "covid-and-credibility#asymptomatic-and-pre-symptomatic-manifestations" >}}). This is nothing new! The majority of cases in India being asymptomatic is an established fact.

> Of course, the death rate is higher.

I found it funny that the message states something fundamentally incorrect as though it were something obvious. No, the death rate is not higher. When the number of active cases was at its peak in India, the mortality rate was at 1.165%. Right now, the mortality rate is 0.547%. Of course, that is a simplistic calculation, but I challenge the claim that the mortality rate is higher now. The fact is that the mortality rate---no matter how you calculate---is lower now, because the doctors now have a better idea of how to deal with the disease. We are not fumbling around with trials and errors any more. Medical professionals have written books about how to deal with this disease, we have scientific papers about the different aspects of the disease, and in general, we have a much better understanding of the virus and a sort of standard operating procedure now.

Yes, a handful of the variants may be more infectious, but the blanket statement of this virus having become _deadlier_ is outright wrong. In fact, the opposite is true---higher infectivity has shown lower fatality.

> Nasal swab is often negative for COVID.

Ironically, the supposed doctor who threw around terms like "domiciled" and "nasopharyngeal" is now throwing accuracy out of the window. Two of the most common tests for COVID use a nasal swab sample: the Rapid Antigen Test and the Reverse Transcription Polymerase Chain Reaction (RT-PCR) test---which of these is s/he talking about? Because:

The Rapid Antigen Test showing false negative results is common knowledge. The Union Health Ministry even put out a statement that symptomatic patients testing negative using the Rapid Antigen Test must get retested with RT-PCR.{{< sidenote retest >}}Retest negative antigen samples with RT-PCR, Health Ministry tells states ([The News Minute](https://www.thenewsminute.com/article/retest-negative-antigen-samples-rt-pcr-health-ministry-tells-states-132764)){{< /sidenote >}} We have all known this for over _six months_.

And no, a COVID-negative nasal swab does not necessarily mean that the virus directly spread to the lungs. This person does not understand how the COVID infection works. In short, the mucous membrane in the part of the throat right behind your nose (or, the nasopharyngeal region) is where the virus can latch on to with ease; this is the most common phenomenon in case of COVID-19. Why? Because this region is rich in the protein receptors that the virus's surface protein (or the "spikes") can "lock in". The virus enters a healthy cell and uses the cell's machinery to replicate. This way, the infection continues.

But the incubation period (which is what I suppose "window periods" means in this message) is not related to the time the virus may take to travel to the lungs. Incubation period is the time between the infection and when the symptoms start to appear. To know more about how this works, read [my previous post]({{< ref "covid-and-credibility#general-behaviour-of-a-virus" >}}).

We have also known for a long time that the virus does not infect the respiratory system alone. In fact, gastrointestinal symptoms are common in COVID-19.{{< sidenote gi-inf >}}Diarrhea and Other Confirmed Gastrointestinal Symptoms of COVID-19 ([Healthline](https://www.healthline.com/health/coronavirus-diarrhea)){{< /sidenote >}} Fun fact: none of this is new.

Remember that in general, the chances of your immune response to the infection killing you is much higher than the virus killing you.

That said, do take all the necessary precautions. You do not need a sensational message to teach you sense, nor do you need a nanny to watch over you. You are a responsible person.

Now, I urge you to be an _informed_ communicator among your friends and family. Share this information as much as you want (and can). Use the social media buttons below (no, [these buttons do not track you](/privacy/#share-buttons)).

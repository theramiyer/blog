---
title: "The 2023 Parliament Attack"
subtitle: "was more than it sounds like"
date: 2023-12-28T13:26:07+05:30
description:
tags:
draft: true
---

On 13 December 2001, terrorists entered the Parliament building with the intention to injure or kill or take hostage our Members of Parliament. They entered with guns and explosives. A silly mistake of theirs not only cost them their lives, but also raised red flags, because of which our security personnel acted in time and saved our temple of democracy.

On the same 13 December, 22 years later, on the Parliament by terrorists, a group of young Indians entered the Parliament complex (two of whom had the pass to the visitors' gallery) to, allegedly, make a point.{{< sidenote MenJumpVisitors2023 >}}2 men jump from visitors’ gallery in Lok Sabha, open smoke cans, cause panic ([The Indian Express](https://indianexpress.com/article/india/men-enter-lok-sabha-open-tear-gas-canisters-9066308/)){{< /sidenote >}}

When I first heard of this incident, this is what came to mind:

On 8 April 1929, Bhagat Singh, a revolutionary freedom fighter, whom so many of us Indians remember in pride today, entered the Delhi Central Assembly (the then Indian Parliament) and threw benign bombs and pamphlets, to 'make the deaf hear'. ('बहरे कानों को सुनाने के लिए धमाकों की ज़रूरत पड़ती है।') Not much good came of it, though. Rather, the good that came of it was not apparent immediately; it may have inspired hundreds if not thousands of Indians to revolt against the 'farce' of letting Indians have their representation (and say) in the Indian Parliament.

There was a lot of noise about this (2023) incident all around. But to me, this merely seemed like a tongue-in-cheek act---to do something like this on the anniversary of a terror attack.

## The dissimilarities

A lot of those upset with this incident correlated this with the 2001 incident. But in my opinion, this incident was similar to the 1929 incident.

First of all, nobody was injured. The intent, allegedly, was not to attack any particular person, or to cause any harm to anyone.

Second, none of these members are part of a terror outfit---at least, no links have come out so far. Those who masterminded this act and executed it, did not have any prior criminal record.

Third, this was carried out not with the intent to terrorise, but to 'make the voice heard'. Their primary concern was that (and I am paraphrasing what the police said they said) the Parliament was not being used to work on the pressing issues of the present.

Fourth, all those who entered the Parliament complex with the intent to carry out this act are Indian nationals.

Personally, I would not equate this to the terror attack that happened in 2001. The two incidents cannot be any more dissimilar.

The only similarity between the two was that both the acts happened on the 13th of December.

## The problem

No matter who says what---whether you were amused by this or not---that this is an illegal act, is a fact. Protesting is fundamental to a democracy. But this kind of protest?

Of course, soon, this matter will be subjudice if it already is not. I will not comment on the legal aspects of this, because I am not an expert in it. We will see how this goes.

But the attack, according to me, did not end with the trespassers (I say trespassers because they entered an area that they were not allowed to) being arrested. Because something far more serious followed.

## The bigger problem

This incident was but a symptom of a more profound, fundamental issue: a dysfunctional administration. Politics is everywhere, and this is a reality whether we like it or not. We have to accept it. No denying that. But look at what the six arrested in this case are saying. They allege that they decided to do this, because the government, in general, according to them, has not concentrated on areas they should have. And I cannot disagree with them on that.

### Inflation

What is going on with the inflation? Although the rate seems to be slowly reducing right now, the number is hovering around 5%  (as of November 2023), against the RBI target of 4%. That is only part of the story, though. The food inflation sat at 6.61% in October, which had risen from 6.56% in September. Basic commodities such as onion and pulses are driving this rate up. And overall, this trend is going to remain so until at least mid 2024.{{< sidenote IndiaRetailInflation2023 >}}India's retail inflation eases to four-month low of 4.87% in October ([Business Today](https://www.businesstoday.in/latest/economy/story/indias-retail-inflation-eases-to-five-month-low-of-487-in-october-405595-2023-11-13)){{< /sidenote >}}

In other words, unless you earn at least 6% more year on year, your standard of living is likely to deteriorate.

And what riles me up is that the politicians show us this shiny large GDP, saying, 'Look, our GDP has grown by so much since the last year!' And I say, well, sure, but inflation contributes to the GDP, which means, if you somehow did nothing in the country, and the inflation rose to 12%, the GDP growth will still be 12% that year. Is that good for the economy?

### Jobs

When I say jobs, I mean both unemployment and unemployability. The unemployment rate in India as of October 2023 was over 10%, according to CMIE. Unemployment has for sure declined among graduates, but it still sits at 13.4% (note that this is just a 1.5 percentage points lower than the last year). The overall unemployment rates in states like Bihar, Haryana, Rajasthan Jharkhand and Delhi are worrying with large chunks of their population in the employable age range being unemployed.

This is not merely the problem of those who are unemployed; this is everyone's problem. Because while the families that have these unemployed members are facing their own challenges, those of us who are employed and pay tax are facing the brunt as well, because a large chunk of our tax money is going into the welfare of the unemployed, because in a good nation, everyone should be cared for, regardless of their contribution.

And I don't say this with a grudge, but that if everyone in their productive age was employed, we could collectively propel ourselves forward. The employed contribute to the economy not only by paying income taxes, but also by increasing the cash flow, which in turn contributes to more indirect taxes and bigger government coffers, leading to better development.

### Law and order

I hear a lot of "Law and order has gotten better in \[enter state here\] since \[enter chief minister here\] took over. But the NCRB reports say something else entirely.{{< sidenote CrimeInIndia2022Snapshots-StateandUTs >}}Crime in India -- 2022 Snapshots ([National Crime Records Bureau](https://ncrb.gov.in/uploads/nationalcrimerecordsbureau/custom/ciiyearwise2022/17016097489aCII2022Snapshots-StateandUTs.pdf)){{< /sidenote >}}

While the overall crime registration reduced by 4.5% in 2022 as compared to 2021, if you get into the numbers, you will see that:

1. Crimes against SC/STs saw a rise (13.1% against scheduled castes and 14.3% against scheduled tribes).
2. Crimes against women increased by 4%.
3. Crimes against children rose by 8.7%.
4. Crimes against senior citizens rose by 9.3%.
5. Cybercrime surged by 24.4%

So where is the drop coming from, you ask? Environmental crimes, perhaps; they decreased by 17.9%. Also, note that the NCRB report shows the number of cases that were _reported_; not necessarily the number of crimes that happened. Not all crimes get reported.

This is a complex subject, but the NCRB document called 'A Word of Caution' gives you a picture of the complexity. And in the end, it states:

> The increase or decrease in crime numbers, however, does call for a professional investigation of _underlying factors_ ... to suitably address the pertinent issues. \[Emphasis mine.\]

Exactly: focus on the underlying factors.

### Misplaced priorities

No democracy is a democracy without politics. But politicians resorting to populism is problematic. I agree with our prime minister that [freebies should not be given away willy-nilly.]({{< ref "are-government-freebies-bad" >}}) But what is happening? Free electricity in Karnataka has a dark side,{{< sidenote poovannaSiddaramaiahGovtFree2023 >}}Siddaramaiah govt’s free power plan could 'cost Rs 12,000 crore/year, increase consumption' ([ThePrint](https://theprint.in/india/governance/siddaramaiah-govts-free-power-plan-could-cost-rs-12000-crore-year-increase-consumption/1594252/)){{< /sidenote >}} for example. And Karnataka is not alone. Freebies were a big hit in the most recent elections that happened (Telangana, Rajasthan, Madhya Pradesh, Chhattisgarh and Mizoram).{{< sidenote EconomicsElectionFreebies2023 >}}The economics of election freebies ([Business Today](https://www.businesstoday.in/magazine/deep-dive/story/the-economics-of-election-freebies-410621-2023-12-23)){{< /sidenote >}}

These freebies are being doled out at the cost of the taxpayer. And when the state coffers empty, it will be the same taxpayer who will have to bear the burden of filling it up.

Another issue: In a secular nation, why is the government interfering in functioning of religious institutions---in positive or negative ways? What business does the government have in running them? Why is taxpayer money being used to build places of religious importance?

Several arguments can be made from both the sides about spending money on the new parliament building. But, at a time when money could be put to better use (in my humble opinion), thousands of crores of rupees were spent in constructing the complex.

And the list goes on.

### Dysfunctional democracy and federalism

This has happened at least twice: whenever a set of significant bills are to be introduced, the Parliament becomes dysfunctional. For one reason or another. Either these bills are steam-rolled, upsetting the Opposition members who promptly stage a walkout making it a cakewalk for the party in power to make these laws, or, a ruckus is created, which culminates in the Opposition being thrown out of the Houses, thereby making it a cakewalk for the party in power to make these laws.

In all the politics and the noise that the Parliament witnesses, the common citizen like you and I suffer. We lose thousands of crores of rupees in running the Parliament, for nothing.

Power of the states has been reducing slowly and steadily over time. The federal structure that our democracy was designed with, is no longer relevant. Petty politics has taken over. The lines have been blurring in the division of Subjects, with the Centre passing more and more laws to gain control over Subjects in the Concurrent List. To top it off, the introduction of GST has shifted more power to the Centre, with the Centre collecting all the GST, and then allocating the States' share to them. The Centre holding the States at ransom is not unimaginable anymore.

There is also this new narrative of "Double Engine" (meaning the same party in the State as that at the Centre). This approach has further twisted the political gain, because the message there is, 'If the same party is in power at the Centre and in the State, there is much better collaboration and better development.' What this means subtextually is that if the State is administered by a different party than that at the Centre, the people of that State should not expect much collaboration.

As great a political step this is, and as much as our fellow citizens cheer for this, we must realise this is a losing proposition for our democracy.

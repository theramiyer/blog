---
title: "The Earth’s Orbit is not Elliptical"
subtitle: "it’s complicated"
date: 2023-03-20T12:09:00+05:30
description: In a discussion with a gentleman, he said that kids are being lied to that the earth's orbit around the sun is elliptical. I agree and disagree with him on this. And in the process, I try to remind you about an important concept in physics.
tags:
- science
- learning
---

A couple of days ago, I had a rather interesting discussion with a gentleman. Usually, these days, uncles who talk deeply about subjects other than politics or do not parrot noisy news anchors are hard to come by. This gentleman was one of these gems---he was interested in the celestial realm.

While discussing about the celestial objects (and their effects on us), a few minutes into the discussion, he asked me what the shape of the earth's orbit was. And, I, without a second thought, said, 'An ellipse, of course.' He smiled and asked, 'Are you sure?' I nodded matter-of-factly, and he said, 'You are wrong.'

I was unsure which side of the wall he was going to jump. I kept quiet. Sensing this, he asked, 'Is the earth stationary?' Carefully, I said, 'No', almost waiting for him to react. 'What is it moving around?', he asked 'The sun.', I said, still unsure which way this was going. (I have encountered a flat-earther once.)

'Is the sun stationary?'

Now I was sure which side of the wall he was leaning towards. And I saw exactly where he was going. So, I said, 'Right, sure, that way, it's a helix, an elliptical one at that.'

Then, he asked me, 'Now do you accept you were wrong about the orbit being an ellipse?' I had my reservations, and so, I said, 'Well, if I said straight away "A helix." when someone asked me about the shape of the orbit of the earth, people would call me crazy, because not everyone is aware of what we are talking about right now.'

His take was that kids, if taught from the beginning that the shape of the earth's orbit is a helix, they should have no problem accepting it.

I held my stand that we are not necessarily wrong in saying that the earth's orbit around the sun is an ellipse. And I am here to explain my take.

I must state that this is not a "debunk" piece or anything. Any debate has many sides to it. I am merely taking a side that respectfully disagrees with this gentleman.

There are two aspects to my view:

1. We do not go straight to helix, for a reason.
2. The path of the earth's motion is actually not a helix either.

Let us break this down:

## Frame of Reference

We studied this in high school. Let us leave the definition aside for now. Let me give you an example to better understand the concept.

You get into a train. Imagine this train to be half a kilometre long, and tubular (a bit like our Metro coaches). Standing at one end of it, you can see the other end.

You are at the very end of the train, within the last coach. You ask your friend to go straight to the first coach. Your friend and you are now standing exactly half a kilometre apart.

Imagine the train is empty.

You throw the ball with a great force; all of what you could gather. The ball takes exactly 10 seconds to land in your friend's hands. The ball travelled 500 metres in 10 seconds, meaning, you threw the ball at 50 m/s.

All this while, imagine the train was also running, straight ahead, at a speed of 50 m/s (or 180 km/h).

You have never been able to throw a ball faster than 50 m/s. And sure enough, if your friend measured the speed of the ball, s/he would get 180 km/h.

Now, imagine the train is transparent, and is passing through a desert. To an observer outside, the ball will seem to go at 100 m/s (or 360 km/h). This is because while with respect to the train, the ball travelled at 50 m/s, it travelled at 100 m/s with respect to the ground. Or in other words, in those ten seconds, the ball actually travelled 1 km with respect to the ground below.

Now, your friend sits down and starts bouncing the ball. To your friend, the ball looks to go down and come back up, along a straight vertical line. But to the observer outside, the ball would seem to be moving along a parabolic wave.

These two examples talk about _frame of reference_. Your frame of reference, in this case, is the same as your friend's frame of reference. But the other observer's frame of reference is quite different from your friend's and yours.

The train is carrying your friend, you, and the ball. While the ball may be stationary in your hand, to the observer outside, the ball is moving at 180 km/h. Similarly, while your friend only sees a linear motion of the bouncing ball in his frame of reference, the observer outside is able to see its sideways motion (at the speed of the train's motion).

## The earth's orbit is ... complicated

But that is not all. If you look at the earth's orbit from within the frame of reference of the solar system, the earth's orbit is about 3% elliptical. This is observable, proven, and known very well.

But before we look at it with respect to the galaxy, and notice that the solar system itself is being dragged around the centre of it by the sun, there is something more:

The moon revolves around the earth. But since the mass of the moon is comparable to the earth's, if you take the absolute centre of the earth, and trace its path to form the orbit (which is what we do, otherwise, you will also have to consider precession), you would be surprised to see that the orbit is actually like that of a "loop-de-loop" that you see at amusement parks. As in, the absolute centre of the earth is actually going around the common centre of mass of the Earth--Moon system. This traces a looping path along the orbit of the earth around the sun.

You must ignore this if you wanted the earth's path to look helical. Even if you do, and you look at our supercluster, then again, the path is not a helix anymore; it is a different shape altogether, because the galaxy itself is moving in the universe.

You see where this is going.

## The common factors

In physics, a frame of reference is an important aspect when you are making calculations. Without a frame of reference, calculations could become extremely complex, and unnecessarily so. Think of this as removing all the common or irrelevant parameters of a given system, to focus just on what we are looking at. This drastically simplifies our calculations. Especially in situations involving moving objects.

Also, we must not overwhelm a learner. Which is why we go through these concepts in stages. You build a foundation, and then, add more information to it, let it settle, add more information, let it settle, and so on. At every stage, we add information and give some time for assimilation before adding further. Much like eating your meal. Giving it all at one shot would be like going all Scooby-Doo.

And, if calling the earth’s orbit elliptical is a lie, then, are we not lying to kids:

1. That there is an up and a down?
2. That the sun rises in the east and sets in the west?
3. That we can make something "colder"?
4. That things are of certain colours. And that black and white are colours?
5. That the regular tube-lights we have are ‘“cool” daylight’. Or that “warm white” is more on the yellow side?
6. That a year is 365 days and that every fourth year, you get a “leap year”? Or even that a year is "actually 365 days and 6 hours long"? (Yep, it’s a tiny bit less than that.)
7. That electric current flows; from the positive to the negative terminal?
8. That tides—high and low—come and go?
9. That there is something called “centrifugal force” or a “coriolis force”?
10. That their _weight_ is x kilograms?

These statements are not lies, but a mere simplification of what are otherwise more complex concepts. These simplifications help with making learning more approachable.

And this is also a great example for that there is always more to anything than meets the eye.

Until the next time.

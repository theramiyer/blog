---
title: "The Politics of the Balasore Tragedy"
subtitle: Part 3 of the series on the Odisha triple train tragedy
date: 2023-06-11T10:04:39+05:30
description: What are the political issues around the Balasore tragedy? Shouldn't Ashwini Vaishnaw resign on moral grounds?
tags:
- travel
- governance
- employment
- politics
---

This is a part of the series on the Balasore triple-train tragedy. In the previous parts, we looked at the different technical aspects of the Indian Railways, as well as the audit observations from the CAG report, that are relevant to the context.

If you did not read the the previous parts, I suggest you do:

- [Do you know Indian trains]({{< ref "do-you-know-indian-trains.md" >}})
- [Operational Lapses in Indian Railways]({{< ref "operational-lapses-in-indian-railways.md" >}})

Here is what we will talk about in this part:

{{< toc >}}

As usual in our country, our politicians picked up the first opportunity available to politicise the tragedy, and as they say, did their own things. From the "ruling" party to the opposition, nobody left a stone unturned after the customary condolences.

My stand on politics is simple: Disregard all politicians unless they have significant work to show, done in the interest of our country and our people. Humility and honesty are perhaps too much to ask for from a politician, so, work can be the first priority.

## Inspection and slogans

I don't dislike Ashwini Vaishnaw as much as I dislike most politicians. I like that he seeks newer technologies and tries to implement them. He may have failed at many levels, but that is tolerable to an extent, at such a scale. While I do not mean to ignore his shortcomings as the head of the Ministry of Railways, I do think he is more competent than many Railway Ministers we have had in the past decade or so.

When he visited the scene, I thought that was normal, and for him not to would have certainly surprised me. But after all the inspection, discussions and the statement, he picked up a megaphone and went, "Bharat Mata ki Jai!" Why? What has this to do with anything in that tragedy? Was he trying to get everyone's morale up that way? Well, news flash: it doesn't work like that. Say that to an army company going into a battlefield, it will work. Say it to the sportspeople leaving for the Olympics, it would work. In this horror, this is not about the country, it is about the hundreds who died, the hundreds who were injured. This is a solemn moment, one should be sensitive to these things. I would expect that of a learned minister. Everything from his clothing to his demeanour otherwise, were perfect for the situation. He did not have to go around shouting slogans. I am sorry, the patriot in me disagrees with this kind of display.

## Setting wrong expectations

Second, he stood as an example for something pointed out as a deficiency in the latest CAG report on the Railway derailments: safety equipment. He was going around, coming out from under mangled coaches, with no safety gear, not even so much as a helmet. Why? What kind of example are you setting? Tomorrow, when a field worker asks for a helmet and safety gloves, his officer might say, "Even the Railway Minister did not bother about these things; why do you need them? Get out on the field now, do your job." Mr Vaishnaw exacerbated the problem.

One thing to note was how the people stepped up. Before authorities or the professionals could arrive, people out there helped those in need, with whatever they could. There was apparently more blood donated than was necessary. We could hear a lot of heart-warming accounts by the victims about how the people in the neighbourhood helped them get what they needed. This simply goes on to show how humane the people in the locality were.

## Ashwini Vaishnaw must resign

No. He should not resign. I find this kind of argument nonsensical. What moral responsibility? What would one do with that "moral" act of stepping away? Tomorrow, when someone holds him accountable, he would show his act of resignation, state that he takes full moral responsibility, and that he is apologetic. And walk away. What good is that going to do?

Resignations may be a moral high ground. Sure. Sure, it may signal to the future Ministers of Railway that they would have to resign if they fail in their duties, but really, really, what good is that going to bring?

1. No Railway Minister who resigned lost his career in the history of Independent India.
2. Is this really, fully, the Railway Ministry's fault?
3. Is that fault directly linked to Mr Vaishnaw? Did he personally make a mistake in this accident? What and how?
4. If he accepts moral responsibility, then his entire team---including the entire Ministry of Railways must resign from their jobs.

I am not saying he is not at fault. But think about it, what exactly went wrong here? If you say a pointperson failed, then him, his boss, his boss, his boss, and so on, up to the entire Railway Board must resign. What does that achieve anyway? 'Suspend the pointperson!' Okay, but then what? Is the suspension going to make him feel ashamed? When was the last time a suspension made anyone feel ashamed? How do you know s/he is not already ashamed? And what after being ashamed? How is someone's suspension or resignation fixing the problem? Or even, how is it, in any way, acting as a deterrent?

And if Ashwini Vaishnaw steps away, who takes over? How sure are you that s/he is better than Mr Vaishnaw?

Again, is Mr Vaishnaw "_nirdosh_" as they say? No. I say, him and his ministry should take ownership of this rather than washing their hands off. Act on it by working to fix all the issues that led to this tragedy. Stepping away is like chickening out. I do not support that. Those that are demanding his resignation, of course, are free to choose their stand, but at least, they should have a solid argument behind what they choose.

## Vande Bharat and faster trains

Another claim that came about was that the Railways were focusing more on newer and faster trains like the Vande Bharat and the Bullet Train. Agreed. But to say that because all the concentration is on those and therefore there is no money available for upgrading safety is inaccurate.

The Indian Railways are sitting on surplus funds. Funds are not the problem here. The problem, well, at least primarily, is management. There are vacancies that are not being filled, the employees (full time and contract) are overworked, they lack training, inspection and maintenance blocks are not allocated, and so on. We will talk about these in more detail shortly.

I personally did not find the *Vande Bharat* all that much better than the Shatabdi in my frequent route at least: the difference in travel time is merely 25 minutes. Also, in my experience, the Shatabdi offers better comfort than the Vande Bharat.

Having said that, I am not against the Vande Bharat. Sure, I find it unnecessarily more expensive than the Shatabdi and all, but I do not oppose it. I think it is time we got trains like the Vande Bharat, albeit with the comfort level of Shatabdi. Vande Bharat can charge the Vande Bharat premium, but at least offer the same comfort if not better, than the Shatabdi.

Having also said that, I think the Vande Bharat is more efficient (in terms of power consumption) than the Shatabdi, because, well, physics. The Vande Bharat loco is more aerodynamic, for starters. Over the long run, the Vande Bharat would consume less electricity than the Shatabdi. Even if not, given the efficiency it brings into the system, it is better than the Shatabdi. I think the Vande Bharat in general is technically better than any other high-speed passenger train we have in existence today.

Oh, and by the way, I hear comments about how even a simple cow manages to damage the Vande Bharat engine. Right, it sure does. And every such collision means that the loco needs maintenance at the yard, making it lose its, say, monthly running time. But there is nothing wrong with its safety. The shell is merely there to deal with the air. (Believe it or not, it matters. If not, we would be flying on box-shaped aeroplanes today and sports cars would look like rhinos.) These shells are made of light metal in order to not add too much weight (aeroplanes are made of aluminium alloy for this reason). These engines still have the strong steel body within. How many loco pilots died because of the Vande Bharat hitting a cow?

## Whom does the Railways really cater to

But right, Vande Bharat caters to a miniscule portion of the total passengers the Indian Railways carry in a day.

The Indian Railways still cater to those who cannot afford even a second class ticket, let alone airlines. And the scale of passenger traffic that the Indian Railways carry are perhaps unimaginable for the airline industry in India anyway.

Like we saw in the previous part, the Indian Railways, in general, must focus on safety of the other trains. And as we read in the previous part, the introduction of the Vande Bharat and its demands will ensure better safety for other trains.

But politics aside, we still have not answered the question, who is responsible for this tragedy? Read on:

- [Responsibility for the Balasore tragedy]({{< ref "responsibility-for-the-balasore-tragedy.md" >}})
---
title: "I Watched The Kerala Story"
subtitle: "what Sudipto Sen has pulled off is impressive"
date: 2023-05-21T08:08:55+05:30
description: "How rampant is the conversion racket shown in The Kerala Story? Is it fact or propaganda? Is the government involved in this conspiracy? What are its implications? Find out in this article."
tags:
- constitution
- religion
- fact-check
- propaganda
- India
---

Last night, we finally went to watch the film. I wanted to know what the film was going to bring to the table. What I heard and what I read about the film were the exact opposites, and that kindled my curiosity. And then, the last weekend, when I met my cousin, she seemed in favour of the film. And that increased my curiosity. Finally, yesterday, I gave in.

Mentioning the plot in short is customary in pieces like these, and so, here goes:

Shalini Unnikrishnan, from a Hindu Malayali family, goes to a nursing college in Kasaragod, against what her grandmother feels---her mother is in support of her leaving town to study. Shalini is this teen who does playful, innocent things like making faces in front of a buffalo, and poking fun at a Kathakali dancer. Very cute, happy.

One morning, she gets dropped off at her college, in an autorickshaw owned by a Muslim man (the windshield gives it away). The place seems like a typical college in any Muslim-dominated place: slogans like "Free Kashmir", something about Osama Bin Laden, etc., on the college walls, students going around in Hijabs, and so on. (The college, during its orientation later that day, distances itself from the wall art, though.)

She enters her hostel room, where two girls---Nimah Matthew and Gitanjali Menon---are chatting. They take less than a second to identify Shalini, and Shalini, playfully mentions the jibe that typically people from the Brahmin community hear in places like Kerala. Everyone laughs and shares a light-hearted moment. Then enters Asifa Ba.

Asifa is supposed to be from a nearby Muslim-dominated town of Malappuram. She knows a lot of people in the college and tells the trio that she will ensure they enjoy the next few years there.

At the orientation session they all attend later that morning, someone clicks pictures of the trio and sends them to a Muslim religious group. The men are happy to see the girls' photos. And so begins the story.

Asifa is a relentless campaigner for Islam, who constantly says things that make the girls doubt the omnipotence of their respective Gods. One day, the girls get molested at the mall, and Asifa tells them that if they wore the Hijab, Allah would protect them from such incidents. Because of the constant peer pressure from Asifa and her cousins, the Hindu girls slowly convert to Islam. The Catholic Nimah refuses to convert and confronts Asifa and raises her voice, when Asifa says anything about the God and the Christ.

The film shows how Muslim men pull off _Love Jihad_:

1. Lure unsuspecting, naïve Hindu girls
2. Get the girls to fall in love with them
3. Get physical with them and get them pregnant (or engage in revenge porn)
4. Make conversion a prerequisite to marriage, saying, ‘How can we take home a Hindu girl? Our family will not agree.’
5. Convert the girls.
6. Marry them (or get them married to other unknown Muslim men)
7. Take them to Syria.

And then come the graphic descriptions and imagery of the travel to Syria, the sex slavery, the ruthless killings, absolute disregard for women, etc. The film is not A-rated for nothing.

Now, let us get to what I really want to talk about.

## An argument for The Kerala Story

My cousin, when we were chatting about random things (including this film), told me how---for lack of a better term---a witch-doctor, managed to make her friend’s family, shell out over three million rupees. It involves playing with people's fears. The friend tried everything they could to dissuade their family from engaging in this, to no avail. The witch-doctor, apparently, even dug out a dead foetus buried in their backyard. (Nobody knows how it got there.) I was more shocked than disturbed by this. My cousin's argument was, if such bizarre things can happen in Kerala without anyone knowing, why is it difficult to believe that 32,000 women went missing?

## A case against the official religious conversion numbers

Live-in is legal in our country, as long as those involved are consenting adults. When you go to register your marriage, you have to show any conversions, because that decides under which act your marriage is registered. But if you are not registering your marriage, and yet convert, _I am not aware of_ how one could confidently arrive at the conversion numbers that we have today. Note that I am not denying the conversion numbers we have, I am questioning them. There is a difference.

In other words, if a Hindu woman marries a Muslim man after converting to Islam, go through a proper _Nikah_, but does not legally register the marriage, they can continue to live as a couple---they are married according to Islam, but not necessarily according to the Indian law. And that conversion may not be part of the official numbers.

## A case for the number of non-Muslim girls that get sent to Syria

Common sense would make us think that the parents would immediately file a case if their child gets converted to Islam and married to a Muslim man. But this is Kerala; Kerala respects and accepts all religions. Kerala is this state run by Communists, who themselves reject all notions of God and religion, but are liberal enough to let their child follow what the child wants. Since the Communists have an equal stand on any religion, it does not matter to them which religion their child picks. As long as the child is happy. And everyone in Kerala is uniform in this thought. Isn’t that why a 100% of those eligible to vote, vote for the CPI there, and everyone wears only red shirts, drinks only tea, and eats only fish and rice?

If the child gets converted to Islam and gets sent to Syria, that would be after the child cuts all ties with the parents. The parents helplessly accept it, because at the end of the day, every parent wants their child to be happy---even at the cost of the parents' unhappiness.

Now that the child has cut ties with the parents, the parents have no way to know where the child is. The girl could be taken to Colombo (which the parents may know), but then where she goes from there is unknown to the girl, even. The girl gets brainwashed to not tell her parents that she is going to Syria. Because, obviously, the parents would get anxious about it all. Everyone knows what happens in Syria (and Iraq). Right?

The Muslim men, after a point, take away the girls' cell phones saying Sharia does not allow women to have cell phones. As rightly pointed out by Shalini in the film, "there was no cell phone when Sharia was written". But then, as the film points out, the husband's word is the wife's command in Islam (and other religions).

The girl, during her indoctrination, gets detached from her family. The family hearing from the girl becomes a rare occurrence. The child's happiness is paramount, so, they leave it at that, because they feel, their child is happy where she is. Because that’s what parents are: naïve and superhuman. And this way, the girls disappear.

## A case against the numbers in the film

Having the points presented by the film on one side, let us examine how the numbers hold up otherwise; a good argument must hold water.

### Conversions and migration

The film (or at least the trailer) first claimed that 32,000 girls went missing from Kerala. There was a lot of noise about how the numbers are wrong. Then came the revelation that the 32,000 girls vanished over a period of ten years, meaning, on average, 3200 vanished in a year.

Not too big a number, one might think.

But even for 3200 girls to vanish like this, there should be some substance to the number. Upon asking, Sudipto Sen said he derived this number from a 2010 report that Oommen Chandy presented in the Kerala Assembly about the religious conversion of girls to Islam in 2010, and the report said the number was "2800 to 3200".

Let me repeat that: Mr Sen claims that Mr Chandy said that up to about 3200 girls are converting to Islam in a year.

Mr Sen extrapolated that to 10 years and arrived at the number of 32,000. Seems fair?

No, here is why: First of all, I could not find a document, a video or a news article that says Oommen Chandy said 2800 -- 3200 girls were converting to Islam in a year. What I did find was him saying that in the period of 2009--2012, 2667 women---of whom 2195 were Hindus---converted to Islam.{{< sidenote radhakrishnan2500WomenConverted2012 >}}Over 2500 women converted to Islam in Kerala since 2006, says Oommen Chandy ([India Today](https://www.indiatoday.in/india/south/story/love-jihad-oommen-chandy-islam-kerala-muslim-marriage-115150-2012-09-03)){{< /sidenote >}} That means about 889 women in Kerala converted to Islam in a year on average, in that period.

But again, this says, "converted to Islam". I found no reference to that all these young women joined the ISIS after converting. We could extend this to the 32,000 as well---no matter how baseless the number is.

In other words, what the film seems to imply is that every woman who converts to Islam, does so only to join the ISIS. I find no reference to this anywhere (other than the film itself).

Most members of the ISIS (if not all) come from one sect of Islam: the Sunni sect. In fact, the Sunni Jihadist sect. Are all Muslims Sunnis? And are all Sunnis Jihadists?

Also, note that all women travel to Syria with their husbands, which doubles the number of people travelling to Syria. I am unable to wrap my head around the idea that 64,000 men and women joined the ISIS. All from Kerala. Though over a period of 10 years.

Oh, of course, Muslim men can have four wives, which could make the number 40,000, not 64,000. Will these men have at least one wife who would not be taken to Syria? Then, the number is about 43,000.

### The numbers superimposed on the population

Kerala, with a population of about 35 million,{{< sidenote govt.ofkeralaAnnualVitalStatistics2022 >}}Annual Vital Statistics Report - 2020 ([Department of Economics & Statistics, Govt. of Kerala](https://www.ecostat.kerala.gov.in/storage/publications/534.pdf)){{< /sidenote >}} has sent 40,000 people to ISIS in a decade?

While the film does not explicitly say that all these 32,000 women went from Kasaragod, it does seem to imply that. Because while the claim is that most of these recruitments happen from Kasaragod and Mangalore, the film shows nothing of Mangalore. Also, the title is _The Kerala Story_. That makes me think this entire number belongs to Kerala (more specifically, Kasaragod). I find it natural to compare the population numbers of Kasaragod to the number of people sent to ISIS.

Kasaragod---a district with a population of about about 1.41 million---is shown to be the hub in Kerala for ISIS recruitment. Let us dice this down to women aged over 19 and under 35. The number of women in this age range in Kerala is about 4.3 million. The percentage of Keralite population living in Kasaragod is 4%, according to the same report mentioned above. 4% of 4.3 million is about 172,000.

32,000 out of 172,000 women (or even 200,000 women) converted and vanished, and nobody batted an eyelid?

Quite a stretch, if you ask me.

Also, consider what the Union Minister of State for Home, Mr G Kishan Reddy, himself said in 2020 in the Parliament,{{< sidenote johnReportPresenceKarnataka2020 >}}UN report on IS presence in Karnataka, Kerala factually incorrect: MHA ([Deccan Herald](https://www.deccanherald.com/national/un-report-on-is-presence-in-karnataka-kerala-factually-incorrect-mha-890513.html)){{< /sidenote >}}

> claiming presence of 'significant numbers' of ISIS terrorists in Kerala and Karnataka, is factually not correct. The Government continuously takes necessary measures to put forth India's correct position through established mechanism in unequivocal and categorical terms at various international, multilateral and bilateral fora and also through diplomatic channels.

Put in other words, if Kerala and Karnataka do not even have 'significant numbers' of ISIS terrorists, the states that do have,{{< sidenote specialcorrespondentMostCasesRelated2020 >}}Most cases related to Islamic State filed in south India ([The Hindu](https://www.thehindu.com/news/national/nia-registered-17-cases-related-to-is-presence-in-south-122-arrested-rajya-sabha-informed/article32619053.ece)){{< /sidenote >}} are supposed to contribute several tens of thousands more of ISIS terrorists than 32,000, given that 32,000 is not a 'significant number'. By that logic, the majority of the ISIS operatives globally would be Indian. How could the film (or the audience) miss this point?

### The passport problem

The other aspect to be understood is passport ownership. A 2017 report from the Ministry of External Affairs said that the passport ownership in India stood at 5.15%.{{< sidenote businesstodayOnlyCentIndia2017 >}}Only 5.5 per cent of India's population have passports: report ([Business Today](https://www.businesstoday.in/latest/economy-politics/story/passport-seva-project-passport-seva-kendra-mea-data-passports-85474-2017-07-24)){{< /sidenote >}} Even if one argued that the percentage had somehow doubled in the last six years and stood at 10%, we are still looking at a small number of passport holders.

If one stretched it that all these people left the country on fake passports and visas, there is absolutely no way in this connected world that the numbers surpassed the legitimate passport holders. That would kick off alarm bells everywhere, not just India.

Superimposing this number on the young women population of Kerala---which stands at 4.3 million---the number of young Keralite women holding passports would be around 221,450. Kasaragod would be much lower at 8858. 32,000 out of 8858 women ... oh, wait.

For this math to work, the rate of passport ownership among young women in Kerala would have to be at least about 18.6%, and _all_ of these women would have to convert to Islam and join the ISIS---and travel.

Granted that passport ownership does seem to be the highest among students and among people whose income is above the poverty line, still, what are the odds?

Also, those above the poverty line are more likely to involve the concerned officials and push for enquiries. This is a wrong group to target, if you ask me.

## Let us talk conversions

The conversion numbers are, in general, low for a good reason. Everyone gets indoctrinated with religious ideas at a very young age. This indoctrination is hard to beat.

A very influential teacher of mine used to say things like what Asifa said in the film, to an entire class of impressionable 10-year-olds, except, our teacher was Christian. None---none---of us converted to Christianity. During the Facebook era (has it not ended yet?), she sent us messages about the greatness of the Christ and the saints. We either shut her off, or respectfully declined to engage. None of us converted.

No non-Muslim I know wears a Hijab. None of those who have gotten sexually harassed, wore a Hijab to protect themselves from it. Instead, they took action. Nobody waited for God to come protect them. Ask any of them and they will tell you, "God doesn't work like that." I mean, it would be silly of any of them to say, "My God is useless because S/He can't protect me from molesters."

In other words, most people I have seen fall in Nimah Matthew's category, when it comes to religion and conversions.

Also, which teen (or tween) converts to a religion for the fear of "hellfire"? This is beyond laughable!

## Should the film be banned

I do not identify myself as a liberal. I stand for freedom of speech, but I also advocate reasonable restrictions to freedom of expression on public forums. Anybody who understands human psychology (and the cinema market) will understand this. This is a nuanced take, and controlling my temptation to explain, generally speaking, though at the risk of sounding arbitrary, a person who is exercising their freedom of speech must exercise it responsibly. Because people are impressionable.

What will banning the film achieve, you may ask. 'People will circulate the film on the Web (or platforms like Telegram), and anyone who wants to watch it, will', you may say. And you would be right. Banning the film will have the same effect as banning the BBC documentary on Mr Modi had.

But ask any producer if they would be willing to pump money into a film that gets banned but circulated on the Web for free.

We must understand, at least after _The Kashmir Files_ and _The Kerala Story_, that these films get made because of the money they make. These are easy wins. They have nothing to do with the communities they claim to represent. These films get made and earn well on the Box Office; political parties that can gain from such films will utilise them to their advantage; the producers will be happy, the politicians will be happy, the audience will feel entertained and walk out of the theatres with their blood boiling.{{< sidenote apterDangerousPleasuresOutrage2018 >}}The Dangerous Pleasures of Outrage ([Psychology Today](https://www.psychologytoday.com/us/blog/domestic-intelligence/201803/the-dangerous-pleasures-outrage)){{< /sidenote >}} But the communities who need a voice will continue to be ignored. Neither the politicians, nor the film-makers will do anything about it (because, ‘What more can film makers do?’), nor will the people hold the government accountable.

It is happening with the Kashmiri Pandits, it will happen to the girls that get converted and sent to Syria for sex slavery. Not just from Kerala, but across India. That part will not change. There will be promises made about these changes, but nothing will happen on the ground.

What _will_ happen, though, is that one section of the society will alienate another for no good reason.

Note that in cases like conversions, the administration alone cannot possibly do much more than creating laws and putting in place an efficient law enforcement.

## The ₹150 lesson

When I said Sudipto Sen pulled off something impressive, I meant it with no sarcasm. Vivek Agnihotri showed the way, and Sudipto Sen raised (or should it be "lowered") the bar.

If you watch the film (which I suppose you could on OTT, later this year), you will feel that people with iPhones and a rudimentary knowledge of Blender make better films than this, in terms of production quality. And the story, as my brother put it yesterday, was "jarring".

Am I denying conversions?

No.

Am I denying Islam extremists having terror links?

No.

Am I denying that members of the ISIS brainwash women into joining them and make them sex slaves?

No.

But that is the thing. Sudipto Sen cooked up something with little substance and mostly baseless hateful fluff. The little investment he made will get him disproportionately massive returns.

What do I mean?

First, he weaponised the trailer. He made a tall claim---of 32,000 girls missing from Kerala, rotting or dying in prisons or ISIS camps. He changed it later to "story of three girls", which is a brilliant stroke. The tall claim resonated in the echo chamber that subscribes to that view, and those looking to enter it. The fact-checkers can come later, and to satisfy them, the number can always be "corrected". It was a brilliant stroke because now, everyone will watch the film with the "32,000" lens.

Second, when challenged, he said he had done his own research, and this number could not have been gotten from government numbers (because “everyone is involved”). In other words, he made up a conspiracy theory. That automatically took away the credibility of any official statistics that we could get. Later, apparently, he asked at the JNU, 'Do numbers matter?'

Third, he touched particularly sensitive areas that people otherwise think twice about speaking in public. This led to people discussing this primarily in closed circles, in whispers. This added to the bit about the conspiracy theory, and ensured it spread a lot more, because we are suckers for conspiracy theories.

Fourth, he did not put the government in a spot. He never mentioned anything about the Government of India, except implying that _agencies_ such as the Intelligence Bureau were sleeping while 32,000 girls disappeared to ISIS camps right under their noses. He probably knew that when "Kerala" appears in the title, the politicians and others in Kerala---and nobody else---will react.

Only those who did not miss their Civics class would know that National Security comes under the Union List in the Constitution---with the Government of India having _exclusive_ control over it. (Which is also why I quote the _Union_ Minister of Home here.)

He perhaps also knew that the incumbent administration at the Centre will keep mum because the narrative favours the most powerful political party there. More so when the narrative implicitly attacks a state which the party is trying to create its base in, and explicitly attacks Communism.

Fifth, blame it on the absence of teachings of Hinduism in families. In broken Malayalam and Hindi, Gitanjali tells her father that because he did not teach her anything about the Hindu values and taught her only Communism, she ended up being manipulated by Asifa. This is a general belief in the insecure Hindu minds; why not play straight into it?

Sixth, the film stars no major actors (except Devadarshini, perhaps). Even the lead actor is essentially a nobody, who has not appeared in any film of consequence or ever played a consequential role. Her acting skills are to blame for it, but whatever. Overall it means low expenditure on the cast.

Seventh, the crew probably hired an intern for the CGI. The flame scenes, for example, were as though done by a novice, using the generic first-tutorial Blender template.

Eighth, the dialogues were absolutely irritating. Force-fitted broken Malayalam, terrible accents, unbearable grammar, etc. Isn't Adah Sharma supposed to be from Kerala? Asifa speaks the Mumbai Hindi in her attempt to seem South Indian. All this riled me up even more. Low effort and probably no expenditure on professional dialogue writing.  (Compare this with the accent of the ISIS operative in _The Family Man_.)

But none of the last three matter, because we are a culture who are innately imaginative. We only need to be shown symbols; we are fully capable of imagining the rest. The CGI does not have to be good, the dialogues do not have to mean anything, the actors' acting skills do not matter. And Sen knew it.

And there, that is your formula to create a multi-billion-rupee Box Office hit on a shoestring budget. The lesson I learnt paying ₹150 for the ticket.

## But strange things happen in Kerala

First, let us address a couple of arguments in favour of the film. Like I said, my sister mentioned the bizarre finding of a dead foetus buried in her friend's backyard, saying, "All sorts of crazy things happen in Kerala; have you ever heard of something like this?"

I had not, of course, but can we equate this with national security?

Can we say with absolute conviction that tens of thousands of girls go missing in a state, and none of the parents or friends even so much as file a habeas corpus petition? Not even one? Or are tens of thousands of families silenced? Doesn't that just make it much worse for the perpetrators?

Keeping tens of thousands of families silent over such a big national security issue is impossible to pull off for a decade. Even if we go by the logic shown in the film: if Gitanjali can contact Shalini's mother and break the news about everything that happened to them, even if to die later, and if Nimah can approach a police officer to lash out at the police inaction, then compound the effect by thousands when tens of thousands of girls who either survived or so much as caught a whiff of what was going on and spoke up; the issue would have blown in the face of the conspirators. Not all 32,000 mothers would stop contacting their children. Not all 32,000 disappearances will miss the radar. Not all conversions can be brushed under the rug. And it would be impossible to ignore the connection when thousands of reports come out.

## How to handle the conversion issue

Scientific temper. Not mere education in science, but promoting scientific thought. Converting naïve believers may be possible, but converting someone with a scientific temper is several times more difficult. Of course, someone with a scientific temper will question your religion much like everybody else’s, but that is what we need today.

Teach your children the difference between God, religion and culture. Culture, to me, is a lot more approachable and quicker-evolving than religions, and therefore, more relevant (though rife with issues like obscurantism). Religion and God do not have to have anything to do with each other. God, if you believe, existed long before religions, and will exist for long after religions die a slow death. Modern laws can replace religion because though often flawed, they are more useful than any religion. Philosophies can remain, to be studied. I am sure the philosophy behind every religion has something to teach us, as long as we approach them in the right sense.

I do not support conversions. Or rather, the only conversion I am willing to support is rejection of all religions. Discovery of the God should be a personal journey, and nobody should have anything to say about someone’s self-discovery. But again, that is my personal view.

If you care so much about your religion, do something that gets your children interested. The more obsolete religious restrictions you show, the more they will be disenchanted. Make them realise that Gods don’t have to be the silly, self-absorbed, insecure jerks that they often are made out to be. Gods need not be heroes either. Teach them about the “incarnations” from the standpoint of a practical life and the values. (Of course, for that, you will have to learn about them yourself, first.) Do not make up magic; kids do not buy that anymore. If you are a Hindu, find out why we have so many gods; what they stand for. Or why they are considered gods in the first place. Tell the children about the different schools of thought we have in Hinduism. Today’s children are inquisitive. They want answers. Find out the answers for them or help them look. And remember, "Because (that is how it is)." is not an answer.

Let your children pick their own paths; tell them enough so that they are empowered to pick one (or many) on their own. Teach them how to tell silly and deep apart. If your child tells you gods are not real, ask her how she came to that conclusion. Get her to think. That is the important part.

More importantly, remember that fundamentalism is not the answer to fundamentalism. Do not listen to fundamentalists (of any religion). Train your children to spot these red flags (and that these red flags apply to every religion). Also, tell them to stay way from any of these “bring back the glory of the past” campaigns, and that humanity should always look forward. Because that is how time moves.

Note: If you did not sense the sarcasm in some of the parts of this article, well, you should.

---
title: "I Watched Animal"
subtitle: "and ... it's complicated"
date: 2023-12-09T12:44:09+05:30
description: Toxic masculinity or a powerful visual delight? Read on for my top ten lessons from the film.
episode:
    spotify:
    google: ZDRlYTliYTAtNmY4Mi00ZThhLWFhOTctYzFhYzE2MDYzZDEz
    apple: 1000638337728
    amazon: 2f315f4b-8bd3-4647-8403-865ba7035ace
tags:
- bollywood
- psychology
- society
- film
- abuse
- opinion
- satire
---

When I say, 'it's complicated', I do not mean the film has a complicated or nuanced story line. (Of course, such a story is not what the audience seems to have gone there for.) My sister dragged me to it, because she did not want to watch the treat all by herself. So, yes, I went for it after telling my brain not to look for logic. However, the fact that I am even attempting to write this post tells you that my brain ignored that advice anyway.

{{< toc >}}

This post contains spoilers. If you have not watched the film and you plan to, be warned.

{{< spotify "6QS2TC8OM1mDbPVpJtV56o" >}}

{{< podcast >}}

## Toxic masculinity

The elephant in the room.

I tend to not like this term, perhaps because it gets used so often these days, and it seems to trigger a lot of us---including me. As a person who tries to see things for what they are, I usually avoid giving a term to anything until I feel I know enough about it.

So, let me approach this from another standpoint. Toxic or not, you decide. Masculine or not, you decide. Noxiously masculine or not ...

This film is violent. No denying that. Is violence a masculine characteristic? If yes, this film is predominantly masculine. Is violence toxic? I think this level of violence is toxic. You may have a different opinion. Because in any case, the dose makes the poison.

## Viewer discretion

Does the film celebrate violence? Yes? No? You know if you have watched it. One argument is, 'Don't watch it if you don't like it.' That is a reasonable ask, of course. Keeping in mind the cinema's refund policy, the money you paid for the ticket is the only thing that can potentially stop you from exiting the hall, if you usually fall for the sunk costs fallacy.

If you think killing and hitting people cannot be shown in films, a large chunk of films will not get cleared to be shown in cinemas. The action genre might completely vanish. If the CBFC has cleared the film to be played, then the film abides by the criteria that the film regulator has specified. The film is A-rated, which should give you an indication of what to expect. You certainly have the choice in all aspects.

You cannot curtail someone's artistic freedom.

## Impressionability

Having said that, people are impressionable, no matter their age. From serial offenders to tribes involved in illegal activities, examples of this abound. Impressionability may reduce as one matures, but one is never fully immune to it.

So, in what sense are we saying that those who have crossed the age of 18 are not impressionable? In fact, one could argue that making age the criteria here is in itself flawed. Many of the traits I have today have been acquired after I turned 18. People switch their religious beliefs, their political alignment and what not, even after they turn 50.

In our world, films like this, which naturally have an element of attraction, coupled with social influence, can lead to undesirable consequences. In other words, if something gets celebrated socially, those who did not yet align in the direction of it will tend to align, irrespective of their age.

## The problematic element

Overall, the film revolves around a boy (and later a man---well), being an "alpha male". It is portrayed very well by Ranbir Kapoor, of course. I heard that most of the film reviews lauded the actors for their performances, and I agree with them on that front. Ranbir Kapoor, especially.

This "alpha" tries to protect all his family members. He's aggressive. He carries a gun to his sister's college and fires at least a hundred shots into the floor in a class. Because some boys ragged the sister. Of course, the father gets livid when he learns about the incident. Slapping the boy is the obvious consequence (one loses track of the number of slaps people serve each other in this film, by the way). Sandeep Reddy Vanga has learnt his lesson from the Arjun Reddy aftermath, though: Ranbir Kapoor does not slap Rashmika Mandanna, she slaps him. Several times.

After all, what love is love if it cannot be expressed in slaps?

Everywhere the Singhs go in this film is owned by them. 'My sister is unsafe in our own college.' 'Intruders come in and kill around in our own hotel.' 'This private airfield is ours.' So on. This is an _atmanirbhar_ family. I am not sure if the logic behind this was about 'Whatever I do is in my personal property (and therefore, I set the law here).' Only problem is, the law does not work like that.

In any case, sinfully sweet privilege drips from everything that Ranbir Kapoor's character did in the film. Will the audience take that message back home? Because this was one of the most underplayed elements in the film. And let us face it, privilege does give you the license to do all this; at least in our country. Well, sure, some things can (and will) bite you in the rear some day, despite your privilege, but that is often a matter of chance.

## The overall takeaway

The way I see it, this whole story was a losing proposition on all fronts. The alpha and the wife split. The father and the son never had a mutually aligned relationship to begin with, which only deteriorated. The alpha lost at least two of his cousins, well, to his other cousins. I could see a lot of property damage. One sister of the alpha's lost her husband. The other did not get married, but lost her love interest. The grandfather had lost his brother anyway. I mean, at the end, nobody---absolutely nobody---won anything. Or I am missing something---I seem to have zoned out at some point. Perils of going for the night show of a 3:20 hour long film.

But, here are the lessons that I think I learnt:

1. The alpha male's female (I do not have to specify this; aren't all alpha males cis-het?) partner lost the relationship in the end. Because the girl became sensible. To paraphrase her, when the wet dreams subside and the testosterone fog clears, you see sense.
2. The alpha male promised his partner that he would never betray her. As an example, he cheated on her, but admitted to it. Transparency is the key, after all. Transfer of burden? What is that?
3. If your father does not show you any affection, become loud and violent, and gain the entire world's attention. And then, blame your father for being so blind that he could not see what the entire world could. Slap yourself several times in the end. Show some self-love.
4. Risk your entire (estranged extended) family's life to protect your interests. Seek them out, get them to protect you, because only your "clan" will spill their blood on your behalf.
5. Hurt your father over and over, and wonder why he dislikes / disapproves of you. Give him a new reason to dislike you every time you meet him. Keep going despite him calling you a criminal, every time.
6. Getting yourself a haircut is the best gift you can give your father. Speaking of hair, never shave down there; God hasn't put hair there for no reason. Right?
7. Always warn your sisters before you kill their husbands.
8. If you are fighting your cousin, make it a one-on-one fight with no weapons. Give him time to realise your superiority. If he lowers his fly when you ask him to promise to never harm your father, pick up a knife and slit his throat. But all this not before two hours of fighting man-to-man. Meanwhile, ensure all your cousins stand around you.
9. Get yourself a Made in India (conceptualised in Delhi, manufactured in Bangalore and assembled in Mumbai) machine gun that can fire hundreds of bullets in a second. Burn down your entire hotel when 200 men attack it. Strategies are for those with brains, the weaklings. Real men show raw power. Do not forget an underwear, though; borrow it from another man in case you forgot.
10. Ask your therapist about her sex life, in the name of establishing a "base line" for yourself. Show her who the boss is.

These are my top ten lessons from the film, which is an absolute delight to watch on big screen. A treat to the eyes and ears, full of powerful messages. These can only come from a creative film-maker. :lowers-the-fly:

---
title: "Operational Lapses in Indian Railways"
subtitle: Part 2 of the series on the Odisha triple train tragedy
date: 2023-06-10T10:05:11+05:30
description: The CAG report, yes. What lapses could have led to this serious tragedy? How is this related to the Railway job aspirants' protest?
tags:
- travel
- governance
- employment
- politics
---

This is a part of the series on the Balasore triple-train tragedy. In the previous part, we looked at the different technical aspects of the Indian Railways, that are relevant to the context. The part explains each of the terms you may come across when reading any story regarding the Balasore train tragedy.

If you did not read the the previous part, I suggest you do:

- [Do you know Indian trains]({{< ref "do-you-know-indian-trains.md" >}})

In this part ... well, everyone is talking of the CAG audit report; why not us?

{{< toc >}}

I went through the report except the Appendix, barring the odd page here or there, which I felt did not cover the topic of focus. One point writ large is deficiency.

But let us break it down here.

Note that this report is not directly related to the crash; an investigation report is something different, and has not come out as yet. This report from the Office of the Comptroller and Auditror-General of India, came out in 2022. The report covers the audit of the years 2017 to 2021. What you will see in this report will show the issues identified within the Indian Railways, by the auditors.

To start, 275 out of 1129 derailments{{< sidenote officeofthecomptrollerandauditorgeneralDerailmentIndianRailways2022 >}}Derailment in Indian Railways ([Office of the C&AG](https://cag.gov.in/webroot/uploads/download_audit_report/2022/Report-No.-22-of-2022_Railway_English_DSC-063a2dda55f3ce6.38649271.pdf)){{< /sidenote >}} during the report period were attributed to operational failures. No accident occurs because of one single failure, which means lapses from multiple departments are usually involved in an accident. In case of 275 accidents, while there were other departments involved, the primary lapse was operational. 84% of these were because of "incorrect setting of points and other mistakes in shunting operations". Over 50 accidents on average per year, were caused because of incorrect setting of points and other mistakes in shunting operations. Derailment during shunting operations, while serious, typically do not lead to a loss of life. But still, that is almost one derailment every week!

## Fund utilisation

Fund utilisation can suffer in more ways than one. The reason we have come to commonly hear these days is the unavailability of funds. In case of the Indian Railways, that is not what is happening. They are sitting on a corpus of trillions (lack-crores) of rupees.

But there does seem to be an issue in fund allocation. The *Rashtriya Rail Sanraksha Kosh* (RRSK) or the National Railway Protection (read: Safety) Fund was created, of which 75% was funded by the government, and 25% from within the Railways. The 25% chunk did not seem to be consistently fulfilled by the internal resources of the Indian Railways.

Why?

Second, for various reasons, the fund allocated was not fully utilised either. This is another lapse on the Indian Railways end.

Third, the actual bookings. The RRSK has a set of prioritisation principles for funding of works. The first priority is around working on initiatives in the Civil Engineering area, focusing on the high speed lines first, and then moving down the speed groups. The second priority was upgradation: rolling stock, maintenance infrastructure, upgrading to coaches with improved safety features, etc. The third priority revolved around initiatives to minimise manual errors.

The Audit observes, "vouchers involving a money value ₹2,995.58 crore, bookings of expenditure to RRSK which do not come under the purview of Priority I, II or III were noticed". The expenditure, reportedly, also rose in the low priority areas, while those in the high priority areas reduced. Because of *paucity of funds*, track renewals have been coming down in the last six years. (289 derailments may have been prevented had these tracks been renewed on time.)

Why is track renewal necessary? Thousands of wheel sets running over them cause them to wear. As the track wears, the less efficient it becomes in its areas of work: traction (friction that facilitates controlled movement of the wheels on the tracks), stability, level, etc. Worn out tracks are dangerous.

Lack of timely approvals, misallocation of funds, miscategorisation of bookings, etc., have all led to this decline in safety, partially defeating the purpose of the conception of a fund dedicated to it.

## Unfulfilled vacancies

To ensure safety, there should be regular inspection and maintenance at the minimum. Both these need workforce. In absence of the required workforce, these works will suffer. The audit report says that there was a 30% to 100% lapse in track inspection. Lack of inspection directly affects the performance of maintenance.

Looking at it from the standpoint that there was a 9% to 26% deficiency in track maintenance workforce, you get a picture of what is going on. On the one hand, we have [railway job aspirants protesting]({{< ref "the-problem-with-government-jobs.md" >}}) about the lack of recruitment, on the other, you have so many vacancies not being filled.

## Training deficiencies

After unfulfilled vacancies and high workload, now comes skill issue. In a way, they are all related. Unfulfilled vacancies could lead to high workloads, and high workloads could mean less trainings (on new technologies) and refreshers (on existing technologies).

When working in a skill-based professional environment, one must not only keep themselves abreast with new technologies, but also refresh their knowledge every now and then, to compensate for the human nature of "losing touch with" something that you do not frequently work on. Also, the workers should be made aware of changes to the policies or designs as well. The Audit report notes these shortfalls.

The Indian Railways have introduced a Web-Enabled IT platform called Track Management System. The track maintenance practices are recorded here. And up to at least 2021, there were people who were not trained in using methods of online working! They were either not trained, or, I suppose, were uninterested. I think "I am not interested in working with computers" in 2023 should be seen as a serious threat to passenger safety. This is not about one person's liberty to choose to learn to do something, this is about how many people his/her area of work serves---the railway customers: from little children to that office-goer to that large corporation that moves thousands of tonnes of material using the railways. Learning "online ways of working" must not be optional in 2023.

## Why the Indian Railways are not getting better

Operational issues aside, there are other issues that slow down the progress of the railways. The Audit report notes shortfalls in most, if not all the areas it covered.

### Reporting and sharing information

Air travel is considered the safest for a reason. This is because of the framework they operate upon. Every little deviation gets recorded and actioned on. After every incident, minor or major, an investigation panel is set up that goes through each little parameter that led to that incident, and this is recorded. The panel gives its recommendation which gets implemented and reported. Every effort is made to ensure that no incident repeats itself. Every parameter that can be controlled is controlled, and little is left to chance.

Did you know that planes are made in such a way that even if it banks at an angle of 30 degrees, it does it in such a way that the pull on everything within the plane remains parallel to the plane's bottom? This is why even when the plane banks and turns, the water in the cup placed on the drop-down tray in front of you doesn't spill out; instead, it stays *horizontal* to the tray. Observe this the next time you fly; the plane's turn emulates the earth's gravity using centrifugal force.

This is not a nice-to-have feature. A lot of investment has gone into making it happen. Why? There must have been some incident somewhere because someone fell down, or something spilled ... who knows? The concerned committee recorded it, shared the findings, sent the recommendations. The manufacturers like Boeing and Airbus took up the task, and engineered a solution. And voila, you have the feature in almost every plane you fly in today.

Indian Railways has been failing in recording and sharing information. This is why similar accidents happen all across the country. This is as much a transparency issue, as an integrity issue. And it indicates general indifference. IR have what they call SIMS (stands for Safety Information Management System), created for "accident reporting, analysis and sharing information".

The module responsible for reporting of the detailed analysis of accidents to the Chief Safety Officer was *not even implemented* in the software by the time the report came out. Why? Any logical agency would categorise this as a core capability of such software, but implementation of this functionality was pending at least until 2022. (The current status is unknown to the public.)

### Lack of standards in some areas

When you set out for an inspection, you set up a certain criteria for when something is considered adequate. For example, when I was in school, the criteria for passing the internal exams was a minimum 40% score. There is *no set criteria* in Indian Railways, as to how many inspections should be done for a certain part of the line. How can this be the case in one of the largest railway networks in the world, in the twenty-first century? The Indian Railways are over 150 years old. How can we still not know what number of inspections per how much of the railways is considered adequate? This way, a zone can perform three inspections in a year, and nobody can tell them that the number of inspections done was insufficient!

### Field safety

Next comes field safety. Field safety, as the report notes, is largely inadequate. From making helmets available to hand-sized gloves, the field workers in the Indian Railways are working with dangerous equipment with little to no protection. Who is to say that some of these inadequacies in inspections and maintenance are not because of the dangers these activities may pose to the workers? And how would an enterprise that does not take its own workers' safety seriously, take its customers' safety seriously?

## But how unsafe are the Indian Railways

Generally speaking, despite these shortfalls and the apparent indifference, Indian Railways are among the safest railways in the world. With just 0.03 accidents per million train kilometres and *zero passenger fatalities* reported in the years 2019-20 and 2020-21, we are still generally safe travelling by the Indian Railways.

One train kilometre is when one train travels one kilometre. Ten trains travel one kilometre each, make up ten train kilometres. In case of the Indian Railways, three accidents occur once in a hundred million train kilometres---one train accident per 3.33 crore train kilometres. And not all accidents cause fatalities.

Compare this with about three lac (300,000) road accidents in a year, and 1.5 lac fatalities.

Secondly, there is a Swiss Cheese model of accident barriers in place, though not fully effective (because of the aforementioned shortfalls). These five barriers are:

1. Rules
2. Training/Counselling
3. Supervision
4. Co-ordination/Communication
5. Inspections

When each of these barriers fail to arrest an accident, an accident can occur. While three accidents per hundred million train kilometres is not a bad record, the Indian Railways should focus on its Vision 2020, which wants to make railway operations free of accidents.

Of all the accidents that did lead to fatalities, most of them were because of either people being run over by trains, or involved people falling off of trains. Fatalities due to crashes or derailments were a smaller number.

Despite the shortcomings, Indian Railways were safe enough to travel, as of 2022. If you look at the table below, you will see a decline in the number of accidents:

| Year     | Accidents |
| :------- | --------: |
| 2016--17 | 104       |
| 2017--18 | 73        |
| 2018--19 | 59        |
| 2019--20 | 55        |
| 2020--21 | 22        |

If the investments in safety keep declining as they are, then, yes, the safety starts becoming questionable. Which is why we must still push for the implementation of the safety measures, including making SIMS better. And the Balasore accident should stand as a reminder of the importance of making the railway systems safer.

Read next:

- [The politics of the Balasore tragedy]({{< ref "the-politics-of-the-balasore-tragedy.md" >}})
- [Responsibility for the Balasore tragedy]({{< ref "responsibility-for-the-balasore-tragedy.md" >}})

---
title: "Responsibility for the Balasore Tragedy"
subtitle: Part 4 of the series on the Odisha triple train tragedy
date: 2023-06-12T10:05:38+05:30
description: Who is responsible for the tragedy? Indian Railways? The government? The people? Or that odd ...
tags:
- travel
- governance
- employment
- politics
---

This is a part of the series on the Balasore triple-train tragedy. In the previous parts, we looked at the different technical aspects of the Indian Railways, the audit observations from the CAG report, which pointed out the areas that need significant improvements, and looked at the politics of the matter.

{{< toc >}}

If you did not read the the previous parts, I suggest you do:

- [Do you know Indian trains]({{< ref "do-you-know-indian-trains.md" >}})
- [Operational Lapses in Indian Railways]({{< ref "operational-lapses-in-indian-railways.md" >}})
- [The politics of the Balasore tragedy]({{< ref "the-politics-of-the-balasore-tragedy.md" >}})

Getting to the part where everyone in the know is asking, 'Who is responsible for this tragedy?' I say, I am more concerned about the accountability aspect of this rather than the responsibility.

## Handling of the case

There has not been too much finger-pointing in this matter, which is surprising given how we typically react to these situations. But there still are questions to be answered. The Railway Board and the Ministry of Civil Aviation (yes) usually head these investigations. The Railway Board reports to the Ministry of Railways, while the Commissioner of Railway Safety under the Ministry of Civil Aviation oversees the safety system in Railways.

In this particular case, the CBI has been involved. Which is unusual. Does *criminal negligence* in the Railways come under the purview of the CBI? Or is there more here than meets the eye?

## Inconsistencies

But this case is not open-and-shut either. The Railway Board said in its statement that they spoke to one of the loco pilots, and he said that the signal was green when they entered the loop line. Which meant that the pilots could go forward at their "permissible speed". But if the switch was set to the loop line, the signal should not have been green; it should have been yellow, which indicates "proceed with caution" or "proceed with restricted speed", which is appropriate for a loop line.

The interlocking system is fail-safe, which means, had it detected something wrong, it should have failed safe to "stop". Under no circumstance should it have indicated "proceed".

According to the report, 13 derailments happened because of incorrect setting of points. About half of the derailments overall were either caused by the Engineering or the Operations departments. About three in every four accidents that were caused by the Signalling department were due to either system or technological deficiency and visibility issues.

Right after the tragedy, a probe panel was set up with five members. They submitted a priliminary report of the accident. A day after the accident, one of the members of the panel, a senior section engineer, submitted a dissenting note, stating that according to the Data Logger report, the signal was indeed green, and that was because the point machine was pointing to the main line, not the loop line.{{< sidenote sOdishaTrainAccident2023 >}}Odisha train accident probe team member now submits a note of disagreement ([The Hindu](https://www.thehindu.com/news/national/odisha-train-accident-inquiry-official-disagrees-with-preliminary-findings/article66938118.ece)){{< /sidenote >}} The signal indication is in line with the loco pilot's statement to the Railway Board. But if the switch was set to the main line, why did the train derail?

The dissenting SSE also notes that the point of derailment was the Level Crossing before the point. But experts disagree with this, as a derailment at the level crossing would be evident. The officials, reportedly, said that the train log "clearly shows it *entered the loop at 128 kmph* and went to zero within seconds" (emphasis mine). There have been mentions of criminal involvement, while sabotage is ruled out. A suspicion for criminal involvement does explain the case being handed over to the CBI, but that also seems to have happened in haste, as pointed out by some in the know of the process.

Overall, there is still fog over the situation, and more will become clear as more data comes out.

## A possible precedent

Interestingly, not all is hunky-dory with fail-safe interlocking systems, as pointed out by an official of the South Western Railway:{{< sidenote sOdishaAccidentSenior2023 >}}Senior official flagged serious flaws in Indian Railways’ signalling system in February ([The Hindu](https://www.thehindu.com/news/national/odisha-accident-senior-official-had-alerted-railway-on-serious-flaws-in-signalling-system/article66930016.ece)){{< /sidenote >}} {{< sidenote southwesternrailwaySeriousUnsafeIncident2023 >}}Serious Unsafe incident happened at Hosadurga Road station of Birur-Chikjajur section of Mysore division on 08.02.2023, involving Train, no: 12649 Sampark kranti Express, leading to condition for averted head on collision with down goods train (BTPN Empty Rake). ([SWR / The Hindu](https://www.thehindu.com/incoming/66931088-Railways-February-warning)){{< /sidenote >}}

> ... incident indicates that there are serious flaws in the system, where the route of dispatch gets altered after a train starts on signals with correct appearance of route in the Station master’s panel. This contravenes the essence and basic principles of interlocking.

Clearly, there needs to be investigation into what is going on, but apart from that, this communication should tell the investigators to tread carefully when making assumptions about the systems, and whether these are indeed as fail-safe as thought of. Note that this came out in February, and no action seems to have been taken yet.

Again, this is not to say that travelling by trains is unsafe, it just isn't as safe as the Indian Railways make it out to be.

## The way forward

First, there is nothing to panic. This one tragedy should not imply that travelling by trains is unsafe. Indian trains, despite all their flaws, are among the safest in the world. We have technically competent staff in the designing and manufacturing areas, in general, and that makes up for some of the operational deficiencies that we currently have.

Complacence is a dangerous enemy. The Indian Railways should take a look at their current ways of working and mend anything that needs mending. They must remember their Vision 2020.

Security of signalling systems is critical. When heavy metal rolls at high speeds, there is very little anyone can do to avert an accident. Given the nature of trains, there is little a loco pilot can do in dangerous situations like this. Which is why we have so many rules and regulations in place. The loco pilot is merely responsible for the speed. S/He works based on the signal s/he gets. Wrong signals can lead to serious disasters.

Upgradation and modernisation are no longer optional. The Indian Railways have grown to a massive scale. At this scale, there is little room for error. Modernisation helps run better operations. The aviation industry was born after the railways, yet, it overtook railways in safety and security. We have the technology to achieve a lot more, and the Railways should utilise that.

Now that the discussion has begun about safety in trains, it should be a primary focus area. Safety cannot be compromised, especially when we are introducing faster trains like the Vande Bharat. But like I said, since the infrastructure requirements for these trains require high standards of engineering, upgrading the tracks along these lines will lead to better safety, but that cannot happen at its own pace over three decades. The Railways have the funds, which many enterprises don't. They are being strongly backed by the government. They should utilise this for the better.

But as these safety standards increase, the competence of the Railways staff also must increase. Hire enough people, train them and get them on the field. I have visited the Integral Coach Factory in Chennai, and that day still remains as one of the most memorable days in my life. It was great to see so many knowledgeable people at work, focusing on making travel comfortable and safe for the passengers. I could sense the passion that some of them had, in the interactions with them.

We have a large pool of brilliant people in our country. We must rise above all the politics, and look at ensuring that Indian Railways become something we Indians can brag about more than we do today. We must learn from our mistakes, and bounce back from this tragedy. The goal should be that such a tragedy or bigger never happens again.

That is the least the Indian Railways and the ministry can do for those who lost their lives in this tragedy.
---
title: Upturned SUV and other stories
subtitle: Bollywood style
date: 2023-04-16T10:34:00+05:30
description:
draft: true
tags:
- justice
- governance
---

Last night, we were sitting on the porch of a relative’s place. Amidst about ten people chit-chatting and playing badminton (it reminded me why I loved summer vacations when in school), my brother exclaimed looking at the phone, ‘Dude, Atiq Ahmed got killed!’ While I had no reason to be surprised, the piece of news did surprise me. Why? Because the entire media’s eyes were on this man. And again, while I should not be surprised given that the media is now merely entertainment, I felt this was too “out there”.

Of course, I kept away from all social media. One moment, I thought, what critical is going on that this sort of distraction was needed. Karnataka elections? Municipal Corporation elections in Uttar Pradesh? Both of these are happening in the beginning of May. Now that 
State Legislative Assembly elections can happen over national issues, why can a Municipal Corporation election not happen over a state issue?

I brushed off the thought of the visuals of the mafioso being killed with the idea that this could be an election stunt, I walked in to get some sleep.

## Extra-judicial heroes

I grew up watching Indian films like anybody in India. We celebrate films like _Singam_ (the Surya starrer, in my case). But really, as an adult citizen of a democratic republic, extra-judicial killings (let us call a spade a spade) scare me. Why? Because I think the law is supreme. Yes, the law is imperfect, yes, it is decades behind the real world, but it is the best thing we have to turn to for justice.

I said it during the Hyderabad killings, and I say it now, this is not good for a democratic republic. Each extra-judicial killing in our country takes us a step towards becoming a police state.

## A police state

‘A “police state”! That’s something!’

A police state merely means 

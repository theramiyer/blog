---
title: "The Odisha Train Crash"
subtitle: "the facts and the lessons"
date: 2023-06-12T08:21:29+05:30
description:
tags:
draft: true
---

Millions of us Indians have been travelling long distances by the Indian Railways.

My first train journey, apparently, was when I was about six months old.

Over these past three decades, I have read about many train crashes.

I have even seen that odd wreckage during my train journeys.

But what happened on the 2nd of June was beyond horrific.

I cannot possibly express what I feel about it

I turned to TV news for updates, thinking for once they could be useful.

Boy, was I wrong.

And then there were WhatsApp messages floating around, loaded with misinformation.

Mr Modi and Mr Vaishnaw visited the accident site.

That only added to the noise, unfortunately.

But let us break down the situation into little pieces.

You might have heard a lot about many aspects of the Railways.

And a lot of information about this crash is already out there.

This episode is not purely about that.

Here, I will put together the different aspects of the Railways to help you gain some perspective.

I will also be talking about it from the technical sense first,

but I will keep it absolutely uncomplicated,

because not all of us are technical people.

Here is what we will talk about in this episode:

{{< toc >}}

## A gist of the crash

The accident took place near a station called Bahanaga Bazar, in the Balasore (or Baleshwar) district of Odisha.

As you may have already read, the Chennai-bound Coromandel Express (or _Hospital Express_, as it is popularly called) entered the loop line at the speed of 128 km/h, crashing straight into a stationary freight train carrying iron ore.

The coaches of the Coromandel Express got strewn around because of the impact of the collision.

The last few coaches of the Howrah-bound Yeshwanthapura--Howrah express got hit by one or some of these coaches, and that train derailed as well.

A tragic triple-train crash.

“How did something like this happen in 2023?” was the first question in the minds of a lot of us.

It kindled a discussion on railway safety.

And it threw light on the deficiencies in the Indian Railways.

I would not be surprised if the CAG report on derailments suddenly became one of the most downloaded audit reports in the country in the days that followed.

I did read some articles on the internet about interlocking systems and loop lines, etc., but did not see enough information out there.

I felt it would be better if I added some here for my listeners.

Of course, I am merely an aviation and railways enthusiast, not a professional in either of these fields.

I will try my best to express what each of these aspects of the railways is, but I may be wrong sometimes.

Take what I say with a grain of salt.

After all simplification often leads to inaccuracy.

So, don't go purely by what I am going to say, if, say, you are appearing for a Railways engineering exam.

You get the idea; this is for general information purposes.

Let us now get to what each of the most popular terms doing the rounds in the papers and other media, mean:

## A loop line

A loop line, as you may know by now, is a segment of railway track that branches out of the “main” line and comes back into it after some distance.

Trains use these when they have to give way to other trains to pass.

Some newspapers and WhatsApp messages said that typically these are used by freight trains, but that is not true.

Any train that must give way to another train would use the loop line.

Typically, freight trains have the lowest "precedence", and you often find them stationary on the loop lines when you travel.

If you travel by superfast expresses, you will also see passenger trains on them.

Technically, these loop lines are at least 650 metres long by standard. 

They can hold _exactly one train_ in them.

This is a crucial point, as we discuss the other components.

This also means that no train of the Indian Railways is longer than 650 m.

This way, every train out there can fit into any unoccupied loop line.

The beginning of the safe point on a loop line is marked with what is called a "Fouling Mark".

You may have noticed that the guard always has his cabin at the end of the train.

This way, he just has to make sure that his cabin crosses this point within the loop line.

If his cabin crosses the fouling mark, it means the entire train is now in the loop line.

Now, there is no danger to the train or any other train passing on the main line.

Higher speed, in trains, does not equate to more danger.

Trains and everything around them, are made for high speeds and efficiency.

Every aspect of the high-speed track is carefully engineered so that it can safely support high speeds.

Every segment on a line is speed-graded as well.

The segment where the tragedy took place, was engineered for speeds up to 130 km/h.

Next, turnouts.

What is a turnout?

It is exactly what it sounds like: turn outwards from the main line.

The "turnouts", are speed-graded as well.

How? Depending on their deviation from the main line.

The sharper the deviation from the main line, the slower the train must enter the loop line.

Why? If a train suddenly changes its direction at high speeds, it becomes unstable.

Just like how your car becomes unstable when you take a sharp turn at high speeds.

Have you ever noticed how highways turn? They turn over a long curve.

Same reason.

Think of a switch as the arrangement of the tracks at the turnout.

They are made to direct the trains.

Or "switch" them between the main line and the loop line---or other lines.

Trains are essentially tied up boxes made up of tonnes of metals.

Given their weight, the trains must pass them carefully, when the switch is set into the turnout.

How fast can a train go on a turnout?

Even a 1 in 24 curved switch turnout allows a maximum speed of 30 km/h.

Some special turnouts do allow higher speeds because of the way they are built.

But higher speeds here means something like 40 km/h or 50 km/h.

No train is safe taking a turnout at 128 km/h.

Even if the loop line were clear, the train was likely to have derailed anyway.

What is a "1 in 24 curved switch turnout", you ask?

Well, you already know what a turnout and a switch are.

A curved switch is basically how the switch deviates---

the turn is a curve rather than a sharp cut away from the main line and into the loop line.

A curved turnout means a much smoother turn, which is safer than a sharp turnout.

1 in 24 here means that the train goes 1 metre away from the main line for every 24 metres it travels on the switch.

Also, a curved turnout allows for slightly better speeds than their sharp counterparts.

Again, higher speeds here means something like 30 km/h, which is higher than, say, 15 km/h.

Next: Precedence

The two loop lines here were occupied by two freight trains.

The papers said that the passenger trains were superfast expresses, and they had higher “precedence”.

(Which is why the freight trains had to go into the loop lines.)

“Precedence” is a way of saying which train gets preference on a certain line.

Relief trains (accident relief trains, for example) have the highest precedence.

Their precedence is higher than even the President's and the VVIP Special trains.

Third preference goes to the suburban trains during peak hours.

Fourth come the superfast trains such as the Rajdhani, the Shatabdi, the Vande Bharat, etc.

Among the general use trains, freight trains have the lowest precedence.

Next: A Point

As you may know by now, a Point is that arrangement that flips the switch between the main line and the loop line.

This could be purely mechanical (which would require someone to go there and make the switch),

or electrically controlled (which it usually is these days),

or electronically controlled (which this point machine at Bahanaga Bazar was).

Let's get to Interlocking systems.

Trains are heavy-weight metal boxes running on wheels, pulled by a very powerful locomotive engine.

When it comes to managing this kind of machinery on smooth railway tracks, there needs to be a lot of forethought

Many pieces of the system should work in sync to achieve this.

For example, when the point machine points the train to the main line:

1. the signal at the end of the loop line should be red, indicating to the train on the loop line that it does not have the clearance to enter the main line just yet.
2. If there is a level crossing, the boom arm must be lowered to ensure that no road vehicle enters the track.
3. The block (we'll get to that in a moment) which is right behind should have a red signal,
4. the block before that must have at least a yellow, 
5. the block before that must have have at least yellow-yellow and
6. and the one before may have a green, and so on.

Also, the signal for the main line must be green _only_ if:

1. the point machine is pointing to the main line,
2. and at least three blocks in front are clear of trains, 
3. _and_ if the main line is safe for the train to travel at the maximum track speed in other aspects.

This is called interlocking: different components locking based on the state of the others.

These systems are made "fail-safe", which means:

they go to their most restrictive state when "on".

We will get to what is the "on" condition in a moment.

Now, let us talk about a section

In general, a railway line between two stations is divided into a few segments.

These called "sections".

(Terms like Section Engineer, comes from this.)

Some railways also call these segments as "blocks".

Indian Railways call them "sections".

A section is typically at least as long as the maximum braking distance for the fastest train on that line.

What is braking distance now?

Nothing going with any good speed can stop right where the brakes are applied.

Except for cars in Rohit Shetty films, perhaps.

Any vehicle that is going at a good speed will take some time to come to a stop after you apply the brakes.

The distance travelled in this time is the braking distance.

In train terms, if the line carries a train whose maximum speed is 160 km/h,

Now, say the train is made up of only LHB coaches with.

And say, these coaches are fitted with the Knorr Bremse air brakes.

In such a case, the section length will be at least 1200 m (or 1.2 km) long.

In reality, sections are usually much longer than 1.2 km.

This is done so that even in case of a danger,

like a train being stationary on the same line in the next section,

this train can come to a stop without colliding with the stationary train.

Every section has its signal.

This signal's indication depends on the conditions of the sections in the next section or sections.

Why? Because of interlocking.

Now, what is the "on" condition.

Basically, the state in which it will be if you don't ask it to do anything.

Like, the on condition of your TV is to keep the screen off, but the little red stand-by LED glowing.

The moment you plug in the TV and flip the switch for the wall socket to "on", this is what its state will be.

It will wait for you to pick up your remote and then press the "power" button on it to turn on the screen.

Well, this is what most TVs do. If yours doesn't, we'll have to pick another example. Like your car.

The on condition for it is to turn on all the accessories and wait for you to press the "engine" button. (Or turn the key to the starter position.)

Similarly, unless someone (or something) changes the state of the signal, it would be in its "default state".

Fail-safe systems have the default state of highest restriction;

meaning, a fail-safe section signal will always show "stop".

Someone (or something) must change its state to "proceed" for it to show proceed.

Now, imagine that there was some issue with one of the systems in the interlocking system.

Say, the point machine failed to switch the track, and it got stuck because of, say, a stone or something.

In this situation, the fail-safe system will go to its most restrictive state of "stop".

All the signals that this point machine's interlocking system is connected to will become red.

Suppose, someone goes to this point, removes the stone and reinitiates the switch.

This time, the switch succeeds.

The interlocking system will check every aspect of everything it controls, examines their states, locks the components in those positions, and then, when it feels it's safe, turns the signals to green or yellow depending on which signal it is.

Only if _all those conditions_ are met, the signal turns green.

Electronic interlocking systems are superior technology in this regard.

They have better sensing, logging, and alerting capabilities.

The probability of these systems failing is extremely low.

Which is what everybody is saying.

And they are right.

Now, let us come to speed and braking (and Kavach).

One of the first claims to come out when this collision (and derailment) happened, was:

Oh, had the Kavach system been there, this collision could have been averted.

No.

Leave all the technical speak from the Indian Railways aside.

From a logical standpoint, think how the braking distance varies between your bicycle, your scooter (or motorcycle) and your car.

Compare these again to that fully loaded lorry you see on highways.

A bicycle brakes can completely lock its wheels when you clutch the brake handles.

But your bicycle does not come to a stop; it slips on the road for a while before it stops.

Why? Because the friction it has with the road is not enough to bring it to a complete stop.

This changes drastically when off-roading, when on sandy surfaces.

Your braking distance is much longer.

Now, compare this with your motorcycle on a good road.

Braking is much more efficient in it, and you usually come to a stop much sooner than your bicycle travelling at the same speed.

Why? The weight of the motorcycle helps.

The weight increases the effect of the friction between the tyres and the road.

This helps in braking.

A car, on the other hand, may take much longer to brake, if you fit it with brakes with similar parameters as a motorcycle.

Why? Because even though the friction with the road is higher in case of the car, the car is heavier.

It has a much bigger momentum.

Now, think about how the braking distance varies in rains. Why? The friction with the road reduces drastically because of aquaplaning.

So, what do we learn from this?

Heavy things have higher momentum.

Friction between the wheels and the surface on which they are running is absolutely necessary for brakes to work.

The pressure the brakes can exert on the wheels is the third parameter.

All these lead to what? Deceleration.

Think how heavy trains are.

Think how slippery the tracks are.

Have you seen how polished they look?

While the weight on the tracks does help, there is also the momentum of the train because of its weight that negates that effect.

So, when you crunch all these numbers together with fancy-looking mechanical formulas, you get a rather long safe braking distance.

For a train running at a speed of 128 km/h---no matter how great the brakes are---it cannot come to a stop within a 100 metres or so.

Remember:

1. The train was already way too fast for the switch.
2. There was a freight train parked on the loop line.
3. The end of the train would have been right at the Fouling Mark. This should have been no more than a hundred metres from the switch.
4. Humans were running the train; there is something called reaction time that comes into the picture.

Even if the wheels had locked in position, the wheels simply would have slipped on the track for hundreds of metres before coming to a complete stop.

Brake systems are designed to prevent this,

Because that kind of slipping permanently damages the wheels by flattening them.

The brake systems will never let the wheels lock that way, which in turn means, the braking distance will be longer.

Even if the freight train was absent on the loop line:

1. The Coromandel Express would have derailed because of the switch position.
2. It would have not come to a stop even within the loop line had the loco pilot tried to brake to prevent the derailment.

The train would have derailed at the end of the loop line any way.

By the time the pilots realised that the train had somehow taken the loop line, two seconds would have passed after the collision.

Nothing in this world could have stopped the derailment of this train at that speed with that track configuration.

Not even a miracle.

Simple mechanics would have _ensured_ that the train derailed.

Outside of the film industry, the laws of physics exist, and they act consistently across the universe.

Now to the speed of trains

You may have understood by now that the power of the locomotive engine alone does not determine the speed of the train.

This is why the _Vande Bharat_, even though designed for 160 km/h, cannot take us from Bangalore to Chennai in 2:15 hours---it takes a solid 4:25 hours.

In fact, I have never noticed its speed go beyond 117 km/h.

Why, it rarely hit the speed of 115 km/h.

Everything from the density of the rail, to the kind of error allowed on the switches, to the sleepers used, to the kind of welding done, to whether any maintenance activity is going on and on what stretch, the presence of bridges (and their construction type), to other trains on the line, there is a lot that goes into determining which speed "group" a line falls under.

This grouping tells you what speed the line supports.

The line on which the accident took place may be a Group B line, allowing speeds up to 130 km/h---the line was *safe* for trains to travel at speeds up to 130 km/h.

Again, remember, the railways highways are designed for speed.

A higher speed does not necessarily mean lower safety here.

In fact, the technical folk in the railways would argue the opposite: tracks supporting higher speeds are safer, because they have a higher standard of construction, and they adhere to more stringent norms with smaller room for error.

The bogies and the coaches---

(bogies are those blocks that have the suspensions, brakes and the wheels; coaches are what people and freight occupy)

---these are designed for better stability and comfort on high-speed trains.

Back when I was in school, I would feel myself swaying for almost half a day at home, after finishing a 48-hour journey by train.

It does not happen these days, not because I've grown older, but because the coaches and lines have gotten that much better.

Now to politics.

As usual in our country, our politicians picked up the first opportunity available to politicise the tragedy, and well, did their own things.

From the "ruling" party to the opposition, nobody left a stone unturned after the customary condolences.

My stand on politics is simple: Disregard all politicians unless they have decent work to show, done in the interest of our country and our people.

Humility and honesty are probably too much to ask for from a politician, so, work can be the first priority.

First: the inspection and slogans

I don't dislike Ashwini Vaishnaw as much as I dislike most politicians.

I like that he seeks newer technologies and tries to implement them.

He may have failed at many levels, but let us keep that aside for now.

I do not mean to ignore his shortcomings as the head of the Ministry of Railways, but I do think he is more competent than many Railway Ministers we have had in the past couple of decades or so.

Yes, we have had a few. Probably it was only Mr Lalu Prasad that did a full term.

Others kept coming and going. So, yeah, we've had a few now.

When he visited the scene, I thought that was normal, and it would have certainly surprised me if he hadn't visited.

But after all the inspection, discussions and the statement, he picked up a megaphone and went, "Bharat Mata ki Jai!"

Why?

What does this have anything to do in that tragedy?

Was he trying to get everyone's morale up that way?

Well, news flash: it doesn't work like that.

Say that to an army company going into a battlefield, it will work.

Say it to the sportspeople leaving for the Olympics, it would work.

In this horror, this is not about the country, it is about the hundreds who died, the hundreds who were injured.

This is a solemn moment, one should be sensitive to these things.

I would expect that of a learned minister like Mr Vaishnaw.

Everything from his clothing to his demeanour otherwise, were perfect for the situation.

He did not have to go around shouting slogans.

I am sorry, the patriot in me disagrees with this kind of display.

Second, he stood as an example for something pointed out as a deficiency in the latest CAG report on the Railway derailments: safety equipment.

He was going around, coming out from under mangled coaches, with no safety gear, not even so much as a helmet.

Why?

What kind of example are you setting?

Tomorrow, when a field worker asks for a helmet and safety gloves, his officer might say, "Even the Railway Minister did not bother about these things; why do you need them? Get out on the field now, do your job."

Mr Vaishnaw exacerbated the problem.

Oh, yeah, that thing, "Ashwini Vaishnaw must resign!"

No.

He should not resign.

I find this kind of argument nonsensical.

What moral responsibility are we talking about?

What would one do with that "moral" act of stepping away?

Tomorrow, when someone holds him accountable, he would point out that he resigned, state that he takes full moral responsibility, and apologise.

And walk away.

What good is that going to do to anyone other than him?

Resignations may be a moral high ground.

Sure.

Sure, it may signal to the future Ministers of Railway that they would have to resign if they fail in their duties, but really, _really_, what good is that going to bring?

1. No Railway Minister who resigned lost his career in the history of Independent India.
2. Is this really, fully, the Railway Ministry's fault?
3. Is that fault directly linked to Mr Vaishnaw? Did he personally make a mistake in this accident? What and how?
4. If he accepts moral responsibility, then his entire team---including the entire Ministry of Railways must resign from their jobs.

I am not saying he is not at fault.

But think about it, what exactly went wrong here?

If you say a Pointman failed, then him, his boss, his boss, his boss, and so on, up to the entire Railway Board must resign.

What does that achieve anyway?

'Suspend the pointman!'

Okay, but then what?

Is the suspension going to make him feel ashamed?

When was the last time a suspension made anyone feel ashamed about anything?

How do you know s/he is not already ashamed?

And what after being ashamed?

How is someone's suspension or resignation fixing the problem?

Or even, how is it, in any way, acting as a deterrent?

And if Ashwini Vaishnaw steps away, who takes over?

How sure are you that s/he is better than Mr Vaishnaw?

Again, is Mr Vaishnaw "_nirdosh_" as they say?

No.

I say, him and his ministry should take ownership of this, not wash their hands off.

Act on it by working to fix all the issues that led to this tragedy.

Stepping away is like chickening out.

I don't support that.

Those that are demanding his resignation, of course, are free to choose their stand, but at least, they should have a solid argument behind what they choose.

"To what end?" is the question.

Another claim that came about was that the Railways were focusing more on newer and faster trains like the Vande Bharat and the Bullet Train.

Agreed.

But to say that because all the concentration is on those and therefore there is no money available for upgrading safety is baseless.

The Indian Railways are sitting on surplus funds.

Funds are not the problem here.

The problem, well, at least primarily, is management.

There are vacancies that are not being filled, the employees (full time and contract) are overworked, they lack training, inspection and maintenance blocks are not allocated, and so on.

We will talk about these in more detail in a bit.

I personally did not find the *Vande Bharat* all that much better than the Shatabdi in my frequent route at least: the difference in travel time is merely 25 minutes.

Also, in my experience, the Shatabdi offers better comfort than the Vande Bharat.

Having said that, I am not against the Vande Bharat.

Sure, I find it unnecessarily more expensive than the Shatabdi and all, but I do not oppose it.

I think it is time we got trains like the Vande Bharat, albeit with the comfort level of Shatabdi.

Vande Bharat can charge the Vande Bharat premium, but at least offer the same comfort if not better, than the Shatabdi.

Having also said that, I think the Vande Bharat is more efficient (in terms of power consumption) than the Shatabdi, because, well, physics.

The Vande Bharat loco is more aerodynamic, for starters.

Over the long run, the Vande Bharat would consume less electricity than the Shatabdi.

Even if not, given the efficiency it brings into the system, it is better than the Shatabdi.

I think the Vande Bharat in general is technically better than any other high-speed passenger train we have in existence today.

Oh, and by the way, I hear comments about how even a simple cow manages to damage the Vande Bharat engine.

Right, it sure does.

And every such collision means that the loco needs maintenance at the yard, making it lose its, say, monthly running time.

But there is nothing wrong with its safety.

The shell is merely there to deal with the air.

(Believe it or not, it matters.

If not, we would be flying on box-shaped aeroplanes today and sports cars would look like rhinos.)

These shells are made of light metal in order to not add too much weight (aeroplanes are made of aluminium alloy for this reason).

These engines still have the strong steel body within.

How many loco pilots died because of the Vande Bharat hitting a cow?

But, but: Whom does the Railways really cater to?

Trains like the Vande Bharat cater to a miniscule portion of the total passengers the Indian Railways carry in a day.

The Indian Railways still cater to those who cannot afford even a second class ticket, let alone airlines.

And the scale of passenger traffic that the Indian Railways carry are perhaps unimaginable for the airline industry in India anyway.

Like we saw just a while ago, the Indian Railways, in general, must focus on safety of the other trains.

But at the same time, the introduction of the Vande Bharat and its demands will ensure better safety for other trains.

Now to the Operational Lapses.

I went through the report except the Appendix, barring the odd page here or there, which I felt did not cover the topic of focus.

One point writ large in that report is deficiency.

But let us break it down here.

Note that this report is not directly related to the crash; an investigation report is something different, and has not come out as yet.

This report came from the Office of the Comptroller and Auditror-General of India

And it came out in 2022.

The report covers the audit of the years 2017 to 2021.

What you will see in this report will show the issues identified within the Indian Railways, by the auditors.

To start, 275 out of 1129 derailments during the report period were attributed to operational failures.

Accidents don't happen because of one single failure

Lapses from multiple departments are usually involved in an accident.

In case of 275 accidents, while there were other departments involved, the primary lapse was operational.

84% of these were because of "incorrect setting of points and other mistakes in shunting operations".

Over 50 accidents on average per year, were caused because of incorrect setting of points and other mistakes in shunting operations.

Derailment during shunting operations are serious, but they typically do not lead to a loss of life.

But still, this is almost one derailment every week!

Next: Fund utilisation

Fund utilisation can suffer in more ways than one.

The most common we have come to commonly hear these days is the unavailability of funds.

In case of the Indian Railways, that is not what is happening.

They are sitting on a corpus of trillions (lack-crores) of rupees.

But there does seem to be an issue in fund allocation.

The *Rashtriya Rail Sanraksha Kosh* (RRSK) or the National Railway Protection (read: Safety) Fund was created for safety.

Of the total funds, 75% was funded by the government, and 25% from within the Railways.

The 25% chunk did not seem to be consistently fulfilled by the internal resources of the Indian Railways.

Why?

Second, for various reasons, the fund allocated was not fully utilised either.

This is another lapse on the Indian Railways end.

Third, the actual bookings.

The RRSK had a set of prioritisation principles for funding of works.

The first priority is around working on initiatives in the Civil Engineering area

It focuses on the high speed lines first, and then moves down the speed groups.

The second priority was upgradation: rolling stock, maintenance infrastructure, upgrading to coaches with improved safety features, things like that.

The third priority revolved around initiatives to minimise manual errors.

The Audit observes, and I quote: "vouchers involving a money value ₹2,995.58 crore, bookings of expenditure to RRSK which do not come under the purview of Priority I, II or III were noticed".

The expenditure, reportedly, also rose in the low priority areas, while those in the high priority areas reduced.

Because of *paucity of funds*, track renewals have been coming down in the last six years.

How ridiculous is that?

(289 derailments may have been prevented had these tracks been renewed on time.)

Why is track renewal necessary?

Thousands of wheel sets running over them cause them to wear.

As the track wears, the less efficient it becomes in its areas of work:

traction (basically friction that facilitates controlled movement of the wheels on the tracks), stability, level, etc.

Worn out tracks are dangerous.

Lack of timely approvals, misallocation of funds, miscategorisation of bookings---these have all led to this decline in safety.

It partially defeats the purpose of the conception of a fund dedicated to safety.

Next come unfulfilled vacancies.

There should be regular inspection and maintenance of the, well, the railway infrastructure, to ensure safety. At the minimum, that is.

Both these need workforce.

If we don't have the required workforce, these works will suffer.

The audit report says that there was a 30% to 100% lapse in track inspection.

What?!

Lack of inspection directly affects the performance of maintenance.

Looking at it from the standpoint that there was a 9% to 26% deficiency in track maintenance workforce, you get a picture of what is going on.

On the one hand, we have railway job aspirants protesting about the lack of recruitment, and on the other, you have so many vacancies not being filled.

Next, of course, training deficiencies.

After unfulfilled vacancies and high workload, now comes skill issue.

In a way, these are all related.

Unfulfilled vacancies could lead to high workloads, and high workloads could mean less trainings and refreshers.

When you work in a skill-based professional environment, you must not only keep yourself abreast with new technologies, but also refresh your knowledge every now and then.

This is to compensate for the human nature of "losing touch with" something that you do not frequently work on.

Also, the workers should be made aware of the changes to policies or designs as well.

The Audit report notes these shortfalls.

Oh, get this Indian Railways have introduced a Web-Enabled IT platform called Track Management System.

The track maintenance practices are recorded here.

Up to at least 2021, there were people who were not trained in using methods of online working! They were either not trained, or, I suppose, were uninterested.

I think "I am not interested in working with computers" in 2023 should be seen as a serious threat to passenger safety.

This is not about one person's liberty to choose to learn to do something.

This is about how many people his/her area of work serves---the railway customers: from little children to that office-goer to that large corporation that moves thousands of tonnes of material using the railways.

Learning "online ways of working" must not be optional in 2023.

That brings us to why the Indian Railways are not getting better.

Let's keep operational issues aside. There are other issues that slow down the progress of the railways.

The Audit report notes shortfalls in most, if not all the areas it covered.

First: Reporting and sharing information

Air travel is considered the safest for a reason.

This is because of the framework they operate on.

Every little deviation gets recorded, and there is action taken on it.

After every incident---minor or major---an investigation panel is set up that goes through each little parameter that led to that incident, and this is recorded.

The panel gives its recommendation, which gets implemented and reported.

Every effort is made to ensure that no incident repeats itself.

Every parameter that can be controlled is controlled, and they leave very little to chance.

Did you know that planes are made in such a way that even if it banks at an angle of 30 degrees, it does it in such a way that the pull on everything within the plane remains parallel to the plane's bottom?

This is why even when the plane banks and turns, the water in the cup placed on the drop-down tray in front of you doesn't spill out; instead, it stays *horizontal* to the tray.

Observe this the next time you fly; the plane's turn emulates the earth's gravity using centrifugal force.

It's mind-blowing. I know.

This is not a nice-to-have feature. A lot of money has gone into making that happen.

Why? There must have been some incident somewhere because someone fell down, or something spilled ... who knows?

They recorded it.

They sent the recommendations.

The manufacturers like Boeing and Airbus took up the task.

They engineered a solution.

And voila, you have the feature in almost every plane you fly in today.

Indian Railways has been failing in recording and sharing information.

This is why similar accidents happen all across the country.

This is as much a transparency issue, as an integrity issue.

And it indicates general indifference.

The Indian Railways have what they call SIMS (or Safety Information Management System), created for "accident reporting, analysis and sharing information".

The module, within this software, which is responsible for reporting of the detailed analysis of accidents to the _Chief Safety Officer_---yup, Chief Safety Officer---was *not even implemented* in the software by the time the report came out.

Why?

Any logical agency would categorise this as a core capability of such software, but implementation of this functionality was pending at least until 2022.

(The current status is unknown to the public.)

Second: Lack of standards in some areas.

When you set out for an inspection, you set up a certain criteria for when something is considered adequate.

For example, when I was in school, the criteria for passing the internal exams was a minimum 40% score.

There is *no set criteria* in Indian Railways, as to how many inspections should be done for a certain part of the line.

How can this be the case in one of the largest railway networks in the world, in the twenty-first century?

The Indian Railways are over 150 years old now.

How can we still not know what number of inspections per how much of the railways is considered adequate?

This way, an entire zone can perform three inspections in a year, and nobody can tell them that the number of inspections done was insufficient!

Third: Field safety

The report notes that field safety is largely inadequate.

From making helmets available to hand-sized gloves, the field workers in the Indian Railways are working with dangerous equipment with little to no protection.

Who is to say that some of these inadequacies in inspections and maintenance are not because of the dangers that these activities pose to the workers?

And how would an enterprise that does not take its own workers' safety seriously, take its customers' safety seriously?

Okay, but how unsafe are the Indian Railways

Generally speaking, despite these shortfalls and the apparent indifference, Indian Railways are among the safest railways in the world.

That may seem contradicting, but here is what I mean.

The Indian Railways are decently safe. Safer than the roads, safer than most railways out there.

But, there is a lot of room for improvement.

With just 0.03 accidents per million train kilometres and *zero passenger fatalities* reported in the years between 2019 and 2021, we are still generally safe travelling by the Indian Railways.

One train kilometre is when one train travels one kilometre.

Ten trains travel one kilometre each, they make up ten train kilometres.

In case of the Indian Railways, three accidents occur once in a hundred million train kilometres---one train accident per 3.33 crore train kilometres.

And not all accidents cause fatalities.

Compare this with about three lac (300,000) road accidents, and 1.5 lac fatalities from them in a year.

Secondly, there is what is called the Swiss Cheese model of accident barriers in place, though they are not fully effective (because of the shortfalls that we just saw).

I came across this model while watching some air crash investigations.

And I liked it. It's a simple way to show how one effort counteracts the defects in another to improve safety.

So, in Indian Railways, these five barriers are:

1. Rules
2. Training/Counselling
3. Supervision
4. Co-ordination/Communication
5. Inspections

When each of these barriers fail to arrest an accident, an accident can occur.

Three accidents per hundred million train kilometres is not a bad record.

But the Indian Railways should focus on its Vision 2020, which wants to make railway operations free of accidents.

Of all the accidents that did lead to fatalities, most of them were because of either people being run over by trains, or involved people falling off of trains.

Fatalities due to crashes or derailments were a smaller number.

Despite all the shortcomings, Indian Railways were safe enough to travel, as of 2022.

The number of accidents went from 104 in 2016--17 to 22 in 2020--21.

If the investments in safety keep declining as they are, then, yes, the safety starts becoming questionable.

Which is why we must still push for the implementation of the safety measures, including making SIMS better.

And the Balasore accident should stand as a reminder of the importance of making the railway systems safer.

Now to the part where everyone in the know is asking, 'Who is responsible for this tragedy?'

I say, I am more concerned about the accountability aspect of this rather than the responsibility.

There has not been too much finger-pointing in this matter, which is surprising given how we typically react to these situations.

But there still are questions to be answered.

The Railway Board and the Ministry of Civil Aviation (yes) usually head these investigations.

The Railway Board reports to the Ministry of Railways, while the Commissioner of Railway Safety is part of the Ministry of Civil Aviation.

In this particular case, the CBI has been involved.

Which is unusual.

Does *criminal negligence* in the Railways come under the purview of the CBI?

Or is there more here than meets the eye?

But this case is not open-and-shut either.

The Railway Board said in its statement that they spoke to one of the loco pilots, and he said that the signal was green when they entered the loop line.

Which meant that the pilots could go forward at their "permissible speed".

But if the switch was set to the loop line, the signal should not have been green; it should have been yellow, which indicates "proceed with caution" or "proceed with restricted speed", which is what is appropriate for a loop line.

The interlocking system is fail-safe, which means, if it had detected something wrong, it should have failed safe to "stop".

Under no circumstance should it have indicated "proceed".

According to the report, 13 derailments happened because of incorrect setting of points.

About half of the derailments overall were either caused by the Engineering or the Operations departments.

About three in every four accidents that were caused by the Signalling department were due to either system or technological deficiency and visibility issues.

Right after the tragedy, a probe panel was set up with five members.

They submitted a priliminary report of the accident.

A day after the accident, one of the members of the panel, a senior section engineer, submitted a dissenting note, stating that according to the Data Logger report, the signal was indeed green, and that was because the point machine was pointing to the main line, not the loop line.

The signal indication is in line with the loco pilot's statement to the Railway Board.

But if the switch was set to the main line, why did the train derail?

The dissenting SSE also notes that the point of derailment was the Level Crossing before the point.

But experts disagree with this, because a derailment at the level crossing would be very evident.

The officials, reportedly, said that the train log, I quote: "clearly shows it *entered the loop at 128 kmph* and went to zero within seconds"

There have been mentions of criminal involvement, and sabotage is ruled out.

A suspicion for criminal involvement does explain the case being handed over to the CBI, but that also seems to have happened in haste, as some in the know pointed out.

Overall, there is still fog over the situation, and more will become clear as more data comes out.

Interestingly, not all is hunky-dory with fail-safe interlocking systems, as pointed out by an official of the South Western Railway (I quote):

> ... incident indicates that there are serious flaws in the system, where the route of dispatch gets altered after a train starts on signals with correct appearance of route in the Station master’s panel. This contravenes the essence and basic principles of interlocking.

Clearly, there needs to be investigation into what is going on.

But apart from that, this communication should tell the investigators to tread carefully when making assumptions about the systems, and whether these are indeed fail-safe as thought of.

Note that this came out in February, and no action seems to have been taken yet.

Again, this is not to say that travelling by trains is unsafe, it just isn't as safe as the Indian Railways make it out to be.

So, what is the way forward?

First, there is nothing to panic.

This one tragedy should not imply that travelling by trains is unsafe.

Indian trains, despite all their flaws, are among the safest in the world.

We have technically competent staff in the designing and manufacturing areas, in general, and that makes up for some of the operational deficiencies that we currently have.

Complacence is a dangerous enemy, though.

The Indian Railways should take a look at their current ways of working and mend anything that needs mending.

They must remember their Vision 2020.

Security of signalling systems is critical.

When heavy metal rolls at high speeds, there is very little anyone can do to avert an accident.

Prevention is the only way.

Given the nature of trains, there is little a loco pilot can do in dangerous situations like this.

Which is why we have so many rules and regulations in place.

The loco pilot is merely responsible for the speed of the train.

S/He works based on the signal s/he gets.

Wrong signals can lead to serious disasters.

Upgradation and modernisation cannot be optional anymore.

The Indian Railways have grown to a massive scale.

At this scale, there is little room for error.

Modernisation helps run better operations.

The aviation industry was born after the railways, yet, it overtook railways in safety and security.

We have the technology to achieve a lot more, and the Railways should utilise that.

Now that the discussion has begun about safety in trains, it should be a primary focus area.

Safety cannot be compromised, especially when we are introducing faster trains like the Vande Bharat.

But like I said, since the infrastructure requirements for these trains require high standards of engineering, upgrading the tracks along these lines will lead to better safety.

But that cannot happen at its own pace over---you know---three decades.

The Railways have the funds, which many enterprises don't.

They are being strongly backed by the government.

They should utilise this for the better.

But as these safety standards increase, the competence of the Railways staff also should increase.

Hire enough people, train them and get them on the field.

I have visited the Integral Coach Factory in Chennai, and that day still remains as one of the most memorable days in my life.

So many knowledgeable people at work, focusing on making travel comfortable and safe for the passengers.

I could sense the passion that some of them had, in the interactions we had with them.

Well, a classmate's father was with the ICF, and he arranged for an "industrial visit" to it.

We have a large pool of brilliant people in our country.

We must rise above all the politics, and look at ensuring that Indian Railways become something we Indians can brag about more than we do today.

We must learn from our mistakes, and bounce back from this tragedy.

The goal should be that such a tragedy or bigger never happens again.

That is the least the Indian Railways and the ministry can do for those who lost their lives in this tragedy.

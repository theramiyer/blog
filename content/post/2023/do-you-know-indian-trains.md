---
title: "Do you know Indian Trains"
subtitle: Part 1 of the series on the Odisha triple train tragedy
date: 2023-06-09T10:03:50+05:30
description: "Understanding how the Railways work is critical to forming an opinion on the train tragedy, because, misunderstanding information can lead to misinformation."
image: https://blogfiles.ramiyer.me/2023/images/curved-turnout.jpg
tags:
- travel
- governance
- employment
- politics
---

I have, like millions of Indians out there, been travelling long distances by the Indian Railways since infancy---since I was about six months old. And over these past three decades, I have read about tens of crashes including derailments. Even seen that odd wreckage during my train journeys.

What happened on the 2nd of June was terrible, and I cannot possibly express what I feel about it, in words. I turned to TV news for updates, thinking for once they could be useful. Boy, was I wrong. And then there were WhatsApp messages floating around, loaded with misinformation. Mr Modi and Mr Vaishnaw visiting the accident site only added to the noise, unfortunately.

In this series, let us break down the situation into little pieces. You might have heard a lot about many aspects of the Railways. And a lot of information about this crash is already out there. This series is not purely about that. Here, I will put together the different aspects of the Railways to help you gain some perspective.

I will also be talking about it from the technical sense first, but will keep it absolutely uncomplicated, because not all of us are technical people. Here is what we will talk about in this part:

{{< toc >}}

## A gist of the crash

The accident took place near a station called Bahanaga Bazar, in the Balasore (or Baleshwar) district of Odisha. As you may have already read, the Chennai-bound Coromandel Express (or _Hospital Express_, as it is popularly called) entered the loop line at the speed of 128 km/h, crashing straight into the parked freight train carrying iron ore. Of course, apart from the derailment, the coaches got strewn around because of the impact of the collision. The last few coaches of the Howrah-bound Yeshwanthapura--Howrah express got hit in the process, and that train derailed as well. A tragic triple-train crash.

“How did something like this happen in 2023?” was the first question in the minds of a lot of us. It kindled a discussion on railway safety. And threw light on the deficiencies in the Indian Railways. I would not be surprised if the CAG report on derailments suddenly became one of the most downloaded audit reports in the country, that day.

I did read some articles on the internet about interlocking systems and loop lines, but did not see enough information out there. I felt it would be better if I added some here for my readers.

Of course, I must note that I am merely an aviation and railways enthusiast, not a professional in either of these fields. While I have tried my best to express what each of these aspects of the railways is, I may be wrong in places. Take what I say with a grain of salt. Also, you often pay the price for simplification with accuracy, which means, what I say may not be a 100% technically accurate, but will be generally good enough for a layperson. That is the whole point of this exercise.

Let us now know what each of the most popular terms doing the rounds in the papers and other media, mean:

## A loop line

A loop line, as you may know by now, is a segment of railway track that branches out of the “main” line and comes back into it. Trains use these when they have to give way to other trains to pass. I read that typically these are used by freight trains, but that is not true.

Any train that must give way to another train would use the loop line. Typically, freight trains have the lowest "precedence", and you often find them stationary on the loop lines when you travel. If you travel by superfast expresses, you will also see passenger trains on them.

Technically, these loop lines are at least 650 metres long by standard, and can hold _exactly one train_ in them. This is a crucial point, as we discuss the other components. This also means that no train of the Indian Railways is longer than 650 m---so that every train out there can fit into any unoccupied loop line.

The beginning of the safe point on a loop line is marked with what is called a "Fouling Mark". The guard of the train, having a cabin at the end of it, ensures that his cabin crosses this point within the loop line. This in turn ensures that the entire train is now in the loop line, without any danger to itself or any train passing on the main line.

Trains are designed for high speed. Higher speed, in trains, does not equate to more danger. Every aspect of the high-speed track is carefully engineered to ensure high safety at high speeds. Accordingly, segments are speed-graded as well. This segment of the main line, where the tragedy took place, was engineered for speeds up to 130 km/h.

## Turnouts and switches

The "turnouts", are speed-graded as well. How? Depending on their deviation from the main line. The sharper the deviation from the main line, the slower the train must enter the loop line.

A switch is the arrangement of the tracks and other machinery at the turnout, which direct the trains (or "switch" them between the main line and the loop line---or other lines).

Given that turnouts change the direction of boxes made of tonnes of metal (trains), the trains must pass them carefully, when the switch is set into the turnout. Even a 1 in 24 curved switch turnout allows a maximum speed of 30 km/h.{{< sidenote indianrailwaysIndianRailwaysPermanent2020 >}}Indian Railways Permanent Way Manual ([Indian Railways](https://indianrailways.gov.in/railwayboard/uploads/directorate/prd/PR/IRPWM2020.pdf)){{< /sidenote >}} Although turnouts on some stretches do allow higher speeds because of the way they are built,{{< sidenote IRFCAIndianRailways >}}Permanent Way ([IRFCA](https://www.irfca.org/faq/faq-pway.html)){{< /sidenote >}} no train is safe taking a turnout at 128 km/h. Which means, even if the loop line were clear, the train was likely to have derailed.

What is a "1 in 24 curved switch turnout", you ask? Turnout here means what it sounds like: a turn out of the main line. A curved switch is basically how the switch deviates---the turn is a curve rather than a sharp cut away from the main line and into the loop line. A curved turnout results in a much smoother turn, which is safer than a sharp turnout. 1 in 24 here means that the train goes 1 metre away from the main line for every 24 metres it travels on the loop line. Typically, a curved turnout allows for slightly better speeds than their sharp counterparts.

{{< figure src="https://blogfiles.ramiyer.me/2023/images/curved-turnout.jpg" alt="A curved switch turnout" caption="A curved switch turnout. Notice how it nicely curves out of the main line and curves into the loop (platform) line." >}}

## Precedence

The two loop lines here were occupied by two freight trains. Both the passenger trains were superfast expresses, and they obviously had higher “precedence”. (Which is why the freight trains had to go into the loop lines.) “Precedence” is a way of saying which train gets preference on a certain line. Relief trains (accident relief trains, for example) have the highest precedence. Their precedence is higher than even the President's and the VVIP Special trains. Third preference goes to the suburban trains in peak hours. Fourth come the superfast trains such as the Rajdhani, the Shatabdi, the Vande Bharat, etc.

Among the general use trains, freight trains have the lowest precedence.

## A Point Machine

As you may know by now, a Point Machine is that arrangement that flips the switch between the main line and the loop line. This could be purely mechanical (which would require someone to go there and make the switch), or electrically controlled (which it usually is these days), or electronically controlled (which this point machine at Bahanaga Bazar was).{{< sidenote sumedaExplainedHowDoes2023 >}}Explained \| What is the electronic interlocking system in railways? ([The Hindu](https://www.thehindu.com/news/national/explained-what-is-the-electronic-interlocking-system-in-railways/article66933809.ece)){{< /sidenote >}}

## Interlocking system

When it comes to managing a high-speed multi-thousand-tonne heavy machinery on smooth railway tracks, there needs to be a lot of forethought, and many pieces of the system should work in unison. For example, when the point machine points the train to the main line, the signal at the end of the loop line should be red, indicating to the train on the loop line that it does not have the clearance to enter the main line just yet. If there is a level crossing, the boom arm must be lowered to ensure that no road vehicle enters the track. The block (explained below) behind should have a red signal, the one before that must have at least a yellow, the one before must have at least yellow-yellow and the one before may have a green, and so on. Also, the signal for the main line must be green _only_ if the point machine is pointing to the main line and at least three blocks in front are clear of trains, _and_ if the main line is safe for the train to travel at the maximum track speed in other aspects.

This is called interlocking.

These systems are made "fail-safe", which means, they go to their most restrictive state when in "on" condition (also explained below).

## A section (or a block)

A railway line between two stations, in general, is divided into a few segments, called "sections" (terms like Section Engineer, come from this). Some railways also call these segments as "blocks". A section is typically at least as long as the maximum braking distance for the fastest train on that line. This means, if the line carries a train whose maximum speed is 160 km/h, and the train is made up of only LHB coaches with, say, the Knorr Bremse air brakes, the section length will be at least 1200 m (or 1.2 km) long.

This is so that even in case of a danger, like a train being stationary on the same line in the next section, this train can come to a stop without colliding with the stationary train.

Every section has its signal, whose indication depends on the conditions of the sections ahead.

## The "on" condition

I said that fail-safe systems go to their most restrictive state when "on". The "on" condition, here, means nothing but the default state---the state in the absence of any input or in the presence of an error. Which means, unless someone (or something) changes the state of the signal, it would be in its "default state". Fail-safe systems have the default state of highest restriction; a section signal will always show "stop". Someone (or something) must change its state to "proceed" for it to show proceed; if you do not ask it to do anything, it would show "stop".

If the operation of switching it to "proceed", either manually or automatically (say, by the throw of the point machine) fails, the fail-safe system will go to its most restrictive state. In case of a section signal, this is usually "stop". The interlocking system should have examined its state based on inputs from all its components, locked the components in those positions, and determined the passage to be safe. Only if _all those conditions_ are met, the signal turns green.

Electronic interlocking systems are superior technology in this regard, because they have better sensing, logging, and alerting available. The probability of these systems failing is extremely low, much lower than the conventional electrical systems, which in turn is much lower than the manual systems.

## Speed and braking (and Kavach)

One of the first claims to come out when this collision (and derailment) happened was that had the Kavach system been there, this collision could have been averted.

No.

Leave all the technical speak from the Indian Railways aside. From a logical standpoint, think how the braking distance varies between your bicycle, your scooter (or motorcycle) and your car. Compare these again to that fully loaded lorry you see on highways.

A bicycle brakes can completely lock its wheels when you clutch the brake handles. But your bicycle does not come to a stop; it slips on the road for a while before it stops. This changes drastically when off-roading, when on sandy surfaces. Your braking distance is much longer.

But if you are on the road on your bicycle, riding at 10 km/h and you hit the brakes, you come to a stop almost within a metre.

Now, compare this with your motorcycle. Braking is much more efficient in it, and you usually come to a stop much sooner than your bicycle travelling at the same speed. Why? The weight of the motorcycle helps. Also, a motorcycle runs with a much higher friction on the road than your bicycle. This helps in braking.

A car, on the other hand, may take much longer to brake, if you fit it with brakes with similar parameters as a motorcycle. Why? Because even though the friction with the road is higher in case of the car, the car is heavier. It has a much bigger momentum. This is why the car's brakes are stronger.

Now, imagine you are riding a bicycle, a friend rides a motorcycle and another drives a car---all on a road that has a thin film of rainwater. Imagine all three of you hit the brakes at the same time, and all your vehicles' wheels stop moving at the same time. How does the braking distance vary between you and your friends? Why? The friction with the road reduced drastically because of aquaplaning.

This means that no train running at a speed of 128 km/h, no matter how great the brakes are, can come to a stop within half the distance of the section. Especially on solid steel tracks that are polished over time by thousands of wheel sets running at high speeds on them every week. That kind of friction is simply absent on these tracks. Even if the wheels locked in position, because of the weight of the train, it will slip on the track for hundreds of metres before coming to a complete stop. Of course, by this time, all its wheels would be completely damaged (they would have a flat portion), and will be incapable of rolling. The brake systems are designed to prevent this, which means, they will never let the wheels lock that way, which in turn means, the braking distance will be longer.

Even if the freight train was absent on the loop line:

1. The Coromandel Express would have derailed because of the switch position.
2. It would have not come to a stop even within the loop line had the loco pilot tried to brake to prevent the derailment. The train would have derailed at the end of the loop line any way.

Also, add the human reaction time to it. By the time the pilots realised that the train had somehow taken the loop line, two seconds would have passed after the collision.

Nothing in this world could have stopped the derailment of this train at that speed with that track configuration. Not even a miracle. Simple mechanics would have _ensured_ the derailment of the train.

Outside of the film industry, the laws of physics exist, and they act consistently across the universe.

## Speed of trains

You may have understood by now that merely the power of the locomotive engine does not determine the speed of the train. This is why the Vande Bharat, even though designed for 160 km/h, cannot take us from Chennai to Bangalore in 2:15 hours---it takes a solid 4:25 hours. I have never noticed its speed go beyond 117 km/h. Also, it rarely hit the speed of 115 km/h.

Everything from the density of the rail (52 kg/m or 60 kg/m),{{< sidenote weight >}}Yes, density is measured in kg/m³; kg/m is a non-standard but easier unit to use for this context. Why? Because the cross-sectional areas of a 52 kg/m track and a 60 kg/m track are exactly the same.{{< /sidenote >}} the kind of error allowed on the switches, to the sleepers used, to the kind of welding done, to whether any maintenance activity is going on and on what stretch, the presence of bridges (and their construction type), to other trains on the line, there is a lot that goes into determining which speed "group" a line falls under. This grouping tells you what speed the line supports. The line on which the accident took place may be a Group B line, allowing speeds up to 130 km/h---the line was *safe* for trains to travel at speeds up to 130 km/h.

Again, railways are designed for speed. A higher speed does not necessarily mean lower safety.

In fact, the technical folk in the railways would argue the opposite: tracks supporting higher speeds are safer, because they have a higher standard of construction, and adhere to more stringent norms with smaller room for error. The bogies and the coaches (bogies are those blocks that have the suspensions, brakes and the wheels; coaches are what people and freight occupy) are designed for better stability and comfort on high-speed trains.

Back when I was in school, I would feel myself swaying for almost half a day at home, after finishing a 36-hour journey by train. It does not happen these days, not because I've grown older, but because the coaches and lines have gotten that much better.

With all this information, let us move on to the Balasore train tragedy:

- [Operational Lapses in Indian Railways]({{< ref "operational-lapses-in-indian-railways.md" >}})
- [The politics of the Balasore tragedy]({{< ref "the-politics-of-the-balasore-tragedy.md" >}})
- [Responsibility for the Balasore tragedy]({{< ref "responsibility-for-the-balasore-tragedy.md" >}})
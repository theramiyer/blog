---
title: "You Must Know Physics to Watch Oppenheimer"
subtitle: "or, must you?"
date: 2023-08-03T09:06:05+05:30
description: Oppenheimer, the Nolan film, is best watched in context. This post is to set that stage. And to give you a crash course on nuclear physics and World War II.
episode:
    spotify: 59SRI2uBpmM2ScXGUyTnZq
    google: OWRjOTJiZjctMDU5Ni00YzMyLWEyZjUtYTNjN2I3NDg4Zjkx
    apple: 1000623578201
tags:
- science
- democracy
- war
---

I finally managed to watch _Oppenheimer_ on the IMAX screen. It was a delight, indeed. Like many of you, I had heard that one must know physics to make sense of _Oppenheimer_. But then, I thought, this film is neither called _The Atomic Bomb_ or _The Manhattan Project_ or even _The Los Alamos Lab_---it's called _Oppenheimer_, which means, this must be about the man, not his creation or the project.

You would be right to think the way I did; this film is about the man.

{{< apple >}}

{{< podcast >}}

But, having an idea of the physics and the timeline of what happened before Oppenheimer will be beneficial to make the most out of the story the film narrates.

This post is about what happened before _Oppenheimer_ (the story).

No spoilers.

{{< toc >}}

Not that you can have spoilers in this (except for the last scene, I cannot think of any other potential spoilers to be given), because this film is strongly rooted in the history of the atomic bomb and that of the WWII. In fact, almost half of the film is derived from _actual transcripts_ of hearings, while the rest is derived from a biography.

The film tries to depict the complex person that Oppenheimer was, the circumstances then, and some of the crucial aspects of the war that led to what happened. And I cannot think of anyone other than Nolan who could do justice to the story of a man as complex as Dr J. Robert Oppenheimer.

Of course, the film does not do justice to the others that significantly contributed to the scientific research. But I get that the subject of the film is not nuclear physics. And a three-hour biopic can do only so much justice to the subject. Although, I would have probably liked it to show the other contemporaries who contributed to the project a little more. The film only really shows Teller to be another significant contributor.

Also, I am not a Nolan fanboy, and this is not a film review.

## The scientific background

Knowing a little bit of physics will help you make your watching experience better. So, here I am, giving you just enough of the science in the film. And no worries if you are not from the "science background"; this is high school stuff. You have read/studied this. I am merely recapitulating it for easier recollection and context.

### Setting the stage

Imagine yourself in the late 19th Century (late 1800s) and the early 20th Century (early 1900s). Einstein is working on his Special and General Theories of Relativity, and will publish them in the first two decades of the 20th Century. The world of Physics and Chemistry are buzzing with discoveries. In the interest of time, I will limit this to a small subset of people who were crucial to the development of the model of atom and the atomic bomb.

### The early days of the atom

Atoms, as we today know, make up all matter. From the air around us that we breathe to the lava that flows down volcanoes to the cells in our body, everything is made up of atoms.

Atoms, as a concept, are not new to us humans. We have been talking about "atomos" (not Atmos) for a few millennia now. Although, nobody truly knew (or knows) what an atom looked like.

Partly because atoms are too small. What do I mean by that? For us to be able to see something, light must bounce off of it, and hit our retina. This kicks off a chemical reaction in the retina, which generates a signal that the optic nerve carries to the brain.

But when we aim to look at a particle that is hundreds of times smaller than the smallest wavelength of the light we can see, well, how can we?

Which is why we must resort to observations in ways other than directly "seeing" these particles.

### J. J. Thomson

Let us begin our journey with the Cathode Ray. This ray was discovered sometime in the mid 19th Century (I am not a historian, I do not remember exact historical dates). J. J. Thomson was the first one, though, to study it further, and conclude that these were "negative corpuscles"---because they got attracted to the positively charged plate placed on the other end of the cathode ray tube. He also noted that they could be deflected by electric field (the negative terminal repelled them, the positive attracted them).

He filled cathode ray tubes with one gas after another, and noticed that the type of gas did not change the nature of the cathode rays.

Given that the cathode rays were lighter than anything known until then, the conclusion was that this constituted something that was a part of matter, and was present in all matter.

These "negative corpuscles" would later come to be known as "electrons".

More experiments were carried out in which electrons were stripped from matter. What remained was a lot more massive than electrons, and was positively charged. Thomson concluded that atoms must be like pudding that is uniformly positively charged, making up most of its mass, while electrons were like raisins, scattered in this positive electric field.

### Ernest Rutherford

J. J. Thomson's student, Ernest Rutherford did some more experiments to understand the nature of atom.

Rutherford was a contemporary of Marie and Pierre Curie. "Radioactivity" was all the rave during their time. There was enough evidence that some elements, naturally emitted some "radiation". Rutherford subjected these radiations to an electric field.

He found two types of rays---alpha and beta---that this radiation split into. The alpha rays deflected towards the negative, while the beta towards the positive, leading Rutherford to conclude that the alpha rays were positively charged, while the beta rays were negatively charged. He also noticed that beta rays were similar in nature to the cathode rays.

Later, it was found that the beta rays were indeed like cathode rays, because they were also high energy electrons. Alpha rays were later found to be positively charged Helium. This was consistent with that the alpha particles were massive compared to the beta particles, and were also much bigger.

Rutherford set up his apparatus and bombarded a thin gold foil with alpha particles. He expected the alpha particles to pass straight through to hit the photographic plate placed at the other end. This was because, based on Thomson's model, the gold foil should mostly have positively charged massive atoms with electrons scattered in them. Since there was no positive charge concentrated anywhere specifically across the foil, the beam should pass straight through, albeit perhaps a little slower.

But when his ... protégés ... Hans Geiger (him whom the Geiger Counter is named after) and Ernest Marsden performed the experiment they found that some alpha particles ricocheted, and way too many rays deflected more than 2°, which made Rutherford conclude that the atom was not like a raisin pudding.

He concluded that since most particles passed right through the foil, the atom must be mostly empty. But at the same time, there must be something heavy in the atom which is making the alpha particles ricochet. This meant that there were some very dense places within an atom. Dense enough to deflect massive alpha particles. (An electron will not cut it.)

Rutherford came up with his model of atom, in which he said the nucleus contained all the positive charges (protons---though they were not called that yet), packed in a small space, while the negative charges (electrons) made up the rest of the atom. Kind of still like the Thomson model---scattered across the atom.

### Niels Bohr and Werner Heisenberg

Rutherford's model answered some questions that were left unanswered by the Thomson model. But there were still questions around the atom. Like how come the electrons (which are the negatively charged particles) do not get attracted to the proton-filled positive nucleus and, well, the atom did not implode?

Quantum mechanics comes into the picture around here. We need not get into this for now, because this will not be relevant to the film. Bohr, using the principles of quantum mechanics, proposed another model, to which Werner Heisenberg's (and others') wave mechanics were added, and we have the atomic model we have of today.

### James Chadwick

We must note that by this time, we had a solid understanding of how to calculate the mass of atoms. (I know, sounds nuts, but it's true.) And some scientists did not buy into the idea that atoms were only made up of protons and electrons. Chadwick was one of them.

Any stable atom is electrically neutral. Which means there are as many positive charges in an atom as the negative charges. For instance, Hydrogen had one proton and one electron in it. Helium had two protons and two electrons.

Going by the idea that there are only protons and electrons in an atom, it would mean that one Helium atom weighed twice as much as a Hydrogen atom. But in reality, a Helium atom weighs four times as much as a Hydrogen atom.

Experiments were performed to find out why. James Chadwick concluded that there was another class of particles in the nucleus, which was electrically neutral, but weighed almost as much as a proton. This particle later started being called neutron (because it was electrically neutral).

This neutron is critical to the atomic bomb.

### Radioactivity

Radioactivity, until now, was thought to be a passive process. In this process, large, unstable nuclei "decayed" (as in broke down by releasing particles) to become more stable.

For example, one atom of Radium would emit one alpha particle to reduce to Radon (an inert gas).

This process happened naturally. Given that an alpha particle (a Helium ion) was being released from Uranium to make Thorium, it followed that heavy, unstable (and therefore radioactive) elements could split their nucleus (and therefore the atom) and form two or more atoms.

Just like alpha particles, atoms could potentially emit beta particles or neutrons (or a combination of these) to get a stable nucleus.

We also found that (and this is critical) this decay also released energy.

### Radioactive Fission

Radioactivity was considered passive only until Otto Hahn, Lise Meitner and Fritz Strassmann performed their experiment of nuclear fission.

They split an atom forcefully by injecting neutrons into them. This was a groundbreaking discovery, which changed the world forever.

This meant that one could take any sufficiently large atom, bombard it with one or more neutrons, get the neutrons absorbed into the nucleus, further increasing its instability, and split it into relatively more stable smaller atoms (and release subatomic particles in the process).

When you go to watch the film, connect from here to the scene in which Alvarez runs out of the salon.

And given that every fission also released energy, it would mean that by breaking elements such as Uranium and Plutonium, we could "create" energy. While every fission only released a tiny bit of energy, a large number of fissions would release a large amount of energy.

Here is what an example nuclear fission bomb does:

1. Bombard one Uranium atom with one neutron.
2. The Uranium atom splits into Thorium and Helium, releasing energy and three neutrons.
3. Bombard another three Uranium nuclei with these three neutrons, making more Thorium and Helium, releasing three times as much energy and nine neutrons.
4. Bombard nine Uranium nuclei with these nine neutrons, making more Thorium and Helium, releasing nine times as much energy as the first fission and 27 neutrons.
5. Bombard 27 Uranium nuclei ... you get the idea.

This will make the reaction go from one nucleus to three to nine to 27 to 81 to 243 to 729 to 2,187 to 6,561 ... creating an exponential runaway chain reaction. This would release an enormous amount of energy, several thousands of times in magnitude as seen in any chemical reaction.

One point critical here (no pun intended) is _critical mass_. Roughly, this talks about how much fuel you need (mass) enclosed in how much space (volume). The denser your fuel is, the more effective the fission would be.

This is all the scientific context you will need to watch the film. The rest will be shown on the screen.

## The politics

In the 1930s, Hitler's forces had started on their capturing spree. While a full-blown war had not yet begun, there were signs of it. Finally, in 1939, the Second World War began, between the _Axis_ powers (Nazi Germany, Italy and Japan) and the _Allies_ (Britain, France, et al.). The United States were not yet part of the war.

Several brilliant minds in the scientific community in Germany were of the Jewish origin at this time. Including Albert Einstein, Leo Szilard, Lise Meitner and so on. They were now being targeted by the Nazis.

Out of fear, these scientists fled Germany for other places such as Britain and the United States. Einstein, for example, went to Princeton in the United States. Some scientists did try to raise their voice against the persecution of the Jews, and one of them even met with Hitler to talk about it. Hitler merely said that he was not against the Jews, but against the Communists. But we all know the reality; whether targets were identified by the ideology they followed, or their origin.

To the west of "the pond", the United States were going through a tough time of their own. Their economy had taken a blow (because of The Great Depression). In general, the American sentiments were against the Axis powers. (And their politicians of the day were well aware of this.)

In the European region, the Soviet Union was initially on the side of Nazi Germany, but when the Nazi forces started advancing into the USSR and taking over their territory, they switched sides and aligned with the Allies.

In general, the binary in this war was Fascism vs Communism, with those opposing the Nazi actions believing that Communism was the only answer to the Fascism the Nazis were engaging in.

But, when you watch the film, remember this: While the sentiment in the United States was aligned against Fascism, it was not necessarily aligned with Communism. That distinction is important; Fascists vs Communists is a false binary in the context of the US. Also remember that the Democrats were running the administration in the US then.

## The war

On the Asian side, Japan was advancing westwards and southwards. Tensions between the United States and Japan had been rising through the Great Depression. The US did not like that Japan was forcefully annexing territory and engaging in civilian atrocities.

The United States have always considered themselves flag-bearers of the _Free World_, and had been allies with the likes of the Great Britain. (Does anyone see the irony here?) Japan's aggression against the interests of the Allies did not go well with the US.

The United States were not part of the war for the first couple of years, although they were "lending" weapons to the Allies. The United States had also imposed sanctions against Japan, and had started supporting the resistance to the Japanese expansionism. This angered Japan, since it saw these actions as "western interference" in "eastern matters".

Japan bombed Pearl Harbour out of the blue. The surprise element was the only edge the Japanese had over the US. Of course, the US saw this as treachery. This pulled the United States into the war. There is also always the political angle in a democracy; public sentiment is of utmost importance to politicians. The American people must have felt violated when Pearl Harbour was bombed like it was.

The United States were also considered a strong power globally. Japan's bombing of the US was a direct blow to that image as well. The US keeping quiet after such an attack is any way unthinkable, even today.

Meanwhile, the American intelligence agencies had also learned that the scientists working under Hitler were working on a nuclear weapon. _How could the US not do anything?!_

Three or so years later, right before the Trinity test could happen, Hitler killed himself. Within days, the Nazis surrendered. Why did the US not scrap the Manhattan Project then? Well, sunk costs, plus, while the Nazi Germany was a serious threat, Japan was the United States' biggest adversary, showing no signs of slowing down.

Would Japan have surrendered eventually? Well, the emperor did not accept the terms in the Potsdam Declaration. Why? Because it called for a _democratic_ government. He did not like it, because he had no role in the democratic government. So, my guess, knowing dictators: no, Japan would not have backed down unless it was defeated.

The US bombed Hiroshima. Yet, Japan showed no signs of surrender, which is what, the US claim, necessitated dropping _Fat Man_ on Nagasaki.

In my view, this information is enough to put Oppenheimer in context. Although, I must mention that the film fully only revolves around Dr J. Robert Oppenheimer. Whatever I have said in this post does not prominently feature in the film, but is alluded to or brushed through.

If you have not watched the film yet, I recommend that you do, because it is a brilliant piece of work. If you watched the film and not all of it made sense to you, you can re-watch it if you would like, in view of this information. Sure, you could also wait for it to come on OTT, come back here, re-read this post and then re/watch the film.

In all, I think the Manhattan Project completely changed how we humans saw wars. Of course, though some may disagree, I think this would have happened regardless of which side ran the programme. Because even though human stupidity is infinite, human intelligence is also a reality.

But, can the bombings on innocent civilians be justified?

But also, can we apply today's lens to what happened so long ago?

Please note: This post features no side/footnotes, because it is not written with reference to specific literature. I wrote this post in a few hours over a weekend, and based it on what I know about the war and the project. The dates and the timelines are rough estimates based on how I put them together and tried to make sense of them in my head.

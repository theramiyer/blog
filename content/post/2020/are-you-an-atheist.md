---
date: "2020-03-30T00:00:00Z"
episode:
    apple: 1000470093615
    spotify: 5zMCST7j5NwbhxgEbGAlv6
    google: YmY4NGMxMjItZDg0Mi00MGMwLTgwZTktMmU4ZDExYTA2OWZm
tags:
- belief
- atheism
- philosophy
title: Are you an atheist
youtube: q4mLXqwy3EA
---

Today, I watched a rather curious video on Facebook, called, [_The Atheist Delusion_](https://m.facebook.com/story.php?story_fbid=483439892323398&id=106313546083516). A decent piece of work. Here is a gist of what happens in the video: A person---let us call him 'Jim'---goes to different people asking them if they were atheists. The people answer in the affirmative. Jim hands them a colour-printed picture book and asks them if the book created itself---if the colours somehow fell from the sky creating those pictures, and then black ink fell from somewhere forming those letters with the capitalisation, punctuation and all. They say it would be stupid to think so.

{{< podcast >}}

Then, Jim goes on to ask them what the DNA is. One says the DNA is the instruction code for the cells, so they could form a certain way. Jim mentions how the strands in the DNA if stretched out, could go all the way to the sun and back, or something along those lines. And then he asks, 'Well, if you think the book could not create itself, how could the DNA, which is the instruction code for every living being on earth, create itself?'

{{< youtube >}}

Then he asks, 'Do you mean to say everything came from nothing?' He even mentions Richard Dawkins in a statement to one of the participants and shows a clip of a video where Prof. Dawkins tells the audience how the concept of "nothing created everything" defies common sense. Then, he says, 'Don't you think there is an intelligent being behind this intelligent design? Do you think the entire Universe happened by accident? Do you think your DNA happened by accident?'

> "Origins don't matter."

In the end, he tells you that the Intelligent Being, created you; that you are not a blob that came from nothing, but someone with meaning and a purpose.

Certainly uplifting. And there sure is an Intelligent Creator, right?

Now is the time you get yourself a mug of hot coffee.

Am I an atheist? No. Being an atheist according to me is as much arrogant as claiming the existence of God. Neither help scientific thought. I am not a hundred percent sure that there is no God. But I am also not a hundred percent sure that there is a God. But let us keep my thoughts aside and look at it in perspective---because I think Jim's actions are a lousy way to sell the concept of God. (And I don't mean "sell" in the literal sense of exchanging something in return for money.)

Imagine a world where someone has no idea about what science is. Science is already hard to understand to most, because simple questions appear to have complex explanations. Imagine a world where we ago about doing what we do, working towards something tangible, chasing happiness and peace; imagine how utterly upsetting the fact is that we would one day become a mere bagful of lifeless organic matter. But, if someone told us that we could resurrect, that would be an incentive for us to actively work towards making the world a better place than the one we came into, so we could reap the benefits of it when we return.

Now, thinking that we all came into being by accident is overwhelmingly difficult to imagine. And so is that we took millions of years to get to where we are.

But let me be the Devil's Advocate (no pun intended; read above). Merely because we have no explanation for that everything came from nothing, can we conclude that some divine intelligence created us? What makes Jim so sure of it? Or that we are a product of intelligent design?

If so, who created the intelligent designer? Did the intelligent designer come out of nothing? Is that not absurd? Or did it come out of itself? Or did the intelligent designer exist even before existence? That makes no sense to me either.

Also, exposing the thoughtlessness on part of the participants in comparison with Jim does not make Jim right. Most people do not even think about all this that often. Most so-called atheists are so because they cannot connect with organised religions, or they cannot come to terms with half-baked explanations about God. Like science, metaphysics or theosophy are not everyone's cup of tea either. And the inability of those interviewed to answer what was an interview based on fallacies, does not equate to their scepticism being baseless.

Natural phenomena work in a certain way. Bookmaking works _against_ the process. That is a loaded sentence, the explanation to which would perhaps need a book (not necessarily in colour). Bookmaking happens instantly as opposed to evolution. Evolution has taken millions of years to get to this stage. If you can wait and watch for so long while something new evolves, be my guest. I could give some examples such as the coronavirus, but I don't see that being productive.

Science works on evidence. Science has no problem accepting its failures. Science does not yet have answers to everything, but science is trying every moment to find better answers. Science makes statements based on evidences and a specific process. You or anyone can verify these for yourselves. Of course, some studies need resources that not everyone has, but science has other ways to prove, within the boundaries of mathematics and logical reasoning.

Science does not take a shortcut to declare any non-existence of answer as a work of God. Science questions. Science is the opposite of a Moses declaring that he saw the God, and the God said what He said, and that there is no questioning it. Science is comfortable with new evidences against a theory (and mind you, a theory in science is different from the colloquial sense of the word).

In that manner, science would also be comfortable if evidence of the existence of God comes out. Science would not sulk, execute or burn down the evidence. It would accept. Of course, until new evidence comes out that finds faults in the evidence gathered so far. Science is not afraid of being wrong.

The cardinal himself said in the video:

> Questioning distinguishes us from the animal.

And here is my question: Is _this_ intelligent design; are we a product of an _intelligent_ design? We are a great design, but are we so intelligently made as Jim implies? Could an intelligent designer create a being which is inefficient and imperfect?

If you have to say that such is His way of making us realise Him, I'd call Him a rather self-absorbed, insecure intelligent designer. Before you get offended and ask Amazon to take down this site, remember, this is merely an argument to make Jim's better.

And before I wind up:

1. Jim mentions in the video, 'Origins don't matter.' If they did not matter, his entire conversation about the existence of an intelligent being that created us is moot.
2. If you look at the complete video, you would see the [cardinal cop out on the question on whether God is the intelligent designer][0eb838f1].

 [0eb838f1]: https://youtu.be/kwPeyssjIPY?t=2438 "ABC Q&A: Professor Richard Dawkins vs Cardinal George Pell 10 April 2012 (HD)"

And then, even if there were an Intelligent Being™, who said that kneeling in front of this being and folding hands and chanting something in a language that the Being probably does not understand, help us? But that question is for another rabbit hole discussion.

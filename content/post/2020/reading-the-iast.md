---
date: "2020-02-27T00:00:00Z"
tags:
- language
- culture
- film
- project
title: Reading the IAST
---

During my visit to Chennai the last month, I came across a film that was community-funded and community-made without paid professionals. This film was about the life of a Vaishnava guru of the thirteenth century, called Vedanta Desika, or Venkatacharya.

{{< toc >}}

## The film

The film is in Tamil. The idea of the film was to reach the story of the guru to common people, "during the times when people are forgetting their roots". The jibe aside, and irrespective of that I do not support religions, I believe that we must expose people to the different philosophies, but at the same time, allow them the freedom to choose what they feel is right for them. This approach is at the core of Hinduism. I did my part of offering to write the subtitles, so that we expand the audience by lowering the language barrier a little.

Writing the subtitles was a unique experience for me. But we will get to that later, so that you can skip it if you feel so.

## Choice of script

The Latin script (these letters that you are reading) is limiting when we write Sanskrit in it. Whereas the Devanagari script (the most common script used for Sanskrit) has 46 to 50 simple letters (depending on which you count), the Latin alphabet has 26. (I have not counted the composites in either script.)

I found it hard to write the words whose pronunciations are hard to express in the Latin script. I decided to switch to IAST, or the _International Alphabet for Sanskrit Transliteration_. But I figured, the alphabet is not popular, and rather limited to juvenile text decoration on social network sites (Anybody remember Orkut?), and needs an introduction. I decided to write a quick and simple guide on how to read the IAST (or "Sanskrit in English", as some say), and a quicker reference to IAST if you know an Indic language.

## Introduction to the IAST

Here is a brief of what it's all about:

Sanskrit has the _hrasva_ and _deergha_ sounds. For example, the sound of 'u' in 'cut' is a hrasva sound, while the sound of 'e' in 'deer' is the deergha sound. In other words, the prolonged sounds are deerga and the short ones, hrasva. We express the hrasva vowels with the Latin letters in their plain vanilla form. For example:

- 'a' sounds like 'u' in blunt.
- 'i' sounds like 'i' in film.
- 'u' sounds like 'u' in put.

You get the idea.

For the deergha sounds, we use a diacritic mark.

- 'ā' sounds like 'a' in 'cart'.
- 'ī' sounds like 'ee' in 'keep'.
- 'ū' sounds like 'oo' in 'too'.
- 'ē' sounds like 'ay' in 'clay'
- 'ō' sounds like 'o' in 'row'

The moment you see the dash-like diacritic above a letter, you know it's the deergha vowel sound. That sums up the vowels (ai, au, etc. don't need an explanation because we are familiar with those sounds from English) except ṛ , ṝ and ḥ; ṛ sounds _close_ to 'rhy' in rhythm, and I cannot find the English equivalents for the other two vowels.

Now come the consonants. They are intuitive as well.

First of all, Sanskrit differentiates between sounds such as the sound of 'c' as in 'cup' (aspirated) and the sound of 'k' as in 'sky' (tenius). In general, when 'h' does not follow a letter, read it as a tenius sound. In other words, 'k' sounds like 'sky' and 'kh' sounds like 'kite'.

- The velars are 'k', 'kh', 'g', 'gh' and 'ṅ'. The sound of 'ṅ' is like 'n' in 'king' or 'pink'.
- Then there are the palatals, 'c', 'ch', 'j', 'jh', 'ñ'. The sound of 'c' in IAST is like 'ch' in 'etch'. The sound of 'ñ' is like 'n' in 'quench' or 'engine'.
- Third are the retroflexes. We denote these with a dot below the letter: 'ṭ', 'ṭh', 'ḍ', 'ḍh' and 'ṇ'. The letter 'ṭ' sounds like 't' in 'curt', 'ṭh' like 't' in 'Thomas', 'ḍ' sounds like 'd' in 'bird', and 'ṇ' sounds like 'n' in 'control' or 'thunder'.
- Fourth: the dentals, 't', 'th', 'd', 'dh' and 'n'. The sound of 't' is like that of 'th' in 'teeth', 'th' is like 'th' in 'throw', 'd' sounds like 'th' in 'this', 'dh' is a stronger version of 'th' in 'this', and 'n' is the regular 'n' as in 'synthesis'.
- And, we have the labials, 'p', 'ph', 'b', 'bh' and 'm'. Note that 'ph' does not sound like 'phone', but like 'prove'.

Sanskrit has three 's' sounds: palatal, retroflex and dental: 'ś' that sounds like 'sh' in 'she', 'ṣ' that sounds like 'sh' in 'crush' or 'worship' and 's' as in 'sound', respectively. Among the liquids, there is 'ḷ', which is the retroflex 'l', as in 'occult'.

## If you know an Indian language

Now, if you know any of the Indic languages, here are some examples so you get a hang of it (this may or may not work on your browser depending on the fonts you have on your device):

|IAST|Tamil|Devanagari|Gujarati|Kannada|
|----|-----|----------|--------|-------|
|hara |ஹர |हर |હર |ಹರ |
|kāraṇaṃ |காரணம் |कारणं/कारणम् |કારણં |ಕಾರಣಂ |
|kṛti |கிருதி* |कृति |કૃતિ |ಕೃತಿ |
|pīṭham |பீடம் |पीठं/पीठम् |પીઠં |ಪೀಠಂ |
|aṅkur |அங்குர |अङ्कुर |અંકુર |ಅಞ್ಕುರ |
|aśvamēdha/aśwamēdha |அஶ்வமேத[^fce4b05a] |अश्वमेध |અશ્વમેધ |ಅಶ್ವಮೇಧ |
|Kāñchipuram |காஞ்சிபுரம் |कांचिपुरम्/काञ्चिपुरम् |કાઞ્ચિપુરં |ಕಾಞ್ಚಿಪುರಂ |
|viśēṣa |விஶேஷ |विशेष |વિશેષ |ವಿಶೇಷ |
|Nēpāḷ |நேபாள |नेपाळ |નેપાળ |ನೇಪಾಳ |

[^fce4b05a]: Those that disregard letters such as ஶ், ஷ், ஸ, etc. calling them 'வடமொழி எழுத்து', please remember that all good scripts evolve, and they must. Had the Latin alphabet not evolved, we would not have had the IAST; had Dēvanāgari not evolved, we would not have had the likes of ज़ in it. Openness to evolution is what helps something—anything—thrive. What would have happened had the cart not evolved; had people destroyed all vehicles, saying that the cart is the traditional form of a vehicle, it signifies our history and that we mustn't use any other form of vehicles?

\* No exact equivalent available

## My experience and discoveries

Now to what I mentioned as a unique experience. I am not a religious person. But I do read my share of texts about and from different religions, because, I feel, disregard for all the teachings of all the religious is arrogant; not a good principle to live by. Besides, these religious texts (which could well be a misnomer), when seen as philosophical teachings, do enrich your thinking in some ways. Of course, we must also should question/change/get rid of a good chunk of them because they do contain obsolete ideas.

And then, when composing the subtitles, I learned that the Vaiṣṇava school also has a Viśiṣṭādvaita branch. I was under the impression for so long that the Vaiṣṇava school had the Dvaita (or dualistic) line alone. I am glad I was wrong. I would perhaps read whatever possible about the works of Rāmānuja and Vēdānta Dēsika. I hope I find good English versions of them.

I also learned to appreciate the work that goes into writing subtitles. The amount of work that goes into time-coding the subtitles is enormous. Of course, once we write subtitles in one language, adding more languages could be a little easier because of the pre-existing time-codes. We, though, had to perform both translation and time-coding in one shot, because the translation given by the dialogue team could not fit the considerations that go into writing subtitles.

What we take for granted, in reality, has a lot going on behind the scenes. For instance, when playing the initial phase of the film with the subtitles, I realised that the line length we use for normal text in books and the web (about 65 characters) is a little too much for subtitles. We do not want the viewer's eyes going from one end of the screen to another; the main focus of the viewer must be at the centre of the screen---this is where most action happens. We also do not want to have more than one or two lines at the bottom of the screen for: one, we do not want to overwhelm the viewer with a lot of text; two, we do not want to cover too much of the bottom of the screen and three, the viewer should spend as less time as possible on the subtitles.

Writing subtitles for a video with my grandmother's journey during the Second World War was easy, the video being a monologue, and because she spoke with the right amount of pauses between phrases. A film is different; more so with a film that uses a rather formal---at times archaic---language, where long sentences are a norm.

That said, this was an experience, and I got to learn something new. I think that is how life should be, no matter how small your learning is. One must not stagnate.

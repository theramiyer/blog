---
title: "Democracies fail"
date: "2020-09-23"
description: "You fail our democracy. Whether you like it or not, you are the reason for its failure. And this is not your regular high-school civics lecture."
tags:
- democracy
- India
---

I am not in a Utopian world. If you are reading this post, you live in my world as well. Let us talk, citizen to citizen, you and I. And by that I do not mean Indian citizen to Indian citizen. You and I are in a world where democracies are failing. And we have no one other than ourselves to blame for it.

I am an Indian, and I boast about belonging to the largest democracy in the world. But then, I have had this question for years now: Are we in a democratic world? Or is democracy still a pipe dream?

We have heard our share of derogatory forms of democracy including "demo-crazy". Politicians have used such references to make their speeches and narratives catchy. But you either have heard a family member or a friend use these terms, or you have used them yourselves. Why? Because "catchy" by definition is something that draws attention. These terms that catch on, catch on because people can connect to them. In other words, such terms become popular. Our brains directly link such terms to popularity, and the terms come in general use (or, "catch on").

Let us keep politics and political agreements aside for a moment, take a step back and look at what democracy is. I will not insult your intelligence by defining it as "people's rule".

## A matter of agreement

> India, that is Bharat, shall be a Union of States.  
> ---Article 1 (1), Constitution of India[^b6831516]

[^b6831516]: The Constitution of India ([National Portal of India](https://www.india.gov.in/sites/upload_files/npi/files/coi_part_full.pdf))

No matter what the popular understanding of what makes today's India, the truth is that India is a _union of states_. No matter that we called the subcontinent as _Bharatvarsh_, way back in the past, when most of the subcontinent was one empire or another, and the little kingdoms within them were the vassals of the empire. Today's India is not the India of 3000 years ago. I think in today's modern world, we must manage to protect what the current piece of land a country is and move on from the obsession to conquer more and more.

The truth is, the more we go about conquest, the more conflicts will arise, and that in turn will lead to stunted economic growth, because every battle costs millions of dollars (or tens of millions of rupees). Every war we fight slows our development by years.

Having made the point against further conquests, let us come back to protecting what we have today. And this argument does not involve international borders or lines of control.

## Understanding a union

The hierarchy that we have established in our minds is like an empire, wherein the empire speaks and the vassals listen. In that sense, the states are "under" the rule of the Centre. But in reality, a union of states is different. And to understand how it all worked out, let us look at a gist of the history of the formation of _Union of India_.

## A brief history

I will be brief. The Mughals ruled most of the subcontinent in 1700, when the French, the Portugese, the Dutch, the Danish and the English had little trading settlements. By the end of the century, the Marathas had taken over most of India, while the British had control over Bihar, Bengal and some coastal areas almost from modern-day Kakinada to modern-day Brahmapur.

By 1851, we had the Bengal Presidency, Bombay Presidency, Madras Presidency and the North-Western Provinces. In another fifty years, the subcontinent comprised of eight major provinces and five minor provinces ruled by the British, and the princely states.

Right before independence of India, Sardar Vallabbhai Patel worked on unification of the provinces and the hundreds of princely states into a single Union of India.

In other words, India is not "divided into its states and union territories"; instead, _the states and union territories unite to form India_.

## The Book says

The Seventh Schedule of the Constitution of India has three lists:

1. Union List: Subjects where the Parliament may make laws.
2. State List: Subjects where the state Legislative Assemblies may make laws.
3. Concurrent List: Subjects where both, the Centre and the states, may make laws. Here, the laws by the Centre take precedence over the states.

Over time, the subjects have changed places. The Union List has seen little changes, but subjects have gotten transferred from the State List to the Concurrent List. Of course, there have been transfer of residuary powers to the Concurrent List.

## Drawing inferences

During partition and the drafting of the Constitution, the Founding Fathers felt that the unity and integrity of India was important. India, unlike Pakistan or any other neighbour of ours for that matter, is vastly diverse in language, culture, natural resources, climatic conditions, you name it. The framers of our constitution understood this. The criteria chosen to form states was language, and that is debatable any day. But the basic idea was that these regions share some cultural uniformity, which included the professions practised, in turn based on factors such as natural resources and native traditions.

This meant that at the grass roots level, some of the subjects would be better handled by the states, rather than the Centre. Had politics not interfered, the states may have had more powers than they do today. Not to mention the 42nd Amendment to the Constitution, which transferred subjects the other way around: from the State List to the Concurrent List.

Over time, the governments at the Centre have worked towards transferring more and more powers to the Concurrent List. This indicates an obsession for homogeneity and centralisation, and in the literal sense, is disrespectful of the federal nature of our system. While that being good or bad is another debate, the attitude and the actions that followed, such as considering contributions to PM CARES or PMNRF as valid CSR, while not considering contributions made to the state Chief Minister's Relief Funds as valid CSR, has created resentment. While centralisation of tax through GST was a good move for taxpayers, it has resulted in the states not getting their share of the taxes on time, to fund their activities.

{{< youtube lFreJdJRs_Y >}}

Co-operative federalism is the key. Those that have the ultimate power must also listen to those that know the pulse of the people. How else will we run a democracy that is so large, so populous and so varied as India?

## Opposition to balance

Co-operative federalism is one part of the story. What has happened today (or over the last half a decade) is that the Opposition has vanished from the scene. With the powers concentrated under one body, the part that keeps check on the activities of the ruling party is the Opposition. The role of Opposition is to tip the balance so that those in power remember that they work for the people. In return, the Opposition get a chance to showcase their work during the next elections.

But what has happened?

1. Opposition opposes every move of the incumbent, irrespective of merit. And people get polarised.
2. The Opposition use outdated tactics that effectively defeat the purpose of the Opposition.

Somebody (Shashi Tharoor? Ramchandra Guha?) said, the reason that the protests and marches against the British rule worked was because we could shame the British. The same tactics may not work today. What happened with the agriculture bills at the Rajya Sabha[^f9e5ede3] is a classic example of this. The Opposition "boycotted" the Rajya Sabha, which became favourable for the ruling party. The House passed seven bills in 3:30 hours; a whopping 15 bills in 2 days.

[^f9e5ede3]: Amid Opposition boycott, Rajya Sabha passes seven bills ([The Hindu](https://www.thehindu.com/news/national/amid-opposition-boycott-rs-passes-seven-bills/article32670933.ece))

From anti-CAA protests to any issue of national importance that you can think of, the Opposition has limited itself to Twitter. Or is all this a little too convenient? "The Opposition does its bit to make this the most efficient government of all time."

## Elected autocracy and democracy

The general idea that we have in our minds today is that we exercise our democratic right once every five years to elect the government into power. Our responsibility ends there. Then, trust the government to do the right thing. We should not be questioning them. The oft-used line is, 'The government has people's mandate. Do you think the majority of our country are idiots?' And then, someone points out that of 91.1 crore people who were eligible to vote, the turn-our being about 67%, the total votes polled was about 60 crore, which meant that the ruling party received between 22.5 and 23 crore votes[^f7595f8c]. The point, though, gets lost in this clarification.

[^f7595f8c]: Regardless, this does not imply that 23 crore people are idiots.

The more important part is whether you can criticise the government, or question the government.

In an elected autocracy, no; in a democracy, yes. And Prime Minister Modi agrees, adding, ‘This is my conviction; not mere words.’

{{< youtube QFJS6onFQzw 55 108 >}}

An elected autocratic government allows you to elect the government, but does not allow any citizen participation. Criticism is a simple form of active participation in a democracy. Not even so much as criticising a wrong move or discussion about the move is shirking your duty as a citizen. Disagreement, dissent, criticism are all part of democracy[^5c185928]. In a democracy, nobody is above people---not even the government. If someone tells you otherwise, they do not understand democracy.

[^5c185928]: The Right to Dissent is the Most Important Right Granted by the Constitution: Justice Gupta ([The Wire](https://thewire.in/law/right-to-dissent-constitution-justice-deepak-gupta))

> The blanket labelling of dissent as anti-national or anti-democratic strikes at the heart of our commitment to protect constitutional values and the promotion of deliberative democracy.  
> ---Justice D.Y. Chandrachud (Judge, Supreme Court of India)

## Summing up

No matter what anyone says, democracy, in reality, is fragile. Its fruits are sweet, and for that reason, the farmer must guard it day and night. Even a slight slip might prove costly. Safeguarding the values of the democracy is the duty of every one of us. Provided we all contribute our little bit to it keeping in mind what we stand for as a people, we will thrive. A slight misjudgement, and we take a dive. A handful of such dives, and the system may have toppled by the time we gain consciousness.

Be awake.

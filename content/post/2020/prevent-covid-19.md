---
date: "2020-06-24T00:00:00Z"
subtitle: Using the simple, tried-and-tested protocol
tags:
- covid-19
title: Prevent COVID-19
---

Unlock 1.0 sounded like a great move. No doubt, a necessary move. The lockdown had made our economy plunge, stranded the poor and the needy across the country, and the new normal started to prove detrimental to us as a system. Of course, some of our cities handled the situation better than the others. Bangalore was one of them. Starting from the healthcare workers to the regular citizens, everybody who could, contributed to alleviating each other's troubles in their own ways.

But the truth still is that the curve did not flatten in its real sense; of course, the exponential growth slowed compared to what it would have been had there been no lockdown. I would still call it a partial win. But as was evident, we could not sustain that way. We decided to unlock, and lock those specific clusters that could be responsible for the spread. Instead of locking the potential targets, we lock the sources. Again, no doubt, a better approach.

But here is the problem: COVID-19 is often silent.

{{< toc >}}

## Understanding the pathogen

A coronavirus causes COVID-19. And a coronavirus is a virus. Stating the obvious, am I? Humour me for a minute.

A virus is not a living being, but a bacteria is. A bacteria is much bigger than a virus. An antibiotic can kill bacteria; it cannot kill a virus. But why are we into "differentiate between a bacteria and virus"? Because there are people out there, claiming that a bacteria caused death by COVID-19[^800b055a]; that the doctors could have prevented the deaths by prescribing antibiotics.

### Transmission

A virus is a non-living thing. A virus, in a sense, is a complex chemical. It attaches to a cell and corrupts it. It naturally follows that a virus cannot move on its own---it has no limbs, no wings. This is where carriers come into play. The technical name for these carriers is 'vectors'. In case of dengue or malaria, the vector is a mosquito. The virus infects a human, a mosquito bites an infected human. The human's blood has the virus, which the mosquito carries to another human, and ends up transferring it to him when biting. The second human is now infected.

In case of dengue, the mosquito is the carrier, and the human is the victim. In case of COVID-19, both, the carrier and the victim are human.

### Viability

We say a virus is viable when the virus is capable of infecting, while unattached to a host. Viruses cannot "live". But the material that can infect organisms remains active for a period of time. After this time, the virus disintegrates, and can no more cause any harm. Think of this as its shelf life.

This period is different for different viruses, in different environments. The Smallpox virus can remain viable for years, while the SARS-CoV-2 breaks down on a copper surface in hours, while it can stay afloat in the air for up to three hours[^4dd1918c] and continue to be active on plastic surfaces for up to three days.

### Medium

The virus affects the human respiratory tract. While different researches have found the virus even in human waste, humans primarily pass the coronavirus infection through fluids in contact with the respiratory tract---saliva and phlegm. The virus also comes out when we breathe, sigh, sneeze, cough, laugh, talk, etc.

The easiest scenario to imagine would be an infected person sneezing while cupping his face in his hands, shaking hands with you, and then you rubbing your nose with it.

'But I'm careful about all this!'

Are you? Did you know that you touch your face about 370 times in a day[^4fd9043b]?

[^4fd9043b]: [Face Touching: A Frequent Habit That Has Implications for Hand Hygiene (National Library of Medicine)](https://pubmed.ncbi.nlm.nih.gov/25637115/)

### Dosage

The chances of getting COVID-19 appears dosage-dependent[^d524e45e]. This means that your chances of becoming sick with the infection depends on how much virus has entered your body. If you get a low dose, your chances of infection are low. But the point of a needle could have a billion viruses---they are that small.

### Incubation

Attention.

Incubation period is the time between the point you contract the virus and the time the symptoms start to appear. This period in case of a SARS-CoV-2 infection is high. You do not know you have the disease until symptoms start to appear. This is why they say, the disease is _silent_.

But all that while, you could be expelling the virus while you talk, while you breathe, and while you laugh[^5f6519aa].

### Immunity

Those who recover from the SARS-CoV-2 infection, may show short-term immunity to re-infections. This is because, according to a study, one of the types of antibodies that fight the infection remained in the system for a short period. The levels of these antibodies dropped in two to three months[^f5814369]. The levels of another antibody, one which attacks the spike protein in the virus did not reduce, which could point at long-term immunity.

But again, this does not mean much at the moment; this is a starting point. We have not had the virus for long enough to know about life-long immunity. The working of the human immunity is too complex to make such blanket claims based on the little evidence we have at the moment.

## What the numbers say

Statistics work a certain way. They tell you the possible outcome based on patterns that we observe during studies. How we infer the statistics changes with new findings. Also, one must remember that the sample matters in statistics. For instance, if 9 out of 100 individuals tested, test positive in a certain locality in a city, it _does not mean_ that the test positivity rate for the city is 9%.

Keeping this in mind, data shows that 4.67% patients become critically ill[^93e7b169]. Critical illness in this case is when you need critical medical attention such as oxygen supply, a ventilator, or intensive care to survive.

## Effect on human beings

Before we go any further, we must understand that the virus is new, and our knowledge about the pathogen is still evolving. As we learn more, our understanding will become better. What is clear is that the virus is potentially fatal.

The virus enters the body through either the nose or the mouth as the person breathes it in. The virus attaches itself to the lining of the respiratory tract and begins to multiply. They hijack new cells and make you more and more sick. This is the initial phase of infection. If the immune system breaks down the virus during this time, the damage is minimal. If not, the virus invades the lungs. When your body sees that there are foreign bodies in the lungs, the body responds to it by sending chemicals and white blood cells to the lungs. The battle leaves behind a lot of pus. What space otherwise air fills, is now full of pus. This makes the absorption of oxygen difficult.

While the virus primarily affects the lungs, it does not stop with that. There have been cases with renal failures, cardiac complications, strokes and other organ failures. It all varies based on how each body responds to the invasion, what other ailments each person has and so on.

## Claims and responses

Of course, we are a billion people; we make at least a thousand claims. Answering a thousand queries are beyond my capacity, but let me address a small number of the popular ones.

### But I am 25, I am not at risk

People of all ages have contracted COVID-19. Stop listening those who tell you otherwise.

The human immune system is immature in children, and worn out in the aged. Statistical observation is that the mortality rate in those under the age of 40 is less than 0.5%[^59da5d72]. But note that this talks nothing about the rate of infection---it talks about mortality. Which means, if you are under 40, your likelihood of dying of the disease is less than 0.5%. But you are not naturally immune to the disease; you become immune after you contract it and defeat it.

You still have to be careful because, while you may come out of the disease unscathed, you may give the disease to that unsuspecting old woman who lives next door. And she may develop complications because of her existing autoimmune condition, for which she is under medication that suppresses her immunity. Or to your adorable two-year-old nephew, whom you hugged.

Remember, you can give others the disease, when you are _not even showing symptoms_.

### But India is warm, the virus dies at this temperature

As disheartening as this is, there is no evidence good enough to conclude that the tropical weather can kill the spread[^cd50f783]. This claim goes two ways:

1. Humid weather makes the air heavier, making it hard for the virus trapped in the expelled droplets from spreading far.
2. The high temperature disintegrating the virus faster.

Both are possibilities in theory. But the months that passed have showed no effect on the coronavirus spread. The number of cases in India continue to rise despite the vibrant and changing weather. And India is no exception---this is the case across the world.

Misinformed, blanket statements, based on anecdotal evidence are best avoided.

### Taking vitamin C to prevent the disease

According to a study, vitamin C helps regulate your immune system when given along with corticosteroids, when treating overwhelming infections[^f0d1527c]. This has no relation to the specific case of COVID-19. As of June 27, 2020, no evidence exists to suggest that vitamin C helps in treatment or prevention of COVID-19[^1fa7f00b].

Also remember that taking too much vitamin C can lead to kidney stones.

### Taking vitamin D to help cure the disease

Vitamin D does not help cure the disease, but it prevents worsening of the situation in two ways[^1fa7f00b]:

1. Makes your immune system healthy.
2. Prevents the immune system from overreacting and jamming the lungs with fluid---this fluid build-up is a major cause for death in case of COVID-19.

You do not need supplements; standing in the morning sun for about fifteen minutes should suffice.

### But they say the sun kills the virus

The ultraviolet radiation present in the sunlight does disintegrate the virus. But factors such as the intensity of ultraviolet radiation, the duration of exposure and other aspects play a role in getting the desired result.

The human body is a different environment. And an inactive, suspended virus is different from a virus present in a live cell. Moreover, ultraviolet does not penetrate into your body. Also, exposing yourself to too much ultraviolet radiation is dangerous.

### What about the hydroxy-something salad

Hydroxychloroquine and azithromycin?

Under clinical conditions. Neither of these is going to prevent the disease, and you should not self-medicate. The claims about these are either speculative, or the experts are questioning the methods of the studies. In either case, we have no evidence to support any of these claims[^1fa7f00b]. Your doctor knows what is best for you.

For more information about these, refer to the [Coronavirus Resource Center](https://www.health.harvard.edu/diseases-and-conditions/coronavirus-resource-center) of the Harvard Medical School, and the [Mythbuster](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public/myth-busters) series by the WHO.

## How to deal with the situation

During the months of April and May, I was part of the _Corona Warriors_ (or more appropriately, _Corona Sainikas_) programme here in Bengaluru. I was a field volunteer, which meant that I would spend most of my volunteering time out with the people---my main role was in helping the hungry, and assisting the migrant workers with their travel. While in the field, we followed a set of rules.

They will seem difficult to follow at first. But in a matter of days, these will become normal.

### Cover your face wherever you go

Masks are compulsory. I know it feels uncomfortable to breathe. But this is merely a phase; you do get used to it. These days I don't even feel its presence. The points of entry into your body, for the virus are in your face---your eyes, nose and mouth.

Can you cover your face with a shield instead? Not without a mask; wearing the face shield without the mask leaves too much open. Although, a mask _and_ a shield become an overkill.

Can a normal mask made from a t-shirt protect you?

The way masks work is by preventing the droplets from directly landing on your face. They create a barrier. Make a mask with two or three layers of fabric for better protection. A mask made from an old t-shirt is cheap, reusable, and environment-friendly. Even though the empty space in the fabric is bigger than the virus itself, the idea is to trap the droplets that are carrying the virus.

This means that the outer side of the mask may have millions of viruses, which is why we say, _never touch the exposed part of your mask_.

Here is a gist:

1. Wear a mask wherever you go.
2. An N95 mask is unnecessary to go to your local store.
3. Handle the mask using its fasteners.
4. Cover both, your nose and your mouth; covering your mouth while leaving your nose open is useless.
5. Wear glasses if you can. They will prevent droplets from falling into your eyes.
6. Remove your mask when exercising. Remember, though, that the virus can remain suspended for hours.

### Domestic help

Some may see this as an unpopular idea, but by calling your domestic help home, you are putting them and yourself at risk and making them the vector. Even if one of their employers has the infection, all their employers and their family members will get the infection.

At the same time, understand that this is probably their sole means of livelihood. Don't stop paying them. We have asked our domestic help not to come for work, but haven't stopped their pay.

Yes, managing work and household chores is hard. But managing a COVID-19 infection is harder.

### Meeting people

Avoid meeting people as much as you can. If at all the meeting is unavoidable, meet somewhere out in the open. Meeting in the open reduces the risk of contracting the infection by diluting the virus suspended in the air.

### Going out shopping

Wherever you go, go under the assumption that you will encounter an infected person. That infected person could be anybody: the auto-rickshaw man, the shopkeeper, the security guard, anybody. Assume that the person standing in front of you in the queue has the virus.

1. Keep a physical distance of about two metres.
2. Do not touch anything you do not intend to buy.
3. Remember that stores are not open spaces. Viruses hang in the air for longer there.
4. Avoid touching your face.
5. If you are in the risk category, get groceries delivered.

### Handling bought groceries

Always assume that infected people have handled the groceries that you bought. The virus can remain active for up to three days. We keep the packaged products aside for three days before using them.

The likelihood of you catching the infection through cartons is low. I haven't come across any case that spread through cartons. That does not rule out the possibility, though.

What do you do in case of milk and other packaged perishables? We wipe them with a mild disinfectant solution. We wipe the surfaces where we had kept them. And wash our hands with soap and water afterwards.

In case of fruits and vegetables, we wash them thoroughly with water. Cooking the vegetable sterilises it any way.

### In apartment complexes

Those of us who live in apartment complexes need to take some more precautions to ensure that we do not catch the infection.

1. Avoid using handrails or touching the common walls.
2. Frequently clean the commonly touched surfaces such as the elevator buttons.
3. Screen outsiders who come in. This may be a challenge in most complexes. As an alternative, limit visitors.
4. Make wearing masks compulsory in the common areas---even for residents.
5. Establish a protocol for professional services, such as plumbers and electricians.

### When outside and after returning

Covering your face is partial protection. When outside, there are more variables than we can keep track of. Here are some of what we did when out and about:

1. Cover yourself fully. Try to wear full sleeves and trousers. Wear shoes.
2. Keep a distance of two metres from those around you.
3. If someone comes closer than that, remind them about the disease; don't be rude about it, though. Tell them that they could get it from you as much as you from them.
4. Do not approach anyone who is not wearing a mask.
5. Be mindful of what you touch.
6. Carry a sanitizer along.
7. Before you reach home, have someone place fresh linen and clothes in the bathroom, in case you did not place them before leaving.
8. After you come back home
   1. Do not touch anything, go straight to the bathroom.
   2. Add a lid-full of antiseptic to a bucket-full of water.
   3. Soak your clothes in this solution for thirty minutes.
   4. Take a thorough shower from head to toe in the meanwhile. Wash your hair.
   5. Wipe all the surfaces you touched before you showered, with disinfectant.

Following these steps will reduce the chances of your contracting the infection. You are still not fully safe from the virus.

This is the new normal. We adapt.

[^800b055a]: Fake news: [How Italian doctors disobeyed WHO and discovered the secrets of coronavirus](https://web.archive.org/web/20200523172503/https://medium.com/@efogator2019/how-italian-doctors-disobeyed-who-and-discovered-the-secrets-of-coronavirus-9eeb7e0b4310) (archived before page shutdown)

[^4dd1918c]: [Aerosol and Surface Stability of SARS-CoV-2 as Compared with SARS-CoV-1 (New England Journal of Medicine, March 17, 2020)](https://www.nejm.org/doi/full/10.1056/nejmc2004973)

[^5f6519aa]: [COVID-19 basics (Harvard Medical School)](https://www.health.harvard.edu/diseases-and-conditions/covid-19-basics)

[^93e7b169]: [Number of critical Covid-19 patients has halved across the country, shows data (Hindustan Times, as of May 11, 2020)](https://www.hindustantimes.com/india-news/number-of-critical-covid-19-patients-has-halved-across-the-country-shows-data/story-kucHQKQkI1TBc0sZO0SleI.html)

[^59da5d72]: [Age, gender and pre-existing medical conditions mortality rate (corona.help, as of 27 June, 2020)](https://corona.help/other-stats)

[^f5814369]: [Antibodies in Covid recovered patients last only 2-3 months, finds Nature study (ThePrint, June 19, 2020)](https://theprint.in/health/antibodies-in-covid-recovered-patients-last-only-2-3-months-finds-nature-study/444872/)

[^cd50f783]: [Will warming spring temperatures slow the coronavirus outbreak? (National Geographic, March 6, 2020)](https://www.nationalgeographic.com/science/2020/02/what-happens-to-coronavirus-covid-19-in-warmer-spring-temperatures/)

[^f0d1527c]: [Hydrocortisone, Vitamin C, and Thiamine for the Treatment of Severe Sepsis and Septic Shock](https://journal.chestnet.org/article/S0012-3692(16)62564-3/fulltext)

[^1fa7f00b]: [Treatments for COVID-19 (Harvard Medical School)](https://www.health.harvard.edu/diseases-and-conditions/treatments-for-covid-19)

[^d524e45e]: [It’s Not Whether You Were Exposed to the Virus. It’s How Much. (The New York Times, May 29, 2020)](https://www.nytimes.com/2020/05/29/health/coronavirus-transmission-dose.html)

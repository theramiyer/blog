---
date: "2020-03-22T00:00:00Z"
episode:
    apple: 1000469362210
    spotify: 0aYI8lAPgz8bPskD7p2iD5
    google: aHR0cHM6Ly9hbmNob3IuZm0vcmFtaXllci9lcGlzb2Rlcy9KYW5hdGEtQ3VyZmV3LWFuZC1zb21lLXN0YXRpc3RpY3MtZWJzZDZq
tags:
- society
- crisis
- health
- government
- politics
- humanity
title: Janata Curfew
---

Some of us defeated the whole purpose of the Janata Curfew today. At five this evening, we hear the sounds of clapping and the shankh-dhvani and people clanging plates and all of that. We look out of one of our windows and see about ten people fist-bumping, high-fiving and shaking hands. The last part was disappointing.

{{< podcast >}}

The point of the Janata Curfew was not showing your political allegiance to someone or sending forwards, and then coming out on the streets at five to show support. Self quarantine was the point of the exercise. But what happened instead was dieting for an entire month, and at the end of it, binge on biryani for three days straight.

Why are people talking so much about social distancing when we have 400 cases in a country of 1.3 billion?

## Rate of infection

Because this is a classic case of an exponential spread of infection.

In the initial days of this outbreak, the WHO came out and said that every infected person infects about 2.6 people on average. On day one, we have one person infected. The WHO said that one generation of transmission was 5--6 days. At the end of the sixth day, this person would have spread it to 2.6 people. Let us go with 2 to make the calculation simpler. At the end of, say, the stage one, two new people have the infection. At the end of the second stage, the number of new patients is 4, third, 8; fourth; 16, 32, 64... At the end of the tenth stage, the number is 1024. At stage 10, in six days, we would have 1024 _new_ cases of the infection.

From then on, we are looking at somewhat uncontrollable transmission. At stage 13, we are looking at 8192 cases; over 1300 cases in a day.

You might say, 'Well, it does not matter; the virus cannot kill me.' And if you are 40 years of age or younger, you are right; the mortality rate is nearly that of your common cold. But after that age the mortality rate goes to 0.4% for people between 40 and 50, 1.3% for people between 50 and 60, 3.6% for people between 60 and 70, 8% for people between 70 and 80 and 16% for people above 80. Yes, curiously, also exponential.

## Incubation period

Suppose this person X caught the virus on the first of March, he would not see any symptoms of it until the 7th or the 10th of March. He has the infection, but he may not have the fever or the cough, until the 10th day. But he could spread the virus. The reason the government---or in fact, governments---across the world are asking for a quarantine is so that those infected contain the virus within themselves and not give it to others. The body of the infected will deal with the virus.

If you have good immunity, your body will be successful in making the antibodies that kill the virus before the virus does any noticeable damage to your body.

## Some statistics

Here are some statistics to show the exponential growth in the number of infections. (Visit [https://corona.help](https://corona.help) for more, and [buy Alex a coffee](https://ko-fi.com/alexdumitru) for his efforts, if you can.)

![India statistics: Coronavirus infections as of 22 Mar 2019 (https://corona.help)](https://blogfiles.ramiyer.me/2020/images/2020-03-23-covid-india-stats.png)

What you see above are India statistics. Even with the smaller sample size (compared to the global statistics), we see an exponential growth. The infection history as well as the daily change, are both exponential trends. At the same time, if you look at the recoveries---the global recoveries---the curve is almost linear.

## Incubation period

Incubation period is the time between someone getting infected, and the time the symptoms start to show. For the regular cold, the incubation period is a couple of days. In case of this strain of the coronavirus, the incubation period is 2--14 days. Some are even _asymptomatic_, meaning, they don't show any signs of the infection.

During this time, while you may be carrying the virus, you will not know that you have the virus. You might go on with your lives, but you _could_ be spreading the virus around even when you are not showing symptoms[^addbbed9].

[^addbbed9]: [COVID-19: How it spreads (Centers for Disease Control and Prevention)](https://www.cdc.gov/coronavirus/2019-ncov/prepare/transmission.html)

## Community transmission

Let us assume that you are at a bus stop along with twenty others. One of those standing about six feet from you, has the infection. He coughs, and splashes droplets on four others standing right next to him. One of them is a teller at a bank, another is a cook at a cafeteria of an IT firm, whose building houses about seven thousand people; another is a TTE and the fourth, a priest at a temple. All these four interact with hundreds of people in a day. Imagine how the virus could now spread.

This is a dangerous phase. We want to cut down on the transmission before we reach a point where the situation becomes uncontrollable. The governments have suspended public transport services, imposed Section 144 in over 70 districts, the companies are allowing their employees to work from home, because we want to:

## Break the chain

Break the chain. Stop the spread. Because, whether we like to admit it or not---and our government will admit it as well---we do not have the medical facilities to match China or Italy. We are not medically equipped to handle the outbreak at the scale of our population. We are better off preventing it.

Here is how all life forms work. The life form takes birth, and multiplies. To multiply, it needs a friendly environment. In case of a virus, this friendly environment is another life form: a host. The virus needs a host to survive; it cannot survive for long on lifeless surfaces.

When the virus finds a host, it starts to multiply at the cost of the cells in the host. This process ends up killing the host after some time because the host cannot survive without its organs. When the host dies, the virus also dies because like we saw, it cannot survive on lifeless surfaces for long.

The host, although, has systems to protect itself. In our case, we have the human immunity. Our immunity creates antibodies that stop the virus from multiplying. The virus has to die either way---the question is who dies before: the virus or the host. The antibodies created against the virus will live on in our body for a period of time. Within this time, the virus cannot infect us again. Now, the virus needs new, healthy hosts. When the virus does not find healthy hosts, the pandemic dies.

Social distancing comes in the way of the virus finding new hosts, which is the first half of the story. The second half is handling the hosts whom the virus has infected. For that, we need resources.

## Distribution of resources

We have limited medical facilities. The budget we have and the amount of medical facilities required, decide this limit. No country expects millions of people falling critically ill at the same time.

When people fall ill with the coronavirus, about 15% of the sick need critical medical care. The hospitals admit these people and treat them. At one point or another, the patient stops using the medical resources making it available for new patients.

If the spread is exponential, we will run out of medical resources in no time, and will fall short of them for new patients. This will lead to a drastic increase in the death rate.

To avoid that situation, we should ensure that the number of people that get infected is less than the amount of available medical resources. This way, more people can survive. Because the incubation period is high and the infected can spread the infection even when they are not showing symptoms, we are in more trouble because the number of those who need critical care shoots straight up and takes us by surprise, making us helpless. We want to avoid this. We want our hospitals staffed. At the same time, we do not want the healthcare professionals to overwork and burn themselves out. Achieving this is in our hands.

## Politics

Let us keep politics aside on this one. When the Prime Minister asked us to self-quarantine ourselves, while we may not agree with everything that he said, we must acknowledge that social distancing is critical, and he has got that right. Please look at his speech as that from the executive head of our country, and not a politician. Of course, this does not mean you do not criticise the government for its blunders.

Let us keep our _political differences_ aside on this one, and work on this not as Indians, but as human beings. People are right in saying that this is not about the borders or races. This is about we the humans. We must stand united as humans in this---let us fight this disease as a species.

## People with great immunity

Then there are those who say the virus cannot harm them. It may be true, but what if you interact with a seventy-year-old who has chronic breathing disorders? What if you interact with someone who has immunity issues? The virus would eat away at his lungs and he would suffocate to death.

Let that sink in for a second.

You will have blood on your hands. Will you be able to live with yourself for the rest of your life if you knew someone died because of your careless attitude?

## Do not panic

But let us not panic. That brings no good. All we need at this moment is honesty. If you feel ill, or you know you have been in physical touch with someone who is ill, stay away from others. Contain the virus in you and let your body fight it out. Seek medical help if you are unable to handle it. But please do not attend parties, or go to office or travel with others. Quarantine yourself and stay away from others for four to eight weeks. You will save lives.

Let us stop thinking that we may get the virus and start acting as though _we already have the virus_.

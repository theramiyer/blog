---
date: "2020-03-29T00:00:00Z"
episode:
    spotify: "4CBrC7ML7vu8f0ecdBStmS"
    apple: "1000469878289"
    google: "YTMzZTZmNzQtYmY5MS00ZDJjLWFkZDUtZDdkODViNjQxMWM1"
tags:
- science
- chemistry
- biology
- coronavirus
- education
title: Can we kill a virus
---

In the last blog post, I had made a statement about how a living organism works, specifically, propagation. I had equated that with that of the behaviour of a virus. Later I realised that it could lead to some confusion, with some thinking that a virus is a living organism.

A virus is not a living organism per se.

{{< podcast >}}

Per se, because a virus does show similarities with living organisms such as possibly contributing to the creation of eukaryotes, or even containing genetic information. But that would become a long, technical discussion, which I do not wish to get into at this point because one, such a discussion would be long, and two, I do not have technical knowledge about the nuances of the concept. The last biology class I attended was fifteen years ago. And biology was not something I connected with.

But what I can do is speak in simple terms, like when asked how an aeroplane flies, I could tell you, 'The air pressure under the plane becomes higher than that over the plane, which lifts the plane', rather than getting into fluid mechanics.

In those terms:

A virus is a chemical complex, the border between chemicals and a single-celled living organism. The debate about whether viruses are living or non-living is still on, but we all agree that they do not show the characteristics of a living being such as metabolism and reproduction.

Wait, what?

You read it right; a virus cannot reproduce by itself. Viruses cannot metabolise either (metabolism is the process of performing chemical reactions in the body to generate energy). I think of viruses as those beings that are non-living when not attached to a host, and a living being when it gets a host. Much like a seed, except that most seeds grow in a non-living habitat such as soil. Let me go one step further and say that this is true as of the 29th of March, 2020.

Experts consider viruses as having a "borrowed life". They cannot "live" on their own. They attach themselves to host cells, break their outer shell---made of complex proteins or lipid (fat) or both---and release their DNA or the RNA on to the host cell. This way, they take control of the host cell and use the host cell's reproductive machinery to multiply. Except in this case, the cells are rogue cells, which do not work as designed for their purpose.

This degenerates the organ that these cells make up and the organ fails. Of course, that is an oversimplified statement.

Our body protects us using its immunity. The immune system is capable of detecting the presence of such infections and act accordingly. But the viruses are smarter sometimes. They can go on doing what they do under the radar, because they have anti-detection systems in place within themselves. The SARS-CoV-2 is one such case---it goes undetected for up to two weeks, which is what defines its incubation period.

When our immunity detects the virus, it tries to interfere with the virus's attachment to the cell. Our immunity also has to stop the infected cells from multiplying. It increases the body temperature, or generates phlegm or floats chemicals, all in an attempt to defeat the virus's methods to take control of our cells. Once successful, the immune system stores this information for future use---again, figuratively. The next time there is such an attack, the body detects it sooner and prevents attachment, and also disintegrates the virus---breaks the complex structure.

'Killing a virus' is not technically correct, because a virus is not alive in the first place. But we find it easier to express it this way. Of course, defining what life is, is for another nuanced discussion.

Now, to some advice, opinion, or whatever you would like to call it:

- Drink a lot of water. The summer is beginning, and our bodies are going to need more water than, say, three months ago. Also, our immunity needs enough water to function. If you have underlying health issues, check with your doctor on how much water you can drink.
- Most of us working from home do not have to groom ourselves the way we would otherwise. I have not had my haircut for some time now. Let us collect that money and either support our local barber, or donate it to a larger fund like the PMRF, irrespective of what the amount is; little drops make an ocean. If every Indian citizen could donate one rupee, towards the fund, the fund would have 1.3 billion rupees.

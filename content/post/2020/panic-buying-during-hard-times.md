---
date: "2020-03-18T00:00:00Z"
episode:
  apple: 1000469054499
  spotify: 3qBPGtJZMriY07DLt8tiiy
  google: aHR0cHM6Ly9hbmNob3IuZm0vcmFtaXllci9lcGlzb2Rlcy9UaGUtY29yb25hdmlydXMtcGFuaWMtcm91bmR1cC1lYm5odjc
tags:
- economics
- crisis
- society
- psychology
title: Panic Buying during hard times
---

The way the year 2020 started bordered on insane. For those in India, this effect was rather pronounced, starting from Kerala passing a resolution against the Citizenship Amendment Act 2019, followed by upping of the protests across the country---Delhi's Shaheen Bagh being the flag ship, to the incidents of dirty politics during the Delhi Elections campaigning, to the novel coronavirus going on claiming thousands of lives.

We are in March already, and more and more people are suffering from COVID-19. Thousands are dead. As the death toll increased, the governments started taking measures to slow down the spread---hopefully to a halt. As the events unfolded, and the governments made announcements, people started panicking and the last time I went to a nearby supermarket, I saw people standing in queues at the counters with loaded carts, which is unusual for the middle of the month. Shelves were empty including those of vegetables with longer shelf lives.

{{< toc >}}

if you would rather listen to this post:

{{< podcast >}}

Panic-buying is when people foresee a shortage of essentials, and buy more than necessary so they do not run out of them.

## How bad is COVID-19

Now, to be fair, going by the number of lives the disease has claimed, the situation is bad. As of writing this piece, the infection has hit about 2.5 lac people, and about 10,000 have died (also, over 88,000 have recovered).

India has about 200 confirmed cases of infection, four have died (one each in Karnataka, Maharashtra, Delhi and Punjab). But also, 20 have recovered. When we stepped out the weekend of 7th and 8th March, we saw a drastic decrease in the number of people out and about, which changed the last weekend, because people were out buying.

## Situation in major cities

Supermarkets had half-lowered their shutters. On asking why, they said that the police had asked them not to open their shops for about a week. This explained why the likes of _BigBasket_ and _Supr Daily_ were talking about delays in delivery.

Everywhere you look, sanitisers and masks are out of stock. Our regular flower vendor told us that greens will not be available for some days, because there is nothing in the main market. While the concerned organisations are keeping an eye on the prices, we see a shortage in supply. Right?

Items such as soaps, coffee, bottled beverages, toiletries, instant foods, etc. come under fast-moving consumer goods, or FMCG. The companies that make these goods have said that _they have enough stock_, except that they are now working harder to get them delivered to the distributors and stores because of the sudden increase in demand.

The other aspect of this situation is the increase in in-home consumption[^25d84257]. Those of us that can work from home are working from home. Had we been at work, we would have eaten at the cafeteria, or walked to the nearby restaurant, or had a tea at the joint at the corner of the street or the _chai-sutta_ point behind our office buildings. Now with the offices closed, we are having our tea and snacks at home, which has increased the in-home consumption. All the consumables that went through the distributor channel to these joints are with the joints, and channelling them to the regular retail route is a challenge.

[^25d84257]: [Consumer goods fly off the shelves as coronavirus spreads in India](https://economictimes.indiatimes.com/industry/cons-products/fmcg/consumer-goods-fly-off-the-shelves-as-coronavirus-spreads-in-india/articleshow/74644159.cms)

Because of all the lock-downs, the e-grocers and FMCG manufacturers report a surge of about 20% in demand. The good thing about online stores is that they can set a limit on who can buy how much of the goods. This will stop people from unnecessarily stocking up on essentials.

Oh, and on the side, smoking is bad for health. Passive smoking is as bad if not worse. I try to avoid these places, and so should you.

## Is panic-buying justified

I find it hard to answer that with the available information. But I do understand why people want to panic-buy. If markets close, and all our vendors stop selling what they sell, we would most certainly be in trouble. Food is necessary for survival. The uncertainty in how long it will take for the virus  outbreak to die down adds to the panic.

But, is panic-buying rational behaviour? Doctors and health workers have all told us that wearing a mask is not going to help a healthy layperson. But you still see healthy people buying and wearing masks. The reason is that masks give you a sense of control; a sense of safety. This is the same as people roaming about the city with the disposable green face mask in hopes that it would help protect from all the pollution.

And according to Nash Equilibrium in Game Theory, this is rational behaviour[^0417862b].

[^0417862b]: [Fear is the key and panic buying is the therapy](https://www.livemint.com/news/india/fear-is-the-key-and-panic-buying-is-the-therapy-11583946988380.html)

> “Even people who were queuing up in the supermarket line to buy toilet paper, they have no idea why they are buying toilet paper,” said Andy Yap, a professor of organizational [behaviour] at the Singapore campus of INSEAD business school. “They ... see other people doing it and start doing it themselves because they are afraid they might lose out.”

The other point is the reaction of the governments across the world. When governments take drastic steps and make shock announcements, it triggers panic in people. Example: Japan.

When people feel such loss of control, they panic; control is a fundamental human need[^ebb62be4]. Since childhood, we condition ourselves to have control, and a situation like the coronavirus spread makes us go into crisis mode, trying desperately to regain control. Panic buying of toilet papers, staples and other essentials makes us feel in better control.

[^ebb62be4]: [Why rational people are panic buying as coronavirus spreads](https://theprint.in/india/why-rational-people-are-panic-buying-as-coronavirus-spreads/378845/)

This effect is more pronounced in societies that are individualistic---the 'every man for himself' kind. Societies that have trust in each other and the government (distinguish the government from political parties, please) are better equipped to handle such situations[^fc3c03bf]. If you need an Indian example for the latter, take Kerala and Chennai---Nipah and the Chennai floods of 2015.

[^fc3c03bf]: [Why People Are Panic Buying as Coronavirus Spreads](https://time.com/5800126/panic-buying-coronavirus/)

## Then why not panic-buy

We are not individuals; we are a society. Beyond that, we are a nation. We are not alone, and we are not the most underprivileged. We have the elderly, the physically challenged and all sorts of people in our society. Shelves being empty tells us that the stores are working beyond their normal capacity. When one section of people overbuys, it puts pressure on the retail system. The stores find it hard to restock. The stockists face a crunch and the wholesalers have to pull strings and go beyond their usual.

<figure>
  <a href="https://www.dailymail.co.uk/news/article-8134387/Heartbreaking-moment-elderly-woman-stares-shelves-Coles-coronavirus-pandemic.html#i-b7d62de91033f3c7">
  <img src="https://i.dailymail.co.uk/1s/2020/03/20/10/26202008-8134387-image-a-9_1584701889203.jpg" alt="Elderly woman stares at empty shelves"/>
  </a>
  <figcaption>Hungry and alone: Heartbreaking moment elderly woman looks on at empty shelves in Coles before breaking down in tears after coronavirus hoarders took all the food. (Source: Daily Mail)</figcaption>
</figure>

We are no more in the pre-eighties India. There will not be a short-term shortage of consumables. At the same time, you the individual cannot preserve consumables for a long period of time. This defeats the purpose of stockpiling. Also, you may end up wasting a lot of what you stored; for example, potatoes.

Your panic-buying will cause a short-term supply shortage of goods for the others. Granted, that would in no way be life-threatening, but why do something like that at all? Why at the cost of someone else? Instead, if nobody panics, life will go on as it used to, and everybody would have the necessary commodities for themselves---because there is no shortage of the products themselves. Think about it, life was normal for well over a decade without your having to take any special steps.

'But what if stores and markets close for a week? Lock-down, bro!'

Again, how much do you need for a week?

'What if the shut down extends?'

Here is why that is unlikely. The perishables will perish. That would be a loss to the producers. We are humans, we have always found ways.

Understand this: the corona virus is not transmitted by an airborne vector. It spreads through touch. Also, once you get infected, your body starts making antibodies that can fight the virus. Once your immunity emerges victorious, the antibodies live on at least for some time, making you immune to infections of the same virus. The virus cannot attack you any more; your body will kill it.

The option that the virus now has is to find a new host; infect those who were never infected by the virus. Some who cannot handle the virus will die. Because the host is no more alive or healthy, the virus in their body will die along with them as well (of course, in time). The rest are healthy individuals, whose immunity will keep trying to make an antibody that kills the virus. And in time, the virus would have infected all that it can infect, some of the infected would have died, and the others would have the antibodies that will kill the virus. Now the virus has nowhere to go. This is how every pandemic dies. Corona is no exception.

Social distancing is the go-to measure for it. Along with it, we must understand that every human has human needs. It would be unfair on your part to live on at someone else's cost. We are not animals. Some of those animalistic instincts that we have are not relevant in this civilised world where we think about the others. Let us be the fittest species---not the fittest human---to survive.

Let us find the "everyone wins" Nash Equilibrium. Let us have some faith in the human systems---governmental and non-governmental.

I resolve that I will not panic-buy. I will be a good member of the community and share the responsibility. I want everyone around me to survive---you included. I have nothing to achieve trying to save myself at the cost of others.

Let us stand not for ourselves, but for each other. Let us stand together.

We got this.

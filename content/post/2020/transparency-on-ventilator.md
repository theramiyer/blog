---
title: "Transparency on ventilator"
subtitle: "pun intended"
date: "2020-09-29T08:19:00+0530"
tags:
- business
- government
- transparency
- covid-19
- governance
- democracy
- India
---

I am wary of all politicians, no matter what party they belong to, what ideology they promote, or any other quality of a political party you can think of. Even if my dear grandmother were a politician, I would be wary of her; more so if she held power. Let us keep politics aside, because politics is politics---a game of whataboutery and other fallacies. Let us talk human life for once.

Ventilators are life support. Let us remember that, above all else.

{{< toc >}}

## The story

I will keep this short, and add footnotes for those who would like details.

At the end of April, India had 19,000 ventilators in Central government hospitals across the country[^b6941580]. In the third week of March, the Union government set up _Empowered Group III_, tasked with procuring ventilators by June. The estimated need was 60,884 more units by June. On March 27, the government floated a tender, saying 20,000 of these ventilators were urgently needed. In April, the government floated tenders for 40,884 ventilators. The government had earmarked ₹2,000 crore towards the procurement of about 60,000 ventilators---from the PM CARES fund.

[^b6941580]: [Indian entrepreneurs rise up to meet COVID-19 medical demand](https://www.deccanherald.com/national/indian-entrepreneurs-rise-up-to-meet-covid-19-medical-demand-832409.html)

By 1 May, the government had identified Hindustan Lifecare Ltd as the procurer of the ventilators, and had placed an order for 60,884 ventilators. Indian companies would make 59,884 of these ventilators, import will cover the remaining 1,000. Here is a breakdown of the major players:

Company | Units
:--|--:
Bharat Electronics Ltd (BEL) and Skanray | 30,000
AgVa Healthcare and Maruti Suzuki | 10,000
Andhra Pradesh MedTech Zone (AMTZ) | 13,500
Jyoti CNC Automation | 5,000
Allied Medical Ltd (AML) | 350

And then, it goes south, except for AML which delivered as promised. They have been one to produce and deliver ventilators to public sector hospitals, over years, and had the necessary certification and accreditation. Yet, they were the one to receive the smallest order. And:

> According to technology specialists focussing on the clinical equipment industry, AgVa and AMTZ have no prior experience in manufacturing high-end ventilators. These experts, who did not wish to be named, pointed out that manufacture of high-end ventilators was time-consuming and that the government had either misjudged the capacity of these companies to deliver or had misrepresented facts about them deliberately.  
> ---Frontline (The Hindu)

Further disturbing is the sketchy accreditation and certification story[^8474387a].

[^8474387a]: [COVID ventilators: Who cares?](https://frontline.thehindu.com/the-nation/covid-ventilators-who-cares/article32290434.ece)

## The timeline

Rather than read prose, let us create a short timeline of the events until the end of August 2020.

Date | Event
:--|:--
Third week of March | Special Empowered Group established. Estimated 2 lac ventilators needed by mid May, 19,398 available.
March 27 | SEG floats a tender for 20,000 ventilators as urgent need.
March 27 | Hindustan Lifecare Ltd places an order for 10,000 ventilators with AgVa Healthcare.
Last week of April | Total needed ventilators revised to 60,884. Tenders floated for 40,884 ventilators. Delivery deadline set to June 30.
May 15 | Hospital superintendent of Ahmedabad Civil Hospital, J.V. Modi writes to Gujarat State medical service provider stating Dhaman-1, produced by Jyoti CNC Automation is not up to the mark.
May 18 | Advisor to the Prime Minister writes to Health Secretary requesting that the ventilators bear the PM CARES logo along with embedded GPS.
May 19 | Ahmedabad Mirror reveals the discrepancies around Civil Hospital's procurement of Dhaman-1.
May 20 | Health Secretary responds promising the logo and the GPS chip, states 58,850 ventilators ordered at ₹2,332 crore.
May 20 | Hindustan Lifecare Ltd places an order with AML, AMTZ and Jyoti CNC Automation.
May 24 | Gujarat High Court criticises State authorities over the shabby health infrastructure at Ahmedabad Civil Hospital[^7afaab7a].
June 15 | BEL states that it has produced 4,000 ventilators against 30,000 ordered.
June 23 | PMO states in a press note that BEL has manufactured 2,923 ventilators until then.
Last week of June | BEL claims to have delivered 15,000 of 30,000 ventilators.
First week of July | AML delivers its share of ventilators.
First week of July | AgVa and Maruti Suzuki deliver 1,500 ventilators against 10,000 ordered.
July | Jyoti CNC Automation and AMTZ get dropped from the Ministry list.
July 20 | Jyoti CNC has not yet received recommendations from the DGHS, an RTI application reveals.
August 3 | Health Secretary Rajesh Bhushan claims that a mere 0.27% patients are on ventilator.
August 8 | CMD of Jyoti CNC claims to have received a green signal for Dhaman-3.
August 21 | 18,000 ventilators delivered to hospitals.
August 28 | Jyoti CNC Automation claims that their Dhaman-3 has passed all tests parameters of health ministry and has secured an order of 5,000 units from DGHS. Wrongly claims that Jyoti CNC has gotten the lowest order.

With inputs from sources including [The Hindu](https://www.thehindu.com/news/national/coronavirus-ventilators-from-firms-funded-by-pm-cares-fail-trials/article32416810.ece), [The Times of India](https://timesofindia.indiatimes.com/city/ahmedabad/jyoti-cnc-claims-that-dhaman-3-ventilator-passed-all-the-tests/articleshow/77802715.cms) and [Ahmedabad Mirror](https://ahmedabadmirror.indiatimes.com/ahmedabad/cover-story/the-fake-ventilator-scam-no-dgci-licence-for-900-fake-ventilators-performance-trial-held-only-on-one-patient/articleshow/75815096.cms).

[^7afaab7a]: [Ahmedabad civil hospital conditions pathetic, painful: Gujarat HC](https://www.thehindu.com/news/national/other-states/ahmedabad-civil-hospital-conditions-pathetic-painful-gujarat-hc/article31663615.ece)

## What does this all lead us to

The government must be under watch---always---malice not being the sole reason. Perhaps the authorities were not malicious in what they did, perhaps they were. We do not know. The outcome, regardless, has been that people died because of lack of infrastructure, something that the government could have prevented.

Am I suggesting that the government is solely responsible for the healthcare system? In India, yes---if not fully, at least the government must be responsible for strict regulation. We have a large population of people who do not have the means to spend lacs of rupees towards the treatment[^9c275412] for COVID-19 and over-the-limit, exorbitant charges towards COVID-19 tests[^262e5138], in private hospitals. My father's uncles and cousins got admitted to hospitals for treatment in Ahmedabad. They had to pay ₹3 lac upfront---each---before the treatment even began. I, for one, cannot afford that.

[^262e5138]: [Rs 6000 for COVID tests: Are Bengaluru's top private hospitals guilty of overcharging?](https://www.newindianexpress.com/cities/bengaluru/2020/jul/06/rs-6000-for-covid-tests-are-bengalurus-top-private-hospitals-guilty-of-overcharging-2166216.html)

[^9c275412]: [Private hospitals continue to charge in excess](https://www.thehindu.com/news/national/tamil-nadu/private-hospitals-continue-to-charge-in-excess/article32250456.ece)

Healthcare is a basic human need. In a place where health insurance is not streamlined enough to reach the masses, and remains something for the middle- and the upper-middle class, with the majority of the population not having access to private healthcare, the government must step up. And there, transparency is important. Transparency is important in any democracy.

## The argument

One of the arguments that my friends put up was that India did not have any company manufacturing ventilators, and so, the government had to promote new players given that the countries that exported ventilators, in the pandemic situation, had raised the prices of the ventilators to anywhere between 10 and 20 lac a piece, whereas the budget allocated for each ventilator was 4 lac rupees.

First of all, ignorance is at the root of the argument that we had no company manufacturing high-end ventilators. My friends are not to blame, though. When were companies like Allied Medical Ltd brought into limelight, despite them manufacturing ventilators for even the defence sector? We must question, though, why the government placed the lowest order to a seasoned manufacturer, while the others got orders in thousands? Were they ignorant as well? Or perhaps AML said they did not have the capacity to manufacture thousands of ventilators in the given time window. In which case, the question should have been, 'If a seasoned player cannot deliver the ventilators within the specified time, how are new players going to? Is it safe to place thousands of eggs in their baskets? What are the alternatives?' We must get used to looking at the track records.

And due diligence is must. We all do our share of due diligence even for a simple product on Amazon, worth hundreds of rupees---we look at the specifications, the ratings, the reviews, we ask around, etc. Then why was no due diligence done in case of a procurement worth thousands of crores of rupees? Why was there not enough due diligence in case of products that people's lives depend on in the literal sense?

Let me remind you, political parties may have their own crap-load of allegations to make. They will make a lot of noise, because this is an opportunity for them. But like I said, politics is politics, and I don't care what political parties have to say. My question is on the governance. The government has failed in this regard. And I am not alone to say this; the courts, the government bodies and government doctors have said it themselves. And I would argue (although in retrospect, to be fair) that if the need was for 30,000 ventilators, the government could have imported them given the situation. Instead of placing 30,000 people in danger at a time, they could have saved 10,000 people at a time. Moreover, given that we already had 19,000 ventilators, buying 10,000 for 2,000 crore would have still worked better. Also to note, the newly-spawned manufacturers were far from meeting the need themselves. And _anybody_ would have guessed they could not have met the demand on time, forget experts. Have we stopped consulting professionals? Or is that a trust deficit?

The second argument is that if PM CARES is disputable, so is Prime Minister's National Relief Fund. Finance Minister Nirmala Sitharaman pointed out---in her usual tone---how the PM NRF was like PM CARES, and I found it funny, because to counter one opaque fund, the government ended up creating another opaque fund. I mean, where is the sense in that? Would it not be a better pitch to make PM CARES a transparent fund? Or better yet, find a way to amend the rules for the PM NRF and prevent fragmentation of funds! The board of PM NRF has the president of _one party_, and that is wrong. Amend the rules[^a8d474ea].

[^a8d474ea]: This article mentions how the fund has been working under the discretion and the directions of the Prime Minister since 1985: [PM CARES Vs PM National Relief Fund: All You Need To Know](https://www.boomlive.in/fact-file/pm-cares-vs-pm-national-relief-fund-all-you-need-to-know-7618)

The third argument was about Self-reliant India (a.k.a. _Ātmanirbhar Bhārat_). 'When else were we going to give a chance to local manufacturers?' To that, I would say, when we get a serious illness, we do not go to a medical intern. We go to an expert. The intern learns from the expert while the expert does her job. Later, under the watch of the expert, the intern treats. As the intern picks up, the expert starts to delegate work. An intern is _never_ let to perform an unsupervised major organ transplant as their first surgery. This pandemic is no less.

We must have started to promote local manufacturers in general. The policies should have been such from the beginning. The 'seventy years' argument is getting old. Their making a mistake is not an excuse for underperformance. Every government has the responsibilities that it has. If everybody fails and points at the predecessor, what is the way forward?

And am I an armchair critic? No. If I had to be a certified car manufacturer to qualify to complain about a cranky engine, that's fallacy. Going by that formula, I cannot complain about bad tea sold behind my office building; 'Come, make your tea yourself.' I cannot complain about a bad motherboard; 'Okay, make your own motherboard, then.' Sorry, the world does not work like that. If I fail at my job and my product owner points it out, I cannot tell him, 'Well, look, if you know so much, come, write code.' I am answerable for my failure in my profession; the government is, for its. No argument there. If the government cannot take that responsibility, people _will_ question its credibility.

Citizens will support where they can. I have, whenever I could. But when something is not in the hands of the citizens, the government is solely responsible for it. Healthcare is the government's responsibility. Across the world. India is no exception.

Own it up.

## Summary

Experts hold government offices. The Cabinet's job is to lead. We cannot expect the Prime Minister to go to every hospital; that is not his job. But his job for sure is to take responsibility for what his Cabinet does. The Ministry of Health and Family Welfare---and Dr Harsh Vardhan---must _oversee_ the health-related issues in the country (we do not expect him to go to every hospital every day to check). Establish a framework on which the infrastructure rests. Managing 1.3 crore people is difficult for a single body, and that is understandable. This is why we have the state governments, closer to the ground. The framework must include them. Let us hold them accountable. Some states are doing a commendable job, let us learn from them. Let us replicate policies that work, discard those that fail. Politics among hundreds of individuals must not affect a hundred crore. That is not democracy.

Human beings fail. That is common. We are not all born with all abilities. Collaboration is the key to excellence in the 21st century. Let us consult with the experts in what they have to say. Let us take their views into account, and not resort to impulsive decisions. Every impulsive decision taken in our country in decades has ended in a disaster.

This is a matter of human life. Let us take it with a little more seriousness.

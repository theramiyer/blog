---
date: "2020-05-16T00:00:00Z"
episode:
    spotify: 1aIKOuo9dleSDqR4mnf4uJ
    apple: "1000475605067"
    google: "NzBhYWM2MzMtZGYyNC00YmViLThmZTctMjI3ZDcyOWYzZDc0"
tags:
- nationalism
- economics
- fact-check
- government
- taxes
title: Can we be free of Chinese goods
---

Ever since [Prime Minister Modi made his speech](https://www.youtube.com/watch?v=g2I20uFMy3E) on the 12th of May 2020, _Aatmanirbhar_ started trending. My Facebook newsfeed showed countless memes and what not, about boycotting Chinese products. Some even questioned my bravery to be able to say no to Made in China.

{{< figure src="https://blogfiles.ramiyer.me/2020/images/say-no-to-made-in-china.jpg" alt="A Say No to Made In China poster from social media" caption="A Say No to Made In China poster [from social media](https://www.facebook.com/PostcardEnglishNews/photos/a.818260655050767/1356674747876019/)" >}}

{{< toc >}}

Bravery to post aside, I started to think, 'How practical would it be to boycott Chinese products?' Or rather, what would it take to boycott them?

{{< podcast >}}

## The current situation

As of March 2020, our total import from China was at $70 billion[^aa6ccf72] in twelve months. That amounts to about 13% of the total imports[^8e184c5a] into India. The pre-COVID-19 period says no different story either: India imported goods worth $514.08 bn in all, of which, goods imported from China were worth $70.32 bn[^290ca9bc].

The issue is not merely importing goods and services from China, but _how much_ we export and import. We call this _Trade Balance_; the difference between the worth of goods and services exported, and the worth of goods and services imported. A positive number indicates a trade surplus, while a negative number indicates deficit. Data source: Export Import Data Bank, Govt. of India.

{{< raw >}}
<div class="flourish-embed flourish-chart" data-src="visualisation/2448356" data-url="https://flo.uri.sh/visualisation/2448356/embed"><script src="https://public.flourish.studio/resources/embed.js"></script></div>
{{< /raw >}}

[^290ca9bc]: [Export Import Data Bank (Ministry of Commerce and Industry)](https://commerce-app.gov.in/eidb/)

[^8e184c5a]: [India's Foreign Trade: March 2020 (PIB India)](https://pib.gov.in/newsite/PrintRelease.aspx?relid=202278)

[^aa6ccf72]: [Coronavirus: India may impose duties on 100 top Chinese imports (Business Standard)](https://www.business-standard.com/article/economy-policy/coronavirus-india-may-impose-duties-on-100-top-chinese-imports-120022901126_1.html)

What you see in red in the chart is the _trade deficit_. In the last five years, we have bought products and services worth $268.9 bn (₹46,74,368 crore) from China _more than we exported_. While oversimplifying this concept is harmful, what we can understand is that India's domestic demand is far higher than our output.

## What do we import from China?

And a corollary: What can we stop buying from China?

The first response of some is, 'Well, ban _TikTok_.' Right, ban the app. Problem solved.

Wrong.

I started looking for data that would tell me what we import from China---what we import the most. Did anyone say electronic equipment? Take a look:

|Commodity                          | Apr 2018--Mar 2019 | Apr 2019--Jan 2020 |
|-----------------------------------|-------------------:|-------------------:|
|Electrical machinery, parts, etc.  | $20.627 bn         | $17.040 bn         |
|Nuclear reactors and machinery     | $13.383 bn         | $11.790 bn         |
|Organic chemicals                  | $8.596 bn          | $7.020 bn          |
|Plastics and plastic articles      | $2.722 bn          | $2.394 bn          |
|Fertilisers                        | $2.053 bn          | $1.721 bn          |

For more information, take a look at the [Import: Country-wise all commodities](https://commerce-app.gov.in/eidb/icntcomq.asp) report by the Department of Commerce, Govt. of India.

If we ask, 'What is the net result across all countries: Are we importing more or exporting?', the data says that India's trade balance of 2018--2019 was a _negative_ $184 bn.

## Why are we buying from China?

If you did indeed generate a report, you would see consumer-facing products such as footwear, apparel, accessories, etc., running into hundreds of millions of dollars. And you would think, 'Okay, let me stop buying those attractive-looking toys and all.' Or, 'Okay, no more make-up kits from China.' But that is hundreds of millions of dollars as opposed to tens of billions of dollars. What effect would cutting down on consumer-ready articles have on the numbers?

> It is important to understand that no one is forcing Indians to import goods from China. The fact that we are doing it is out of free will. The Chinese goods are either more value for money (i.e. better quality for the price demanded) or they are goods which are simply not widely available in India.  
> ---Vivek Kaul, _Easy Money_ trilogy

The crux, to me, doesn't look like is in consumer goods. We are importing billions of dollars worth of chemicals, fertilisers, etc., apart from plastics and electronic goods. We are also importing tonnes of metals and metal articles, medical and surgical equipment, and automotive parts.

Do we as consumers have control over these?

No. But even if we could nudge the companies that we buy from, to stay away from materials of Chinese origin, the situation would come down to two questions:

1. Can we produce as much as we buy from China?
2. Can we get the materials at the prices that China sells them at?

The two are inter-related. If we produce as much as China does, and if we have an edge over them in quality, we will have consumers ready to buy from us. This encourage us to produce as much, and so, the cycle would go on and we would be able to keep the prices competitive. This will in turn nudge industries to buy from within our country, and reduce the trade deficit, and also contribute to national income.

## Why does the government not hand-twist?

Our government will not force us to buy from within the country. Think about it this way: We will have to make all products in India to stop Indians buying from China. Why?

China sells at cheap rates. The cost of producing the goods in India is higher than for China, because, economies of scale. Second, no other country sells as cheap as China does. For instance, if a tonne of smokium[^885c7b1a] costs $400 when bought from China, Australia sells the same substance for $470. Assume the tax on smokium bought from China is 14%, while when bought from Australia is 12%. Calculate the total cost and compare. China: $456; Australia: $526. Buying from Australia is more expensive despite the lower tax. Manufacturing within India is a better choice; at least, there would be no import taxes.

[^885c7b1a]: Smokium: Something I made up

But the case has been that producing in India is still more expensive than importing from China despite the tax benefit. We will talk about the reason in a moment. We could bring down the cost by increasing production, but can we produce more?

The government does not force anyone against buying from China for a simple reason: If the products become more expensive, people will buy less. Less raw material or components bought translates to lower output of final products. This has a ripple effect as well. For instance, if the price of my favourite brand of coffee powder goes up by ₹300/kg, I will either buy 250 grams less of the coffee, or stop buying something else (like, avoid the social _chai_ for a week every month). This makes the _chai wala_ lose his business.

This direction is undesirable for the government.

## So, no Make in India?

Make in India is a fantastic phrase and a good plan, but requires enormous amounts of work. The first step is to make business easier in India, so that more manufacturers come up. The goal should be to reduce import of finished goods. Import of raw material is inevitable at some point.

Coming back to setting up businesses, the 2020 _Doing Business_ report[^a09c7b45] ranks India at 63 in ease of doing business. We have come up from 134 back in 2014, and that is no small feat. But China ranks at 31. We still have a long way to go, if we are competing with China. Even if not, think of how easy starting a business in India is, like registration, getting construction permits, electricity supply, or the kind of aid available to new businesses, the tax norms, and such factors. These factors impact the ease of doing business. The reality is that we are in an age where small businesses are unable to understand GST, even three years after its implementation.

Sure, the government has all the documentation, but how accessible is it? And by that I don't mean being able to find links and click on them; how simple is it for the _general population_ to understand this? If not, what percentage of businesses can afford to hire a chartered accountant?

[^a09c7b45]: [Ease of Doing Business rankings (Doing Business)](https://www.doingbusiness.org/content/dam/doingBusiness/pdf/db2020/Doing-Business-2020_rankings.pdf)

Secondly, the basic infrastructure required to enable this is not in place. If it were, we would not be looking at 7.6% unemployment (December 2019---three months before the COVID-19 lockdown)[^2d029c00].

[^2d029c00]: [Unemployment Rate in India (CMIE)](https://unemploymentinindia.cmie.com/)

## Will the boycott impact China

No. Not noticeably, anyway. China's exports to India constitute about 3% of their total exports. While China will be "upset" about the sentiment, the boycott will not make an actual impact.

The boycott will have adverse affects on India instead. If in doubt, [read above](#why-does-the-government-not-hand-twist).

## Show me the light

The good news is that the import from China shrank last year. And the export is increasing as well[^3e028758]. Two points to note:

1. Export of petroleum products to China.
2. Assembly of cell phones in India.

[^3e028758]: [Chinese cheer for Indian exports (The Hindu Business Line)](https://www.thehindubusinessline.com/opinion/chinese-cheer-for-indian-exports/article26431736.ece)

## Where India needs to step up

Chinese companies are more efficient in logistics, apart from production. On an average, Indian firms spend ~9% more money in logistics[^05081da6]. While the taxes India imposes on Chinese goods offsets the gap, Chinese firms receive subsidies from their government on exports. Chinese banks also offer cheaper loans (at almost half the rate of interest as the Indian banks).

[^05081da6]: [Can India Really Boycott Chinese Goods?](https://www.managementstudyguide.com/india-really-boycott-chinese-goods.htm)

Second, and perhaps obvious, is infrastructure. The infrastructure that the Chinese have---starting from transportation systems to availability of space for factories or special economic zones---gives them a great advantage.

We have spent decades, mired in Hindu-Muslim skirmishes and scams and "internal issues". We play the game of, 'Yes, I sucked your blood, but at least I didn't make you walk upside down.', while the other says, 'Well, I made you walk upside down, but at lease didn't suck your blood.' When we find time off of these, and our slogans don't merely remain slogans, we might make some noticeable headway in the development of infrastructure.

Third, we also seem to miss out on recycling. Recycling plastics and metals would mean less of them bought from elsewhere. Apart from reducing the size of landfills, recycling could generate more jobs, and at the same time, lend a hand in reducing imports as well as the cost of raw materials. Why we are not encouraging recycling in the right way is beyond me. What happened to the 'No waste is waste' campaign?

The moral of the story is, tweeting 'Boycott Chinese goods' or making postcards of 'We are saying no to Made in China' at this point is stupid, short-sighted, irresponsible and absurd.

Sure, you can exercise your right to expression of views; opine that I am anti-national. But, will silencing the pain with a pill fix the broken bone?

---
title: "Arnab Goswami"
subtitle: "and why he matters"
date: 2020-11-21T07:24:14+05:30
tags:
- journalism
- propaganda
- democracy
---

A couple of days ago, my aunts and uncles had come over. They are all hardcore supporters of the _Bharatiya Janata Party_ (BJP); they feel that the party is "the true saviour of Hinduism", a religion that the "secular" parties of our country have disregarded---or worse, degraded. Whether Hinduism as a religion needs anybody's protection or not is your personal opinion and mine as much as theirs.

These discussions suffer the one-thing-led-to-another syndrome, and in general, these discussions have a pattern. Within minutes, the point of highlight was Mr Arnab Goswami---and his recent arrest.

As you would expect from every BJP loyalist, they flared up against the _Shiv Sena_, calling them "goons" and what not.

My brother and I have a general disregard for politicians, but love to play the Devil's Advocate. We listened with patience while they went on about how the police had arrested him for no reason despite giving a statement in the court that they could not find any evidence of any foul play in the death of Mr Anvay Naik.

On the Internet, you can find tens or hundreds of blog posts and videos about whether Goswami is a journalist or not. You will see arguments that a journalist can or cannot pick a side, and you will also find Goswami's speech before leaving Times Now, where he says that a journalist must take a side---"how could one not?"

And he may be right.

## What this discussion is not about

The moment we mentioned how his arrest video seemed amusing (remember, Devil's Advocates must be mean) and how this was a sort of poetic justice, for a moment, our living room erupted like water in superheated oil. And we had no plans to smother it.

Anyway, my aunt would not bother to hear about what he did to Rhea Chakraborty, or him claiming not to care about the law that grants personal liberty (oh, the irony); she wanted to talk about what she thought were "Rhea's actions" instead. Also, the general understanding seemed to be that journalists have more liberties than the rest of the citizenry including Law Enforcement.

Newsflash: No, they don't.

But no, this is not a discussion about any of that. What value will my repeating what hundreds of people have already said, add to this, unless this is a Twitter trend game?

And this is not even about whether the target of this case was Goswami's press freedom, or that Mr Naik's letter named Goswami, making him a criminal suspect, nothing to do with the fact that Goswami is the Editor-in-Chief of Republic TV.

## What matters

When my people felt their star anchor attacked, they pounced on us with instances where other journalists were spreading propaganda. 'Oh, how come you don't talk about Aaj Tak or India Today, or Zee or other media houses?' We did not even wait a second to call out the whataboutery, adding, 'Well, while we would love for them to go back to their first class in journalism, the point is that irrespective of what Zee and other channels spread, what Goswami says matters _more than any of them_.' For an instant, we saw pride flash across faces, which confirmed that they had not understood the gravity of the statement.

We had to break it down.

Republic TV and Republic Bharat, both of which Goswami runs, top the respective TRP charts[^trp-charts], and by a long distance---which is the crux. Whether you think the methods that BARC uses are truly indicative of viewership or not, or whether Republic did involve in meddling with the TRP data, there can be no doubt that millions more tune into Republic (and Republic Bharat) during prime time, than other channels.

[^trp-charts]: A sample report, and is that why they ran into trouble?: [Republic TV hits back at India Today & Times Now over ratings surge](https://www.bizasialive.com/republic-tv-hits-back-at-india-today-times-now-over-ratings-surge/)

When Goswami says what he says, millions hear it. The majority of them---also in millions---agree with it. In a democratic republic (notice the lowercase 'r'---why do I have a smirk across my face?), that number matters. Had Goswami's channel bottomed the TRP list, it may not have mattered (like [my YouTube channel](//youtube.com/ramiyer)).

Such channels as those of his, have the loudspeaker effect (ahem). And the more you publish the TRP numbers, the louder the effect is, because of conformity bias[^confirmation-bias] (do not confuse it with confirmation bias---cough). And so, what Goswami says, matters. If he understands journalistic principles and ethics and exercises them---thinking beyond his personal gain---it will have a profound effect on not merely journalism, but also on the well-being of people. Perhaps a journalist can have bias, but towards what?

[^confirmation-bias]: [Conformity Bias: The Profound power of Group](https://www.mokshamantra.com/conformity-bias-the-profound-power-of-group/)

_That_ is what matters.

---
date: "2020-06-21T00:00:00Z"
tags:
- business
- India
- China
- globalisation
- trade
title: How to take on China
---

My previous post gives a gist of what it would take to boycott goods of Chinese origin. Meanwhile, as tensions mount at the border, the national sentiment in our country is touching new levels every day. I even saw videos showing people throw away products of Chinese origin that they own. My brother was quick to point out---as usual---the folly in the act: 'What's the point? They'd already paid for the product; China has their money anyway.'

Don't get him wrong; that's a valid point.

> Sentiment: 1  
> Logic: 0

China laughed.

Wrong strategy. The idea is not to break existing Chinese goods, but to reduce imports. Whatever Chinese goods are in India are already bought. What we do with them affects China in no way. Such a deed dents your pocket. Think of it this way: You broke an appliance you had in your home. Now you either have to do without it or buy a new one. You do without it, your loss. If you buy a new one, even if you buy the same (electronic) product from an Indian brand, what are the chances that 80% of its components were not sourced from China? Whose loss?

Do we give up?

No. Although, at the same time, we don't make shortcuts. Outrage is a shortcut. It does more harm than good. It lets out the steam. And after it has passed, calms you down. It brings you back to "normalcy". Dust settles, and everything begins to go back to the state before.

We _do not_ want that.

What we want is to set a new normal. We want change. And change happens through deliberate effort. Change happens through work. By channelising the energy and pumping it into action.

{{< toc >}}

## Identifying the channels

There can be no doubt that we see China as hostile. But like I mentioned in my previous post, boycotting Chinese goods will adverse effects on us, and make a mere 3% dent on China's exports---not 3% of their GDP, but the exports alone. I would disagree that China will face any impact significant enough to change its ways.

Granted, India is one of the largest markets in the world, but a ban on Chinese goods will not make a _direct_ impact on China.

To break the Chinese economy, we must know what brought it to this state. After we find these pieces, we can begin to dismantle it. Let us begin the analysis; I will keep it as simple as I can.

## The beginning

I am a Millennial. I was in school in the nineties and the noughties. I have known China as a manufacturing beast. What we fail to see is that China became the manufacturing giant in the last three decades. This growth is nothing short of remarkable. (Recognising the strength of the perceived opponent is an important part of planning a strategy---underestimating the strength is folly.)

In the nineties, China began opening up to the global market---what India also began doing by the end of eighties. The difference was that India was honest in the approach, while China exploited it. I watched a nice documentary called _The 80s India: Relive the Era_ on NatGeo, which portrays the transition rather well. I am unable to find it anymore, anywhere.

Anyway.

The large capitalistic economies such as the US wanted China to shed its protectionist attitude and join the world in globalisation. Such a move would do the world some good, right?

## A Free Market

First, a market economy. A market economy has privately owned businesses. These businesses compete among themselves, without restrictions. The _demand_ decides the supply of the goods. The demand, along with their supply, decide the price of the goods.

A _free market_ is one where the government does not intervene at all, or intervenes minimally, in deciding the demand, supply, or the prices of goods or services. In such a system, the enterprises have the power to take these decisions, and the government restricts its interventions to cases such as regulations. In reality, there is no purely-free-market economy in existence; every government plays a role in regulating their markets.

The crux here is the individual right to property and wealth. In a system that respects individual right to property, individuals take it as an incentive to grow their businesses.

## A Communist Market

In a communist market, the State controls rights to property. The State decides the production based on _need_. This directly hits the demand and supply, and places the control of prices in the hands of the State.

You can see everywhere you look in the context of China, that people complain of its market practices as unfair. This is the beauty of "Communist Capitalism"---a beautiful oxymoron that oozes convenience. The modus operandi of the Chinese economy is a fascinating case study of how a welfare-oriented system can become a so-called dragon burning down everything in its wake, leeching on the world economy.

In short, China has modelled its policies in such a way that they benefit China no matter what anyone does. China was a protectionist market, which the world saw as bad. Then, China entered into globalisation, and made it worse. At the core of this is mercantilism (or, neomercantilism, in the post-WTO era). But before we get into it:

## State-owned Enterprises

Let us take an example of car manufacturing, which Prof Chang-Tai Hsieh talks about in his paper[^d07c3e2c]. This is one of those strategies that make China successful.

[^d07c3e2c]: [Crony Capitalism with Chinese Characteristics](https://pdfs.semanticscholar.org/aadb/87c81db7223b50f1bbb608796bf2b92cb32b.pdf)

A company, say, from the US, approaches China to start business there. China is a large, lucrative market. The way China operates, local governments, such as the government of the City of Shanghai, work almost as an independent entity. This government owns a firm, Shanghai Automobile Industry Corporation. The US-based company cannot manufacture and sell products on their own. They must go into a joint venture with a local company. This company in Shanghai is SAIC.

When getting into the joint venture, GM, the US-based company, must transfer the technical know-how of the manufacture to SAIC, who would now be free to use it in its own ventures. These companies, of course, do not share the know-how of their latest technology, but it still means that the companies manage to sell outdated technology to the people of China. A classic example for why an authoritarian system is not good for the people and the market---the deep dark side of Communism.

These State-owned companies use the intellectual property to capture and saturate the local markets, and then, begin exporting the goods at dirt-cheap rates, eliminating competition from other international players. This crony capitalism and a communistic market is a dangerous combination because the capitalists make great profits by selling goods that are not worth what we pay, and at the same time, give the politicians excessive power and unfair advantage.

## Subsidies

State-owned enterprises operating at the global level is detrimental to free market trade. Look at it in simple terms. The government is powerful. The government gets funded through the taxes it collects. Power and money. If the government gets into business, it wouldn't care about profits as much as a businessman would. Loss would not worry the government either. This becomes a bigger problem when the state-owned enterprise reduces costs to such levels (by not caring about profits) that the privately-owned enterprises cannot compete at all. A simple example for this is Air India. No, we are not engaging on the debate on the decision of the government to sell off Air India---that would be for some other day.

China gives subsidies to its State-Owned Enterprises (SOE), which drastically alters the balance, and in fact even angers the private players within China, let alone foreign private players.

## Tariffs

In classic mercantilism, a country discourages imports by applying high tariff on imports. This way, individuals and companies within the country would prefer to buy or produce within the country. While subsidies are a way to encourage local players, tariffs are a way to discourage foreign players. Their combination is, again, harmful to free markets.

The modern world sees mercantilism as a bad practice, learning from our past that such policies have led to wars and colonialism. But do countries not place tariffs on foreign products at all? Almost every country does, but we limit its extent. Why, even countries like the US apply tariffs on some foreign goods. This is a matter of balance.

Countries today apply newer techniques rather than simple tariff: Non-tariff Barriers. These practices could range from customs delays to complex policies in establishing trade or import quotas. What level of NTB are bad would make this post a lot more complicated than needed---developing economies need some level of such practices to have a level playing field and maintain a healthy trade balance. Although, China, apart from imposing tariffs, also aggressively engages in predatory neo-mercantilist practices to emphasise on exports and reduce imports, to maintain a high trade surplus.

## Putting the findings in context

Now, to be fair, China has indeed changed some of its ways. For instance, in 2014 when Hsieh et al. published their paper, China was at rank 96 in _Ease of Doing Business_. In 2020, it ranked 31. This could happen for two reasons:

1. China is reforming its formal economy and doing better than others
2. Other countries that were ahead of China are doing worse than China

Assessing the scores of other economies and comparing them with China's would be an ideal way to go about finding out which the case is, but the process would consume an enormous amount of time. Another method is to go by the indicators; see what has improved, if anything. The official report[^3f8af399] assesses two Chinese cities, and shows improvement in nine out of ten parameters. The list of reforms shows that China has been making a great deal progress in the last two years[^c2b08064].

![China's reforms - Doing Business 2020](https://blogfiles.ramiyer.me/2020/images/2020-06-21-07-59-02.png)

[^c2b08064]: [Business Reforms in China](https://www.doingbusiness.org/en/reforms/overview/economy/china)

[^3f8af399]: [Economy Profile - China - _Doing Business_](https://www.doingbusiness.org/content/dam/doingBusiness/country/c/china/CHN.pdf) (dynamic data, updated annually---refer snapshot for data as of 21 June 2020).

'Hey, wait a minute, you are talking good about the Chinese economy! Were we not aimed at criticising it?'

When we pick any case study, we must look at every aspect relevant to it. We learn lessons from others' successes as well as failures. Where China shows consistent success is in its reforms. Over the last six years, China jumped 65 places up in the rankings. Remarkable progress.

'But isn't India also doing it? We went up from 134 in 2014 to 63 in 2020; 14 places in a year[^7e031249]. Isn't that remarkable?'

[^7e031249]: [India moves up 14 places to 63rd position in World Bank’s _Doing Business 2020_ survey](https://www.thehindubusinessline.com/economy/india-jumps-14-places-to-63position-in-world-banks-doing-business-2020-survey/article29783189.ece)

Of course! It shows we are going in the right direction.

And to be fair, either way, we must also remember that the _Doing Business_ report is not foolproof or a complete picture in any way. The 2020 report uses data from two cities in China: Shanghai and Beijing. Similarly, with India, it uses data from Mumbai and Delhi. But a bad sign is that we showed no improvement in three parameters, and slipped 0.3 points in _Registering Property_, in the last year. Also, India showed reforms in a good number of areas in the 2018 and 2019 reports, but seemed to have lost momentum a little in 2020.

![India's reforms - Doing Business 2020](https://blogfiles.ramiyer.me/2020/images/2020-06-21-09-09-59.png)

## What next

The most important part. And the hard part.

Before we move on, I am a regular engineer who reads. I am not an economist, nor do I hold an office that expects me to know the microscopic details. I can give a rough map, like the one we used in school. I am not the one who will be able to say what is 50 metres to the north-east of Fatehpur Sikri.

Let us split this into three divisions:

### As an economy

Don't worry, I will not expect an angry microphone to work out differential calculus. We ignore the microphone for now. Any way I am a millennial.

An important aspect of the Indian economy is such a large chunk of it being informal. The _Doing Business_ survey looks at the formal economy. Most policy decisions that we make today are for the formal economy. This informal economy has the power to contribute to the momentum. We either bring in new reforms taking the informal economy into account, or formalise the informal economy through a different approach and drive a holistic development.

Next, we revisit the policies that are not relevant in the 21st Century, including State-owned businesses. That does not mean we remove all State involvement; India is India---nothing that works for the rest of the world is fully successful here. We are a unique system. We must find our own model. The 21st Century has technology that humans had not even imagined in 1950. We tweak our policies on a war footing and make them relevant. Like they say, battles today happen within closed rooms; not using conventional weapons, but using economic power and diplomacy.

We remember that we do not want to become another China, but be the better alternative to it. We be a responsible global citizen. We be the friendly, reliable business partner. We protect our interests, while being open to change. We respect each other, we stand by each other. We unite. Our intention is not to break China, but to prove to the world that there can be better.

We focus on innovation. Manufacturing is one thing; that alone is not sustainable. We need better research facilities. We need to invest in areas like smart systems and artificial intelligence.

### As enterprises

For enterprises, this is a challenge. 14% of all our imports (by value) come from China. We don’t have a measure of the quantity of each of the goods. Assuming even 70% of these imports make up unfinished goods or components required for manufacture of goods, stopping these imports will hit the enterprises by reducing the available base materials, or forcing the enterprises to buy more expensive alternatives. For more information on the ripple effect, read my [previous post on the matter]({{< ref "can-we-be-free-of-chinese-goods.md" >}}).

What we would need here is for the enterprises to work together and along with the system to gradually transition to using local-made components and raw materials. This process could take years if our economic system does not change.

### As consumers

The first step for us is to channelise our emotions into real action. Soldiers lost their lives, yes. Chinese soldiers killed them, yes. China is dumping goods into our markets, hurting local businesses, yes.

Despite all that, breaking Chinese products in your home will hurt you. If you get hurt, our economy gets hurt. Banning Chinese goods that are already in our markets will hurt local sellers because they have already paid for them. China has already sold these products to us; they already have our money. Buy the products that have already come into the Indian markets.

India may impose restrictions on a select categories of products. Don't try to circumvent those restrictions. Support the strategic decisions. Question the decisions that you do not agree with, demand an explanation, but don't be a hypocrite by working around them.

If you are so inclined, put off your electronics upgrades. Our market is unlike China's; ours runs on demand and supply. If you do not show demand for these goods, our traders will not buy them. Same goes for any finished product imported from China. It will be a small contribution, but if that gives you any satisfaction, good enough. Those who cannot do without that new gadget can buy the pieces already in the market. What is more important is that you invest money in our economy, if you can. Apart from helping our local businesses with capital, it will also reduce Chinese investments and the exploitation of our firms.

I hope to be able to write about the last bit in more detail, in the coming days. Until then, keep calm, research more and buy smart.

---
title: "What Justice"
subtitle: "when sadism and voyeurism have taken over"
date: "2020-09-08"
description: "The death of the actor came as a shock to most, but today, has it all become more about eyeballs than brain food?"
tags:
- media
- journalism
- India
- bollywood
- politics
- economy
- covid-19
---

The number of Bollywood stars to die in three months was abnormally high in the second quarter of this year. Death of any human is sad. The circumstances of the death of Sushant Singh Rajput were salt to the wound.

I subscribe to _The Hindu_, and the story did not get much prominence. I wondered why, because the news of the death of Irrfan Khan and Rishi Kapoor had smacked us right in our face. The piece about Sushant's death also carried suicide helpline numbers. My eyes narrowed. Thoughts work fast. My brain instantly took me to the _Werther Effect_; I had read about it. I searched. And I found the WHO guidelines on reporting suicide[^1566402e], whose _Quick Reference_ reads:

[^1566402e]: [Preventing Suicide: A Resource for Media Professionals](https://www.who.int/mental_health/prevention/suicide/resource_media.pdf) (WHO)

> - Take the opportunity to educate the public about suicide
> - Avoid language which sensationalizes or normalizes suicide, or presents it as a solution to problems
> - Avoid prominent placement and undue repetition of stories about suicide
> - Avoid explicit description of the method used in a completed or attempted suicide
> - Avoid providing detailed information about the site of a completed or attempted suicide
> - Word headlines carefully
> - Exercise caution in using photographs or video footage
> - Take particular care in reporting celebrity suicides
> - Show due consideration for people bereaved by suicide
> - Provide information about where to seek help
> - Recognize that media professionals themselves may be affected by stories about suicide

A couple of days later, I read a piece in Newslaundry[^8ef4e3a7], which was a round-up of all news covering the death in those couple of days.

[^8ef4e3a7]: [Media most foul: How Sushant Singh Rajput’s death was turned into a spectacle](https://www.newslaundry.com/2020/06/16/media-most-foul-how-sushant-singh-rajputs-suicide-was-turned-into-a-spectacle) (Newslaundry)

And mainstream TV journalism took a plunge into the deep abyss of reality TV in the months that followed. It tapped the avenues in the abyss, which the reporting of the death of Aarushi Talvar had left untapped. TV newsrooms became the new reality TV---a closer-to-reality reality TV.

## Facts about the SSR case

But what actually happened in this case of death of Sushant Singh Rajput? What is the progress of the investigation? What details do we know about the case? Which of the details are facts in the haystack of rumours?

What about the 'super lobby'? What about all the allegations that Kangana put forth? What about the inconsistencies in the probe? Was Sushant depressed? Were pages in his diary torn to remove evidence? What does that imply? Why was the Mumbai police adamant on handling the case? Did the lobby go in hiding? Is Salman involved? Is Mahesh Bhat involved? What about Sooraj Pancholi?

Ah, Rhea! Did Rhea involve in black magic? Did she take 15 crore from his account? What about all the cover-ups that news channels exposed? The resort clincher and all? The lapses in probe? Sandip Ssingh? What about the ED angle? Are Rhea and Maharashtra government involved? Oh, what happened to Disha Salian? Was Sushant murdered by the 'Rhea Machinery'? Didn't the suicide theory crumble? It became a global movement. Someone who was passionate about his future could not commit suicide, right? Where did Dipesh Sawant go? How could a depressed man look so happy in those videos? What about the "explosive" Cooper Hospital footages? Did Rhea go into the Cooper mortuary? Sandip? Why did he call the ambulance driver? Almost nobody does that, come on! What about the eight hard disks and all other electronics?

Oh, what about the drug syndicate? Was Rhea not giving him Canabidiol? Was his flatmate not involved in all those shady activities? The boys had come the day Disha had died and left the day Rhea had left, correct? Oh wait, _netas_ were also involved along with Bollywood celebrities in the "murder". The CBI interrogated Rhea for ten hours; what came of it? Now that Miranda is in custody, will Rhea be next? Is the Maharashtra government targetting Kangana Ranaut for exposing the drug mafia and seeking justice for Sushant? The NCB has arrested Rhea; she must be part of the drug racket. What now, homicide angle again? Sushant's soul had a lot to say, didn't it?

It beats me: why did we not ask the soul who separated it from the body?

In all, this has been a case of cheap gossip. Someone pointed out:

{{< tweet user="PritishNandy" id="1303186514159960065" >}}

Law enforcement has its process. The agencies must follow rules when investigating a case. While we may all have grown up with CID and the likes, with daily soaps showing every household having a personal police officer to dance to the tunes of the household, the reality is different. You cannot go around arresting people according to the whims of the public. Is the offence established? What is the nature of the offence? Is it cognizable?

Did we educate ourselves before we went about running hashtags?

Or did we rely on Mr Goswami to give us the hashtags?

Out of curiosity, I looked at the list of debates in the series, and here is what I found:

{{< youtube 2mMA_XQXilQ >}}

Accompanied by psychedelic animations playing on loop, the high-pitched voices of so-called journalists rage on, piercing our ears and filling our living rooms; journalists who do not like to waste time on trivialities like the economy or the future of children, because, there are more pressing issues like actors' personal videos which---"one could conclude without any effort whatsoever"---was evidence of drug abuse. 'Oh, what is happening to our nation?!'

But, so that I do not become part of the click-bait gang, here is what the situation as of today is:

- The CBI is investigating the case. The ED, and the NCB are also part of the probe.
- Investigation is ongoing.

## Some reality

Let us get something straight:

1. Nepotism has always existed in Bollywood. And everywhere else. Whether you fight it or not, the market will make sure the untalented know their place.
2. In what is the 'lingo' of our generation, 'bounce' means 'to leave'. It has nothing to do with banking instruments.
3. _Hook-up, Marry, Kill_ is a stupid game. The jobless play it, the more jobless watch others play, and nobody connects it to real life.
4. Commercially available cannabidiol, although controlled, is legal in India. A no-brainer. And weed is not the sole source of it.
5. The police were wrong in not registering an FIR. This is not debatable.
6. You cannot talk to dead people's souls. Nobody can.
7. The press must also respect people's privacy. The press are not above the law. As per law, you cannot trespass on someone's private property. Irrespective of whom it belongs to---accused, convicted or whoever.
8. Of all the people making noise, a countable, miniscule minority perhaps do want justice for Sushant. To the rest, though, the issue is a milking cow. In its literal sense.
9. With all due respect to the dead, those using him are using him with no remorse. You are becoming part of this sadistic circus---the word is scavenging: feeding on the dead.
10. If you are a true patriot, you must care about your fellow citizens (notice the plural form). Millions of fellow citizens are suffering for reasons that are in complete control of those in power. Irrespective of party or ideology. This is not merely an act of God. If it were, God favoured a nation that claims there is no God.

Think about it for a moment.

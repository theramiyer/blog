---
title: "Farmer Protests"
subtitle: "The big picture"
date: 2020-12-13T12:30:54+05:30
description: |
  The farmers have been protesting the "farm reform" laws for months now. What we hear from the mainstream media is conspiracies, namecalling and discrediting of the protests. But why are the farmers protesting? Independent media on YouTube will give you a good picture, but here, we talk about arguments from both the sides. This post tells you why it makes sense for the farmers to protest, and why it makes sense for the government to introduce these laws. As for the view on how these were implemented, well.
tags:
- democracy
- business
- farming
- capitalism
- socialism
- food
- protest
- governance
- law
- debate
- politics
---

I have been following the farmer protest for weeks now, but did not pen down anything because I am not one to form opinions on such complex issues without first understanding the different facets of them---I am not that loudest Bollywood actor after all. I have a day job, and my weekends are busy with chores, virtual socialising (which includes reading WhatsApp messages), hobby coding, and experiments in the kitchen. I take my time to understand issues and form lasting opinions about them.

This post is not for the impatient or the short-sighted, nor is it for the prejudiced.

While the post will be long, I promise you that it packs answers to critical questions about the three farm laws. With this post, I try to help you understand issues that people (including the government) have talked little about. This will help you debate both, a _librāndu_ and a _sanghī_. I will take a non-partisan approach, because I have a nuanced take on this. Besides, going one side or another may lead you to lose sight of the larger picture. In this post, we do not look at the issue through a window; we go a full circle, zooming in and out as needed.

I am already through half of my Sunday, and with the amount of notes I have and the flu which would not allow me to sit in front of the computer for more than an hour at a time, it will be late night by the time I finish this post; a podcast episode is unlikely to come out today.

Here is what we cover:

{{< toc >}}

Before we go any further, let me mention that this is merely a compilation of the information put out by some bright minds and good media houses. While I cannot possibly list everyone, here are those that feature prominently:

- [Vivek Kaul](https://vivekkaul.com/)
- Nidhi Suresh and _[Newslaundry](https://www.newslaundry.com/)_
- Shekhar Gupta and _[ThePrint](https://theprint.in/)_
- [Prof. Ashok Gulati](https://indianexpress.com/profile/author/ashok-gulati/)
- Karan Thapar and _[The Wire](https://thewire.in/)_
- [Faye D'Souza](https://www.youtube.com/fayedsouza) and her team

## A brief about the three laws

If you remember, back in September, we had spoken about how the [Rajya Sabha had cleared agriculture bills]({{< ref "democracies-fail#opposition-to-balance" >}}) in the absence of the opposition parties; the opposition parties had "boycotted" the Rajya Sabha proceedings because of how the Speaker had cleared the three bills through voice vote amidst vocal opposition.

Thanks to Faye D'Souza{{< sidenote "what-are-farm-laws" >}}_Farmers Protest Explained_ (Faye D'Souza, [YouTube](https://www.youtube.com/watch?v=eVa-fzdqC6g)){{< /sidenote >}} and Meghnad{{< sidenote "farm-laws-nl" >}}_What are the Farm Bills and how will they affect farmers?_ (NL Cheatsheet, [YouTube](https://www.youtube.com/watch?v=s8EfuRDc2hw)){{< /sidenote >}} for the great layperson explainers on what these laws are. Without getting into the details, here is what they mean:

1. Farmers can sell their produce outside of the APMC _mandis_.
2. The private sector players can now enter into contract with farmers---before production---asking the farmers for a specific crop.
3. Anyone is now allowed to store---or stockpile---any amount of food (including essential commodities).

## The protests

The bills came into the public domain in perhaps August. Even before the bills got cleared by both the houses of the Parliament, the farmers have been out protesting. But these protests came out in the national TV media in November with the farmers’ _Dilli Chalo_ movement. Government agencies blocked the farmers from entering the State, and so, they are now stationed at the State border blocking the roads that lead to the national capital. They are sleeping in their tractor trailers and on the roads, despite the cold wave.

At one point they even faced water cannons and tear gas shelling. Some claimed that those protesting were _Khalistani_ separatists in the guise of farmers, and were anti-national elements. To "control violence", the agencies said, they had to use water cannons and tear gas. But this force was later withdrawn amid backlash.

In an agrarian culture, calling a farmer an anti-national element is like stabbing yourself in the chest. But what do you expect from politicians?

The leaders at the protest have categorically disallowed political parties from entering with their symbols or sharing the stage. There has been no instance of violence, no talks of separatism aside from unsubstantiated claims by the Godi Media™.

Godi Media
: Lapdog media; channels engaged in government propaganda

The farmers even made a statement of protest at a meeting with the Agriculture Minister, and other ministers and officials, by refusing the government-provided lunch; they instead ate the food brought to them from a _langar_ at the protest site, sitting on the floor, instead of the table.

## The one side

Now, we will look at what one of the sides---the farmers and the farmer bodies---says about these laws and their implementation. We start at one point, and link others as we go.

### Government claims

The Central government claims that there is a lot of corruption and leaning towards monopoly in the APMC system, which is affecting farmers. These laws remove the dependency on the _mandis_, and the farmers can now take home more money, which, they say, is in line with their policy to double the farmers’ income by 2022. To quote the government's stand:{{< sidenote sca1819 >}}Standing Committee on Agriculture (2018--2019) ([report](http://164.100.47.193/lsscommittee/Agriculture/16_Agriculture_62.pdf)){{< /sidenote >}}

> Commonly observed lacunae in implementation of APMC Act are as under—
> 
> (i)   Market Committees are reportedly democratic institution but in fact in most of the States, Chairperson and members are nominated or appointed and Committee is dominated by politicians and traders not by farmers as required;
> 
> (ii)   The provisions of the APMC Acts are not implemented in their true sense. For example, market fee and commission charges are legally to be levied on traders, however, same is collected from farmers by deducting the amount from farmers’ net proceed;
> 
> (iii)   In some of the States APMC Acts, market fee is levied even after trade-transaction taking place outside the market yard. Market fee is collected in some States even without actual trade-transaction has taken place and simply on landing the commodity at processing units. While in other States trade-transaction outside the market yard is illegal;
> 
> (iv)   Multiple licenses are required for trading in multiple APMC markets and also multiple time market fee on same commodity even within the State is collected; and
> 
> (v)   APMC Acts are highly restricted in promotion of multiple channels of marketing and competition in the system.

Another claim is that the _mandis_ were anyway inefficient, in that there are too little of them, and the cost the small farmer incurs to bring in his produce to the _mandi_ or take the produce back is enormous compared to what he would fetch from his produce. The farmers, although, fear that unregulated markets would spring up under these laws as opposed to the regulated APMC markets, which they feel is worse.

The government also wants to bring in what they call the "One Nation, One Market" system. The farmers say that through these Acts, the government would be making two markets: One under the APMC Acts and the other with the new Act. The farmers say that the large corporations will offer great prices for the produce for the first two or three years, and the farmers will sell the produce at the market created by the new Act. In a couple of years, the APMC markets will shut down because nobody would sell there at a lower price than what the corporations offer. The players that buy from APMC would avoid it in favour of the open market because the open market is tax-free, which is the final nail in the coffin. Once the APMC markets wither away, neither will the states get their taxes from here, nor would the farmers fetch much because, the competition having gone, the corporations would stop offering as good a price.

The government claims that MSP benefits a mere 6% of the farmers. The rest are either selling outside the APMC markets, or at these markets at a price lower than the MSP. The farmers say, it may be so, but again, the marketing system in Punjab and Haryana are among the best in the world, having sprung up during the Green Revolution. The farmer leaders say to the government, 'We have been telling you to develop such a good marketing system for our farmer brothers of the other states for a long time now, but you do this instead.'

The government claims that now, with these laws, the farmer would be free from the restriction of not being able to sell his produce outside of his State. The farmers tell us about the 1974 agitation in which they carried wheat sacks on their heads from Punjab to Himachal Pradesh. The police would arrest them for "wheat smuggling" and imprison them for forty days. About 1,450 farmers went to prison for this. But in 1976, the High Court of Punjab and Haryana, in response to a writ petition by the farmers, ordered that the government has no right to restrict where the farmer sells what he grows in his fields, and that one cannot call him a stockist for stocking his own produce. Now, if there were no restrictions to selling the produce across State borders, where is the question of "bringing" One Nation, One Market? 'The Janata Party government was the one to pass what was its first resolution in its term, in 1977, that the entire nation would be a single zone for agricultural produce. Farmers from Bihar sell paddy from Punjab's markets, Punjab's farmers sell potatoes in South India, farmers of Rajasthan sell vegetables in Punjab, everyone sells fruits everywhere, and there is no restriction; what is new with One Nation, One Market? We are already that.'

### The problem with the laws

First of all, hoarding. With the removal of limitations on how much of what one can stockpile, players who have enough resources and large enough stocks will be able to control the prices. In any market, the demand--supply ratio drives the prices. If players stock enough to starve the market of a certain produce, the prices will skyrocket. Similarly, if at a point where the prices should be fair to the producers, the large players can flood the market with the produce, and the prices would plummet.

Second, the Minimum Support Price (MSP): the farmers want a legal assurance of MSP with the new laws, for which the Central government says, 'There is no legal provision for MSP in the current system either. But we can give you a written assurance of MSP.' The farmers do not buy it, stating that MSP holds no meaning if the state does not buy from the farmers, through the APMC market. The farmers any way sell produce like maize at half the MSP. But there are farmers who depend on MSP for their livelihood, and not having a legal assurance while having private players in the game would spell a disaster to small farmers. This is because large farmers make a large profit from produce sold at MSP, while the small farmer _prevents loss_; without food security (where the State buys and distributes at a fair price), the small farmer will suffer (with the large corporations controlling the prices unmindful of anything other than corporate profit).

As per Vivek Kaul, the consumption of the small farmer matches his produce. While the large land-holding farmers will get affected the most, the small farmer would suffer as well.

Third, what I saw a small number of people talk about was the _Right to Constitutional Remedies_. With these laws, the farmers can approach a government official if they run into issues with the corporates or their contracts; _not a court_. Also, they cannot sue the government, a government office or the government officials, if they need redresal of their grievance with regard to these laws. Right to Constitutional Remedies is a fundamental right, by the way. The farmers ask how passing the power from a regulated system prone to corruption to an unregulated system prone to corruption (because of the involvement of large corporations) with no aid from the judicial framework is helpful to the farmers. The farmers say that the playing field is uneven, tipped in favour of large corporations that wield enormous power.

Agriculture is part of the State List in the Constitution. This means that the Parliament cannot pass laws on this subject. The Parliament in this case, instead, has passed these laws under the _Trade and Commerce_ section of the Concurrent List. The farmers claim that the farmer does not trade, and so, the laws should not be governing the farmer until he markets the produce. Marketing is again a State subject, with which the Central government cannot interfere.

The farmers also say that implementing these laws will have a ripple effect on the service providers (_ārthīyas_), their accountants, the labourers in those markets, and so on. Punjab and Haryana are the two states that stand to lose the most because they have wheat and paddy---two crops that the government buys at MSP---in huge surpluses already, and the government washing hands off of buying the produce would make the market prices of these two crash, driving thousands of farmers into poverty, apart from crashing the market itself.

Lastly, the farmers say that sections of the laws contradict themselves. An example they give is about a corporation entering into a contract with the farmer wherein the corporation gives the farmer the seeds, the fertilisers, the pesticides and some money. Say the produce failed because of a natural calamity. The corporations, one section says, cannot recover the loss as a piece of land, while another says they can.

### The trickiness of farm produce

Farming as an occupation is fickle. If you have a failed crop, you lose. If you have a large produce, you lose. This is because of how the market functions. Farming in a certain region is almost uniform across that region. For instance, farming conditions across Punjab, Haryana and northern Uttar Pradesh are common. This means that if a farmer has a large produce a certain year because of the climatic conditions at the time, every farmer in the region would have that kind of produce.

Now, they would flood the entire market when they bring their produce to it. This, by the rules of demand and supply, would drive down the prices, which means that the farmer will not make the kind of profit he expected, because _everyone_ had a large produce. The concept of MSP helps them here: If nobody buys their crop, the FCI would buy it at the MSP.

The farmers, even now, enter into contracts with sugar mills and others in the food processing industry. The farmers say that when the produce is surplus, the corporations bring out reasons not to buy the produce, like, 'Well, this potato is too small; we cannot buy it,' or complain about pesticide residue, whereas during times of shortages, they buy these potatoes. In other words, the farmer is at the receiving end.

In India, 86% farmers own less than two hectares of land {{< sidenote farm-land-size >}}_What the agriculture census shows about land holdings in India_ ([The Hindu](https://www.thehindu.com/sci-tech/agriculture/indian-farms-getting-smaller/article25113177.ece)){{< /sidenote >}} as per Agriculture Census (2015--16). These farmers cannot make a large profit. These farmers are dependent on the availability of tools, seeds, fertilisers, pesticides, farm equipment like tractors, etc., because they cannot afford to have their own. Given the low potential for profit and dependence on these factors, the non-availability of MSP would bring a lot of unknown in the life of the small farmer. While MSP itself was no profit, corporates arm-twisting them would make life worse for the farmers. Like one of the protesters said, 'The current system made us ill, the new system will land us in the ICU.' This is probably the answer to the usual Ms Loudmouth who said, 'If the existing system was so good that you are protesting the new one, why were there such a lot of farmer suicides?'

### The meetings

The farmers have had meetings with ministers and officials starting October. In the first meeting, the Minister of Agriculture did not turn up. The Secretary of Agriculture had come instead, to "educate" the farmers of these laws. The farmers’ leaders walked out of the meeting because the Central government's action of sending someone to talk about the virtue of the laws upset them; they wanted someone to listen to their concerns. They also did not like the undertones of the claim that someone must educate them about the laws. The farmers left a memorandum with the Secretary.

The farmers also debunked a government claim that they are in protest because they have not understood the laws. They broke down the laws to the points, and explained their understanding of the laws, which the government acknowledged as accurate. The farmers say that they have presented enough evidence to show that nobody had misguided them.

In the November meeting, the Minister of Agriculture was present along with other ministers. That meeting was unfruitful as well, because the ministers were "beating around the bush", and the leaders left a memorandum with the ministers. In the December meeting the leaders said, 'We have already given our demands in writing.' The ministers did not give a concrete answer to any of the claims made by the farmers. The meeting failed.

### Trust deficit

The one complaint the government has is that the farmers do not trust them. In fact, the one complaint that the ruling party has everywhere is that nobody trusts them---the previous most recent instance of this was in Bihar elections.

Naturally, journalists had the question: 'Why do you not trust the government?'

'Where do we even begin?' the farmers ask.

Keeping aside overnight implementations of major decisions with a four-hour notice window, the farmers are suspicious of the way the government passed these laws---remember the Rajya Sabha episode?

{{< youtube eIa6NC2Hbk0 105 128 >}}

Those who defend this move parrot Amitabh Kant's words, saying, that India is "too much of a democracy" for such "historic" reforms. While the Opposition trying to decelerate a reform is the norm in our country, such heavy-handed behaviour in even a lesser democracy is undemocratic. I will come to the point why the ruling parties feel so insecure in the end, but the farmers ask, 'If this was such a great reform which would improve our lives, why did the ruling party bring this in without so much as a consultation with our farmer bodies or have a civil discussion in the Rajya Sabha? The bill cleared the Lok Sabha because the ruling party has full majority; in Rajya Sabha where it does not, the party used its heavy-handed approach.'

The farmers claim that even in the last meeting, the ministers and the officials did not answer straight to the questions put forth---they went beating around the bush.

The farmers say, the Prime Minister and other ministers go around campaigning and speaking ill of the protesting farmers, but none of them has bothered to sit down and have a mutually respectful conversation around the laws---all they say is, 'The government is ready to have talks with the farmer bodies, but they try to "educate" us instead.'

The farmers say, 'The government is ready to give a written assurance of MSP, but are not ready to include it in the law. Why? Does it mean that they will do away with State procurement as the next stage?'

The farmers ask if the government had no bad intentions, why did they restrict the farmers from approaching the courts and direct them to government officials instead?

If you remember, this law came during the time of shortage of onions. The government banned exports of onions, in favour of local consumption. Back when the protests began, Vivek Kaul had written a piece,{{< sidenote export-onions >}}_Why Farmers Are Protesting Against Laws Which Will ‘Supposedly’ Help Them_ ([Vivek Kaul](https://vivekkaul.com/2020/09/21/why-farmers-are-protesting-against-laws-which-will-supposedly-help-them/)){{< /sidenote >}} where he said:

> The politicians suddenly want farmers to trust corporates and the market process, after spending decades abusing them. This is not going to happen suddenly, especially in an environment where there is big fear of large corporates taking over many other areas of business. All this is happening precisely at a time when the government has banned the export of onions. The messaging just isn’t right, given that if the government trusted the market process, it wouldn’t have banned the export of onions.

## The other side

But there is another side to the argument, like every other. We have looked at the farmers’ side of the issue, which is the Economic Left take on the issue. Valid, but one part of it. The other valid part is the Economic Right take on the issue.

The problem at the moment is, we do not have enough representatives of the Economic Right; instead, we have the Cultural Right, which is people tired of the previous regime, who think we need a "[benevolent dictator](https://twitter.com/kaul_vivek/status/1336339508258697219)", so that we have a New India. This is not necessarily a bunch that engages in a legitimate dialogue about issues---there are exceptions, of course, although they are countable. None of the politicians who have a large stage today in our country has been able to engage in a civil discussion without calling names or blaming the _sattar sāl_.

The regular media houses have continued to lower the level of debates showing full disregard for their audience's IQ.

The farmers who are protesting have a much higher ability to understand nuances than the stage-bearers give them credit for. These farmers are:

1. Tired of all dirty political gimmicks including name-calling.
2. Gutsy enough to publicly call out the propagandists.
3. Disciplined enough to give those in power the chills.

Having said that, here are some of the points the intellectuals in the Economic Right have put forth.

### The hot zones

No, I am not going to discuss the ordinance about stubble burning---you cannot charge the farmer for an issue you created. But back in the last year, while on the way to work, cruising on the NICE Road, I heard an episode of Cut The Clutter, wherein "Gupta Uncle" spoke about what he called the "underlying issue" of the smog problem of North India. He talks about India's surplus rice.

Wait, what?

Understand this. India is the largest exporter of rice{{< sidenote rice-export >}}_Amid Ladakh standoff, China turns to India for rice for first time in decades_ ([Hindustan Times](https://www.hindustantimes.com/business-news/china-buys-rice-from-india-for-first-time-in-decades-as-supplies-tighten-says-trade-officials/story-pveRQyJ9R2pD0fzo2oVSPP.html)){{< /sidenote >}} in the world. We export 12 million tonnes of rice every year. The problem with rice is that each kilogram of rice needs 5,000 litres of water, going by Prof. Ashok Gulati's words. And India is exporting so much rice because it must; we are producing so much. Why are we producing so much? Well.

Mr Gupta says, 'You get free water, free power (to pump out the water), and at harvest, Food Corporation of India buys the produce from you at a fixed price---easy crop.' He said, against the buffer stock need of 10.25 million tonnes of rice, India held (back then) 27.6 million tonnes.

In all this, we seem to miss that we are depleting the water table. The Punjab--Haryana region is a global hotspot for depleting water table.

Secondly, when the Punjab and the Haryana governments saw the issue of depleting water table, they introduced a law that forbade the farmers from sowing paddy during the dry months. Because of this law, the farmer could start growing paddy not before the beginning of monsoon. The idea was that monsoon would take care of the water requirements. But what instead happened is that the harvest of paddy went all the way to the time when the next sowing season was near. This compelled the farmer to get rid of the stubble in time, to which he responded by burning them, which created another problem: smog. The reason is, during this time of the year, there are no winds. The carbon particles remain suspended in the air for everyone to breathe (or rather, choke).

There is also the problem of too much urea and nitrates in the ground water of Punjab and Haryana, which are harmful for the states themselves.

Ecological sustainability is the first point to support the idea that Punjab and Haryana should stop cultivating paddy.

### Debt vs equity

Prof. Ashok Gulati, in an interview, speaks about surpluses. He says that the price of rice in India is higher than the rest of the world. We are already spending a lot of water to grow rice. When the rice grows in surplus, we must sell it outside our country. But if the price of rice is higher here than the international market price, nobody would buy from us. The government then has to subsidise rice, which is more loss to the government.

Prof. Gulati then adds, 'Rice, we are still competitive... wheat, we have become non-competitive.' If you thought surplus rice was bad, surplus wheat is worse. He also speaks about surplus sugar. In a Newslaundry video, Nidhi Suresh interviews a farmer leader, in which the leader talks about how bad the situation is already that some of the private sector sugar mills have not paid the farmers for the raw material. Prof. Gulati mentions the same point from a different perspective: because of the surplus, and the government's State Advised Price, the whole situation is a "mess" since the sugar mills are running out of money to pay. Also, the price the farmer must fetch for sugar is higher than the international price of sugar, which brings us to the same issue as rice.

In other words, because of the availability of resources at heavy subsidies to the farmer, the farmer chooses to grow paddy, wheat and sugarcane, leading to surplus with the Food Corporation of India, and non-payment of dues to farmers in other cases.

In the last section we saw the Food Corporation of India buys agricultural produce from the farmer at MSP. With the surpluses, this has led to FCI whose equity is about ₹5,000 crore, sit on a debt of ₹1,80,000 crore. Debt that is about _40 times_ its equity. This is loss of taxpayer money---your money, mine, and the farmer's. The Punjab and Haryana governments do not complain because the rural cess and the _mandi_ taxes fetch them thousands of crores of rupees.

Also, even after free distribution of rice and wheat during the pandemic situation last summer, the FCI still had a buffer stock of 97 million metric tonne of grain, against the buffer stock norm of 41.2 million metric tonne. That is the size of our surpluses.

### The history

An episode of _Cut The Clutter_ talks about the history of MSP. I will skip the Colonial aspect of it, but mention the part about the green revolution. Back in the sixties, we fell dangerously short of wheat. The United States stepped in with their PL-480 scheme and sent us wheat. The US government of the time also persuaded India to move away from "price control" and instead offer price support to incentivise farmers to grow more---the idea was, if you incentivise the farmer to grow more and stop restricting what he makes from the produce, he may produce more.

To assure the farmer that even if the market gets flooded with grain, the government will still buy it at least in such a way that the farmer does not incur a loss, the government introduced MSP.

The green revolution started in Punjab. First, the reason was the Punjab produced great wheat. Second, Punjab had the water. In ten years, India's wheat production doubled. Paddy has a similar story.

But as years passed and farmers of the other states learned the new techniques and got access to the resources that Punjab's farmers had, they also started growing paddy in surplus. Today, we are a rice surplus nation.

### 'At the cost of other crops'

Another observation is that farmers of Punjab and Haryana, because they have such a great support for their wheat and paddy crops, are _moving away from other crops_ that do not fetch them even the MSP---as observed before, crops like maize sell at half the MSP. Why would a farmer put in all the effort to grow maize when he does not fetch enough to make a solid return?

What happens, though, is that because the farmer is switching to wheat and rice for good returns, he is not growing other crops like oilseeds. A NITI Aayog paper points out:{{< sidenote niti-paper >}}_Raising Agricultural Productivity and Making Farming Remunerative for Farmers_ ([NITI Aayog](https://niti.gov.in/sites/default/files/2019-07/RAP3.pdf)){{< /sidenote >}}

> Even if India doubles its current level of oilseed production, the import dependence will remain at 40 per cent level. The situation is worse in pulses. Per capita intake and availability of pulses in the country has declined to two-third since early 1960s. During the 50 years between 1964--65 and 2014--15, per capita production of pulses declined from 25 kg to 13.6 kg. Even imports, which constitute about one-fifth of domestic demand, have failed to arrest decline in the availability of pulses in the country.

What is also pointed out is how the smaller farmers have diversified much more by growing fruits, vegetables, milk and others.

Also, the mentioned Cut The Clutter episode says that although Green Revolution started in Punjab (then Punjab and Haryana were one), the productivity has not gone up in the recent years, nor has the diversification increased. But now, Haryana has introduced an incentive of ₹7,000 per acre if they shift away from the thirsty paddy.

### The case of Bihar

In one of the videos of the protest (was it ScoopWhoop?) a farmer from Bihar, who has come in support of the Punjab farmers, says, 'Well, look what happened in Bihar. They abolished APMC markets, and now, we sell rice at a much lower price than the MSP. If abolishing state procurement was so good, why are we Bihar farmers suffering and not growing paddy in our own fields, and instead, coming here to work?'

The counter-argument that Mr Shekhar Gupta puts forth is that the excessive production of rice in Punjab is offsetting the price in Bihar. He says that the Food Corporation of India buys rice from Punjab and Haryana, and under the Food Security Act, sells the rice in states like Bihar at ₹2 a kilo. When rice is available from the FCI at that price, who would buy the locally grown rice at ten times the price from the market? The traders who sell rice in Bihar do not want to buy rice from Bihar because the end customer buys from the PDS; the local trader cannot sell much. Because the demand is low, the price of rice is low in Bihar. He says, West Bengal, Assam, Uttar Pradesh and Bihar are all grain-surplus states.

> The farmer's income, this way, cannot go up by more than 4% or 5%. The farmer is caught in a trap.

### The infrastructure problem

Prof. Gulati says (and I think Vivek Kaul agrees) that farming of today needs much more infrastructure to give a level playing field to every player including the small ones, and the government is incapable of providing it given its current capability to handle food. The food processing industry, though, is much larger than the government's agricultural infrastructure; the private sector has a much bigger capacity to build and handle the necessary infrastructure.

Also, in states other than Punjab and perhaps also Haryana, the private sector buys 90% of the produce from the farmers.

The question the professor asks is, 'Which is wiser: let the private sector set the bar higher by reducing the costs, or suppress the private sector and keep incurring losses?' His stand is that while the farmer may get benefited today, this model is not sustainable anymore, because of how much surplus we have everywhere.

### What about milk

86% of the farmer, as we note above, hold marginal or small land. The concern raised is that these small farmers cannot understand the complex contracts written by large corporations. The claim is that the corporations will exploit the farmers.

Prof. Gulati says, in his interview with Karan Thapar, 'Look at milk!'

His point is that even the smallest farmer who has as little as two litres of milk surplus, sells to large conglomerates like Nestlé. Private sector players like Nestlé and Hatsun have been buying from these farmers without exploiting them. He almost calls the idea that large corporations would exploit the farmers, unfounded. He says that the corporations build a long-term relationship with the farmers. The reason is that the corporations who want to see any profit, must invest in infrastructure. This is their initial investment. If they exploit the farmers and the farmers back away, they cannot even recover the initial investment.

He says, 'Aggregation is the key!' This was something that my father also suggested when we were talking about the protests at home, although he did not specifically mention the dairy co-operatives. Prof. Gulati says, 'Don't underestimate the small holder.'

He also mentions poultry as an example, which the private sector leads today. The farmers do not get exploited. Large scale exploitation is unsustainable.

### The curse of the Public Distribution System

Statistically, about 80 crore people depend on the National Food Security Act. But Prof. Gulati says that the government incurs a procurement-and-distribution cost of about 40% of what it gives the farmer in exchange for his produce, over the price of the produce itself. In other words, if the government buys a kilogram of some grain X for ₹10 from a farmer, the actual cost that the government incurs is ₹14 including the procurement and distribution. He says that the market can handle this in under 15%. To add to this, an NSSO report observes that about 46% of the grain and other farm produce sent by the Centre leaks away, and never reaches the end beneficiary, including losses from the leakage.

In other words, the government actually ends up spending ₹26 for a kilogram of grain X that reaches the end beneficiary.

### Transactional costs

In the last example, ₹16 are the transactional costs. In a surplus economy, where we are looking at the global market, we must reduce the transactional costs to stay competitive. At the moment, the situation is that the market price for the grain is about ₹18, while the government spends ₹26. If India wanted to sell this grain, it must do so at a loss of at least ₹8 for a kilogram of grain X---which makes the total cost as ₹34.

The professor also notes that the government loses about ₹90,000 crore because of lack of storage. This happens because the government is buying more than the need, spends in transactions, cannot sell it because of the price being less competitive, and since the government is unable to store the produce, it gets wasted. Mr Gupta in one of his Cut The Clutter episodes also notes that the government does not build more storage because the storage will fill up the coming year.

### Consultation with the State governments

Prof. Gulati says he does not buy the claim that nobody consulted with the State governments or the farmer bodies. He says that Mr Vajpayee introduced these reforms in 2003, and every political party has been promising one or the other form of these in their manifestos. But, he says, nobody took concrete actions on them. 'For how long will you keep discussing these reforms without implementing them?'

He says that the Central government still has consulted with the State governments, and even said that the Punjab government can levy the same taxes on purchases outside of APMC markets as that on the APMC purchases. But he also warns against the State governments going down that path, because the buyers will buy from States that charge the lowest tax.

## What now

We see that the concept of MSP was a necessary step at the time when we had scarcity of food grains. But that is perhaps now leading to a slow destruction in return for lower market risk. The times have changed from 1890s and 1960s. Also, we are no more a purely-socialistic economy. The world is not the same as that of the 1960s. We as a country have moved towards globalisation. We are not the protectionist economy of the past; we are a global market player of consequence. Because we are now part of the free market, we must drive our production _based on demand_.

While price support was necessary back when we introduced it---and it worked well at the time---we must now move towards _income support_ for the farmers to keep farming sustainable. Telangana, for instance, has done this. Other States followed soon after.

Of course, if Punjab or Haryana stopped producing wheat and paddy, it will affect the reserves, and so, it does not make sense to make a blanket order against growing wheat or paddy. The idea is to not overdo it.

This also means that the government must take account of how much of what it needs to replenish its reserves. The government must buy no more than this and, it could perhaps use the money that it used to buy surpluses with, towards reform packages or incentives so that the farmers diversify more and reduce our dependence on imports.

Having said this, the farmers’ anxiety is legitimate. There can be no debate here.

The track record of this government in implementation of ideas and reforms has been opaque, thoughtless and in general, abysmal. Most of the sweeping moves (fondly known as "masterstrokes") have ended up hurting more than benefiting.

While the general move towards capitalism is welcome, the regulatory framework plays a critical role in capitalistic economies. Governments of capitalistic economies have a solid framework of checks and balances. How are you checking unfair hoarding and price manipulation? How are you protecting the farmer from exploitation? How are you ensuring that the farmer does not lose his land, profession or dignity? What makes you think there will be no corruption with the new system; how do you ensure fair play?

This is not to say that the government should interfere with businesses or that the farmer should depend on the government for his life; the framework in which the businesses operate must be strong enough to balance the two parties. Building and maintaining this healthy business framework is the responsibility of the government as the powerful non-partisan entity. The farmers’ fear that the current government is favourable to the corporations is not unfounded.

Those governing must also understand that the first step towards trust is transparency. You cannot go about doing your own thing in stealth and ask everyone to trust you. Instead of pushing narratives, the government should have put some efforts into communication. In the right way. 'This is good for you; trust me.' is not the way to go in our country. There must be a 'because', followed by why you think your move is good. You must allow open debating. The problem is, in our country, we have created this environment of dirty politics where no thought gets through. Having said that, you cannot throw the '_Bāp se sawāl kartā hai?_' card at the citizens---you are not the _bāp_ to begin with.

And is this how you bring a reform---by steam-rolling? In a co-operative federal system, you do not slap people around; you balance power. The ruling party had a respectable politician with them; they should perhaps read up about him.

---

Update 05 February 2021: After looking at how the officials and the government agencies handled the farmer protests and how the officials tried to silence dissenting voices, it makes you wonder about the intentions.

Second, are we going in the right direction? No country is perfect. International media criticises everyone. If we have good intentions and value transparency, we would accept our error, apologise to those affected, and move on. This obsession over "the image" makes one assume absence of substance.

It makes one think there is malice.

Putting in place five or six layers of security systems and armed personnel, blocking or harassing journalists who tried to report, slapping irrelevant cases on them, cutting off Internet access, police threatening checking of social media accounts for "anti-national" content makes one feel that something is wrong.

What is wrong with us?

One tweet from a pop singer rattles the entire system? Filing FIR against an activist for a "toolkit" (which, technically, is a shared digital document)? How much more absurd is this going to get? Where are we going with this kind of behaviour?

Why such insecurity?

---

Now, to something interesting:

## Making informed decisions

People and parties in power make comments like 'too much democracy' because of the fear that anybody can sway people either way, because people are too ill-informed. Keeping you ill-informed makes lives of some easier. This is also the insecurity of the populist, who, unintentionally, is also questioning his / her own victory in elections, by questioning the intellect of people.

Be that as it may, outsourcing governance does not work. And there is no such thing as a "benevolent dictator". In a democracy, we must keep awake, because at the end of the day, good governance is also a citizen's responsibility. If we do not know what is good for us and we do not know what to ask for, we will keep hitting the roads in protest. Because sometimes, we must also gauge the intelligence and the intentions of those we vote into power---one way or another.

As for these farm laws, the government should man up, face the farmer and show some sincerity by speaking about the government's problem, and _offer_ solutions instead of imposing them. Remove all political middlemen---make it a straight communication; not from a citadel, but on the ground in front of the farmer. Because they are farmers. Show some respect---they are among those doing a thankless job. Also, citizens including our farmers, can understand the government's problems.

They are doing what they are doing because of a trust deficit. And one does not build trust this way. While the problem of surpluses is real, the government cannot overcome it by imposition.

Unlike the powers that be, the citizens have always been transparent and accommodating.

Also, now that we are talking about informed decisions, if you thought that the new laws affect no one other than the farmers of Punjab and Haryana, think again. True, farmers of other states have been dealing with private players. But these laws change the environment across the country, in a fundamental way, including preventing the farmers from going to the court.

Also, while true that the private dairy players have not exploited the farmers, know that dairy is a different ball game. Dairy has much more consumption than wheat and rice put together, and its general _value_ is higher than wheat and rice put together. The returns are different. The risks are different than agricultural produce such as grains, pulses, and vegetables.

> Past performance is not indicative of future returns.

I am not questioning the learned experts who have been in the field longer than I have been alive, but merely saying that nobody has addressed these queries on a public platform; the farmer is perhaps unaware of these points as well. Perhaps those in favour of this reform are right, perhaps the corporations will not exploit the farmers and indeed build a lasting positive relationship with the farmers. But is the government making such a field available? Who is talking about the nitty-gritties of this issue? Rather, why is the government not talking about them?

---
title: "Time to rethink print media"
date: "2020-10-02T10:28:16+0530"
tags:
- journalism
- propaganda
- business
---

Having stopped watching TV news years ago, I see those noisy faces is in meme content. While they are fun to watch in short spurts, they are also a constant reminder of what journalism has come down to in our country in this age. When Rahul Shivshankar said what he said, I heard him as meaning, 'Look, if you want news, read the morning newspaper.' (I would not take his suggestion on which newspaper to read, though.)

{{< youtube IEV-CsTBKPk 1568 1613 >}}

Regardless, for most of us who do not like all the masala and the drama, but care about the content, the newspaper has been the source of news. My family has trusted The Hindu for generations. We've held the daily in high regard.

{{< toc >}}

We temporarily paused our subscription of the print edition in March, in view of the pandemic situation, because some of the members of my family are in the "risk category", so to speak, despite claims that newspapers, in general, are safe[^cd6ef6b4]. I continue with the digital subscription, though. But yesterday, someone shared a couple of pictures in one of the groups I am part of. And that deeply disturbed me, threw me into denial for a while, and then went on to almost causing cognitive dissonance:

{{< figure src="https://blogfiles.ramiyer.me/2020/images/prc-day-ad.jpg" alt="National Day of PRC advertisement on The Hindu, pixelated on purpose." caption="National Day of PRC advertisement on The Hindu, pixelated on purpose." >}}

[^cd6ef6b4]: [Coronavirus | How safe are newspapers, WHO clarifies](https://www.thehindu.com/news/national/how-safe-are-newspapers-who-clarifies/article31156164.ece)

When I went out for a fact check, I could find no mentions of this in any of the reputed outlets. My denial continued. And then the member shared a picture clicked and sent to her by her parents who subscribe to the daily in Bangalore. I was defenceless.

## The business

While not the epitome of journalism by any scale, the newspaper has stood its ground against propaganda, shown spine and reported facts for what they are. No matter what, _The Hindu_ stands out in the heap of rubbish that outlets call newspapers today. The fact that the daily does not shy away from loads of advertisements has not gone well with me, despite rationally knowing that newspapers need their revenue to continue to put food on journalists’ tables, pay taxes and so on.

But here is the issue. Journalism is a business of criticism. I spoke to a/the Deputy Editor of Business Bureau of _The Hindu_ during a school debate programme where I had been as a judge. I'd asked him, 'Why does journalism have to be so negative?' He said, 'Well, there are a lot of people to talk about the good. There must be someone who watches everything and points out the bad to the public. Companies put out advertisements about all the good that they do, but who is going to point out where they slip, when they break the law or deviate far from ethics? They are never going to tell you. That is _our_ job.'

## The cost

My subscription to the print edition cost ₹160 a month or so. Meaning, I paid a mere ₹5.33 for a copy of the daily. While that may seem like a decent sum, let us remember that _The Hindu_ sends an average of 20 sheets a day, including the supplements. The quality of the paper used is good, and the printing is good as well. All this costs money. Add the logistics such as the transportation and the last mile delivery, including paying the agents, and you would know that ₹5 a day does not even come close to meeting the logistical demands.

They must resort to advertisements to cover their costs.

## The conflict

Journalism is the fourth pillar of democracy, because there must be someone who watches over the Legislature, the Executive and the Judiciary, because we can never know when the three may join hands, otherwise.

Now, let us break this down into points:

1. Advertisement revenue is directly proportional to readership.
2. The more the advertisements, the less the readers continue with the subscription, because advertisements, designed to grab attention, prove to be a distraction from the flow of reading.
3. During an economic slump, companies target advertisement expenses among the first to cut.
4. During an economic slump, government agencies manage to give advertisements. But would the government give advertisements to a newspaper that constantly criticises it?
5. In an age where narratives run the society, journalism swims upstream. That directly hits the readership, and thus, the revenue.

This is a vicious cycle. Companies try to find a balance in them. This is where _The Hindu_ slipped yesterday.

## The alternative

The online platform that _The Hindu_ runs is much cleaner. Because I pay for a subscription online, I do not see any advertisement on their platform---web as well as the app. Although I got it for cheaper, because I was an early bird (and took a long subscription at the time), the subscription today is a mere ₹1,199 a year. That is a small price to pay.

I can bet the number of online subscribers is nowhere close to the print subscribers. Print is still where the daily makes its money. The BBC is an example for a platform to not receive advertisement revenue, and run fully on public funding instead. In India, platforms like [Newslaundry](https://www.newslaundry.com/subscription) run on this model. In fact, _Newslaundry_ does not even monetise its YouTube videos. But again, perhaps thousands of people read or watch _Newslaundry_; a small number. Agencies like [The News Minute](https://www.thenewsminute.com/membership) and [ThePrint](https://theprint.in/subscribe/) are starting to lean towards charging for an ad-free experience.

I must say, though, that the model that _ThePrint_, _The News Minute_ and even _The Hindu_ have adopted is not the same as that of _Newslaundry_ or the BBC, because the former give what we call the "Freemium" experience, which still means that those who do not pay, will see ads on their platform. And these advertisers may have the leverage to drive narratives, or arm-twist these outlets.

## The summary

Propaganda is powerful. What _The Hindu_ did was outright atrocious, by running a full-page advertorial about the National Day of the People's Republic of China. Whether a patriot or a nationalist, I do not think any Indian sees China as a friendly State anymore, given the situation. My view is that China is hostile to India, playing its hard hand in geopolitics. In a globalised world, we cannot fully boycott an entity like China---that does not even make sense---but we could at least not spread their propaganda in our country, while we stand against propaganda in our own country. What _The Hindu_ did was show double standards, and their insensitivity aside, this action for sure makes me question their integrity.

I think _The Hindu_ should introspect, see what they stand for. You can push them to think if you wish to: [write a letter to the Editor](mailto:letters@thehindu.co.in?subject=Advertorial%20about%20the%20National%20Day%20of%20the%20People's%20Republic%20of%20China&body=Dear%20Editor%2C%0D%0A%0D%0AThis%20is%20in%20view%20of%20the%20advertorial%20you%20printed%20in%20your%20newspaper%2C%20dated%2001%20October%202020%20%E2%80%A6).

While rolling back what they did is impossible in our world, what they can do is apologise to readers, or at least, ensure this never happens again. Also, they must think of moving away from their current business model before readers move away from the outlet. The latter, apart from hitting their business, will also hit journalists who are true to their profession.

May good sense prevail.

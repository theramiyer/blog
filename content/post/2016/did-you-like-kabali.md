---
author: ram
date: "2016-07-30T23:49:00Z"
tags:
- opinion
title: Did you like Kabali?
---

It’s funny how people just assume that I’m obnoxious. No, I’m not trying to deny facts here, just stating that I’m not
that predictable. So a few days ago, thanks to a friend, I watched _Kabali_—probably the most-awaited Tamil movie of
2016. When I told people I’ve watched the movie, people (predictably) asked the question, ‘Well, did you like it?’ To
save myself from the trouble of unnecessary argument, I just used to answer depending on the tone of their question,
which, in most cases, was, ‘No’. It was partially true. I’m a person divided between two opinions, in most cases. My
final word is usually the stronger feeling out of the two—my mind cannot process absolutes for the most part. One good
thing about this is that no statement of mine is an absolute lie. Yes, you may chuckle, by all means.

Going by that logic, I did not like _Kabali_ in some ways at least. First of all, although, I would want to mention the
good thing about it: I absolutely loved the work that the marketing team did, in case of Kabali. However, the movie
turned out to be a Trojan. Trojan, in computer terms, is something that looks like something, but turns out to be
something else on the inside. So in that way, I was disappointed—nobody likes Trojans; except the ones who make them. So
yes, since I’d watched the trailer, I was disappointed with the movie, in that it turned out to be something I didn’t
expect. But that should’ve been a good thing in another way: The movie should’ve seemed full of surprises. No.

The story seemed random in some places, seemed too fast in some, seemed stagnant in the other areas… Overall, as a
story, I did not like the pace. The story was predictable, yet random. It seemed as though the writer just had a list of
dots, which he desperately wanted to connect, but didn’t know of a good way to do it. However, the dots themselves were
predictable.

The other point of disappointment was that, according to the trailer, I thought it was the typical Rajnikanth movie,
where there’s a lot of action involved. I instantly got _Badsha_ and _Padayappa_ in my head. But what to I get? Well, I
got what Kabali really is. I did not like the feeling at all. Especially for the kind of hype the movie got. Some might
say that it was my fault that I went in with that kind of an expectation, but hey, we all know how the human mind works.

But there certainly were things I liked about the movie. For starters, Rajni Sir’s acting. I never thought he could act
this way. He’s usually known for what we Tamilians call, _mokka comedy_, like what’s shown in Chandramukhi, and action
and style (think: Badsha). This was an entirely new face of his, and it was good. Just that it felt a little unusual.

Also, unlike the usual mindlessness, this movie had some reality to it—it was, at least, believable for the most part.
Perhaps the lack of mindlessness in the movie made it look a little bland. Yes, the crew could’ve concentrated on
building the characters and scenes a little more, but I wouldn’t call it bad work at all. It was good work, and maybe
the movie would’ve run longer, had they attempted to build the pieces perfectly. But overall, it wasn’t bad experience.

Would I watch the movie again? Maybe not. But would I suggest the movie to others? Yes. Although, I would tell them that
it isn’t the usual Rajnikanth movie, and one should brace themselves for the change in his persona. Also appreciable is
his appearance. And that’s another feather on the reality cap. And boy, does he look good! But yes, I still am going to
watch Badsha on Sun TV tomorrow evening. I think I’d like to see Rajni Sir as an action hero rather than the purely
sensible character. And I miss Raghuvaran—long since I saw him on the screen.

Cheers!
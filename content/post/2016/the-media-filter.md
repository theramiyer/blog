---
author: ram
date: "2016-05-10T01:48:00Z"
tags: null
title: The media filter
---

It’s been a while since I started thinking about the daily dose of negative that we receive as newspapers, or on news
channels. And this is not the first time I’m expressing my aversion to it. So yes, this doesn’t come as a big surprise
to many.

However, we all know that actions speak louder than words. And no matter how strongly we feel about something when we
express it, it is of no use unless we take action against it. But, controlling the Press isn’t something that is in my
power—or in the hands of most of us.

Last Sunday, my father picked up the paper, and read out the headlines from the front page. And then, out of
frustration, slammed the newspaper on the floor. Mind you, it’s not some random newspaper that we subscribe to—we
subscribe to the largest-selling English daily of South India. Most of you have, by now, probably guessed the name. Be
that as it may, I was unhappy.

> Update : As part of "growing up", I have come to realise the importance of journalism, and its [role in showing the negative]({{< ref "time-to-rethink-print-media.md#the-business" >}}). My stand on the issue has changed. I am glad this did not pick up (not that I tried beyond making this post anyway), and I hope this trend does not pick up either. The media must be a critic. We need it.

Immediately, pulling out my beloved notebook, I did some Google search on relevant terms—I wanted to create a custom
newspaper. Yes, too ambitious; too, too ambitious. But what had to be done, had to be done; and _someone_ had to do
this. But how do we get all the data? My favourite newspaper’s site came to the rescue. Yes, the daily has a high
standard with respect to the language, and everything, but the good stuff is, more often than not, buried among junk—the
junk usually constitutes about 60 – 70% of the content, excluding the ads.

I came to know about the service called [Paper.li](http://paper.li). But there was this catch: The good stuff offered by
them with granular controls comes at a price. They asked me for $9/month (or $99/year). With the free version, there’s
hardly any control.

For now, I’ve resorted to Flipboard with a [magazine](http://flip.it/5PE2l) that contains hand-picked, filtered,
necessary, good content. It would be amazing if people could pitch in to collect $99 for the year, so we could start the
newspaper, this time, fed by the Press Trust of India. It would be wonderful if anyone would like to contribute to it.
Send a message to my [Facebook page](https://www.facebook.com/iamramiyer) if you’d like to join the movement!

Rest assured, every contribution (monetary or otherwise) would be accounted for, and displayed publicly. And everything
would be done through online transfers.

What we get would be a clean, filtered newspaper, with no ads whatsoever.

Cheers!

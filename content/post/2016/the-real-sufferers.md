---
author: ram
date: "2016-09-13T04:25:00Z"
tags:
- opinion
title: The Real Sufferers
---

After a disturbing number of incidents of vehicles set ablaze and tourists beaten up, one really wonders how we call
ourselves a civilised society. As one of my friends rightly mentioned, ‘No wonder the British were able to “Divide and
Rule” us! #Cauvery’ The sad part about it is, we are so short-sighted that we just do things because we feel like,
whether or not it is going to have an effect on anything.

> No wonder the British were able to “Divide and Rule” us! #Cauvery
> ---A friend

Let’s get some perspective, though: what exactly is the issue at the moment, anyway? Let’s revisit some facts, and look
at where we _might be_ going wrong. And sure, what’s written here is purely my understanding of the issue and its
history. This information is available on Wikipedia, and is hopefully, trustworthy. Also, I’m going to steer clear of
statements such as, ‘The politicians do this because they own a considerable chunk of the water tanker business, so
since they want to earn money from it, they’re doing this.’

## A brief history

Source: Wikipedia

|Year|Milestone|
|--- |--- |
|1890|Conference is held between Mysore and the Madras Presidency, that Kaveri water would be used to develop both the states, without affecting anyone.|
|1892|Agreement is signed to implement what was discussed in 1890|
|1910|King of Mysore plans to build a dam that could hold 41.5 TMC of water.Madras refuses to consent to this stating they’ve planned to build an 80 TMC dam at Mettur.Government of India interferes, and permission is given to Mysore to proceed with the construction of the dam to hold 11 TMC. The foundation, though, is laid according to the 41 TMC plan. Madras feels a little insecure, and the dispute continues.|
|1914|Government of India upholds the decision and allows Mysore to construct the dam to hold 11 TMC of water.|
|1924|Mardas appeals against the decision. An agreement is arrived at, and the agreement is set to lapse in 50 years.|
|1956|State boundaries are drawn based on linguistic demographics. (The worst decision ever made by the Government of India.)Coorg becomes part of Mysore, Malabar goes to Kerala, and Puducherry remains a Union territory. Subsequently, Puduchery and Kerala jump into the issue: Kabini, a major tributary having now become part of Kerala, and Puducherry, where the river ends, having requested for availability of water for drinking and minor agricultural uses.|
|1972-73|The Cauvery Fact Finding Committee gets constituted. The committee submits a report.|
|1976|The Central government holds a discussion along with the two states, and a draft agreement, which also provides for the creation of the Cauvery Valley Authority, is accepted by both the states. The agreement is signed.|
|Late 1970s|Construction of Harangi begins; Tamil Nadu goes to court demanding the constitution of a Tribunal, under the Interstate Water Disputes Act (1956).Stoppage of construction is also demanded for.|
|1980s|Tamil Nadu withdraws the case, and the negotiation begins.|
|1986|A farmers’ association from Tanjavur moves the Supreme Court, demanding the constitution of a tribunal.Talks continue until 1990, yielding (surprise-surprise) no results.|
|1990|The Supreme Court directs the government to form a tribunal. The tribunal gets formed in June 1990. The states present their requirements: Karnataka claims 13 km3 of water, Kerala claims 2.83 km3 of water, Puducherry claims 0.3 km3 of water, and Tamil Nadu refers the agreements of 1892 and 1924, and asks the tribunal to ensure the flows are complying with the aforementioned agreements (16 km3 for Tamil Nadu and Puducherry, 5 km3 for Karnataka, and 0.1 km3 for Kerala).|
|1991|The tribunal gives an interim award on 25 June. The tribunal directs Karnataka not to increase the area of irrigated land. Karnataka issues an ordinance seeking to annul the award. The Supreme Court steps in and strikes down the ordinance and upholds the tribunal’s award.|
|1995-96|Monsoons fail badly. Karnataka is unable to release 0.85 km3, as asked by Tamil Nadu. The tribunal asks Karnataka to release 0.31 km3. Karnataka pleads against it. Tamil Nadu involves the Supreme Court, which asks the Prime Minister to intervene. Karnataka releases 0.16 km3 as per the Prime Minister’s decision.|
|1997|Government of India proposes setting up a Cauvery River Authority, which would have powers to even control the dams, if the Interim Order was not obeyed. Karnataka protests this.The Government does away with the dam control powers, and creates two bodies instead: The Cauvery River Authority, which has the executive powers, and the Cauvery Monitoring Committee which would be entrusted with the duty to assess the actual situation.|
|⋮||
|2016|No decision yet, situation continues.|

So that’s the last 120 years in a nutshell.

## Humanity takes precedence over politics

That’s pretty self-explanatory. Also, while the law is to be obeyed at all times, there are times when humanity takes
precedence over the law—and the legal system understands that. That is to say that sometimes, in a democracy, humanity
is chosen to be placed above the law, and then the law is changed to take into account the particular humanitarian
factor.

Also, let’s understand that we’re law-abiding citizens—you and I both. We’re not terrorists. Terrorists attack the
innocent; we don’t. So let’s stop beating up tourists from Karnataka visiting Tamil Nadu; let’s stop breaking
TN-registered vehicles, just because it’s a TN-registered vehicle. They are all innocent people, they have no role to
play in what decisions the governments take, other than their having _contributed towards_ the election of the leaders.

> They are all innocent people, they have no role to play in what decisions the governments take.
> 
> We’re law-abiding citizens … not terrorists.

## Probable further steps

1. Let’s not choose rivers and streams to dispose of effluents and other wastes. Investing a little bit into drainage
systems would be an option to consider.
2. Let’s invest a little on rainwater harvesting to naturally recharge the water bodies, including the underground water
table. It would reduce the dependency on rivers.
3. Let’s stop encroaching lakes. It may seem like a no-brainer, or otherwise, a stupid suggestion. But little drops
constitute an ocean—quite literally, in this case. Not encroaching on lakes would lead to use of local water sources,
and reduced dependency on river water.
4. Let’s stop selling Kaveri water as a commodity—as part of our construction projects. This has to do a lot with the
issue, starting from inflation to corruption.

## Why we haven’t found a solution to this

Tamil Nadu has a significantly weaker system of tributaries to Kaveri. Kaveri is primarily the water that flows down the
Ghats, including Kabini (which primarily flows through Kerala’s share of land). This situation of flatlands gives Tamil
Nadu a tough time consolidating all water that just flows down to the sea through tiny streams. Lack of consolidation
means lowered control. The worst part is, Tamil Nadu is geographically placed that way—they can do nothing about it.

There’s no way—no formula—to decide how the distribution should be, if there are bad monsoons. So, there’s no way to
actually stop Tamil Nadu from demanding an unfair share, or to object the decision of Karnataka when they are unfair to
the people of Tamil Nadu. This is a major point of failure with the current system.

All cities, which consume a major chunk of the water from Kaveri, have encroachments on areas formerly occupied by
lakes—lakes, which could store rain water. Since there’s no rainwater (and no water table because of exploitation),
cities have to depend on Kaveri water.

## Getting some perspective

It is nonsensical to hold on to a natural resource like some sort of identity. We did not create the river. We did not
buy the river. We don’t own it—nobody owns it. Would it be right if Madhya Pradesh fought with Meghalaya, stating that
the water that falls on Meghalaya is the water that is drawn by the Sun from the Arabian Sea and travels over Madhya
Pradesh, and hence, Meghalaya should return that water to Madhya Pradesh? Sure, that would be beneficial to both, but
does it make sense?

> It’s nonsensical to hold on to a natural resource like some sort of identity.

A river caters to everyone who can benefit from it. It is a lifeline to our food: farmers greatly depend on it. Water is
a basic necessity: we need it to drink, to wash … to live. It’s not some competition.

> Water is a basic necessity … not some competition.

It is an inhumane trade-off, to decide that a certain state should get “its share” of the river water, when the other
state’s people are struggling for water. Let me remove the ambiguity in it: if Karnataka didn’t have water to drink,
it’d be wrong if Tamil Nadu demanded for an amount of water that’s more than what Karnataka can provide. Let’s not lose
sight of the real meaning of “share”. Similarly, when Tamil Nadu doesn’t have water to drink, it is wrong on Karnataka’s
part to refuse release of water, stating that Karnataka’s farmers need it for what could, _comparatively_, be luxuries.
That’s not just inhuman to the people of Tamil Nadu, but is unfair to all of humanity.

## Descending into anarchy

> How is burning of … private property going to help anyone in anyway?

Everyone concerned has a problem, we all get it. But irrationality is only worsening the situation. How is the burning
of buses and vehicles of a certain state, which are private property, going to help anyone in any way? It only provokes
more and more people to do what makes no sense whatsoever, thereby snowballing the whole issue.

For example:

1. The Supreme Court declared something in favour of Maharashtra, and against the interests of Gujarat.
2. People of Gujarat began a protest.
3. Some MH-registered vehicles were attacked, and windshields were broken. The occupants were beaten.
4. Some in Shirdi, beat up some Gujarati pilgrims who had travelled to the place from Gujarat.
5. Because someone hit the Gujarati pilgrims in Shirdi, protests began in Ahmedabad, and 40 buses were burnt (which were
either MH-registered, or belonged to a Maharashtrian), lorries were burnt, shops were destroyed …
6. Because all this happened, a branch of Bank of Baroda was vandalised.

You see how none of this makes sense starting from point 3? What did the person travelling in the car have to do with
what the farmers need in Maharashtra, what the Maharashtrian _government_ decided, or what statement the Supreme Court
gave? For all you know, the person was a native of Gandhinagar, who had happened to buy the vehicle in Nashik. Or, he
was someone who’d travelled to Ahmedabad because of work, and then, had started to love Ahmedabad more than his hometown
in Maharashtra (it happens). You essentially drove someone away, with the idea that _Amdavadis_ are discriminative!

And how did the beating up of pilgrims in Rameshwaram make sense? Were they the ones who’d beaten up a certain
TN-vehicle-guy? How can people become so illogical? That’s not even collateral damage; it’s just random! And then it
only got worse. People vandalised a few branches of Adyar Ananda Bhavan, because it’s a Chennai-based chain of
restaurants. Hello, the people working there were natives of Bangalore or neighbouring towns! The same goes for
Karnataka Bank. How can people stoop to this level of stupidity?

> We’re harming our own home and our own kin.

Dear fellow _Indians_, let’s wake up. We’re harming our own home and our own kin—the home that is India, the kin that
are Indians—by indulging in irrationality.

Anyway, you should be all worked-up, now that you’ve read the whole post. So let me indulge you in a story.

I once owned a row-house—the fifth one from the eastern end of the street, and the twenty-first from the western. Two
flower vendors used to visit the street—one came from the eastern end, and the other from the western. One of them used
to sell roses, while the other sold jasmine. The sight of the rose vendor with a basket full of roses, entering from the
eastern end was lovely—who doesn’t love full baskets? The jasmine one, on the other hand, usually came with a half-empty
basket.

A few days later, I decided to set up a temple, and made a deal with the rose vendor that she’d sell half of her basket
of flowers to me, so I could offer the flowers to the Almighty. Curious enough, the man who owned the third house from
the western end of the street, also set up a temple. Now, by the time the jasmine vendor came to my house, only a
handful of flowers were left! I was disappointed with the vendor. I decided to teach the other temple owner a lesson. I
spoke to the rose vendor and asked her to sell me three-quarters of her flowers.

All was well for a few days. But then, the whole of the last week, the jasmine vendor did not turn up at all. Yesterday,
I spotted her in the market, and confronted her. I asked her why she never brought jasmine to the temple. She said that
her flowers got sold out by the time she was midway into the street. I suspect that the other temple guy has a role to
play in this. And I’m unhappy with him.

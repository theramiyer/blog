---
author: ram
date: "2016-03-13T23:19:00Z"
tags:
- opinion
title: Indifferent or grounded?
---

This is about those times when a side of me thought that I was heartless (it may well still be the case); it was one of
those times I hated myself and thought I was a hypocrite (spoiler: no, I’m not a hypocrite).

Let me give you the background first, and then we’ll go to a story.

### The background

I don’t give money to beggars on the road. I look them in the eye and deny them any offer. Some give me a disgusted
look, something like, “You’re a disgrace to humanity.” I respond to that with a blank stare. The first time I did that,
I didn’t recover from the shock I was in, upon thinking that I was stone-hearted. Of course, that was the irrational
side of me who thought that—the rational one was what made me do it in the first place. I went to the Café Coffee Day
outlet nearby and ordered myself a samosa-and-iced-tea combo, sat down to treat myself to it. Sugar and cold water calm
me down and make me think better. I ignored the taunts by my irrational self for the time being. It had said, “You can
spend a hundred bucks on a stupid samosa and iced tea, but you cannot give five bucks to a needy person. How do you
drink that in peace?”

I sat my irrational self down in front of the thought table of my rational side, to show it all the documentation that
my rational side used to come to that conclusion. It seemed a little convinced. It took a while for it to not raise its
voice when I denied a beggar anything. These were some of the major points put forth:

* You never know where the money is going. There may be a major conglomerate behind all the cash. How do we know these
people aren’t being exploited? Discouraging beggary would reduce the exploitation in the long run. I just did my part.
* It accounts for black money—there are millions of people who give away millions of rupees as alms, which never gets
accounted for. My irrational self put up a (surprisingly practical) counter-point, though: The money that you give to
doctors at the small clinics, most lawyers, and to all those shops who do not give you a tax invoice in return also
doesn’t get accounted for. Fair point. My rational side did not have an answer to it.
* How do we know the money we give as alms is actually being used for the good? How do we know the person isn’t drinking
his ass off, out of it?
* Saving the most relevant for the last, [the law of the land prohibits
beggary](http://dpal.kar.nic.in/.%5C27%20of%201975%20%28E%29.pdf).

A few days later, an incident followed.

### The story

My friend and I went to Decathlon with an intention to purchase a pair of shoes. On the way back, we decided to eat our
supper and then go home. We went to the _dosa_ stall outside Reliance Trends on the Kathriguppe Main Road. We had parked
in the street next to the Café Coffee Day, so we went back to the street to retrieve my friend’s bike.

A little girl, who didn’t seem more than ten years old, came to us selling flowers. My friend chuckled and said to her,
“Do we look like any of those who would buy flowers?”

“No, _annā_ (elder brother), I know. I just came to ask for books. I want to study, and I cannot buy books. Can you get
me some? Please?”

I was moved. Words didn’t come out. I just looked at her face in admiration.

Her face, I thought, was the same as that of any beggar that I’d seen throughout my life—it wasn’t much visible in the
very limited ambient light, but still. She had the look of helplessness, the yearning for kindness. The head tilt
accentuated it. It seemed practiced, but seemed genuine nonetheless. But I had to keep quiet. It was the end of the
month, and I had only ₹20 in my wallet at the time—I’d spent everything that the wallet had, on shoes, t-shirts and
stuff. I had forgotten my debit card in the backpack I carry to work, and I was carrying my wallet which just had my
credit card in it. I wanted to give her a pat on the back, take her to the nearest stationery store and buy her all the
books she wanted. But I was in a much worse situation than her (most of us are, perhaps).

“Yeah, I’ll get you some tomorrow. Now go. I have to rush home,” his voice was cold.

“_Annā_, please… I need books, I want to go to school.”

I swallowed. It was a little overwhelming, to be honest. But I couldn’t even make my presence known in the dark street,
with the neon light from the Levi’s store, from about a hundred feet away, being the only source of light. I kept quiet.
Yes, I was obviously being naïve to think I was invisible. She shifted her focus towards my face.

“_Annā_, could _you_ get me some books at least?”

“Umm… I don’t… have cash on me…” Funny how embarrassed you feel when you have to admit that you don’t have money, in
front of a beggar, even though it may be true that their financial status is way better than yours. A tangential
thought, does that make us bad people? I don’t disrespect them or anything, but still.

Madhu pitched in, and raised his voice, “Don’t you understand when I say I’d get you some tomorrow?”

The girl went away, mumbling something.

“Fascinating how this girl wants to study, no,” I asked.

Madhu guffawed. My face never lies—it is incapable of it (and I hate it); it held a confused look.

“You really think this girl wants to study?”
I nodded.
“You’re too innocent, bro. This is what happens when you’re stuck in front of a computer screen all the time.”

I needed an explanation. I kept quiet and maintained the expression on my face. I know rudimentary behavioural
psychology.

“You see, it’s all con. She doesn’t want books, she doesn’t want to study. She wants money. They take the books from us
and don’t even open the polythene laminate. They take them to some store or the other and sell them for half the marked
price. The shopkeeper buys them and resells them at the marked price. I’ve seen it happen in front of me. I don’t trust
these people. These guys are much more shrewd than you can imagine. If I give her our company-branded notepad that we
use for meetings, she’ll not accept it. Wanna see?”

The expression on my face had changed. My jaws had relaxed, slowly, as he spoke. And my eyes turned blank, an indication
that my brain was stuck in comprehension. It’s been over a thousand words, this post, so I should probably stop, but
I’ll give you a couple more incidents someday, which created the basis for my attitude towards these things—I hope I
have the patience.

Sticking to my principle to not _preach_, I wouldn’t ask you to stop encouraging beggary, even though in this case, I
would just be propagating the law of the land. However, I would like to make more and more people aware of this.

Nothing is what it seems to be.

I changed my attitude about myself—I decided that I was grounded, not indifferent, in this case; rationality is never at
the base of indifference.

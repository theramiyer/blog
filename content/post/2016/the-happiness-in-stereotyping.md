---
author: ram
date: "2016-01-17T18:39:00Z"
tags:
- opinion
title: The sweet smell of stereotypes
---

_Over 1700 words. A mug of coffee is recommended._

I grew up in a society where there were people of different kinds: different languages, different birthplaces, different
traditions, different cultures, different customs and so on. Until I was 18, I’d heard of a very small number of
stereotypical comments from friends because almost never did it happen that two of us were the same in all of the above
aspects (unless we were siblings).

And then I came to Chennai.

On the first day of college, I was asked by my classmates if I were a ‘North Indian’. I didn’t know what to make of it
(honestly). I asked them what they meant by that—that I grew up in a place on the northern side of India? There was a
general agreement to that the question was meant to be if I were a _native_ of the northern part of India.

‘Well, I was born here (in Chennai),’ I said.  
‘Really? What language do you speak?’  
‘I speak six languages.’ I was hell bent on not letting them categorise me on any basis whatsoever; true to the Indian
philosophy, I’m against such categorisation.  
‘OK, what language do you speak at home?’  
‘Depends…’  
‘On…?’  
‘…on whom I’m speaking to.’  
‘Really? All right, to your mum.’  
‘It’s a cross between Malayalam and Tamil.’  
‘So could we call it Tamil?’  
‘You could also call it Malayalam. I don’t really care.’

That conversation went on… tiringly for about 15 min, after which they gave up. Some thought I was a Gujarati, some
thought I was a TamBrahm (short for Tamil Brahmin). Well, I could be called both. And a Malayali because both my parents
hail from the same town in Kerala…

And then I moved to Bangalore after college, thanks to Cognizant.

‘What place are you from?’  
‘The last city I was in is Chennai.’  
‘Is that where you studied?’  
‘College, yes.’  
‘Before that?’  
‘Gujarat.’  
‘Do you speak Gujarati?’  
‘Of course I do, isn’t that kinda obvious?’  
‘OK… So you’re a Gujarati. But you’re from Chennai.’  
‘Well, I was born in Chennai.’  
‘What exactly are you getting at?’  
‘What exactly are _you_ getting at?’  
‘Oh my God, do you speak like this all the time?’  
‘I’m trying to answer your questions, but I’m unable to find the intent.’

‘Do you like Chennai?’  
‘Of course I do.’  
‘How?’  
‘_How?_’  
‘I mean how can you like a city where people are rude all the time, they never learn another language, they think theirs
is the greatest language, and oh my God, the weather!’  
‘Do you like Bangalore?’  
‘Of course, what’s there not to like?’

‘_How_,’ I wanted to ask. I laughed inwardly at the previous statement (except the weather part) that I got. I still do.
Why? Because I felt exactly the same way about Bangalore until the time I thought of giving life here a shot. And I’m
not kidding about this.

When I bought my first ticket, it was something like this,

‘_Elli?_’  
‘KR Puram.’  
‘KR Puram _elli_?’  
‘Umm… Bridge?’  
‘\*something I couldn’t make sense of\*’ except that I saw that the conductor gave a contemptuous scorn as if I were
some centipede stuck in his underwear.

‘_Kyā bolā unhōne?_’  
‘He said, “These people don’t know Kannada, but come to Karnataka and make our lives miserable.” Where are you from
anyway?’  
With a sigh, I said, ‘I don’t know. India?’  
My co-passenger laughed, ‘No, I mean…’  
‘All right, let’s say Chennai. Now, why?’  
‘Really? _Chennai_?’  
‘Why?’  
‘You don’t look like a Chennaiite.’  
‘Haha really? What do I look like? And why would you say something like that?’  
‘No, I mean… People of Chennai… Don’t take it the wrong way, but… you’re fairer than me! I thought you were some
North-Indian. I wouldn’t have gotten Chennai in a thousand guesses!’ He walked away without answering my why; his stop
had arrived.

And that wasn’t the only time I experienced (or witnessed) such an incident. The conductor was more than rude with the
way he grabbed the money and handed over the ticket with a grunt. In all honesty, that’s never happened to me in
Chennai. Well, it could’ve been because I never spoke to them in any language other than Tamil, but my point is that
there was absolutely no difference between the cities, when it came to treating people who didn’t speak the local
language.

People generally complained about Chennaiites insisting on speaking the local language—that people in Chennai insisted
that people learnt Tamil to interact with them. It happened to me in Bangalore too, Kannada in my case.

I went to a bakery and wanted to buy a loaf of bread, and asked, ‘Uncle, _ek packet bread dijiyegā_?’
‘Sorry, I won’t give it to you.’

I just looked him, eyebrows raised in amazement. I thought he was joking.

‘I’ll give it to you if you ask me in Kannada.’ Seeing me hunt for words, he said, ‘OK, I’ll give it to you even if you
promise to learn Kannada. Starting today.’  
‘OK… Uncle, bread _siguttā_? Is that right?’  
‘That’s correct, but do you _know_ Kannada?’  
‘No, but if you teach me, I’m willing to learn. I can understand a little; it’s been three months since I came here.’

He was overjoyed. And throughout the eight hours I spent there (an hour every day for eight days), he appreciated my
_āsakti_ (interest/enthusiasm) and commended the pace every time, stating it was amazing to see someone learn to speak
the language in less than two weeks. Well, in my defence (LOL), it’s that I had an understanding of the grammar and the
structure of six languages at the time, so I could just relate each aspect of Kannada to the other languages that I
knew, find a pattern, learn the prefixes and suffixes, and go BINGO. But again, I wanted to point out the insistence on
learning the local language.

Don’t get me wrong; I’m more than thankful to the Uncle for teaching me Kannada. I know an additional language because
of him. I feel at home here _primarily_ because of him. But I could mention many such instances, of different things
that happened over the last 65 months I spent and became a _localite_, although, that would be a long post. I (and you)
would instead prefer a (non-exhaustive) list of myths that I’ve recognised having lived in—or been to—multiple places
across the country:

## My favourite myths from around

> Chennaiites are rude.

It’s wrong to generalise, and is untrue in my case. People have treated me and my friends from here who visited the city
with as much respect as we could get in any given metro city. Chennai was no different—I’ve witnessed as many instances
of rude behaviour in Chennai as in Bangalore, Palakkad and Ahmedabad.

> Chennai has better roads than Bangalore.

Not everywhere. Yes, the major roads are pretty good in Chennai, but nothing in Chennai beats the NICE Road in
Bangalore.

> Chennaiites don’t know any language other than Tamil.

Wrong. English! Yes, English is a language too, and today, close to 60% understand Hindi, _if spoken at a low pace,_ and
the score is improving with the current generation. But in all fairness, Bangalore scores higher with the number of
speakers of the local language (as compared to Chennai), who know other languages—no denying that.

> Tamilians are the sole reason Hindi is not our national language.

Wrong. It was not even a week ago when I saw a photo by [Bengaluru
Memes](https://www.facebook.com/memebengaluru/photos/a.457192407803751.1073741832.150949535094708/457192491137076/?type=3&theater)
on Facebook, titled, ‘No, Hindi is NOT India’s national language’. Not that their stand is wrong—there should be
linguistic equality, but still.

> Tamil is a rough language, Kannada sounds much better…

…to people who understand and speak Kannada. And Tamil, just like any language, has many, many dialects, some of which
definitely sound very rough, but Tamil, just like any other Indian language, also has very sweet-sounding dialects.
Besides, nice-or-not-nice is a matter of taste, of personal opinion.

> Auto drivers cheat in Bangalore.

So do auto drivers in Chennai. And Ahmedabad. And Kochi. And New Delhi. And Chitradurga. And Palakkad. That’s what many
auto drivers do!

> Bangaloreans skip a shower here and there. Implication: Bangaloreans don’t care much about general personal hygiene.

Wrong. Being in Chennai (or Mumbai, or Vishakhapatnam), it is _mandatory_ that you shower at least once a day. The
weather here doesn’t _mandate_ that. But in general, I think the average number of people who skip a shower once in a
while within any region, in a given span of time is the same throughout the world.

> Bangaloreans are open-hearted and never complain about people coming here—that they’re changing cultures, and don’t
speak the local language.

If only the not complaining part were any true… Not all Bangaloreans are open-hearted. Besides, ‘Seriously… These people
come from outside and… Kannada isn’t even the most-used language in Bangalore anymore,’ is an oft-heard comment around
here.

> Kannadigas don’t step out of Karnataka (said by natives of Karnataka, as well as non-natives—can hence be taken in
both ways).

Wrong. I’ve had Kannadiga friends since school. Kannadigas are everywhere too. You just don’t know perhaps because they
too are referred to as _Madrasi_ or _Indian_ depending on the context.

> Tamilians don’t learn Kannada.

Wrong. It’s not about Tamilians or Punjabis, but about individuals. Perhaps some of the natives are angry that the rest
of India manages to enforce the mandate to learn the local language, but Bangalore by large, fails at it. If that’s the
case:

1. It’s wrong to use the open-hearted comment, because you then seem to be open-hearted just to pacify yourself, and not
out of choice.
2. It’s good! You can speak the listener’s language, be proud of it! It’s amazing to speak the language of the other
person in the conversation, and see him feel comfortable that he doesn’t have to change his language of preference. I
know the feeling. I experience it every day.

Also, I could personally point to many of my Tamil-speaking friends, who speak very good Kannada.

> Keralites, Tamilians and Andhraites create groups with people of their home state and never involve others.

Wrong again. _People_ create groups of _people who speak their language_—the confusion is perhaps because the basis of
division of India into states was language. And the creation of groups is again because they all have a common language
of preference! You’d think that never happens with Kannadigas, but you’d be wrong again. I know because my sister got a
comment in Chennai, when she went for a training there, ‘_Happā, Kannadigaru sikk’drappā!_’ :)

Borders of states are merely for counting the number of seats in the Parliament; language is merely a mode of
communication; birthplace is merely a government record—none of those things should become our identity. Let’s go beyond
these petty things such as language, birthplace, castes and race.

Let’s transform our country. Let’s appreciate our diversity; it’s our strength, let’s use it.

Jai Hind!

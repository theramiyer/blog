---
author: ram
date: "2016-07-03T22:31:00Z"
tags: 
- whatsapp university
title: The curious case of vision loss
---

Thanks to WhatsApp and our family groups, I came across a video by Zee News that claimed that you could lose your vision
because of smartphones. I was amazed. No, not by the piece of ‘news’, but by the level to which people can stoop to get
hits. I couldn’t help but laugh.

> If you don’t wanna feel weird, don’t do weird stuff

{{< youtube gMQ8MqwQHa8 >}}

Big News : Use of smartphones during night can lead to blindness

Let me state it upfront: It may be uncomfortable, or a little harmful, but not fatal—neither to you, nor to your vision.

Now to the technicalities of the report that I’m sure most aren’t going to read at [the
source](http://www.nejm.org/doi/full/10.1056/NEJMc1514294). This report that the news channel is talking about is a
phenomenon called _Transient Smartphone Blindness_. The women, in both the cases, used to look at their phones with one
eye, while the other eye was closed by the pillow. How does this make a difference, you ask?

Human eye (or perhaps the eye of any living being) is an amazing organ. For the sake of sanity, I’ll just stick to the
human eye in this post. If what the news clip (if at all it can be called that) says is true, then you also can become
blind by walking outside during daytime! All right, allow me to clarify.

When the ambient light is bright, or in other words, there’s a good amount of light hitting your eye, your pupils
constrict. The pupil is the small _hole_ created behind your cornea by the iris; the iris is the muscle in your eye,
which determines the colour of your eye. Its main responsibility is to control the light that goes into your eye. It
constricts (tightens) the pupil in bright light, and dilates (loosens) it in the dark. The irides in the two eyes are
independent. So when the right eye sees a lot of light and the left is in the dark, the pupil of your right eye is
constricted, and the left remains relaxed/dilated.

Also, the chemical compound in the retina of your right eye (which is exposed to bright light) gets _bleached_ while
that in the left doesn’t, because it is not seeing any light at all. The depletion of the chemical is what causes your
eye to send signals to the brain. Depletion due to bright light is called bleaching in technical terms. It happens when
you see any source of light directly, and then you turn towards the other side and see a _blind spot_. Try this with a
candle in the dark, you’ll know what I’m talking about. Don’t overdo it, though. You’ll again feel weird.

Now, when both the eyes are opened and exposed to the same amount of light after the
one-eye-in-the-dark-other-in-the-light situation, your eyes kind of _get confused_. You feel weird.

A lot of process goes on in your brain to make a single image out of the two separate images that the two eyes produced.
When the images received are of different brightness levels (because of the temporary bleach-out of the compound in your
retina in one eye and a healthy level of chemical in the other), your brain is unable to process the signals properly.
You feel you’ve been blinded.

However, as you may have already guessed, this situation is temporary. Although, it may take anywhere from a minute to
forty minutes for the chemical levels to return to normal (and equal) in both the eyes, depending on the duration the
one eye was exposed to brightness. But in any case, this situation is temporary. And for Heaven’s sakes, this is no
threat to your lives! But seriously, a round of applause to Zee News to have hit an all-new level of exaggeration!

Now, the verdict: As you’ve already read, if you don’t wanna feel weird, don’t do weird stuff. LOL.

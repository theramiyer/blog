---
author: ram
date: "2016-04-28T15:36:00Z"
tags: null
title: Those lovely, noisy machines…
---

Recently, my friend, Gazala, put up a picture on Instagram—a picture of her Kindle with the screen showing the lock image—the slugs of a typewriter—expressing her desire to get a typewriter for herself, someday.

{{< figure src="https://blogfiles.ramiyer.me/2018/images/typewriter.jpg" alt="Caption: Out of all the amazing lockscreens kindle has ,this is my most favorite! ! Some day , some day in life I am surely gonna own a fancyass typewriter, until then it's this screen I'd hold on to" caption="Caption: Out of all the amazing lockscreens kindle has ,this is my most favorite! ! Some day , some day in life I am surely gonna own a fancyass typewriter, until then it's this screen I'd hold on to" >}}

And the arse that I am, I posted a comment, ‘As a typographer, I consider a typewriter as the killer of typography. :p’

Don’t get me wrong; I love the typewriter. For one, it made communication much easier, because much less people had to read crappy handwriting. The oh, the beautiful forms that the typewriter creates on paper—the way the ink delicately crosses the line that it is supposed to stick to, and the subtle depression on the page, on every letterform… there are really no words to express the beauty of a work that a typewriter creates. Not to mention what a wonderful invention it is.

But, this is my peeve point with the typewriter: It did indeed kill much of the typography today. I am not talking about the professional works such as a book—those parts are thankfully untouched by this issue for the most part. My problem is with the kind of influence the typewriter has had on this era of computers and modern word processors. I’ll tell you why—and Gazala, here’s the explanation.

## Case I: The email that my junior once sent

I don’t know how he got the idea that the word processing engine, built into Microsoft Outlook 2010, could not automatically reflow text to the next line (Outlook has had text reflow ever since the 90s). He placed a line break wherever _he thought_ the recipient’s Outlook reading window would truncate the line. I know, that was really thoughtful, but I have no idea where he got that idea that Outlook could not handle text reflow, or how he thought he could guess the width of the recipient’s reading pane.

People break lines like this, because they’re influenced by the typewriter. I know that this was a very unusual case, and something that I’ve seen happen only once in the last six years of my career as an IT guy, but, this seemed heavily influenced by the typewriter.

## Case II: The double line breaks to separate paragraphs

I’m sure you can relate to this; you would’ve done this at least once (up to perhaps a million times) in your life. Tell me if you didn’t hit the Enter key twice when you’d finished a paragraph and wanted to start the next one.

This is wrong. And you know what? Even Google’s engineers haven’t been spared by these. What’s laughable in their case is that they _know_ the difference between the `br`, the `div`, and the `p` tags. And yet, they choose to code the Blogger Editor to use the `br` and the `div` tags in place of the `p` tag! Amazing! I’m yet to see the point in doing this, but it gives me an impression that they too have been influenced by the typewriter. And they’re killing typography more.

In Word, the space between paragraphs must be controlled using this property called, ‘Space between paragraphs’ that can be found in the Design tab of Word (and a similar menu in the other Office applications such as Outlook, that use the Word engine).

## Case III: The series of paragraph breaks to move the text to the next page

This particular one pisses me off. Pisses me off. So much that I want to pick up a jackhammer and blow off the computer that was used to do this. I know, I know most of us are not aware how else to go to the next page. Let me tell you: Hit Ctrl + Enter. Yes, it’s that simple. It puts in this character called a ‘Page break’ at the cursor position. Not comfortable with keyboard shortcuts? Go to the Insert tab on Word and click on ‘Page break’. That’s it, it’s _that_ simple.

What’s the problem if you hit Enter so many times to go to the next page, you ask? OK, so the next time you hit the Enter key to go to the next page, just go to the first paragraph of your document and change the font size by say two points. Now remove the second paragraph of the document—you can hit Ctrl + Z to undo it, so don’t worry.

Scroll down to the place where you hit the Enter key multiple times to reach the next page and tell me what the offset is. Messed up the formatting? Thank you for seeing the point.

## Case IV: Monospaced fonts for body text

Now this is not so much pissing off than laughable. I have a few friends who think Courier New is the best font to use to type. Guess why? Exactly, the output _looks like_ that of a typewriter. I concede; the output does look an awful lot like a typewriter’s, but is it a good thing?

You guessed it again: No, it isn’t. Courier New is a monospaced font. Typewriters used this kind of fonts because they couldn’t have the platen (the cylinder on which the paper rests) move the page horizontally _depending on the width of the character_, as the hammers impressed the characters. For instance, the letter ‘l’ uses roughly just one-fourth of the space the letter ‘m’ uses. But on a typewriter, ‘m’ and ‘l’ use the same amount of space. I’ll show you an example.

{{< raw >}}
<blockquote style="margin-bottom: 1em;">
<p>qwertyuiopasdfghjklzxcvbnm?</p>
<code style="font-family:Courier,'Courier New', monospace; margin0; background: none; padding: 0; color: black; font-size: 1em;">qwertyuiopasdfghjklzxcvbnm?</code>
</blockquote>
{{< /raw >}}

See the difference? Now let’s look at a paragraph of this kind of text:

{{< raw >}}
<blockquote style="margin-bottom: 1em;">
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

<code style="font-family:Courier,'Courier New', monospace; margin0; background: none; padding: 0; color: black; font-size: 1em;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</code>
</blockquote>
{{< /raw >}}

Is the latter is easier on the eyes to read? No? Now imagine the same for a page. Now three. Good, you got the point. Beauty is perspective, the definition changes from person-to-person. But it’s sort of a fact that text written in a proportional font (like this) is much easier to read than one in a monospaced font.

I guess I should stop here. Yes, bashing the typewriter has kind of become like a style statement for typographers, according to some, but we really can’t help it. It was an awesome invention, and one cannot thank the inventor enough for his contribution to the world, but what pisses us off is how it drove the word processors of today.

That’s not to say that I do not understand the idea that the makers of word processors of today wanted the transition to computers easy, back when they became available to the market, and people started ditching typewriters to switch to word processors. They had to be user-friendly.

My point is simple: Let’s move forward. We’re in the 21st century.

Cheers!

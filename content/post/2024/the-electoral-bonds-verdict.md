---
title: "The Electoral Bonds Verdict"
subtitle: a step in the right direction
date: 2024-02-19T21:21:35+05:30
description: In a nutshell, we look at the different aspects of the historic verdict on the Electoral Bonds by the Supreme Court of India.
tags:
draft: true
---

When I woke up on the 15th of February 2024, and picked up my e-Paper, what I saw on the front page got me sitting upright in under a second. 'What a great day to be alive', I thought. This was a milestone for my country, something that upheld the spirit of democracy. A lot of people were celebrating it because they heard others say that this is a historic verdict, and is good for us the _People of India_. I took it as a belated gift for the 75th Republic Day.

In this article, we will look at the different aspects of the Electoral Bonds, how they undermined democracy, how politicians unite across party lines to put in place whatever earns them money, how the different pillars of democracy are responsible to uphold the democratic values, and of course, what all this means for us.

I think this issue is a beautiful lesson in Civics, if you can appreciate the depth of this issue.

{{< toc >}}

Fasten your seatbelts as we embark on this journey to understanding _Electoral Bonds_. This article is the result of hours and hours of reading and watching and listening, and upon reading this, you will know everything there is to know about Electoral Bonds.

## Democracy in a nutshell

Democracy literally stands for "rule of the people". In effect, a democracy consists of three 'pillars':

1. The Legislature
2. The Executive
3. The Judiciary

The three pillars balance each other's power---also known as _separation of powers_---to ensure that at the end, it is the people whose rule is enforced. And above everybody (and everything) is the law.

### The Legislature

In simple terms, if you consider the Union of India (you are not that MLA from Tamil Nadu; hold your horses), the Legislature is the Parliament, specifically, the Lok Sabha (or, the House of [Representatives of] People) and the Rajya Sabha (or, the House of [Representatives of] States). The representatives that common people like you and I elect, become members of the Lok Sabha.

The members of the Rajya Sabha are not directly elected by us, but by the States that make up the Union of India (Union in the sense that India is not _divided_ into 28 states and 8 union territories, but 28 states and 8 union territories _unite_ to make India).

These are people from across different parties and states who make up the two Houses of the Parliament, and these are the people responsible to make laws. This is why they are also called 'Lawmakers'.

### The Executive

Again, simply put, think of the Executive as the 'cabinet'. The executive authority rests with the prime minister, with the president being the nominal head.

Typically, the Executive is concerned with running the administration. In other words, we can look at the cabinet as 'Policymakers'.

### The Judiciary

This wing is the _Guardian of the Law_ of the land. The authority of interpretation of the law of the land also lies with the Judiciary, with the Supreme Court of India at the apex (or top), headed by the _Chief Justice of India_.

## How a law is made

Before we understand what Electoral Bonds are, why they are problematic, and why the scheme was shot down, one critical aspect to know is how a law is made. Because if you know it, you will know how such a controversial system became a law in the first place. (And how legal shutting it down was.)

Any law begins its journey as a 'Bill'. This Bill gets introduced in the Lower House---or the Lok Sabha. The Bill is taken up for debates and discussions, and after the hundreds of minds applying their thought to the Bill and making any necessary modifications to it based on the several inputs, the Bill is put up for voting by the members of the Lok Sabha.

Of the members present in the House at the time of voting, if two-thirds vote in favour of the Bill, the Bill is then sent to the Upper House---or the Rajya Sabha.

The Bill goes through another set of discussions, from the States' perspective, before it is put up for voting again. Just as in the Lok Sabha, if two-thirds of the members present during voting at the Rajya Sabha vote in favour of the Bill, it goes for the president's assent.

The President may have their inputs for the Bill, and if they do, after consideration, they may send the Bill back without assent. In such a case, the Bill will again land at the Lok Sabha and go through the entire discussion and voting process based on inputs from the President (or otherwise). If the Bill gets cleared by both the Houses for the second time and it lands at the President's desk for assent, the President has no choice but to give their assent.

The Bill thus becomes law.

There are exceptions to this process, though. A Money Bill is one such exception. In case of a Money Bill, it goes straight from the Lok Sabha to the President. If the President sends it back, it comes back to the Lok Sabha, and if it gets cleared again, it goes straight to the President again, and this time, the President has no option but to give their assent. The Bill thus becomes a law, completely bypassing the Rajya Sabha.

The Speaker of the Lok Sabha gets to decide if a Bill can take the Money Bill route, if they feel it concerns taxation or appropriation of public funds, etc.

Here is the hook: Any Bills that the concerned feel is contentious and may not clear the Rajya Sabha scrutiny---especially if the Opposition has more seats in the Rajya Sabha---may get introduced as Money Bills (subject to fulfilling the Money Bill criteria, of course), and if the Speaker (of the Lok Sabha) is in favour of the Bill being considered a Money Bill, will get cleared without scrutiny by the Upper House.

The Bills that lay the ground work for Electoral Bonds were introduced as Money Bills (Finance Act 2016 and Finance Act 2017---the Acts that introduced the Annual Union Budget in those years).

## Fundamental Rights

Fundamental Rights are rights accorded to every citizen of India. Article 12 to 35 in the Constitution of India deal with the Fundamental Rights.

Right to Freedom of Expression is one such Fundamental Right, which, of course, has its 'reasonable restrictions'. These restrictions are defined in Article 19(2) of the Constitution. More importantly, nothing other than the reasonable restrictions defined under Article 19(2) can infringe on the right to freedom of expression of an individual.

Right to Information (RTI) is another right, but is not defined as a Fundamental Right. But since the RTI is implied in Article 19 (Right to Freedom of Expression and Speech), the Right to Information is an _implied_ Fundamental Right. This means, again, that Article 19(2) can be the sole control on the Right to Information. In other words, if the State wants to restrict the Right to Information in any case, the State's argument must make its case _solely_ based on the restrictions set in Article 19(2).

## The Reserve Bank of India and currency

The Reserve Bank of India (RBI) is our central bank, which is the ultimate authority for currency in India. India has two currencies: the Indian Rupee (INR) and the Indian e-Rupee (e&shy;INR); technically, the latter is merely the digital implementation of the former. Both of these currencies are fully controlled by the RBI.

Legally speaking, the RBI is the only body authorised to issue currency in India. This is another key aspect to be remembered in this case.

While the RBI does come under the Ministry of Finance, (Government of India), the RBI, at least on paper, enjoys autonomy.

## The Election Commission of India

The Election Commission of India (ECI) is a constitutional body, created to oversee and regulate elections in India. As a constitutional body, the Election Commission of India does not come under any ministry, and this, in principle, enables it to conduct free and fair elections.

Another body that enjoys autonomy.

## Laying the groundwork for Electoral Bonds

The basic framework necessary to enable Electoral Bonds was laid out in two phases, using two separate Money Bills that introduced the two Finance Acts: 2016 and 2017.

### Finance Act, 2016

Before we get into this, we need to understand a point or two about foreign contributions. _The Foreign Contribution (Regulation) Act_, also known as the FCRA, controls and governs the foreign contributions.

Political parties and politicians are generally prohibited from receiving foreign contributions, according to this Act (as defined in Section 4 of this Act). But here is an interesting story around it.

This dates back to 2010. A new _Foreign Contribution (Regulation) Act_ was enacted, which had some changes, supposedly to address the major shift in opening up our economy to the world that followed from the P V Narasimha Rao-led government of the early ʼ90s.

In 2014, the Delhi high court held the Bharatiya Janata Party (BJP) and the Congress guilty of violating the FCRA. Both the parties eventually filed what is called a Special Leave Petition, in the Supreme Court, as an appeal, to withhold actions laid out in the judgement of the high court.

The government decided to make amendments to the FCRA, allowing political parties to accept funding from the CSR fund of the companies in sectors where Foreign Direct Investment (FDI) is allowed.{{< sidenote tripathiGovernmentFCRATweak2015 >}}Government’s FCRA tweak plan to benefit BJP, Congress ([The Economic Times](https://economictimes.indiatimes.com/news/politics-and-nation/governments-fcra-tweak-plan-to-benefit-bjp-congress/articleshow/50259957.cms)){{< /sidenote >}} In other words, foreign companies with majority shares in Indian companies were allowed to make donations to political parties.

The Finance Bill 2016 brought in this amendment. This amendment ended up legitimising the donations the parties received in the past, which were so far in violation of the Act. And as it came into being, it repealed the _FCRA, 1976_.

The two parties then argued in the Supreme Court that the law states that foreign contributions of the nature that they have received is in line with the law now.

The Supreme Court, the court did not accept the parties' argument, as the petitioner had filed a case on donations received until 2009, which would come under the 1976 Act, not the 2010 Act. The Delhi high court judgement had also specifically noted that the case was being tried based on _FCRA 1976_.

### Finance Act, 2017

The Finance Bill of 2017 introduced the Electoral Bonds Scheme, by amending four laws:{{< sidenote "supremecourtobserverElectoralBonds" >}}Electoral Bonds ([Supreme Court Observer](https://www.scobserver.in/cases/association-for-democratic-reforms-electoral-bonds-case-background/)){{< /sidenote >}}

1. Representation of the People Act, 1951
2. Reserve Bank of India Act, 1934
3. Income Tax Act, 1961
4. Companies Act, 2013

The Amendment to the Income Tax Act was made such that the political parties did not have to keep a detailed record of the donations received via Electoral Bonds.

The Amendment to the Reserve Bank of India Act permitted the Union government to authorise "any scheduled bank to issue electoral bond(s)".

A Section of the Finance Act, by adding a proviso into the Representation of the People Act, exempted political parties from publishing contributions received through electoral bonds in their "Contribution Reports". Basically, as long as the political parties receive contributions up to ₹20,000, they don't have to disclose it in their report to the Election Commission of India. Any contributions received in excess of ₹20,000 has to go in these reports, so that the Election Commission of India is aware of which political party received how much from whom.

The Amendment to the Companies Act removed the upper limit on how much can be donated to political parties. Until then, companies could donate up to 7.5% of their profits (averaged over 3 years) to the political parties. This Amendment removed that limit.

### Finance Act, 2018

For completeness, let us also look at what the Finance Act, 2018 did to the FCRA. Here is the story so far:

1. The 1976 Act did not allow political parties to receive foreign funding.
2. A case is filed against two major political parties where the petitioner states that the parties violated the law in receiving foreign funding until 2009.
3. The 2010 Act had been introduced to make modifications to some of the provisions of the 1976 Act. This effectively repealed the 1976 Act, which is explicitly mentioned in the new Act.
4. The case continues beyond the point, but as the Delhi high court specifically points out, the case is being tried based on the 1976 Act.
5. The Parliament, through the Finance Bill 2016, makes an amendment, but it can only make an amendment to the 2010 Act as the 1976 Act is repealed.
6. The case continues, because, again, the case is being tried based on the 1976 Act.

The day was in March 2018, as Daksh Tyagi writes with a tinge of cheekiness in his book, _A Nation of Idiots_ (which I happened to read to post a review on Meraki Post). The same day as the Allahabad high court was supposed to deliver a verdict on the Ayodhya issue. The commercial news channels were covering that at a fever pitch. Politicians were protesting outside the Parliament.

Within the halls of the Parliament, a law was being re-written.

The Parliament, on that day, make an amendment to the Finance Act of 2016, stating that the amendment is to the 1976 Act instead of the 2010 Act.{{< sidenote "chhokarChronologySubterfugeAmending" >}}The Chronology of Subterfuge on Amending the FCRA ([The Wire](https://thewire.in/government/the-chronology-of-subterfuge-on-amending-fcra-and-the-omerta-code)){{< /sidenote >}} But this is different from the issue of Electoral Bonds, so, we will not go into the technicalities of this issue in this article.

## The case for Electoral Bonds

Understanding the motive behind any such major scheme is important before coming to conclusions. In case there are legitimate points making us consider a scheme, they should be taken into account. At the same time, while the motive has been found debatable (and for obvious reasons), there are some legitimate points in favour of schemes being introduced to fund political parties.

Arun Jaitley, the Finance Minister of the NDA-1 government, was the prime mover of this scheme. He, in his Budget speech of 2017 introduced this scheme, calling it legitimate and transparent{{< sidenote "ptiJourneyElectoralBonds2024" >}}Journey of Electoral Bonds: Arun Jaitley defended anonymity ([ThePrint](https://theprint.in/economy/journey-of-electoral-bonds-arun-jaitley-defended-anonymity/1967776/)){{< /sidenote >}} In the speech, he'd said, the Electoral Bonds Scheme:

> envisages total clean money and substantial transparency coming into the system of political funding.

His points were:

1. One could only purchase electoral bonds from banks using banking instruments (and not cash).
2. The amount of money used to purchase electoral bonds will have to be disclosed in their accounts ledger.
3. The life of the bonds will only be 15 days.
4. The bond could only be redeemed into a pre-disclosed account of a registered political party.
5. Every party would have to declare the total amount of money received through electoral bonds in their returns.

According to him, the donor anonymity was at the core of the scheme. He said that if the donor was forced to disclose their identity, they may fall back to cash for political funding and the issue of black money would return.

And in the typical way of a member of the Cabinet, he said people merely found faults in the scheme and did not come up with alternatives.

However, he was right in saying that electoral bonds ensured that only "clean" taxed money would go into electoral funding via electoral bonds, through proper banking channels.

Interestingly, though, the same Arun Jaitley had moved a Bill back when he was the Law Minister in the Atal Bihari Vajpayee-led government, legitimising donations to political parties made via cheque. He even incentivised it in that the donor could calculate these donations as deductible when calculating Income Tax.

Pranab Mukherjee, as the Finance Minister in 2010, brought out a second reform, by creating what are called "Political Trusts", which would receive money from donors and donate to political parties.

But according to Mr Jaitley, that was still not good enough, as electoral trusts only "masked" the identities of the donors, and did not give them the privacy the electoral bonds did. (And I agree with him on that. But more on this later.)

## The case against Electoral Bonds

In short, the _Association for Democratic Reforms_ (the same NGO that filed a case against the BJP and the INC for violation of the FCRA), _Common Cause_, the _Communist Party of India (Marxist)_ and an MP from the Congress, filed petitions in the Supreme Court against the introduction of the Electoral Bonds Scheme as well as challenging the use of the Finance Bill for such changes to the law.

This came within months of the government notifying the Electoral Bonds Scheme. The petitioners also alleged that the scheme was opaque.

The issue raised was that the identity of the buyer of these bonds were anonymous to everyone but the State Bank of India (SBI). The SBI, as it is owned by the Government of India, is liable to disclose the details of who bought bonds for how much and who redeemed the bonds, to the Government of India, if or when asked.

By extension, this meant that the Government of India could easily know who these buyers and redeemers were, and the party(ies) running the government could easily use this information in ways that they see fit. In other words, the buyer anonymity was guarded from everyone except the SBI and the GoI. In turn, this could mean that the buyers would be careful buying bonds for the parties in Opposition, and those not exactly in good terms with the party(ies) at the Centre. It unfairly tipped the balance against the parties in the Opposition, which is inherently detrimental to our democracy.

But there is more.

### The Election Commission's stand

The Election Commission of India filed an affidavit opposing the EBS. The affidavit said that the ECI had informed the government about the repercussions of the transparency of political funding.

The second issue they raised was about the foreign contributions in the dark, meaning, large, unchecked political funding by foreign companies could influence decisions in the country, potentially against the national interest ("national interest" was a significant point in the _FCRA, 2010_).

In 2017, the ECI had written to the Ministry of Law and Justice, stating that given the contributions via electoral bonds were not reported in detail to the ECI, there was no way of ascertaining who the buyer of these bonds was.

Also, the Representation of the People Act prohibits donations to political parties from government and foreign companies. In the absence of information about the "originator" of the bond, there was no way of knowing whether a certain contribution was received from a government or a foreign company.

The ECI raised the issue of removal of the cap on donations by companies as well. The ECI said this opened up doors for shell companies to come up, leading to black money being funnelled into the system. It said that the purpose of having the cap on the donation meant that only profitable companies could donate to political parties, thereby addressing the issue of shell companies and black money.

### The Reserve Bank of India's stand

The Reserve Bank of India, another respondent in the case, also was against the EBS. Essentially, the RBI did not want what it called "non-sovereign entities" such as scheduled banks to issue bearer instruments.

To break it down, an electoral bond is essentially a bearer instrument, in that whoever carries the bond to the SBI to redeem, provided they were depositing the bond into the account of a registered political party. Meaning, even me, as not a member of an XYZ political party, could receive the bond on their behalf, and deposit into the account of the XYZ political party. Why is that a problem? Because the electoral bond can change hands between multiple people and/or organisations from the time it is bought and the time it is redeemed. The people and organisations involved in these transactions can effectively use these bonds as currency, though intermediate, to handle any transactions until the point the bond is redeemed by a political party. The buyer of the bond does not necessarily have to buy the bond with a political party in mind.

This, apart from opening doors for potential subversive actions, also undermines the sovereignty of the currency, Indian Rupee---and by extension, the Reserve Bank of India that controls it.

The RBI also said that there was no need for a separate instrument for political funding, as cheques, electronic transfers and other banking instruments were already available for it.

### Exchange between the Reserve Bank of India and the Government of India

The government was quick to react to these statements of the RBI, saying it failed to understand the core purpose of electoral bonds, which was anonymity. They also said that the issue of these becoming currency was "unfounded" as these bonds only had a limited validity.

The RBI then suggested that the validity be set at 15 days for the bonds with mandatory KYC compliance, but did not want commercial banks to issue these bonds. Instead, the RBI said it would issue the bonds from the Mumbai branch.

The government seems to have ignored these suggestions when they circulated the scheme again, asking for comments from the RBI.

The RBI then flagged another issue, which was issuing these bonds in "scrip" form. It instead suggested use of electronic methods, so that the trail of transfer could be established. Because in the current form, only the buyer was known, and any change of hands that the bonds may have gone through, were not known. This led to the issue of these bonds being potentially used for money laundering as well.{{< sidenote "jainCredibilityTransparencyWhy2024" >}}‘Credibility, transparency’ — why RBI & Election Commission had opposed electoral bonds scheme in 2017 ([ThePrint](https://theprint.in/politics/credibility-transparency-why-rbi-election-commission-had-opposed-electoral-bonds-scheme-in-2017/1968459/)){{< /sidenote >}}

### Timeline of Supreme Court hearings

The first direction from the Supreme Court came in April 2019, when the Bench led by Chief Justice Ranjan Gogoi directed all political parties to submit details of donations, donors and bank accounts to the ECI in a _sealed cover_. The Bench did not issue a stay on the scheme, stating that such weighty issues needed in-depth hearing.

The petitioners approached the Court at least thrice after that, right before some state elections and rounds of sales. In 2021, a Bench led by CJI S.A. Bobde again denied a stay on the scheme. They held that the “apprehension that foreign corporate houses may buy the bonds and attempt to influence the electoral process in the country, is … misconceived.”

Despite the explicit firm discouragement received from the Supreme Court Bench in the 2021 instance, the petitioners approached the Supreme Court in October 2023. This is when the Bench led by CJI D.Y. Chandrachud referred the issue to a five-judge Constitutional Bench.

The result of hearings by this Constitutional Bench is the scrapping of the Electoral Bonds Scheme.

## How our democracy works

Before we understand the gravity of the Judgement, we should know how exactly our democracy works. All that most of us seem to care about is that we vote for our _representatives_ (not "leaders").

### Members of Legislative Assembly and Parliament

We all know Members of Legislative Assembly (MLA) and Members of Parliament ({{< smallcaps "MP" >}}). An MLA is a member of the State Legislature, who represents you (your constituency) at the State level. An {{< smallcaps "MP" >}} is a member of the Lok Sabha or a Rajya Sabha, which are the two houses of the Parliament of India, representing you at the Union level (or "National" level, if you find that more comfortable).

### Lawmaking

The Constitution has Subjects (things that concern the life of a citizen and the country), such as education, healthcare, defence, foreign relations, etc. These Subjects are divided into three lists:

1. The State List
2. The Union List
3. The Concurrent List

The Subjects in the State List are fully governed by the State government, and the State Legislature makes laws on these subjects.

The Subjects in the Union List are fully governed by the Union government, and the Parliament makes laws on these subjects.

The Subjects in the Concurrent List are governed both by the State and the Union governments, and both, the State Legislative Assemblies as well as the Parliament make laws related to them.

When you elect a representative for the State Legislative Assembly, you are also indirectly contributing to the election in the Parliament, in that these MLAs that you elect are the ones who elect the {{< smallcaps "MPs" >}} at the Rajya Sabha.

Which is why both, the State and the General elections are both important.

### Representation of people

Ours is a representative democracy. This is given the size of India (in terms of the varied people, the vast areas, the diversity in terrain, climates, etc., the varied cultures and traditions, the languages, and so on). In this system, we send representatives to the Legislatures from our localities, which are defined as "constituencies". These are the people we elect, and these members we elect are elected so that they represent us at the State and the Union levels.

This makes the job of an MLA a lot different from that of an MP.

## Who received funding

---
author: ram
date: "2017-06-03T18:09:00Z"
tags:
- opinion
- casual psychology
title: Keep the change
---

Every Facebook frequenter (at least in India) comes across this post every now and then, which bashes our government for
being hostile to the farmers. It goes something like this:

> At the vegetable market in the morning:
>
> ‘How much is that cabbage?’  
> ‘Twenty rupees a kilo.’  
> ‘That’s too expensive; how about I give you fifteen?’
>
> At Domino’s for lunch with colleagues:
>
> ‘How much is a medium pizza?’  
> ‘That would be two hundred and eighty-five rupees.’  
> ‘Here, three hundred; keep the change.’
>
> Status update on Facebook later that evening:
>
> ‘Farmers are dying. They are not getting a decent price for their crops. What’s the government doing?’

As dramatised as it may sound, or as much as some of us may like to deny this, _it is a valid problem_. And we all have
done something like this at least once in our lifetime. But is it really something wrong? Or is there something more
fundamental here that we honestly don’t realise?

As it turns out, this behaviour, if not anything, only makes us more human. In other words, that’s how we’re wired to
behave—it would be surprising only if we didn’t do something like this. I’m exactly the kind of guy mentioned above.
Well, not literally with farmers and all, but in general.

Amazon made me realise this behaviour several months ago. At the time, I was looking to buy a thirty-two-gig pen drive.
The price mentioned was some four hundred rupees. Upon that, Amazon said there would be another forty rupees added as
shipping charges. Refusing to pay so much for shipping, I chose not to buy it online, went to a local store, and bought
it for the same four hundred rupees. All was well.

A couple of months later, I wanted to change my phone. I liked one which was priced at some seventeen thousand rupees.
Happily, I added it to the cart and proceeded to checkout. At checkout, Amazon gave me an option to choose “expedited
shipping” at a hundred-and-fifty rupees or so, as opposed to free “standard” shipping. I chose the former. Well, who
would want to wait a day to get a phone?

After I was done ordering, I thought about what I’d just done. I was reluctant to pay forty rupees for shipping a pen
drive a couple of months ago, but was more than happy to shell four times the money for expedited shipping (which, when
I think of it, wasn’t even _necessary_). Why would someone do something like that?

We humans, as it turns out, cannot work on absolutes. We work on comparative values. In some ways, yes, it is
unfortunate; others, it is a great way to calculate. This nature forms the basis of a lot of our psychology. It decides
whom we talk to, how we buy things, how we build things, how we choose our partner, how we go about doing many of the
things we do on a daily basis and so on.

While one could write an entire book on this behaviour, my goal at the moment is to try to throw some light on this
particular phenomenon.

When Amazon said there would be a shipping charge of forty rupees, I decided not to go for it because it was 10% of the
total cost of my purchase. I was OK with a hundred-and-fifty rupees because it was less than 1% of the purchase. When I
was presented with the charges, my mind quickly ran a calculation, and in the case of the phone, said, ‘Ah, I’m spending
seventeen grand. Might as well add another 150 and get it sooner.’

In the case of the pizza, we don’t really see the fifteen bucks as fifteen bucks, but as 2% of the total cost. We don’t
realise that the money could actually buy us a cabbage. We simply don’t see it that way. And that’s not because of
capitalism or materialism or any of the -isms—that’s just how we are. We just can’t help it.

So the next time someone bargains at the market, or says, ‘Keep the change’ to the waiter or the driver, please don’t
frown at them. They’re only being human.

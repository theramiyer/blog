---
title: "Remote work"
subtitle: "and the real reason why people prefer it"
date: 2022-09-09T19:16:36+05:30
description: |
    A friend visited a real estate meetup in Bangalore, in which the attendees discussed the effects of remote work on real estate, and suggested steps to bring back people to offices with incentives. But I think they missed the mark by a mile, in the latter part.
tags:
- employment
- covid-19
- coronavirus
- health
- economy
- market
- business
- technology
- millennials
- bangalore
---

When the COVID-19 pandemic hit in early 2020 and a country-wide lockdown was announced, we all went virtual. The hitherto exception became the norm.

Unable to cope with the "new normal", we found the first weeks unsettling. A month or so later, we started to love it.

Two and a half years since, we now have people unwilling to come back to the office. What triggered this article was a discussion I had with a friend who had been to an event organised by the realtors in Bangalore. The afternoon session was about how to get people back to the office.

The conversation made me think.

{{< toc >}}

Bangalore is a city of the young. Most localities along the Outer Ring Road are full of people under 40. A large chunk of the Bangalorean population have their roots elsewhere in the country. During the lockdown, those of us that have made Bangalore our home, stayed, while the others travelled back to their respective hometowns.

## The honeymoon phase

Weeks later came the shocker: The productivity of the employees went to an all-time high. We did not quite understand why, but the numbers were right in front of us. Our employers liked this!

And we employees had our own benefits as well. We Bangaloreans saved hours of travel time, which we could potentially use to learn a new skill, spend some time with the family, or to make Dalgona coffee. No more of incessant honking or inching on the road, no ordering a Friday pizza from _Car #37, Edge Lane towards Jayanagar, Silk Board Junction_.

The IT giants also saved a lot on employee transportation and utilities (a former employer used to pay 1,50,000 rupees _per day_ per block on electricity, back in 2011, if I recall correctly).

Also, hundreds of kilos of food was not being wasted per meal time per building in the tech parks.

Not only this, but during the lockdown, green house emissions in cities saw a drastic drop. Anecdotal, but my weather app consistently reported healthy air quality.

What was more, now spouses did not have to be hundreds of kilometres apart. People could find jobs sans boundaries. We could set our own work timings, and were no longer required to start our commute at our cabbie's (or the Transport department's) whim.

Bengaluru was no longer Bengaluru; it was Bangalore (of the good ol' 90s).

A few weeks later, IT giants started terminating their lease agreements for some of their facilities. This saved them several millions every month. Some of us received a "COVID bonus" to help us buy office furniture, UPS units and what not. Now, we were all sitting in our personal office spaces. We could work in our pyjamas. We could "flexi-work". I, for one, did not have to wake up at 5:30 in the morning to shave, shower and show up; I could wake up at 7:30 to do all this.

But wait, something was _sus_. Can everyone be happy?

## Initial realisations hit

A few months passed. Just as the IT giants were gearing up to make work permanently remote, people like Satya Nadella started talking about COVID fatigue. And somehow, I related to it.

No "new normal" will ever be easy to transition to. The COVID pandemic brought a paradigm shift in how we balanced work life with off-work life. While we were surprised to see so many benefits that it almost seemed like a win-win-win-win-win everywhere, this was not to be. At least, we should have known that this was a short-term effect. Because without getting the fundamentals right, we cannot make anything sustainable.

The can of worms opened.

Studies after studies started bringing out the downsides of remote work. The first and most apparent effect seen across the board was blurring of the lines between work and off-work lives. On average, there was a 28% increase in after-hours work, and a 14% increase in weekend work. This in itself was a big deal, because a 28% increase in a 9-hour work day means a 11½-hour work day. In fact, some companies even lengthened their work day, officially. Apparently, overall, the workday span grew by 13%.{{< sidenote "cabralMicrosoftSatyaNadella2022" >}}Microsoft's Satya Nadella speaks out on employee well-being as workdays lengthen ([The National](https://www.thenationalnews.com/business/2022/04/09/microsofts-satya-nadella-speaks-out-on-employee-well-being-as-workdays-lengthen/)){{< /sidenote >}}

Next came stress. Humans are not machines. Increasing the number of hours we work does not mean increased production (on the contrary, you would find studies that show the productivity deteriorates as work hours increase). People started complaining of _Virtual Fatigue_, two of the reasons being a 252% increase in meeting duration and a 153% increase in the number of meetings.{{< sidenote microsoftGreatExpectationsMaking >}}Great Expectations: Making Hybrid Work Work ([Microsoft](https://www.microsoft.com/en-us/worklab/work-trend-index/great-expectations-making-hybrid-work-work)){{< /sidenote >}}

All this also meant that we were taking shorter (or lesser) breaks. The already unhealthily sedentary lifestyle had become more sedentary---people were moving around a lot less. More of my friends started complaining of backache.{{< sidenote "ergo" >}}Apparently, not everybody utilised their COVID bonus for furniture and UPS.{{< /sidenote >}} More of us were getting dehydrated. Coupled with lowered immunity owing to these unhealthy ways of leading life, there were more reports of illness all around.

Months later, I read another report that said there was a 13 percentage point drop in satisfaction with work-life balance. We also realised that as virtual collaboration increased, satisfaction with work-life balance decreased.{{< sidenote klinghofferHybridTankedWorkLife2021 >}}Hybrid Tanked Work-Life Balance. Here’s How Microsoft Is Trying to Fix It. ([Harvard Business Review](https://hbr.org/2021/12/hybrid-tanked-work-life-balance-heres-how-microsoft-is-trying-to-fix-it)){{< /sidenote >}} It was harder for people to find the time to focus, because of the increase in synchronous communication (such as Teams/Slack messages, phone calls, meetings, etc.). People started multitasking during long-running meetings, or in meetings which they did not feel engaged in. "I missed that; say again, please?" became a regular occurrence.

People also saw an overall decline in learning and creativity.

## What may have caused the increase in productivity

The absolute numbers did show a hike in productivity; the number of tasks completed versus assigned saw an increase. The number of hours people were available for meetings and interactions increased. Some sectors even reported an increase in profits.

But this general increase in productivity was short-lived, as predicted. Apart from people taking less breaks, another reason could be these interactions becoming transactional. And synchronous.

Before the pandemic, we would have meetings in the meeting room. And our meetings used to be a lot quicker. Because the communication had a large chunk of non-verbal element to it, which made communication efficient. At the same time, we did not get a chance to zone out in the meetings or multitask, simply because that was frowned upon. This meant that some of us even left our laptops at our desks when going for a meeting, and carried a notepad and a pen instead---a lot less distraction.

While away from our desks, a bunch of emails landed in our inboxes. But oblivious of it, we continued with the meetings. Once back, we responded. The new model of working cut short this cycle time.

Also, all our interactions with colleagues became transactional. Now, there was no fluff. We all talked business and only business; no casual conversations or walking to someone's desk to say hello.

No _chai_ breaks, no watercooler discussions, nothing.

When in the office, we used to spend a fraction of the day in transactional interactions. A large chunk went into focused work, while the rest (also a big chunk) was reserved for social interactions.

A couple of days ago, Kurzgesagt uploaded [a video about friendship](https://www.youtube.com/watch?v=I9hJ_Rux9y0), in which, they said,  'Society locks you and your peers in a building for several years.' Of course, this was about schooling, but the underlying point was, spending casual time together, with similar activities and similar schedules, helps build relationships. According to a study, they said, proximity was more important for this than similar interests.

This entirely vanished with remote work. I, for example, made no new friends working remote.

The increase in productivity came at the cost of social capital.

## The Hybrid Work model

On the one side, the remote model had hit people. But the transition was so gradual that most did not realise its ill effects, or attributed those effects to something else. A colleague told me---when she was about to leave the organisation---that she felt disconnected from the project, and had no idea what was going on, and therefore, felt that she was not contributing enough, the guilt of which led her to leave. Was that her fault?

Learning took a hit for me. Despite having more time saved from not having to commute, I could not allocate it to learning. Because that is not how learning happens for me. Learning for me is about interactions, experiences and experimentation. What I learn in an hour of interaction takes me a week when remote.

The hybrid model was supposed to fix this by giving us the best of both the worlds. Work from office a couple of days, and be remote the rest of the week. My organisation allowed us to choose the number of days and pick the days, after consulting with our teams and bosses.

On the other hand, I also came across people who did not want to go to the office to work. The general sentiment I see around me is to not go to the office to work.

## What made people go to offices

Our work lifestyle has evolved over time. If we think about it, the primary reason people used to go to their workplaces was logistics.

Power. Internet connectivity. Collaboration. Equipment (phones, computers, photocopiers, etc.). The most important, in most cases was correspondence.

Think about it. When we started officially communicating centuries ago, people needed a physical address for correspondence.

Today, almost nobody in tech even remembers their office address. We all have an email address to which people send emails. Now, communication is even synchronous with the likes of Slack and Teams.

But we continue to have an office space, despite the fact that today, power backup, and robust high-speed internet connectivity is accessible to most, in the tech industry. Work culture is evolving. Equipment is mobile; today, we get a laptop, a headset and a WiFi hotspot on the first day of our joining our organisations.

With all this, why would people not like to sit at the comfort of their homes and work?

## The real estate issue

Coming back to the meet-up organised by the real estate folks in Bangalore, let us look at the discussion about bringing people back to the offices.

One of the execs, apparently, said something along the lines of:

> When an employee walks in, his computer should be powered up and a cup of steaming green tea should be awaiting him. His workspace should be pleasant and calm, so that he is already in the frame of mind to work when he gets in.

Is _this_ supposed to be the incentive? First of all, that part about powering up a computer on schedule is easily doable within the BIOS or the OS settings. We can make our rooms calm and pleasant with the help of professionals. I have a couple of LED battens in my room that have been set up to light up the room with the light of the right temperature and intensity for work, on schedule: weekdays at 0800. This is easy stuff.

In fact, any techie who knows how to work with IoT-enabled devices will be able to set up a little home automation server that watches for the work laptop to connect to the LAN. That can trigger the bulbs or battens to create a work-friendly light, control the fan/AC accordingly, etc.

Sure, not everyone may have the know-how to do this. But some chores done for you and a cup of green tea are not going to entice en employee to come to office, because we are not addressing what we should:

## The real issues with coming to the office

Here is the elephant in the room: urban traffic. My brother used to spend _five hours_ in his commute every day---South Bangalore to Whitefield and back, in 2017. The situation is likely to have worsened a lot now. A colleague from Hebbal spends four hours a day in commuting to our office in Electronics City. That is _twenty hours a week_.

Compare this with the time and effort needed to set up the workspace and getting a cup of tea.

Second, the urban physical infrastructure. Ours in Bangalore is beyond pathetic. Pothole-ridden roads that kill people, lack of parking spaces, unreliable and perpetually slow bus service, absolute chaos during rains ... all this drains us. Not everyone lives near their offices. The pandemic and the unavailability of cabs during the time has led to a lot of us buying personal vehicles. This led to an increase in the traffic further, once everything opened up.

The fuel prices do not help either. And if your workplace provides transport, you end up working according to your Transportation team's clock instead of your own.

Safety is a problem as well, especially for women employees. The crime rates seem to have increased. A new wave of the pandemic is almost always around the corner. These are also a factor discouraging people from coming to work.

Also, the price of food has doubled, compared to the pre-pandemic era, while the quality has deteriorated. I, for one, prefer home-cooked food today. A lot of my friends and colleagues echo this sentiment. We can eat it hot and fresh when working from home.

These issues must be addressed before we start talking about "nice and calm" workspaces and "a cup of green tea". When compared with the price you must pay to work from the office, these "incentives" are negligible.

## But that is not all

There are indeed some points in favour of going to the office, apart from the "soft" benefits such as casual conversations and better mental health.

The first among them is ergonomics.

Office furniture, at least in the highly regulated corporates, is a result of a lot of thought. Our workplaces take ergonomics seriously, and the furniture and the workspaces reflect that. The office spaces of today are designed to enable employees to work for hours with minimal impact on their physical health.

Not everybody uses such furniture at home. I see most of my colleagues working from their couches or beds. This is far from ideal. Why are we even surprised about backaches?

The problem is, most of us do not understand the intricacies of ergonomics. Some of us do not even care. And we are unlikely to, until we start facing serious consequences for our indifference.

Second, we are comfortable with our interactions being transactional. Again, this is something that will affect us in the long run.

This is a boiling frog situation.

## What we need

Work will not go back to the pre-pandemic style. The real estate folks or any other folks can lobby all they want, but going back to the old ways of working is not going to happen.

That said, remote work is not perfect; only way more convenient in the short term, considering our ignorance about the unapparent issues.

The problems with remote work are primarily around mental health. Our brains overwork in our virtual meetings, because they have to extrapolate for the non-verbal cues that we otherwise get in physical meetings.

We do not realise all the unconscious processing our brains do when in physical meetings; today's technology only focuses on the transactional element of it. Sure, Microsoft, for instance, brought features such as speaker highlight and reactions, but I think this has only moved the unconscious processing into the conscious realm.

For instance, silence in virtual meetings makes us think if our network connectivity has dropped, and we say, 'Hello?', thereby disturbing everyone else's thought train. In a physical meeting, a silence would merely make us look around and unconsciously understand that everyone is merely processing what we just said.

Keeping the cameras on is more stressful, because, now, you are letting all your attendees into what is your personal space. This would not be the case in the office. It's these "little things" that make the experience taxing.

Meeting our colleagues on a regular basis is better for our mental health at work. This deepens our work relationships. We need not be friends with all the colleagues whom we work with, but that does not mean we avoid all interaction, or that a deeper relationship is unnecessary. A team must bond at a certain level, to be organically productive.

We must learn to set up focusing time. This time should be reserved for deep work. Like thinking of a design for a module.

We need more time off to recharge when working remote, to help our brains handle the added stress and the lack of sense of belonging.

We also need prioritisation support. Because our workplaces are increasingly becoming virtual today, we get lost in tasks that keep piling up, since we each work in our own silos, in the absence of "Hey, what's going on with your project?"

Buffer time between meetings is a nice feature that Microsoft brought in. This is called something along the lines of 'End my meetings early'. Another rule to follow is no bookending, meaning, no meetings on Mondays and Fridays. This will create a gradient of virtual interactions, focused work and unwinding.

How much of this are we doing today? How do we even handle some of these issues when flexi-working remotely? These are the points we should be thinking about, when addressing issues like bringing people back to the office. Focus on the fundamentals of work environments, instead of non-essential trivialities like warm beverages.

## My setup, since I mentioned ergonomics

Keep in mind that I use what I use, because they work for _me_. Your mileage may vary. Ergonomics is never one-size-fits-all.

### The table

I bought [this table by Jordyblue](https://amzn.to/3Bw51Jy), because they promised it to be modular, meaning, we can add more components to it later. But they have not been able to keep up their promise so far. So, while adding more modules is possible in theory, it has not practically materialised yet. Having said that, the table is well-built, and works well for me.

### Chair

Because the table is on the taller side, I had to buy a chair that could match the height. This [one is by Wipro](https://amzn.to/3RyxpjL), which is a well-made chair overall. It is adjustable, has mid-back support, and is tall enough. It has great thigh support as well.

### Footrest

If you think that you are straining to place your feet on the ground when seated on the chair, you could use a footrest. I bought [one by Ikea](https://amzn.to/3QzwChe), but there are plenty on Amazon ([like this one](https://amzn.to/3L4LAe4)), which are good. Of course, if you are about 180 cm tall, this is merely a nice-to-have.

### Monitor

I like BenQ. All my monitors, so far, have been theirs. LG is also a great monitor maker, though. My current one is [this 27″ 4K monitor](https://amzn.to/3eDxynB). A true workhorse. Great colour reproduction, plenty of settings related to ergonomics, automatic brightness adjustment, KVM switch, multiple interfaces, daisychaining capability, height, tilt and rotation adjustments, along with panning and a lot more. Worth what I paid.

My previous monitor was a [21″ FHD monitor](https://amzn.to/3qzmVoo), also with automatic brightness adjustment, tilt adjustment and other picture-related ergonomic features.

### Laptop stand

I do not use the laptop on its own. I connect a keyboard, mouse and monitor to it. I like working on a single screen (because I have not yet learnt to efficiently use two screens). I place my laptop on a stand to save space on my desk (as it is, the laptop is a 14″ FHD one, which cannot be used from the distance at which I sit, without meddling with the DPI settings, which, in my view, kill the UI experience).

[This laptop stand](https://amzn.to/3BwtBdl) is sturdy and compact. Made of solid aluminium, it can be adjusted according to the thickness of your laptop using the screws provided. Once set, it does not budge. Also, it has silicone pads to protect your laptop from getting scratches from the stand, as well as a pad at the bottom to restrict movement.

### Mouse and keyboard

My keyboard is a custom-built one. I cannot recommend that given the amount of Customs Duty you would have to pay to get the parts. Also, collecting the parts may take months. Before this, I used a [mechanical keyboard by Logitech](https://amzn.to/3RUaoI1). I would not recommend it either, because of all the trouble it gave me. I am a keyboard person, so, a keyboard that works the way I want, is important to me. You may not be, and therefore, you may not want to spend a lot on it. If the latter describes you, I would recommend this [TVS Electronics keyboard](https://amzn.to/3BbSMQW). Check out [this one by Corsair](https://amzn.to/3DejN95) if you can afford it, because the switches are better.

I do not recommend Logitech keyboards because their quality may require servicing once the warranty is over, but Logitech does not have service centres in India for non-warranty repairs, last I checked. Buying a non-repairable keyboard at that price is not worth it.

My mouse is the [M331 Silent Plus](https://amzn.to/3qvBedW) by Logitech. I have been using it since June 2019, and it has worked great so far. The battery lasts about a year. The connection is robust, and the mouse has great accuracy.

### Mouse pad

Using a hard wood table means that you will inadvertently end up getting micro-scratches on it from the contact points on the mouse. If you live in a locality that has free dust in the air, your mouse may catch it over time, and rub the dirt into the exposed fibres of the finished top that you scratched.

I personally do not like little mouse pads. I like to use a pad that works as a workspace pad, which also holds the keyboard and my notepad.

I went with [the Amazon Basics one](https://amzn.to/3qw9F3W) for its cost-to-quality ratio, and cut a few centimetres length-wise to my liking. I am eyeing [this one by Spigen](https://amzn.to/3B5nECM) next, available on and off, on Amazon.

### Charger

I use [this GaN charger by UGreen](https://amzn.to/3U1lOf2) to charge my laptop when mobile. The rest of the time, the charger that came along with the laptop is connected. What I like about the UGreen charger is that it has USB-C and USB-A ports, and outputs 65 watts, which is about right for my laptop's specs. I use a [2 metre long USB-C to USB-C cable](https://amzn.to/3B67OrK) to connect it to my laptop.

### Headset

I use the [Aeropex headset by Aftershokz](https://amzn.to/3B5j0Vh) as my daily driver. This pair of bone conduction headset keeps your ears open, preventing microbial build-up, thereby preventing irritation, sweating or infections. This also means that you can hear your surroundings. I like that; zero anxiety.

Of course, I bought it before its price shot up, but if you would like to give it a shot, please do.

A note, though. This has zero noise cancellation, by design. This is not for audiophiles who would like to listen to music in noisy environments like the Metro.

## Summary

The pandemic has changed how the world works. We are not going back to the old ways of working, ever again. But that does not mean we have found the perfect way to work. There is a lot of scope for improvement.

While a lot of us have benefited from the new way of working, a lot have faced serious losses. Finding the balance will take a while. But no matter how much who lobbies, the new normal is here to stay; we have to now learn to work with the new variables.

Also, the price we pay for what we thought were benefits of remote work in the beginning, is slowly becoming apparent in certain spots. We can continue to ignore the symptoms, but we will not be able to do that for long.

Food for thought: Are we now in the introverts’ world?

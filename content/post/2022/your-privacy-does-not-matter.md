---
title: "Your Privacy Does Not Matter"
subtitle: "kidding, of course"
date: 2022-08-12T15:48:09+05:30
description: "“I have nothing to hide” still reigns supreme in discussions about privacy. But does that mean you show everything to everyone? “What will X do with my data?” is another, and this article deals with that question. It shows you the tricky areas of privacy, touches upon the situations of conflict it may create, and the not-so-apparent aspects of privacy violations."
tags:
- privacy
- democracy
- tracking
- internet
---

I have been away for a long time now. As with my growth journey, I have been experimenting with ways to work with my screen addiction. This took some effort. Using the screen less also meant that I limited my online communication. One of the motivating factors was how this would positively affect my privacy.

But before we move forward, here is a look at my screen. See how even the cursor does not blink with fancy, _phase_ animation? This is where I have reached.

{{< youtube "3C7TPYQjdmo" >}}

Coming back to privacy: An uncle of mine visited us mid-July. He had come over after over three years. During breakfast, we got into a discussion about privacy. He is not the kind that reads; he had not read [my posts on privacy](/tags/privacy/). And I heard the usual: "What will anyone do with my data?"

This is the usual question you get when you talk to anyone about privacy these days. And a lot of ... privacy enthusiasts (?) ... have created a rather binary approach to privacy, leaning towards locking it all down entirely. This gives an impression of paranoia to the uninitiated.

That is not true about privacy. Privacy is not a destination of absolute anonymity, zero interference and a primitive, tough life.

Privacy is about taking practical steps to protect yourself, and in the online world, protecting your data.

{{< toc >}}

## Do you need privacy

We all care about privacy---no, we do. But probably not in the way most think. Privacy, to us as part of this civilised world, comes naturally. We build houses, we build walls, we build doors to control who comes in, and most basic of it all, we wear clothes.

In a sense, no human is unique in their bodily appearance. And yet, we cover our bodies with clothes.

At one point, millennia ago, people may have thought wearing clothes was paranoia, saying, 'What is there to hide?'

We are even uncomfortable if someone comes too close to us when we do not want them to, because we feel they are invading our _private space_.

All this tells us that we do value privacy. But none of the steps we take towards personal privacy in the physical world seems unnatural, or as "extra effort" to us, like it does with the online world. And that is understandable, because to most of us, the online world is too abstract. We do not yet understand the boundaries there.

## How the internet works

Today, most services we use, track you in one way or another. Some may find it great to see an advertisement about, say, a hotel in a place that they planned to visit for a trek. Others (like me) find it creepy. Getting an advertisement _when we want one_ is different, because then, we are actively seeking that information. But getting it when we are not expecting it, means that someone collected some data point about us without our knowledge or asking.

This, to me, is an invasion of my private space. This, to me, is like snooping.

But all this is by design, by default, in the online world. You must make an effort to stop this behaviour of the internet. And I am with you when you say that the task seems almost impossible.

Today, there are a handful companies that have ways to track you as you visit the different pages and sites on the Web. They collect and correlate this information to form a profile about you (who you are, what you do, how much you earn, where you spend, how much you spend, what your interests are, what ticks you off, whom you interact with, for how long you interact with them, what you talk to them, etc.), and then, serve you advertisements based on it.

## The advertisements are not the problem

To me, the advertisements themselves are not the issue. While advertisements sometimes do kill the look and feel of a site, they are a legitimate way for people to earn (a little) money. I use advertisements on my [professional blog](//ramiyer.io/), (but they are privacy-respecting, in that the content on the page decides what advertisement appears, not who visits it).

The real problem is how the corporations use this data. Or more specifically, how the corporations share this data with other parties. And then, of course, what these other parties do with this data.

## The tricky territory

The bad elements of the society are secretive. They operate in shadows and the law enforcement or intelligence agencies find it difficult to catch them. But that does not mean everyone who is trying to be private is an antisocial element.

In the physical world, compromising citizens' privacy so the law enforcement could keep a check on criminal activities is like keeping a camera and a microphone in every room of every house and watching everyone go about their lives in real time. Would we allow something like that?

Would we allow a law enforcement person in the house all day long to observe as we sleep, wake up, do our chores, go to work, etc.? Heck, we are not even comfortable with our bosses watching over our shoulders as we work!

Does that make us criminals?

Does it mean we have something to hide?

But with online services, it gets trickier. Imagine this. You go to a mall. You see CCTV cameras all around you. The mall management has installed them for your safety and the safety of others. Because a mall is public space (or rather, a space where people gather).

An online service like Facebook is indeed similar. Safety of the users is important, and so is their experience. Of course, from that angle, it makes sense to measure user activity on the platform to see where users might be facing difficulties, and what would be a good way to fix these issues.

But now, imagine the mall started identifying you every time you visited it, and saw how much you spent in the stores. And then, based on your spending pattern, told the auto drivers parked outside the mall how much you may be earning, what your spending power is, and let the auto drivers drive up the price of your ride by 90%, because you can afford it.

This is what happens on the internet. There have been instances where iOS or macOS users saw a hike in flight rates (or product prices), merely because they were using an Apple device{{< sidenote mikhailovaCrackdownRetailersThat2021 >}}Crackdown on retailers that use Big Brother tactics to identify shoppers with expensive Apple computers - so they can quote higher prices ([MailOnline](https://www.dailymail.co.uk/news/article-9798689/New-rules-stop-online-retailers-charging-Apple-owners-higher-prices.html)){{< /sidenote >}} (the logic being, if you could afford an Apple device, you can afford a higher price for products and services).

And this is not a distant happening, even. Swiggy showed my brother a higher price for a dish than it did me, because he tried to order the dish on iPhone 11, while I used iPhone SE---same time, same network, same everything, except, different phones.

## Why this is a problem

Privacy is now (since 2017) a constitutionally protected (fundamental) right in India,{{< sidenote thesupremecourtofindiaWRITPETITIONCIVIL2017 >}}The Justice K. S. Puttaswamy vs Union of India judgement ([The Supreme Court of India](https://www.main.sci.gov.in/supremecourt/2012/35071/35071_2012_Judgement_24-Aug-2017.pdf)){{< /sidenote >}} even though not _explicitly_ stated as a fundamental right (or a right at all). In colloquial terms, this is the _right to be left alone_. But the specific context of this judgement is with regard to the limits of the government's rights to interfere with an individual's private life and dignity. This does not explicitly cover data protection, even though it talks at great length about data in the online world.

But it does say:

> Privacy _ensures_ that a human being can lead a life of dignity by securing the inner recesses of the human personality from unwanted intrusion.

(Emphasis theirs.)

As the judgement rightly notes, corporations and other entities do keep track of all that you do, and then, change your behaviour in ways you do not want to. For example, by making you buy something you would not otherwise buy.

Of course, most---like my uncle claims he is---may be immune to advertisements. "I would not buy anything I do not want to." I am not fully immune to it. But advertisements are not merely about making purchases. Advertisements could be about lobby groups, political parties, ideologies, anything you can think of. Is my uncle immune to these as well? While one can tell whether they are immune to advertisements about products by looking at their wallet or bank statement, they cannot say the same about their own biases.

For example, based on my behaviour about half a decade ago, social networks showed me advertisements that were homophobic, making me a serious homophobe. I even stopped talking to a friend who came out of the closet and told me he was gay. My response to him was, 'Good for you, man.' and that was the last I spoke to him.

Today, I think about how stupid that was. That is because today I understand homosexuality a little better. And I do not see it as a "problem". More importantly, I did not see then, that social media had influenced my thinking, suppressing my rationality---all that while, I thought I was immune to manipulation.

This is one of the tens of examples I can count right away. From choosing an ideology, to following a cult, to picking a political side, advertisements (and "engaging" social media posts) can do a lot. Time and again, Facebook has shown us how it can (mis)use our data: Cambridge Analytica;{{< sidenote meredithHereEverythingYou >}}Here's everything you need to know about the Cambridge Analytica scandal ([CNBC](https://www.cnbc.com/2018/03/21/facebook-cambridge-analytica-scandal-everything-you-need-to-know.html)){{< /sidenote >}} Frances Haugen's testimony about Facebook promoting posts that invoked specific reactions;{{< sidenote feinerFacebookWhistleblowerCompany >}}Facebook whistleblower: The company knows it's harming people and the buck stops with Zuckerberg ([CNBC](https://www.cnbc.com/2021/10/05/facebook-whistleblower-testifies-before-senate-committee.html)){{< /sidenote >}} psychological experiments with unconsenting users, which may have affected their mental health;{{< sidenote bbcFacebookEmotionExperiment2014 >}}Facebook emotion experiment sparks criticism ([BBC](https://www.bbc.com/news/technology-28051930)){{< /sidenote >}}; recording patients' health issues and doctor appointments on hospital sites;{{< sidenote feathersFacebookReceivingSensitive >}}Facebook Is Receiving Sensitive Medical Information from Hospital Websites – The Markup ([The Markup](https://themarkup.org/pixel-hunt/2022/06/16/facebook-is-receiving-sensitive-medical-information-from-hospital-websites)){{< /sidenote >}} Rohingya killings;{{< sidenote milmoRohingyaSueFacebook2021 >}}Rohingya sue Facebook for £150bn over Myanmar genocide (Rohingya sue Facebook for £150bn over Myanmar genocide){{< /sidenote >}} there are countless examples of social media affecting people's thinking. And the point is, Facebook shows you content based on your behaviour on other sites as well---if you are "on the wall", they could push you to one side.

Signal once even lay bare what Facebook may know about you, and how any consumer of Facebook (the advertisers; you are the product, not the consumer) could get access to it.{{< sidenote saqibshahSignalTriedUse >}}Signal tried to use Facebook's targeted advertising data against it ([Engadget](https://www.engadget.com/signal-facebook-targeted-ad-data-100549693.html)){{< /sidenote >}} This does not necessarily mean that Facebook shares who you are with the advertisers, and the advertisers may not care, but think about this: anyone who wants to incite violence in a city, could create an advertisement with inflammatory content, and target it towards people who meet a specific set of criteria.

And that is from one entity: Facebook. We have others, like Snapchat, Google (with its “filter bubble”{{< sidenote haoGoogleFinallyAdmitting2018 >}}Google is finally admitting it has a filter-bubble problem ([Quartz](https://qz.com/1194566/google-is-finally-admitting-it-has-a-filter-bubble-problem/)){{< /sidenote >}}, “YouTube Regret”{{< sidenote mozillafoundationYouTubeRegrets >}}YouTube Regrets ([Mozilla Foundation](https://foundation.mozilla.org/en/youtube/regrets/)){{< /sidenote >}} and more), among others.

Social media can influence us. Confirmation bias (showing you what you think is true, even though it may not be, and reinforcing what you believe), availability cascade (saying it enough number of times that it seems true), reactive devaluation (not believing something because "the other side" is the source of it)---all these could lead us to process information in an unhealthy way. Do you not feel that our society in general has become a lot more polarised than from ten years ago? I do.

Campaigns by anti-vaccination communities, flat earth community, and such have undermined our scientific progress over millennia. Social media (with its targeting algorithms) has played a critical role in this.

## The not-so-apparent aspects

Human society has evolved over millennia. We started as animals and became what we are today. What we need to understand, though, is that the human society is fragile, because to build this, we have had to sometimes go against our basic instincts.

Concepts such as democracy, inclusion, etc., are not natural to us, and developing these systems has taken tremendous efforts. But given the loose regulations on social media, and the vulnerability that is intrinsic to its operating model of today, pose a serious threat to these.

Secondly, data in the wrong hands could turn dangerous. Raw data in itself cannot help do much, but interpretation of the data---and how it gets used---is what matters. Imagine you went to your favourite restaurant to have a _vada_. You saw a couple hundred youngsters protesting right outside the restaurant. Your friend appeared out of nowhere, because they also had a craving for vada. You saw them, and spent two hours catching up on events of the last three years you did not meet them. Six weeks later, you have the police knocking on your doors, because according to the location data they collected from your favourite maps provider, you were in the area during the protest. Turns out, there was some violence in the protest, and your friend and you are suspects.

Using your search history to decide your insurance premium. Using your device type to decide what commodity price to show you. These and more are potential pitfalls of sharing too much data.

The worst part is, most of us do not even know what data we have shared with whom. That process is hard to keep track of. More so with the involvement of invisible players like cookies.

And do not make the mistake of underestimating these algorithms. Even over a decade ago, we had algorithms that could predict a teen girl's pregnancy even before the family knew.{{< sidenote hillHowTargetFigured2012 >}}How Target Figured Out A Teen Girl Was Pregnant Before Her Father Did ([Forbes](https://www.forbes.com/sites/kashmirhill/2012/02/16/how-target-figured-out-a-teen-girl-was-pregnant-before-her-father-did/)){{< /sidenote >}}

## What to do about it

This is an art of balancing utility, convenience and privacy. But of course, this is a large topic, which will not fit in a single article. I have given a primer of the steps I have taken in the past in my [articles tagged _privacy_](/tags/privacy/).

Maintaining privacy online is an ongoing process, the first step to which, is to create a threat model. This is to pen down whom you want to protect your data from, how much effort you need to put in, and how much you already are. Then, weigh these efforts against the benefits. In my case, most of my concern is around corporations that are frivolous with data (such as Meta).

A threat model evolves over time, because your life changes over time. Also, my threat model will not work for you, and vice versa. And, your steps keep changing not only because of changes to your threat model, but also because of the ways the entities around us collect, process and share our data. This is why there is no "one size fits all" solution to privacy. You must learn about the environment you live in, what clothes are appropriate there, and what your budget is.

The key is to understand your borders, know how corporations, government and people around you handle your data, whether you want to protect your data (if yes, whom from) and accordingly, keep evolving your steps.

Treat your data as though it were you. And today, this is not even metaphorical, nor is this a matter of privilege. All your metadata combines to create a meta personality of you, and you should protect it the way you protect yourself.

---
title: "To wear or not to wear the Hijab"
subtitle: Are we missing the point of this multi-faceted issue?
date: 2022-10-30T10:12:29+05:30
description: The Hijab issue became interesting when women of Iran, an Islamic country, were out in the streets protesting against the piece of cloth, while people in India, a secular country, were protesting against the prohibition on it. In this short article, we look at the different aspects of it, and look at whether we are missing the point.
tags:
- religion
- culture
- freedom
- constitution
- politics
- education
- justice
- social
- protest
- debate
---

Let me start this by saying that the school of thought within Hinduism that we follow at home, teaches us to be not disrespectful of any school of thought. This does not mean we agree with everything, though; we instead appreciate the fact that there are more-than-one ways of seeing anything.

Philosophy aside, I am a man of science. Scientific thinking partially goes in line with the school of thought we follow, but digresses a little, in that scientific thinking allows _every_ form of questioning. And questioning, in my view, is not disrespectful; questioning is how you get answers, and that is how you acquire knowledge.

Also, I am comfortable with people questioning my beliefs. This challenges me to think and opens the door for nuances. This is the essence of scientific thinking.

Second, I am an apolitical person. Politics is politics, and given the space I am in, politics has little to do with the realities of our lives. Politics today, in general, is a game of rhetoric.

To make matters worse, politics and religion have now gotten married, becoming a dangerous duo.

{{< toc >}}

## The beginning

When the issue began in early 2022,{{< sidenote UdupiHijabIssue2022 >}}Udupi hijab issue: The Indian girls fighting to wear hijab in college ([BBC News](https://www.bbc.com/news/world-asia-india-60079770)){{< /sidenote >}} I did not pay attention to it. I assumed that elections were coming up somewhere, and this was one of the easiest ways to polarise people for votes. (Polarising does not work on one pole---left or right; it works on both poles---left _and_ right.)

As expected, the public got divided, with one side justifying saffron scarves{{< sidenote timesnowdigitalKarnatakaCollegeStudents >}}Karnataka: College students wear saffron scarves to protest against hijab in classrooms ([Times Now News](https://www.timesnownews.com/india/article/karnataka-college-students-wear-saffron-scarves-to-protest-against-hijab-in-classrooms-details/846442)){{< /sidenote >}} in classrooms, while the other tried to protect the rights of girls to wear their headscarves (a.k.a., the _Hijāb_).

In no time, this became an issue about "secularism", as usual, with everybody forgetting that secularism is not about caring for or supporting every religion, but about _no religion_ having any take on the running of the State.

I am old enough to expect these actions and reactions now, and none of this surprised me. Not even the Muslims playing straight into the hands of the Hindu extremists ("fringe groups" is the term in vogue these days).

## People approach the courts

As usual, people approached the Judiciary with two issues (I am paraphrasing, of course):

1. Educational institutions (especially the State-run ones in a secular State) should not curtail religious freedom.
2. The hijab is an essential religious practice in Islam.

The petitions filed did not necessarily split these two aspects of the hijab issue, though.

The Karnataka High Court delivered a verdict{{< sidenote >}}Restriction on wearing hijab ‘reasonable’: Full text of Karnataka HC judgement upholding ban ([ThePrint](https://theprint.in/judiciary/restriction-on-wearing-hijab-reasonable-full-text-of-karnataka-hc-judgement-upholding-ban/874034/)){{< /sidenote >}} stating that the girl children should comply with school uniforms, and that it was a "reasonable restriction", adding that it was "constitutionally permissible". In the Karnataka High Court's view, the students could not object to this restriction.

The court dismissed the claim that the hijab was an essential religious practice in Islam.

In October, the two-judge Bench of the Supreme Court had a split verdict{{< sidenote theprintteamDivergenceOpinionFull2022 >}}‘Divergence in opinion’ — Full text of Supreme Court’s split judgement in Hijab ban case ([ThePrint](https://theprint.in/judiciary/divergence-in-opinion-full-text-of-supreme-courts-split-judgement-in-hijab-ban-case/1166211/)){{< /sidenote >}} on whether the girls should be allowed to wear a hijab. The issue remains undecided from the legal angle as of writing this article.

## Iran's take on the hijab

Something interesting happened in September 2022: Women in Iran intensified their protests _against_ the Hijab.{{< sidenote deatonIranianWomenBurn2022 >}}Iranian women burn their hijabs as hundreds protest death of Mahsa Amini ([CNN](https://www.cnn.com/2022/09/21/middleeast/iran-mahsa-amini-death-widespread-protests-intl-hnk/index.html)){{< /sidenote >}} In no time came the allegation:

> All the "liberals" who fought in favour of the hijab in India are now supporting the women in Iran who don't want to wear the hijab. What's the deal here?

I recently even heard that a certain Ayesha, who allegedly shouted "Allah hu Akbar" outside her college earlier this year during the pro-hijab protests, was found fighting against the hijab in Iran. I have no way of establishing the veracity of this claim; I took it with a grain of salt.

Regardless, I felt it was absolutely plausible that someone who protested in favour of the hijab here was protesting against it in Iran, because even though both the protests may seem to be on the opposite poles, they are not, if you look at it from the angle of _State control_.

Should everyone not have personal freedom of clothing? I do not mean that nobody should take issue with someone walking in his underwear in a railway coach full of people. Of course, there should be reasonable restrictions. But who can decide on behalf of someone on whether they should or should not wear a piece of clothing when wearing and not wearing it are both respectful of the reasonable restrictions?

If the State banned shorts or black shirts, I would ask why. If the State asked me to wear a polo hat at all times, I would ask why.

## On uniform

At the same time, I do not take issue with my workplace prohibiting frayed jeans.

All this boils down to State control and reason (as in, what is reasonable). Prohibiting frayed jeans in the office is a reasonable ask. Prohibiting dhotis on a college campus is a reasonable ask. In my school (neither a religious institution, nor backed by any religious institution), our female schoolmates were not allowed to wear a _bindi_. Though the majority of the students were Hindu, everybody found it reasonable. The girls who wanted to wear a bindi would wear one in the after-hour classes, because we could go for them in "civil" clothes. In the same way, prohibiting hijab on campus to comply with a dress code, according to me (you may disagree), is a reasonable ask.

But not everybody sees it that way. Some see it as an attack on their faith. Justified or not, not my place to say, because everyone has their personal liberty of thought.

To me, the literal reason for a uniform is uniformity. I never wore a mark on my forehead in school, to comply with the code.

But it does not end there, and we will soon get to it.

## State control

But an educational institution and a country are not the same.

I am personally pro-choice in the matter, but I agree that a uniform is a uniform, and one must respect it. At the same time, I find it reasonable to be torn between whether a State-run educational institution, in a secular (in the sense that no religion has any say over the running of the State) State, can prohibit a piece of clothing on religious grounds or not. Because allowing and disallowing a piece of clothing which is a norm in a religion or culture, are two sides of the same coin. On the one hand, the State cannot interfere in religious matters, and on the other, it cannot allow religion to interfere in State matters. In the case of the hijab, which of the two takes precedence? Can a religion interfere with the uniform of a school, or can the school interfere with a religious norm? ("Norm" is more appropriate here, until the question of whether wearing a hijab is an essential religious practice or not remains undecided.)

In the case of the protests in Iran, the issue is State control as well: the Iranian women are being forced to wear a hijab.

Should it not be the women and girls who decide what they want to wear, and not the government? Like, if someone forced me to wear a dhoti at all times of the day, I would not like it. Who is anybody to _ask_ me to wear anything? One may suggest, not compel or order.

## But this is not as simple an issue

Coming back to the hijab issue in Karnataka, some petitioners and one of the judges of the Supreme Court Bench pointed out that prohibiting the hijab may deprive girls of education. Justice Dhulia said in his judgement:

> If she wants to wear hijab, even inside her class room, she cannot be stopped, if it is worn as a matter of her choice, as it may be the only way her conservative family will permit her to go to school, and in those cases, her hijab is her ticket to education.

Of course, one may have the question, 'How can one specific religion get special treatment?' And to counter this question, a fraction of the petitions call the hijab an essential religious practice, much like a _Pagri_ in Sikhism. The October 2022 verdict by the two-judge Bench of the Supreme Court does not say whether the hijab is an essential religious practice or not.

## Lose both ways

Some people in the know of this issue have pointed out that this is a lose--lose proposition for the liberals.{{< sidenote "liberals" >}}By "liberals", I mean those people who are pro-choice, not necessarily those who disagree with the Right-wing political ideology.{{< /sidenote >}} This is because a pro-hijab verdict would mean a win against personal freedom, and an anti-hijab verdict would mean deprivation of education for the girl children, in which case, in turn, the girls lose again.

## Hijab and misogyny

But in all this, we are missing something critical. Perhaps I need to understand this from someone.

The point is, the hijab is---sort of---a misogynistic practice.

I am not an expert in Islam; I have not read even a single piece of its literature. I am not interested in it either (nothing against the religion, it's just not my thing), even though I have asked questions and sought answers on some of the topics, out of curiosity. And these answers came from those who practice the religion.

What I heard about the hijab is that it covers the most attractive parts of a woman's body: the hair and the neck. You may agree, you may disagree; that is beside the point.

The premise is that a man is prohibited from looking at a woman who is not his partner, in the "wrong" way. But a woman who does not satisfy the desire of a man is also a sinner. Therefore, it is imperative on part of the woman, that she does everything she can to prevent a man from having lustful thoughts about her, if she does not want to satisfy his lust.

This view bothered me. More so because this came from a woman; a woman I respect. And I hope, to date, that she was wrong about this.

A parent protecting their child from sin is fair. But should it not be the other way around, that you teach your male children to not  take any action against a woman's will? Going by this logic, a child could be held responsible for a paedophile engaging in paedophilia. Then, do we ask our children to grow up faster, or act like a grown-up from the very first day, of say, their school, or clothe them in an "unattractive" way, based on some arbitrary assumptions?

No. Instead, we punish the paedophile. We look down upon adults having lustful thoughts towards children.

Personal safety is everyone's responsibility, but is it fair to make someone take additional precautions, depriving them of personal expression, merely because they are born a certain gender? Sure, we may discourage provocation, but at the same time, the males must not feel provoked merely in the presence of a woman because she does not go out of her way to hide her features.

I am not challenging a religious belief here; I am challenging the thought behind _a practice_, in the context of the twenty-first century.

Every religion or culture has some great practices that remain relevant today. Such as the idea of allocating one-fortieth of your income (or a fortieth of your savings; not sure which) to charity. This is a practice in Islam, I have been told by the same woman I mentioned before. I think, irrespective of the book we follow, we can adopt this practice.

But at the same time, I think we must question the validity of a practice that has become irrelevant today, just like we did with the _Sati pratha_.

## Back to choice

Why did I say "sort of" in the previous section? Am I unsure whether this is a misogynistic practice?

No, but there is another aspect to it, and that is choice. Justice Dhulia mentions this in his judgement. One may ask, what choice? For all we know, a girl's family may be forcing her to wear the hijab.

The reality is, some women wear a hijab because they like wearing one. A friend describes this as "neither forced, nor freed". This aspect is somewhat deep-rooted, and a little hard to unpack. But no harm in trying, I suppose:

A girl born in a family full of women wearing hijabs would see that as a norm. She would start wearing one from a young age---at an age when she cannot differentiate between, say, milk and cream. She does not know the reason women wear a hijab, but she now sees is as part of her dress. Then, one of the women says she need not wear it when among her _mehrem_. Over time, she builds an idea around the use of the hijab, even though she does not know the real reason behind it.

Now, asking her to not wear a hijab at all could make her feel uncomfortable. Once she attains teenage, she may learn the real reason behind wearing a hijab, and may even disagree with the idea behind it. But despite that, continue to wear the hijab, because she considers it part of her attire. She is comfortable in it. And she may think, 'Well, my wearing a hijab harms no one.'

She may even feel attacked if someone asked her to remove the hijab.

In other words, she disagrees with the idea that women should go out of their way to protect their "modesty", but she chooses to wear the hijab. She knows that logically she does not have to, but emotionally, she wants to. She equates removal of hijab to undressing. She is comfortable removing it where she is comfortable removing it---a matter of choice.

To many, this might seem like going down a slippery slope, but that would be because I could not quite explain this aspect properly. This approach is from the emotional angle, and _writing_ about them has been a challenge.

But let me put it this way. I am a south Indian man. In my culture, men going around shirtless is not inappropriate. But I am not comfortable with it. How can someone _liberate_ me from wearing a shirt?

## In summary

This practice of wearing a hijab is complex. I will give you an example, courtesy the friend I just mentioned; let us call her Varsha.

Varsha is an architect. She told me about this girl who would come to the site in a burqa. The girl was comfortable in her burqa at the site; no tripping, nothing. But Varsha asked her not to come to the site in a burqa, for a simple reason that loose clothing is unsafe at places like a construction site, places with heavy machinery, etc.

Varsha asked me, '\[Was\] I controlling her freedom? Or practising a general code of conduct?'

As a society, we need a nuanced and sensitive approach to it. One cannot exercise one's liberty at the cost of someone else's liberty. But, someone wearing a hijab out of choice is exercising one's liberty, without infringing upon anyone else's liberty. And I agree with Justice Dhulia on that. But I also agree with Justice Gupta on the part about respecting the uniform. If anyone wants to know, yes, I went to the Sabarimala shirtless, in a black _Muṇdu_ and _aṅgavastraṃ_. Also, I removed the _aṅgavastraṃ_ when walking in the forest, when wearing it became uncomfortable. That is the uniform for _Ayyappans_ (the pilgrims). But I also wore trousers with tucked-in shirts, and shoes to my college during the six-week _vrataṃ_ before the pilgrimage, because that was the dress code there. I am sure Ayyappan (the deity) understood.

But also, for the record, my college (a Christian minority institution, by the way) exempted _Ayyappans_ on _vrataṃ_ from strictly following the dress code; we could wear black trousers with black untucked shirts, wear a _chandana kuri_ on our forehead, and go barefoot. I chose against the exemption, because, to me, a dress code is a dress code.

All of this boils down to choices and mutual respect; very different from State control.

Can we take a balanced approach to this? Can we recognise that changing a practice such as this is not going to happen overnight? Can we recognise that every person should have the right to choose what s/he wears, as long as the choice is reasonable with respect to the others around? Can we recognise that a uniform must be respected, because it stands to fetch everyone wearing it equitable treatment, and that an institution (educational or otherwise) has a decorum to maintain? Can we do all this without having anything against cultural or religious sentiments, or without moral policing? And can those of us who want reform remember that it’s not _an instrument_ of misogyny that we are after?

And most importantly, do we now want to be a place where people have neither the consideration for norms nor the freedom of choice, by having the State regulate our attire?

---
title: "The Problem With Government Jobs"
subtitle: "and in general, the government sector in India"
date: 2022-01-31T16:52:54+05:30
description: "The situation with the RRB–NTPC selection invited massive protests by students and other aspirants. If the odds of selection weren’t bad enough, the way the Railways handled the applications worsened the situation, infuriating the aspirants, bringing them out on the streets. Tutors like “Khan Sir” got pulled into the mess with the police accusing them of instigation. And there was this interesting narrative of why so many people in India want government jobs. In this article, we discuss the issue, along with some facts and figures."
tags:
- education
- employment
- politics
- governance
- democracy
---

Happy 2022! May this year keep us all in a much better shape than the last two; in better health---physical, emotional and financial---better relationships, better work–life balance, better everything. (Holding your breath for the Budget, anybody?)

Of course, not all is rosy in the country at the moment. We are looking at elections in three states, with politicians going around being all nice, making promises, and trying hard to apply chalk to the blemishes on their images, and of course, defecting.

{{< toc >}}

One point that I saw come up way too often was the issue of unemployment. The highlight of all this was the RRB--NTPC (Railway Recruitment Board: Non-Technical Popular Category) exams.

## Unemployment in India

Before we begin, we must get a general picture of unemployment in India. The year 2020 was perhaps the worst year in the history of independent India for employment. With a large number of businesses shutting down, millions of people lost their jobs, April and May 2020 recording 23.52% and 21.73% unemployment respectively.{{< sidenote mishraCovid19ImpactIndia2020 >}}Unemployment Rate in India, [CMIE](https://unemploymentinindia.cmie.com/kommon/bin/sr.php?kall=wtabnav&tab=4020){{< /sidenote >}}

{{< raw >}}
<div class="flourish-embed flourish-chart" data-src="visualisation/8568801"><script src="https://public.flourish.studio/resources/embed.js"></script></div>
{{< /raw >}}

Going by the the CMIE data for the past six years, the rate of unemployment was at its lowest in mid 2017; it rose to a little over 7% in the beginning of 2019, after which, it has been around 8% barring a couple of peaks.

With a market as large as ours, one cannot pinpoint at one single reason for the unemployment landscape. But this also means that going by the numbers, one can say with certainty that something is wrong on more than one front.

## Government and jobs

As a welfare state, our government must ensure jobs for the citizens. This is where schemes like MNREGA come into the picture. The governments have boasted about MNREGA giving millions of families a source of income; a guarantee of income, in fact. But there is another way of seeing it.

MNREGA is a scheme under which its beneficiaries get what is close to the minimum wage. The only reason people would go for this is when they have no other alternative. In other words, the more people enrol with MNREGA, the worse the job situation in the country is.

The primary reason for people to prefer applying for jobs in the government sector is job security. This has become more of a driving force since the pandemic. Of course, now the government is changing some of its ways of giving jobs,{{< sidenote muralidharanWhyCrazeGovernment2015 >}}Why the craze for government jobs? ([The Hindu](https://www.thehindu.com/features/education/careers/why-the-craze-for-government-jobs/article7877282.ece)){{< /sidenote >}} but people who get a permanent job with the government do have a secure job.

Second, the entry level pay, as the article cited above says, is much better than what an entry-level professional would make in the private sector. Then come the medical benefits; ask anyone about their CGHS benefits and what they say will positively surprise you. Add to it partial future security (partial, because the inflation does not help, and that the government's policy on pensions is changing), and you find yourself fumbling for good reasons to dissuade people from applying for government jobs.

Although, given the situation around RRB--NTPC you would know how competitive the selection for government jobs could be. Here is a word of assurance: the RRB--NTPC is no different from other government jobs when it comes to the ratio of applicants to the vacancies; they are all in the same ballpark. The situation in state government jobs is perhaps worse.

## The employability

Over the past years, we have heard the term, 'unemployable'. As discouraging as it may sound, the reality sure is that we cannot hire a lot of our people,{{< sidenote EmployabilityIndianGraduates2019 >}}Employability of Indian graduates jumps to 47% in 2019 from 33% in 2014: Survey ([Mint](https://www.livemint.com/news/india/employability-of-indian-graduates-jumps-to-47-in-2019-from-33-in-2014-survey-11575983561692.html)){{< /sidenote >}} plainly because their competency (not competence), more often than not, does not align with the industry requirements; my industry is a classic example. Sure, one may ask, 'Can anyone actually be unemployable?', and to that, the answer would be complex. I am talking specifically about the competency matching with the industry needs---our education system is inadequate. Even for skilled labour.

But there are other sides to this as well. One, what is the opportunity landscape? What is the job market like? How stable is the market? How diverse is the market? And two, are students getting the necessary resources to study? Do we have enough good colleges? Do we have teaching staff in our schools and colleges?

When you see odds like one vacancy to 355 applicants, the graduate or post-graduate applicants competing with Class X-pass candidates for the same posts, and the Class X-pass candidates stating that the higher qualified getting preferential treatment while they should not be (given the requirements for the jobs) you feel sorry about the state of affairs.

Our situation is worse than we thought.

## But are Indian Railways a job factory

This was an interesting (read: funny) perspective brought out by none other than Mr Sudhir Chaudhary. In this video clip, Mr Chaudhary almost admonishes the attitude of the people that the Railways are a source of employment, and takes it to a whole new level that I found nothing short of laughable. I do not watch his show (I keep away from mainstream news channels; I prefer to read news instead), and so, I am in no position to judge his journalism. But I disagree with him when he says this:

{{< youtube 8hM2bfdeonE 2494 3360 >}}

First, some fact corrections. The Indian Railways have 1.4 million (or 14 lac) employees, not 1.2 million (12 lac). Second, despite that, the Indian Railways are the 14th-largest employer going by the number of employees. Third, Indian Railways may not be the largest employer in India; Indian Armed Forces are ahead (by about 45,000 personnel as of 2020)---but sure, the Indian Railways also claim that they are the largest employer in the country, so, let us flag that as doubtful.{{< sidenote indianrailwaysIndianRailwaysYear >}}Indian Railways Year Book 2019--20 ([Indian Railways](https://indianrailways.gov.in/railwayboard/uploads/directorate/stat_econ/Annual-Reports-2019-2020/Year-Book-2019-20-English_Final_Web.pdf)){{< /sidenote >}} Perhaps Mr Chaudhary read their Year Book (which I hope he did, because I am going to bring up something interesting next).

{{< figure src="https://blogfiles.ramiyer.me/2022/images/worlds-largest-employers-2022-01-31.png" alt="World’s largest employers as of 31 Jan 2022 (Source: Wikipedia)" caption="World’s largest employers as of 31 Jan 2022 (Source: Wikipedia)" >}}

With that out of the way, let us look at this interesting perspective of his. McDonald’s Corp. is the seventh-largest employer in the world. Is McDonald’s a real estate company (oh, you didn’t know?) or a job provider? Is Walmart, Inc. a retail chain or a job provider? What sense does such a question make? How do people manage to utter such nonsense on national television?

He then goes on to say that people complain about the lack of quality of service in the Indian Railways, and asks, 'How can we expect the Indian Railways to provide great service if we view it as a job provider rather than a service provider?'

He could mean two things with that statement: Either that those working for the Indian Railways are so unqualified to work that they struggle to uphold the service standards of the Indian Railways, or that given the hundreds of thousands of people working for the Railways, the organisation is not left with funds to concentrate on the services.

And here are my counter-arguments to both:

1. The Indian Railways do not recruit under-qualified individuals. Mr Chaudhary's statement contradicts what he claimed a few minutes prior, in the same video.
2. Indian Railways made a profit of ₹1,589.62 crore in 2019–20, going by the official Year Book linked above. Either Mr Chaudhary did not read the report, or knows something that the Indian Railways and I have missed. Or worse, that the Indian Railways are being dishonest. Which is it, Mr Chaudhary? You are the journalist here. Right?

For the record, I do not mean that the Indian Railways are able to uphold the standards they promise. All I am saying is that Mr Chaudhary's reasoning in this regard is lousy.

The next argument he brings out is to his own detriment.

He claims that the USA have the largest railway (or railroad, as they call it) network in the world. And he is right about that. He appreciates how they run their 2.5 lac km railway network with a mere 1.43 lac employees. Okay. Next, he says, China has the second-largest railway network (again, correct), and that they run with a mere 2.88 lac personnel. Wrong. The China Railway, as you see in the graphic above, has 20 lac (2 million) personnel. A million is ten lac, Mr Chaudhary. At this point, I do not know where he found the 2.88 lac. It would be better if he linked the source.

I could not read the script on the Russian Railways’ website, but going by _Datanyze_, the Russian Railways have 9 lac personnel.{{< sidenote RussianRailwaysCompany >}}Russian Railways Company Profile ([Datanyze](https://www.datanyze.com/companies/russian-railways/212893960)){{< /sidenote >}} What Mr Chaudhary has (the figure 7 lac) is perhaps from the Wikipedia page of the Russian Railways, but that figure is as of 2017---companies change a lot in five years. Mr Chaudhary, Wikipedia cannot be your sole source of information, if you are presenting something on national television. But hey, who am I to give you any advice?

Also, Mr Chaudhary is mistaken about high speed railway in the USA. Trains in the US are slow. Talk to any American who has used a train and s/he will tell you. People in the US use trains for specific reasons:

1. For the experience.
2. When travelling long distances from rural areas where either there is no airline connectivity, or air travel is too expensive in comparison.

The US primarily uses “railroad” for freight. And the rail companies run trains for passengers for reasons such as a legal obligation. Very few routes in the US are popular among travellers; Americans typically prefer the road or air.

Here is a perspective I offer instead. Look at the passengers : personnel ratio. China is ahead of us in population, and it reflects in the number of personnel working for their railways. In fact, their ratio is better than ours. We have a large population of train travellers (given the practicality of train travel in India), and we have such a large number of personnel working for our railways. Occam's Razor, Mr Chaudhary.

To put an end to this meaningless narrative that found its way to national television, let me say that the Indian Railways announce vacancies when there are vacancies to fill. Nobody is creating new, unnecessary vacancies. One must remember that given the 14 lac people working for the railways, there would be tens of thousands if not lacs retiring from or leaving their jobs on a yearly basis. This attrition is normal in any organisation. Such vacancies need backfilling. Plus, railways are expanding every day. Our railways have not yet adopted technology enough to be able to replace people with tech, and so, with expansion in infrastructure and facilities, we need more personnel.

Also, given the rate at which the government sector adopts technologies, I think the scale of consumers will always offset it, making us need more personnel working for the Indian Railways.

Unless we start modernising the railways, we will never get to how modern, say, the Japanese railway is. But one must also keep in mind the scale of Indian Railways. The network is way too vast for it to be practical to modernise to the level of the Japanese within a year or two. The government machinery needs fixing; merely lamenting how much the government has to spend on its employees is meaningless.

Next, merely because 355 candidates apply to a position, they are not all going to get a job. One out of those 355 will get a job; the other 354 will walk back home. Also, the students are not out there to ask for all the 355 to get a job; they are more than aware that the vacancies were 35,281, while the applicants were 1.2 crore (12 million). The students are out there because of the way the organisation processed the applications.{{< sidenote 35000Posts2022 >}}35,000 posts, 1.25 crore aspirants: railway recruitment process, and controversy ([The Indian Express](https://indianexpress.com/article/explained/explained-controversy-recruitment-railway-jobs-7741550/)){{< /sidenote >}} And what question is ‘Why do they want a job only with the Indian Railways?’ Nobody said they would work for no one other than the Indian Railways. When someone wants a job, they apply everywhere they hope to get a job with their qualifications and/or capabilities. If they find something wrong with the way their application gets handled, they will have a problem with it and may choose to raise their voice. Nothing wrong with it.

Anyway, the video has too much wrong with it to talk about in a single article. I will leave it here. My suggestion would be to read the Indian Express article instead watching Mr Chaudhary’s show; he has a lot of facts to get straight before he gets qualified to go on a national stage to talk about a national issue. I choose to question his eminence rather than assuming malice on his part. Either way, his video is unworthy of your time. How unfortunate.

## The fundamental issue with the government sector

The problem is on a much deeper level than what appears on the surface. The basis of design of our government organisations is the population and the socio-economic structure decades ago. Today’s India is different from that. We have a much larger population (which may start declining soon). The challenges and the requirements we had five (or seven) decades ago are different from those of today. Today’s India is more modern in thought and actions. We are a much more diverse society. On the negative side, we have a horrible economic divide, our politics is much more polarised and morally corrupt, and the environment is worsening.

And we are always short of people in the government sector.

Why is the garbage still lying here? Nobody from the Corporation came to collect it. Why not? Because they were busy elsewhere. Why? Because that neighbouring locality does not have anyone. Why? Because.

Why did I lose power in the night? Because the transformer had a meltdown. Why was it untouched until afternoon? Because we do not have anyone in the night shift. Why do we not have anyone in the night shift? Because everyone we have has daytime duties, and we have no one for the night. And those working during the day are overworking. Why? Because we do not have enough people.

Why is the crime rate high? Because we do not have a large enough police force. And the existing police personnel are overloaded.

Why is healthcare so bad in our country? Because we do not have enough medical and paramedical professionals.

Pick any government organisation, and you will find such a situation. From banks to hospitals to public works to anything.

Why?

And what do we do about it? Change the government? Yeah? Has that changed anything in the last seventy-five years? No. Why? We will talk about that in a future article.

---
title: "How Should Politics Work"
subtitle: "because “if not this party, then the other” does not"
date: 2022-02-05T08:10:23+05:30
description: Over time, our understanding of politics of our country has changed. We look for alternatives. Is that the right way to go? Or is that one among the games that politicians play on us? Most importantly, if alternatives is not the solution, what is?
tags:
- politics
- governance
- democracy
---

Often, we find ourselves criticising one or the other government institution or organisation. In a country with such a population as ours, we have millions complaining every day. But after complaining and screaming and getting frustrated about the futility of talking about it all, we place the issue on the backburner for a couple of days, and unless we receive fresh stimulus, the issue fades away in the coming weeks.

This is the case with all government-related issues---be it of personal or national importance.

2011 was the first time I took part in a protest, as an insignificant member who joined because he had nothing better to do on a Saturday afternoon. My colleagues and I had decided that we would go to the _India Against Corruption_ protest happening at the _Freedom Park_ in Bangalore. We sat there for a few hours, listened to a couple of people speak, and then, came back home.

And in 2014, the people chose a different party to lead the government. Why? Not because this party promised zero corruption (we all know those are hollow words). But because the party had a manifesto that promised progress. The promises were not about freebies, but a half-decent roadmap on what the party had planned for.

It has been seven and a half years since. And where are we today? In a place where the Finance Minister continues to spew hyperbolic numbers in the name of the Union Budget, the taxes on emerging technology is unreasonably high, and in general, provides zero benefit to that chunk of the society whose purchasing power is high and the income-to-expenditure ratio is low. This is the part of the economy that potentially drives the economy---the middle-class.{{< sidenote etbfsiBudget2022Disappoints >}}Budget 2022 disappoints, no income tax relief to the middle class ([ET BFSI](https://bfsi.economictimes.indiatimes.com/news/financial-services/budget-2022-disappoints-no-income-tax-relief-to-the-middle-class/89287784)){{< /sidenote >}}

You and I belong to social groups---family or friends or others---that have people who say, ‘Well, fine, let us change the government the next time we vote.’ But does that help at all?

The history of the last seven decades will tell you it does not.

This is because the majority in the political class are people with a similar mentality and agenda. They know how to dangle the carrot in front of the people. And that the people will chase it.

Over the past couple decades or so, as an aware citizen, I have seen parties throw biscuits to the people, and the people catching them, and being happy about it. I have known slum-dwellers who are “made happy” every time an election is around the corner, by giving them, say, an LPG connection, or documentation for the land they live on, or a pipelined water connection, etc. Notice the “or” there; they do not get them all at once, but one item every election season.

This is 2022, we have completed 74 years of independence. We are among the largest economies in the world. We are the second-most populous country in the world. We have brilliant minds in our country. Why are we still struggling with the basic needs like clean drinking water, sanitation and free basic education seventy-four years since independence?

Because the political class knows that they are answerable _once_ every four or five years.

One year before the elections, road works begin. During the election season, the fuel prices magically remain constant.{{< sidenote ndtvPetrolDieselPrices2022 >}}Petrol, Diesel Prices Remain Unchanged. ([NDTV](https://www.ndtv.com/business/petrol-and-diesel-prices-today-february-2-2022-fuel-prices-remain-unchanged-see-rates-2744066)){{< /sidenote >}} You see the god-like men and women of the political class walk from home to home, asking the mere mortals if everyone was okay, and make arrangements for when not.

We smile, shake our heads, join our hands, and promise to vote for them.

This is what we have reduced democracy to. But is this the real democracy? If not, what is?

Let us bust some myths here.

## The job description

When anyone asks a question of the ruling party, they say, 'Oh, now, are you the mouthpiece of the opposition?' When you ask something of the opposition, you get a 'Oh, we are not in power, why do you ask us; ask those in power.'

Story of seventy-four years in a single paragraph.

Both the times, we got nothing. Why?

Because we forgot the fundamental concept of _people's representation_.

Those that we elect must speak for us, not the other way around. People must never campaign for the candidate they like. People must never speak for the candidate they like. This is not people's responsibility.

The contestants must be the people's mouthpiece instead. I cannot stress this enough, but _that is their job description_---being the mouthpiece for the people in the respective assemblies.

## Freebies

None of what you get is free. Again, I cannot stress this enough. You paid for that road. You paid for the healthcare. You paid for that vaccine. Nobody gave it to anyone for free. The state government may have gotten it from the union government for free; that is because the state government did not have to pay for it. But the people themselves did not get it for free; we all paid for it, each one of us.

Every time we bought clothes that we cover ourselves with, every time we bought food to feed ourselves, every time we filled fuel in the vehicle, every time we bought a sanitary pad, every time we visited a restaurant, every time we bought a ticket to travel, every time we did any of this, we paid for the vaccine, for the roads, for the schools, for the hospitals, for everything around us.

These are nothing but prepaid products and services.

The next time a politician (or anyone) tells you that you got something for free, tell them this. Nothing came for free, if anyone in your household earns anything in any way, and you buy anything from anyone in any way. Every recorded transaction gets taxed. One way or another.

Another way to think about this is, how did the government get any money? The govenrment is not into businesses with hundreds of thousands of crores of rupees of profit. If anything, the government makes losses in most of its businesses. Otherwise, what is the source of income for the government?

Correct: taxes. Who pays taxes? We do. One way or another.

## Government institutions

It has become so ingrained in our psyche to discard government products and services as of abysmal quality. We have come to accept it: government hospitals are bad, government schools are bad, government buses are bad, government anything is bad quality.

That has led to this comeback of ‘Your school is obviously bad; what do you expect from a government school? Go, enrol yourself in a reputed private school if you want good education.’

Wrong.

I expect a government school to be no less than a reputed private school.

‘But what voice do you have to speak about this? You have no kids going to these schools.’

No, but I paid for that building. I paid for the classrooms. I paid for the desks and benches. The land belongs to my country, whose government is led by people I elected into power, made of people of the same rights on the land as me---I own a piece of that land. I paid for the duster and chalk, and the food that the kids eat there. I have the right to ask, because I paid for it. If you want to rid me of those rights, waive off all taxes; I will stop complaining.

‘But you don’t even use it!’

That is even worse; I have more rights to get furious, then. I am paying for something I do not use. I have more rights to question it.

## ‘Did you vote for me?’

You are constitutionally wrong to ask whether I voted for you or not (or whom I voted for); I could sue you for that. Second, again, you are constitutionally wrong in being conditional about your representation. Whether you vote for someone or not, once they get elected by people’s mandate, they agree to represent everybody in that constituency---those that voted for them, and those who did not.

If the representative does not know this most fundamental concept of our system, s/he is not fit to represent people. They must read up a little.

## ‘If you know so much, why don’t you contest?’

Because that is not what I do.

Every time I have a problem with my car and complain to the manufacturer, if I get a comeback of ‘Okay, if you know so much about cars, make one.’; if every time I complain about the tea at the stall behind my office building I get, ‘If you know to make such great tea, make one yourself.’; if every time I complain about the plaster in my house collapsing I get, ‘If you know so much, build a house yourself.’, what all am I supposed to do---build my own house, make my own tea, build my own car, build my own toilet, cut my own hair, fly my own plane, run my own country as the Prime Minister, as the Health Minister, as the Finance Minister, all at once?

Such a comeback is pure absurdity.

## ‘How can you question a government that was formed with the people’s mandate?’

Well, because I am (and you are) people.

Understand that people choosing someone to represent them does not mean that the people agree with everything that the representative does or says or thinks. It does not work like that. Again, the representative (your area councillor, your MLA, your MP, etc.) are the people's mouthpieces, not the other way around.

This automatically means that the representative must listen to the people; he or she cannot work at his or her own will---s/he must work at the will of the people.

When the people do not like what their representative chooses to do, they question the choice. People are not electing someone to lead them; people are electing someone to represent them among the other representatives who are representing the people of other constituencies. Because we want to run collectively as a country, on the global stage.

It follows that the representatives must act as per the people's will; not act according to his or her own will and impose that will on the people.

These “leaders” are leaders in the government, but the government is not above people---the government is _for_ the people. This is the definition of a democratic republic. These “leaders”, therefore, lead the government, _at the will of the people_---because we the people of India say so.

The reality is that the representative against whom the people are protesting must feel ashamed of themselves. The people are protesting because they failed to represent the people in the right spirit.

Also, if the representative does not like people questioning them, they must step down, because they have taken up a job whose description they do not understand or agree with.

## ‘Stop stating the problem; give a solution’

Simple: You have a wrong idea in mind: that we exercise democracy by voting, and voting alone. Throw this idea away and this will make a lot more sense.

Voting is one of the many duties of a citizen of a democracy.

Assuming that you are capable of going through this shift in thinking:

1. A citizen of a democracy must keep in touch with their representative.
2. The representatives must listen to the people, not tell people what to (or not to) do. The law tells people what to, or what not to do.
3. People can question the making of a law or challenge a law; there is due process for it. For specifics, read the constitution or consult with a legal expert (I am not one).
4. When the people feel that their representative (or other representatives on a collective forum like the Lok Sabha) is not listening to them or acting to their will (the will of the people of the constituency, not an individual), they must let the representative know this. This can happen through an open letter, using the media, or other legal means.
5. When none of those avenues work, or the representative chooses to ignore the voice of the people, the people must come out into the public space and speak up---a protest is merely one way of doing this.
6. Not all those who protest are anti-government. Protests need not be anti-government (and if it is, there is nothing wrong with it, provided the issue is in the interest of the people that make up the country); a protest could merely be to make those in power listen. A protest is merely a way to direct attention towards an issue which the people feel the government is not handling right.
7. Media run by the government---or in favour of the government---is not good media. A good media organisation must focus on the positive and the negative---_in equal proportion_.
8. More importantly, a good media organisation must be biased _in favour of the people_; this may mean that they are biased against the powers that be, depending on the situation.
9. If elections are the only time the representatives work, then we must know that we have broken our system. This should be a blaring alarm bell that wakes us all up from our deep slumber of complacence.

Anger is almost never the way to solve something of public importance. If someone provokes your anger, your passion, your hatred or any of those strong emotions, stay at a safe distance from them.

Also, the question should not be about an alternative, but about how to make the incumbent work for the people.

And no, this is not a political piece; this is merely civics in 2300 words.

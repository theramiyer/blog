---
title: "Social Media Is Dead"
subtitle: "Nah, just kidding"
date: 2022-11-12T13:28:00+05:30
description:
tags:
draft: true
---

Social media dies every other day and gets resurrected by the weekend. Every now and then, someone mourns its death. And yet, the very next moment, shares it on the same social media. Or the same "social media"---whatever it is when you are reading this---suggests the article to you.

## The first look

An article I read today, stated that 10% of Twitter users contribute to 90% of its traffic.{{< sidenote jrSocialMediaDead2022 >}}Social Media Is Dead ([VICE](https://www.vice.com/en/article/pkgv79/social-media-is-dead)){{< /sidenote >}}

The article talks about implosion of Facebook, and the endless serving of ads across its platforms. It then goes on to lament how polydirectional communication has now become unidirectional with platforms like TikTok and YouTube.

Anyone who analyses social media will tell you this:

1. Now social networks have moved to being social media. (Meaning, now media has higher importance than connections.){{< sidenote elbermawySocialMediaDead2022 >}}Social Media is Dead: From Connection to Consumption ([NoGood](https://nogood.io/2022/07/27/social-media-is-dead/)){{< /sidenote >}}
2. Social media is the place for hate and all things bad, because emotions drive engagement, engagement increases time spent on the platform, which in turn means more opportunities to advertise.
3. Brands want to meet people where they are,{{< sidenote kerpenSocialMediaDead >}}Social Media Is Dead, Right? Well... ([Forbes](https://www.forbes.com/sites/carriekerpen/2021/01/26/social-media-is-dead-right-well/)){{< /sidenote >}} which means that innovation is going to keep happening to keep you hooked to that new platform.
4. Social media of today is primarily governed by algorithms that decide what you see.
5. Social networks are dead, and nobody is ready to accept it.
6. Content moderation is all over the place, trolls dominate the platforms, and so on.

Yes, social networks seem to have gotten replaced by social media, and most of the points above are spot on.

## The root of all evil

Advertising dollars. No rocket science. Everybody knows this, right? Privacy advocates warn us against these platforms, tell us about data collection; security experts give you hundreds of examples of data breaches and selling of your data on the dark web.

And yet, social media platforms record millions of active monthly users.

Ignoring the cries of (or petitions by) its users, Instagram continues to push Reels over content from your friends.{{< sidenote ruthKylieJennerInstagram2022 >}}Kylie Jenner and the Instagram Algorithm Are Fighting ([Vulture](https://www.vulture.com/2022/07/instagram-kylie-jenner-criticizes-algorithm-reels.html)){{< /sidenote >}} "Advertising dollars!" people cry out.

If a hundred people leave a social media platform today, a hundred others join. The cycle goes on. This is no big surprise given that new humans come into existence every day.

And then, there are some who talk about "taking back" social networking from these social media platforms, adding a phrase, 'whatever that means' to it.

## Let us face it

The reality today is, social media is the answer to television in the 21st century. A much more accessible, approachable and trackable answer.

1. Now you no longer need those boxes (or whatever) TRP calculation agencies put up in homes; you have analytics instead.
2. Consumers no longer have to be at a certain place to be entertained or have a remote control device in their hand; thanks to 4G and 5G networks and flashy smartphones available at a fraction of what TVs cost, everybody can get entertained anywhere.
3. Consumers no longer have to go hunting for interesting content to watch; there is AI that does this for them. All they have to do is scroll after every video (or probably not even do that).
4. Ads are no longer hit-or-miss. The algorithms select the right ad for the right kind of people, and deliver them for maximum impact possible. In any case, this impact seems magnitudes better than the TV ads. (Why else would companies shell out billions of dollars for online advertising on social media platforms?)

## Taking a step back

All of the points mentioned in the last section may be true. But let us try to make sense of where all this has come.

First, social networks started off as places where people could connect. Quickly, platforms realised that this was no good, because people weren't spending much time on the platform. Why? Because our lives are mundane. There is not much going on. Even if you do connect a hundred people on a platform simultaneously, a discussion or two might start, but soon, die out. The hundred people will walk away. "How are you?", "What's new?", "How was your day?", these questions could start off discussions, but these are mostly not designed for groups.

I started using social networks in 2009. My classmates and batchmates, engaged in discussions around topics such as the new film that started airing, or places to visit over the weekend, etc. Some would post dares and the others would engage with them for a while.

But we each logged into these platforms (Orkut and Facebook were the go-to places) in the evening, spent some thirty minutes and logged off. We did not have much time for this from our classes and playing outside (yeah, that was also a thing back then).

I did not have an internet connection at my place. I went to my friend's place for (online) social networking. A few of us friends would gather around this friend's computer, log into one of our accounts, and see what's going on. We did not have (read: could not afford) smartphones back then.

Now that I think about this, I feel we had our physical social network of our closest friends. We got together in schools, colleges and little (inexpensive) eateries, enjoyed the time together, and went back home. We used social networking platforms to connect to those with whom we were not close friends. These were people like distant cousins, batchmates from other classes, or from tuition classes from three years before, etc.

Weren't these social networking platforms supposed to help us with these distant connections?

## But where is the money in that

Social networking works on advertisements, much like most of the internet. This means, the more time you spend on these, the more likely you are to see an ad. The more you see ads, the more money the platforms make. Simple logic.

Connections with "distant" friends and family don't matter to you as much as your "close" friends and family matter. You don't need social networking platforms for your close friends and family (many of my closest friends don't even use social media). This means, you are unlikely to give much importance to these platforms by caring to log in every day. I remember a time when my Facebook feed would just be a repeat reel of the previous day, after a while. That did not happen the last time I actively used it a couple of years ago or so.

This meant that the social media platforms that wanted to stay in the game had to change their strategy. They introduced the ability to share other people's views: Twitter added the "Retweet" feature, social networks opened their doors to sharing links, etc.

Meanwhile, the platforms kept suggesting "people you may know". The bigger circle you had, the more posts and views you were likely to see and engage with. Your circle grew bigger by the day.

## The engagement drop

Soon, social media platforms realised that the engagement scores were not as high as they would've liked. Which is when some of these platforms started asking you if you found what you saw on your feed, relevant, interesting, agreeable, etc.

They learnt that the more people found their feed irrelevant or not agreeable, the sooner their engagement dropped. The platforms quickly started working out what each _user_ liked, and prioritised that content over what the user was unlikely to agree with.

This increased the engagement, because now, there was a more significant dopamine hit every time someone logged in and engaged with the content.

You see where this is going.

Today, like people rightly point out, social media is not centred around sociality. We can plot them on two axes: semblance to sociality on one and interest on the other, like on the NoGood article.

Add user experience engineering to it, and the interactivity could be the third axis.

## The real issue

Whenever someone says, 'If you're not paying for the product, you are the product.', people get irritated.

When the likes of Ola or Uber ask our cabbies or auto drivers for a commission, they get irritated.

Most of us fail to understand that the applications that run in the background, invisible to you, need millions of dollars to run.

Even a 4 terabyte NAS grade storage disk costs about 10,000 rupees. 4 TB can store about 500,000 photos captured on an entry-level DSLR. At the rate at which these social networks operate, they burn through thousands of such disks in a day. That is just the storage. If you factor in the servers that need to run the application, the CDNs that facilitate streaming, the salaries for the engineers that build these applications and maintain the infrastructure, the electricity cost to run the datacentres and offices, you are looking at millions of dollars a month in bills.

_Someone has to pay for all this!_

## Taking another step back

Money is money for everyone. The pharmacy we go to, usually has an A3 sheet with the name, address and the services offered by, say, a physiotherapist or a doctor who has moved in recently into the street and started practice nearby. Similarly, the Tech Corridor in Bangalore usually has billboards advertising tech jobs. You see ads about affordable housing at your local multiplex like INOX. Why?

Ad relevance is important for effectiveness. This is why companies like Google and Facebook collect seemingly unnecessary amount of data about you. They need to know how much you earn, how much you usually spend, where you care to invest, at what stage of life you are in, what are the causes you care about, etc. All this, so that they can show you relevant ads and increase sales for the advertisers that pay them.

## If you had to take back social media

You must have noticed that all popular social media platforms are privately owned. This is because these companies have created business models that can leverage advertising to make them profit. This is how they please their investors. This is how they get paid.

And this, like many point out, is a win-win-lose proposition. While you may think that you are getting entertained and are able to connect with people you care about on these platforms, your mental health indicators show a dark picture. This means, the platforms make money, the advertisers get better sales, but you only get an illusion of social interactions on these platforms. In other words, the platforms and the advertisers win at the cost of your health.

Which means, if you must take back social networking, you must act differently.

Of course, I am not suggesting that one must shun all social media and live under a rock.

Those who pay, get served. If you want real social networking, you must pay for it. Think about it this way: how did we build our social networks before these platforms came into being? We gathered at places designed for gathering (like coffee shops, club houses, libraries, restaurants, bars, etc.), paid for the products or services there, and got served. Or used public places, for which, (Surprise!) you paid taxes. We built networks at workplaces. We wrote letters and emails. We called up people to catch up. We gathered to play, to eat, to camp, to fish, etc.

In the virtual world, one must pay for social interactions as well, if one wishes to be served as a customer, rather than be treated as a product. Today, most of our meaningful social networking happens over messaging. (It surprises me that this is still a thing in 2023, when we thought everything would have gotten taken to the next level, so much so, that we would think texting was so 2010.)

In my view, we must pay for these services. I donate to Signal. I wish I could pay for WhatsApp and be excluded from all the data collection, but why would Meta settle for small change like that, right? I pay for my email. I buy overpriced Apple products, and I think that's enough money to use iMessage. Phone calls, of course, are paid for, even though we get sub-par service with privacy nowhere to be seen, but hey.

There is a solution to social networking, and I may explore these at some point: ActivityPub-based services such as Mastodon. What I liked about them is their federated nature. Who knows, I might, some day, host my own Mastodon instance for my family and friends.

Another solution is peer-to-peer services, such as Jami. The biggest plus with them is that they don't use a server infrastructure for ongoing communication. Many of these services are open source, and if you would like to pay for them, consider supporting the projects.

Yes, we can indeed take back control of social networking. But that involves handling a lot of the not-so-glamorous work that happens in the background, which is not everyone's cup of tea. And making users pay for the services is a lot less profitable than getting money from advertisers by harvesting user data that you "voluntarily give" to these services, as defined in their Terms.

Remember, there is no such thing as a free lunch.

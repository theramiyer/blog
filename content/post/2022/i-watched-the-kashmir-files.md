---
title: "I Watched the Kashmir Files"
subtitle: "and … I have mixed feelings"
date: 2022-03-28T20:06:00+05:30
description:
tags:
draft: true
---

As a man living under a rock, watching a film within a month of its release was surprising. This last Thursday, the 24th of March 2022, I watched _The Kashmir Files_ by Vivek Agnihotri. I stand for the freedom of expression within reason (what defines “reason” is for some other time). I felt that Agnihotri had exercised his freedom when he made this film. I am in support of that, personally.

But as a person who is apolitical and aware of the power of propaganda, I knew to take the film with a grain of salt.

My circles of friends and family span the entire ideological spectrum, and I mean that on at least two scales: political left to right plotted on one, and the economic left to right plotted on another. My not identifying with any (and leaning slightly to the right on the economic scale) gets me flak from across the space. Growing comfortable with the flak is one aspect, but more importantly, I think I can now comfortably see most points of view without feeling attacked (but often feeling annoyed).

Coming back to the film experience.

## The Tashkent Files

I went to the theatre to watch The Tashkent Files as well, back when it had released, because my uncle and aunt (more my aunt than uncle) wanted to watch it, and asked me if I would tag along. I did.

The film came off as too dramatic at some places. Loud, rather. At one point, I felt as though I was losing track of the connections. But in the end, I think it had the effect that its makers wanted it to have. My aunt and uncle liked the film.

I do not remember the details from that long ago, though. Also, I would not want to watch it again.

## The Agnihotri Interview

In mid 2019 or so, I bought a subscription to _Newslaundry_. While checking out the interviews section, I came across the one where Abhinandan Sekhri interviews Agnihotri. In a second, I said, ‘Wait, isn’t this the same guy who made The Tashkent Files?’ I watched the interview. By the end of it, Agnihotri had lost all the credibility I thought he had. Why? I felt that he walked back on some claims he made.

The most popular one, it seems is this: Sekhri asks Agnihotri how he got the figure that the Maoists have taken over 40% of our cities, referring to a speech Agnihotri made at a college event. Agnihotri claims that he merely used the government figures. Sekhri asks for the source, and Agnihotri goes something like ‘You do your research.’ Sekhri pulls out the government figures, and says, Maoists are present in some 44 districts (not percent), and goes on to ask Agnihotri again about the source, at which point, Agnihotri casually goes, ‘This was my research; we had researched this. You can deny it, you can throw it in the dustbin if you want.’

Now, I do not mean to say that the data from governmental agencies is fully accurate, but I believe that the government data is credible enough to get cited. For example, I believe that data from NCRB is reasonably credible. Any day, crime data from NCRB is more credible than the crime data coming from a professional dentist. Or a filmmaker for that matter.

Then comes the punchline in the interview:

> Who says facts are facts?!
>
> ---Vivek Agnihotri

You can catch all this in the [teaser of the interview](https://www.youtube.com/watch?v=0P9pVwYzZ50).

## Pre-theatre conversations

I had heard enough from my political right circles about how great the film was, and that the film had spoken something the others had not managed to speak. And I agree with that. Even the Vidhu Vinod Chopra film that released in 2019 or 2020, getting people’s hopes all up, had turned out to be a disappointment.

Haider was a decent film, which portrayed the situation of some Kashmiri Muslims. I heard there were others that did something similar. I have read [Ministry of Utmost Happiness](https://amzn.to/3uDc7Hs) by Arundhati Roy as well, and it also claims that the armed forces and the police commit atrocities against the Kashmiri Muslim youth. Of course, that is a work of fiction, but a couple of former colleagues of mine, Kashmiri Muslim youth themselves, have told me about what had happened to their friends and cousins. That the Kashmiri youth can potentially become victims of atrocities by the forces is an established fact.

The more important point is, people speak freely about it. People feel free to show the details as well. That has not been the case with the Kashmiri Pandits. Of course, there is [Our Moon has Blood Clots](https://amzn.to/3IK1W9d) by Rahul Pandita, and other such books, but what percentage of our population reads books these days? Which media house has covered the mass killings and the atrocities against the Kashmiri Pandits to detail?

And then, those other than the political right (meaning everyone else---not the left-leaning alone) outright called it a propaganda film. Most had issues with the numbers. Some had issues with the generalisation that all Muslims in Kashmir were against the Pandits. Someone even asked, ‘If the Kashmiri Pandits were a mere 2% like the film claims, how could they manage to outwit the 98% to come out? Surely the Muslims also helped them get out.’

I kept telling them that I was still going to watch it, because I also know from reading books and listening to some interviews that the Kashmiri Pandits had indeed gone through a lot. Nobody who has even a shred of conscience can---or should---deny that. Vivek Agnihotri has the right to show what he sees, much like how Arundhati Roy has the right to write what she feels.

## The movie night

We reached the theatre right in time for the film. And the film began with the disclaimer, which also said that the film did not claim factual accuracy. For some reason, my eyes got drawn straight to that line. I chuckled for a second.

And then everything went grim. I remained glued to the screen throughout the film. When we walked out during the intermission, we walked with the preceding gory imagery in mind. The film is not for the faint-hearted. Agnihotri has made it in its raw form, the blood and gore seeming almost real.

I walked out of the theatre in silence. The scenes in the film were unsettling. Disturbing. Horrific. Dark.

The film is nowhere near flawless, but is enough to get you feeling enraged.

Before we go any further: For the record, I do not go on morning walks.

Also, spoilers ahead.

## Blood rice and other stories

Someone hides in the rice drum, the terrorists break in, shoot the drum unleashing tens of bullets and making that someone's wife eat the blood-soaked rice---that is a true account. Murdering judicial and other officers in broad daylight is a true incident. Murdering IAF officers in broad daylight is also, if I remember right, a true incident.

The film does show true incidents, most likely narrated by Kashmiri Pandits themselves, to the filmmakers. The Kashmiri Pandit community, as far as I can tell, is a close-knit one. They have formed associations and they keep in touch. Everyone knows everyone else's “exodus” story.

## Exodus or not

Personal opinion: no. I think the community did witness a targeted mass attack amounting to ethnic cleansing if not a genocide. To me, the call, “Convert, Run or Die” is ethnic cleansing. Killing men and gangraping women of a particular sect (or even forcefully marrying them) is not too far from genocide.

Exodus is what happened in case of my forefathers and foremothers. A famine led them all to evacuate their home and seek refuge in what was then a different kingdom (and today, a different state). Technically, the departure of Kashmiri Pandits was an exodus, but calling it that would undermine the gravity of how it happened or why.

Someone said that the film merely claims to validate what the Kashmiri Pandits went through. While I would not say that that is all the film does, I do agree that what happened to the Pandits must be acknowledged. Without even that, there is no moving forward.

## The role of Muslims

The film has one scene where a Kashmiri Muslim man, a childhood friend of the protagonist’s brother’s---who, by the way, also saves the brother from attacking Muslim men in the beginning of the film---says how he misses the brother. He asks the protagonist, ‘Do I, or this kid sitting next to me, look like terrorists to you?’

That is genuine as well. The truth is, most that fled, fled with the help of their Muslim neighbours and friends. I have heard of people whose Muslim friends still run their businesses (I think someone even showed this in a film, if I am not wrong) and send their share.

Except for that one shikara scene, everywhere else, the film shows Muslims---all Muslims---as being involved in the terror attacks. I am sure that is not how Kashmir was or is. Of course, the film can claim that it shows the hardships of one Hindu family or two, but the image one walks away with is not that.

Do I care? Well, yes. As much as I support free speech, I think it must be within reason when the masses see it. Films are an impactful mode of communication. Films can be a weapon. And one must use powerful weapons responsibly.

## JNU and Friends

Yes, I have read about a JNU professor who believes that India is illegally occupying Kashmir. A (mainstream) media channel even did a show on this professor’s views on Kashmir and the North-East. The character named Radhika Menon portrays someone like this in the film.

“Indian Occupation of Kashmir” is an oversimplified statement, devoid of nuance. Some may say they want a plebiscite in Kashmir, but is that practical today? The Kashmir issue is too complicated to explain in a single paragraph (which is why I wrote the series, [Kashmir and Article 370]({{< ref kashmir-and-article-370 >}})).

I did not like the way the film painted the university. Not that I have any connection to it whatsoever. But the image that it creates of the university is unhealthy. Universities are places that encourage free thought. (And the film accidentally shows this in the end; I found that interesting.) In a university, a professor may have an opinion, while another may have an opposing opinion. The students listen to both and form their own opinion. That is how learning happens: you look at something from several viewpoints and then form your own image of it. Indoctrination is the word for knowing just one view of something; that is not learning.

Secondly, the way they portrayed _Hum Dekhenge_, to “Bhārat tere ṭukḍe honge”, to the chant of “Pakistan Zindabad” (an incident which was questionable to begin with{{< sidenote indianexpressZeeNewsProducer2016 >}}Zee News producer quits: Video we shot had no Pakistan Zindabad slogan ([Indian Express](https://indianexpress.com/article/india/india-news-india/zee-news-producer-quits-video-we-shot-had-no-pakistan-zindabad-slogan/)){{< /sidenote >}}), made it seem like feeding a specific narrative.

The line between disagreement (or dissent) and sedition is thin. But dissent is an important part of a democracy.{{< sidenote DissentSafetyValve2020 >}}Dissent is ‘safety valve’ of democracy: Justice Chandrachud ([Indian Express](https://indianexpress.com/article/india/justice-d-y-chandrachud-caa-protest-democracy-anti-national-6269831/)){{< /sidenote >}} Dissent within the constitutional framework is necessary to maintain balance in a democracy---more so when the democracy is as vast and as diverse as India. That is not to say that saying “Pakistan Zindabad” or “Bhārat tere ṭukḍe honge” is harmless; the principles of responsible free speech apply here as well. And slogans (any slogan) in the absence of context can lead to serious consequences.

## Narrow viewpoint

The point against the film that I agree with is that the film shows the Kashmir issue from a narrow viewpoint---at least based on what I have read, heard and seen about the issue.

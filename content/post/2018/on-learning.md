---
date: "2018-09-16T00:00:00Z"
published: false
tags:
- opinion
title: On Learning
---

When my way of learning was encouraged and appreciated, knowledge along with numbers automatically flowed in.
---
date: "2018-05-14T00:00:00Z"
tags:
- opinion
title: My Take on Pari
---

Movies I seldom watch; review them I almost never do. (Except saying whether I liked a certain movie or not.) This is probably my first-of-its-kind post. Now this is not a movie review---it is what I saw in the movie that most of people with whom I spoke about this movie didn't seem to see.

I usually avoid the so-called "horror" movies for the sheer stupidity, lack of a meaningful story and innovation---they can't differentiate fear from disgust and reflex, for starters. _Pari_, however, was one I wanted to watch at the theatre (but didn't get a chance to).

Personally, I have a whole different understanding of the concept of God. In most cases, therefore, I am agnostic. I'm not a bot, so I tend to be "somewhere between" the two poles (theism and atheism). So, by extension, I don't think that God is all good.

> The supernatural is merely a manifestation of man's arrogance.  
> ---Yours Truly

I'm someone who looks for deeper meanings to these "supernatural" phenomena from the philosophical side and evidence and explanation from the scientific. Usually, I take the objective stand. That's the same way I try to look at the "Devil" as well---same treatment as the God. Now this is why I particularly liked Pari.

**If you haven't watched the movie, stop here.** Go, watch the movie (it's available on [Amazon Prime](https://primevideo.com?tag=ramiyer-21)), and come back. This post isn't going anywhere.

You've been warned.

The movie starts with Arnab meeting his "potential partner" who asks him about his life in Kolkata. They talk for some time, and then, the man leaves with his parents. On the way, as they discuss about the girl and rejections and all, something hits their windshield. They stop the car, get down and go back to see what that was and find a woman in _burqa_---dead. The father promptly goes to the police station and surrenders. The police begin the investigation and find out who the woman was; she was the "_kutte wali_", whose daughter is Rukhsana (Anushka).

I was told that this movie is about, and I quote, "The making of the Devil". And to most, this is what the movie looks like. However, I disagree. The Devil already exists with God. Fun fact: you cannot possibly believe in one and not in the other.

> ...logic dictates that if you believe in the one, you have to reconcile the existence of the other.  
> ---Dr Spencer Reid (Criminal Minds)

There’s a scene in the movie that has an Islamic professor feeding his grandson, during which the grandson asks what a _Djinn_ is. The grandfather says, ‘It’s all the bad thoughts in you; thoughts such as, “I won’t go to school.” or, “I won’t listen to my parents.” ’ This movie is based on the dark supernatural side as per Islam, so the terms are new. And one good thing about this movie is that there's no crazy exorcism or gore in places of worship.

Now to the real content: this movie focusses on one of the demons called _Ifrit_. _Ifrit_ is considered to be the worst demon. Here are some of his characteristics as per the movie:

1. He cannot be seen, but his breathing can be heard.
2. He talks through _The Black Witch_.
3. He's capable of having sexual intercourse with women and impregnating them.
4. He is invoked in an orgy. The woman is laid on a table with her limbs tied. She's then covered with a shroud. Ifrit arrives, slips under the shroud and rapes her.
4. The children so born are devoid of the umbilical cord and the navel itself.
5. These children grow in the womb within a month.
6. His children grow their nails really fast (Rukhsana cuts her nails every day.)
7. Such children (only the girl is shown in the movie---the others ostensibly have been killed in the _Qaiyamat Movement_) spew out poison every month---they get abdominal cramps and all during this time.

I think, this movie has subtle meanings hidden under the story. Let's break some of them down one by one. Starting with what the professor said---that can be taken verbatim.

In my opinion, Ifrit is nothing but a sexual predator. He walks in the shadows and cannot be detected soon enough---almost no rape victim sees it coming. Some predators hide in plain sight (like members of the family). The woman who's being raped only feels the suffering. The orgy and the binding of the victim could suggest that the society actively takes part in the emotional killing of the victims and the very act of restricting the victim while letting the predator free (remember what the politicians had to say about the rising number of rapes in our country).

In the days when the mythology was just being formed, we can assume that contraception or abortion were common or accessible. Therefore, the children that were conceived even from rapes had to be born. We humans are capable of strong and complex feelings. Add to it the symbolism of the shroud---rapes kill the victims on the emotional level. The absence of the umbilical cord suggests the absence of the nourishment of the soul of the children.

Nails are an underrated part of our body. I understood their importance after an accident involving a fracture in my finger. Nails are supposed to protect our bones from the shocks that hit the tips of our fingers and toes. That is apart from helping with defence. The symbolism looked like hyper-vigilance to me.

Apart from all these, there were other subtle symbols in the story, things like Rukhsana not being able to listen to any of the supposedly holy utterances and all. To me, they seemed to indicate "morality", which I believe most of such victims find abhorring. Anyway, the point is that the movie has a lot of subtle messages---some of which I understood, some are still under process. In general, it is about the children forgotten by "God".

All this apart, this movie was the first one that made me feel sorry for Rukhsana. Especially when she was betrayed by Arnab, whom she loved wholeheartedly. It was plain sad. I doubt I've ever felt that way for any of the "supernaturally evil" elements shown in so many stories.

Although I thought that the movie would end with a pragmatic note that good and evil must co-exist, it ended by saying that Rukhsana's child was born with the umbilical cord, and unlike Rukhsana who was born of the hatred of the demon, she had given birth to the child with love. She says in the end, 'He's not like me. He's very much like you; a human.' The child was conceived from mutual affection, and not rape. "Spread the blood; spread the bloodline" was no more valid.

Overall, I think this truly is a movie that Bollywood (and the rest of the Indian film industry) can learn a lot from---how to pick a concept, how to build a story around it, and how to make viewers think.

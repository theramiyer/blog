---
date: "2018-10-16T00:00:00Z"
title: The Massive Metal Crawlers
---
I read in the [newspaper yesterday that the Bangalore Metropolitan Transport Corporation had ranted](https://www.thehindu.com/news/cities/bangalore/traffic-growth-hits-city-bus-services-hard/article25222415.ece) (rightfully so, no doubt) about how the increased traffic on the roads was to blame for the drastic drop in speed and efficiency of our dear buses. I thought I’d extend the discussion a little, today.

During my graduation, in Chennai, I was used to travelling by bus. And even to go from one end of the city to another, I did not feel that I was spending a lot of time on the road. The service was so efficient, it almost made the city look small, compared to Bangalore, where going from Jayanagar to Iblur took longer than travelling from Broadway to Valasaravakkam in Chennai. In August 2010, I took the entire late morning and afternoon to travel from Nagawara to Banashankari via Shivajinagar (because I was told, this was a better route than 500A, plus, I was broke).

Over the past eight years, (as a Bangalorean now), I have only seen the service deteriorate. Once in 2017, it took me two-and-a-half hours to go from Uttarahalli to Basavanagudi and come back---2017. Imagine that!

This issue of deterioration of <span class="small\-caps">BMTC</span>’s services is not as straightforward as it might seem to be. Here are some things that I think are the contributing factors, a couple of which snowball the issue:

## High fares

<span class="small\-caps">BMTC</span>’s bus fares are among the highest in the country. At a point they went so high that it was more economical (on average) to get a dedicated Uber to the destination, if you were travelling with two others. If you were travelling within a single BMTC stage with someone accompanying you, it was more economical to take an auto-rickshaw. Of course, the officials realised that and reduced the fares within a few weeks. But the status of BMTC remains---the fares are still among the highest in the country.

But how would they reduce the fares? They have their employees who need to be paid, and the pay should enable the employees to sustain in a city with a high cost of living. Their buses need maintenance. They have to consider the fuel price hikes as well.

Despite all facts, we Bangaloreans still feel that using your own vehicle for intra-city travel is less troublesome and more economical, which brings us to the next point, also noted in yesterday’s newspaper.

## Increased road traffic

This is a classic snowball. Buses are slow, so we prefer our bikes; because there’s such a high two-wheeler traffic, the buses slow down further.

One of the solutions BMTC suggested was having dedicated lanes for buses. Having lived in Ahmedabad and used the BRTS, I know that this is a tempting option. However, there are a few things to consider here. For example, Ahmedabad has the luxury of major roads connecting the entire city with very few bottlenecks. A large part of Ahmedabad developed gradually over time; the government bodies could handle the gradual development, and could plan the city accordingly. Bangalore almost exploded, and it took a long time for the authorities to wake up to the situation. (No conspiracy theories, please.)

## Unorganised roads

A pain in Bangalore. Apart from a few localities like Jayanagar, it is hard to find roads going straight and turning at right angles. This also means that we have roads like the _Ittamadu “Main Road”_, which is barely wide enough for two buses to go side-by-side.

To add to this, these days there are “white topping” and _Namma Metro_ works going on which have narrowed arterial roads significantly. Peak time commute is a nightmare in today’s Bangalore.

## Thoughtless riding/driving

Another major contributor to today’s traffic is the general attitude of those riding or driving on the roads. Back to Ittamadu Main Road. There is a nick (or what they call an S-curve) on the road which is a literal bottleneck. I’ve witnessed countless number of times, a bus tries to go through the nick, while coincidentally, a returning bus also does the same. In order to not get nicked in the process, the buses try to adjust themselves---an inch here, an inch there. In the meanwhile, riders overtake everybody and halt right in front of the oncoming buses. Within seconds, there are about fifteen bikes on either side twisting and turning in order to let the buses pass. Nobody can move backwards, by the way---all that space would be efficiently occupied by vehicles. Bumper-to-bumper. Nobody moves. Solid.

Cousins, colleagues and comrades (yes, bad attempt at alliteration) tell me, on the other hand, that the countries where they reside don’t have this issue. There is apparently something known as “inter-vehicular spacing”. Of course we haven’t heard of any such nonsense!

## The unfriendly routes

All of BMTC’s issues aren’t external. BMTC routes in themselves are pretty messed-up. Chennai has something called the _Cut Service_ wherein they slash the route number, denoting that the trip would terminate at a designated major stop on the way. Back in the day when I was in Chennai, we had 17M which used to go from Broadway to Aiyappanthangal. The slashed 17M would go only until Vadapalani.

In Bangalore, we have 210H going from Banashankari to Shivajinagar (which is almost an extinct route---and probably nowhere in the 210 vicinity), 210N is the most frequent one, from Majestic to Uttarahalli. Then there’s 210E which goes from Majestic to Vasanthapura (and in my opinion, this is a valid route). Then there’s 210J (Ramanjaneyanagara), 210P (Chikkalasandra) and 210Z (Abbaiah Naidu Studio) which are only different from each other with respect to the last halt. Why have these routes at all? These points are all essentially Chikkalasandra! Similar is the case with 201---there’s 201, 201R, 201G, 201J, 201M ... This doesn’t really serve much purpose except confusing the commuters. Probably have 201 and 201R, extend the routes a little here and there if needed, and be done with it. You don’t have to cover every metre of the city.

## The route scam

My brother used to travel to Whitefield from Uttarahalli every day. In his initial days, he would wait for a bus to Banashankari, and would get one after a long wait. One day, he decided to pick the bus to Silk Board, then take a bus to Banashankari, and then, home. (Three buses, yes. It frustrated him to no end.) The bus that brought him to Silk Board, changed the route number at Silk Board, and go to Banashankari.

Why is this a “scam”? Take this example. I take a bus from Banashankari to Marathahalli, I would pay, say, ₹85 for the ride. I take a bus from Banashankari to Hebbal, I would pay the same ₹85. Therefore, BMTC has to take me from Marathahalli to Hebbal at zero additional cost, whereas, the actual fee from Marathahalli to Hebbal is ₹55. So, the bus starts at Banashankari, saying the route would end at Marathahalli. At Marathahalli, they end the trip as promised, and then, change the route board to bear a new number — of the route from Marathahalli to Hebbal. Now, the bus is of a different route, and I have to pay another ₹55 to get to Hebbal. This way, they make ₹140 in place of ₹85. (The charges that I mentioned are fictitious; they may not be accurate — the strategy is, though.) I have been in this situation in 2012, wherein, after 17:30, no 500A (Hebbal–Banashankari) would be available; there would only be 500D (Hebbal–Silk Board).

Combine this with the aforementioned issues. I pay ₹140 and travel for three hours. Instead, if I paid ₹220, I would reach in an hour and a half, using a ride sharing service. Why wouldn’t I choose the latter? Then, why wouldn’t the road traffic increase: eight people occupying the same road space that fifty can?

## The solution

Oh, where do we even get started?! I mean, seriously, where do we get started? Do we address the route issue? Or the fare issue? Or the attitude? Or the timings? Or the road width? Deadlock.

This has to start slow. But the problem is, Bangalore is a cosmopolis that is not even a metropolis. Life here is fast and slow at the same time.

Brainstorming on the solution is for some other time. Another post, perhaps.

For now, it remains that BMTC is an inconvenience. So much that sometimes, we feel that the roads are better on the days of BMTC strikes. But of course, we have the hawks like the autorickshaw drivers, and Ola and Uber to rip us apart on those days. Probably the reason we’ve never really missed BMTC.

What do I do?

I cycle.

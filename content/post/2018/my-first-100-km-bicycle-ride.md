---
date: "2018-09-24T00:00:00Z"
tags:
- cycling
- exercises
- tips
title: My First 100 km Bicycle Ride
---

Before I start, let me mention, very clearly, that I'm just a beginner cyclist. Sometimes I like to push myself to my limits to see where I stand. As I go through these experiences, I learn more about myself, and this post is just an observation, thinking it might help someone looking to go on a long bicycle ride; some things to consider, some facts, and probable best practices.

It had been long since I'd been on a long ride. The first long ride I'd been on was of 60&nbsp;km, on the Outer Ring Road, Bangalore. On that ride, I'd taken with me a bottle full of ORS, and some packs of Appy. A few hours before the ride, I'd eaten a solid lunch. I left at about quarter past six in the evening and returned at half past nine. Overall, I'd enjoyed the ride. The gear I used for this ride was the 21-speed MTB, Hercules Ryders Contour.

I think it was in July 2017 that I went on another ride, to Chikkabanavara. We started in the morning. By the time we returned in the afternoon, I was half-dead. Apart from the company, there was nothing I enjoyed on that ride---a stark contrast. I noted down some of the points about the ride:

1. I'd not carried food.
2. I'd not carried enough water.
3. I'd not slept the previous night.
4. It was too sunny.
5. I rode a bike that did not belong to me, so it was not adjusted to my body (Merida Big Nine).
6. I carried a full-sized, thoughtlessly-packed backpack.
7. My bum ached. A lot.
8. It had been long since I'd worked out.

For the Sep&nbsp;2018 Century Challenge, I took help from Strava. I understood that I burnt roughly 300&nbsp;kcal in an hour of cycling. I needed about 750&nbsp;ml of water an hour on sunny days. I knew I needed a sunblock or sun-protective clothing. I was riding my own bicycle this time, and it had been adjusted to my body ratios to an extent. I carried two bottles; one on the bike, the other in a small backpack. In one bottle, I emptied two packs of Gatorade. I also bought four packs of Snickers---lots of glucose and some protein. I still had to carry a backpack, but I took only the essentials. Not even a kilo and a half. Not to forget, I wore padded cycling shorts this time.

Before I go into further details, let me mention that the ride was fantastic. By the end of the ride, I felt I'd accomplished something I never thought I would, surprised a few (which in itself is a dopamine rush), was assured of my capabilities and most of all, felt really confident, in spite of being exhausted and sunburnt (because I did not care to wear any sun protection; the weather forecast had said it was going to be overcast), while my thighs cried for a massage. Here is what I enjoyed and learnt from the ride.

Before starting, we were given a banana, some coffee and _chikki_, which is a brittle sweet made of broken groundnut and jaggery. We were also given a sachet of _Fast and Up_ energy gel "for the 70&nbsp;km mark". Unfortunately, I'd forgotten to pick up the four bars of Snickers.

It started really smooth. The first twenty kilometres did not even feel like a distance. We picked the Pipeline Road for the ride, in order to avoid the highway traffic. There were marathoners on the way, and that slowed us down a little. Lucky we, being among the faster lot, did not face oncoming (returning) runners.

My gear this time was my own road bike: B'Twin Triban 100: all-aluminium frame, a single speed chainring, a 7-speed casette, the drop bar, 32&nbsp;mm tyres and caliper brakes. Things were actually fine almost until we got to the climbs near our destination, Thattekere. The climbs were really hard. This is when I realised the importance of my 21-speed bike; a 7-speed bike is good for short bursts of climbs of about 30&deg;, but not for long climbs of that gradient.

![Route Elevation Profile - Strava](/assets/images/postimages/my-first-100km-ride-elevation-profile.png)

> 'So, this is your first 100&nbsp;km ride?'  
> 'M-hmm.'  
> 'And your longest has been sixty?'  
> 'Yup, back in 2014.'  
> 'That's... brave?'

We still managed to reach the destination without much exertion. A co-rider who'd stuck around for a while sensing my inexperience, asked me to eat well to refuel, at the pit stop. I refilled my bottle as well, knowing very well that I'd need more than a litre and a half of water on the way back, especially, now that the sun was out (nope, it was not going to be overcast).

We started our return journey at about 08:45, after the fifteen-minute pit stop. The sun was still getting warm for the day. By the time we crossed about ten kilometres from the destination, we could feel the sun on our skins. However, stopping to apply sunscreen and waiting under a tree for twenty minutes was not an option anymore, or so I thought, given that we did not want our muscles to cool down. In retrospect, I think I should have applied the sunscreen before starting on my return journey and let it absorb while on the ride. That would have still reduced the impact.

Now, it was time to pay back for the gravity assistance down the Pipeline Road. The terrain felt merciless; the upward incline of the Pipeline Road, combined with gravity, sapped every last bit of energy in me: in spite of eating a yoga bar, two bananas and a few grams of chikki. Thanks to my high metabolic rate, nothing less than 350&nbsp;kcal of intake per 20&nbsp;km was acceptable. So I did another pit stop, ditching the others, to buy another banana. Unfortunately, it wasn't ripe enough (so not enough glucose), but I couldn't eat more than one at a time for the fear of bloating. I also refilled my water bottle (I'd finished 1.5&nbsp;litres in little swigs, in just 32&nbsp;km).

![The long Pipeline Road](https://blogfiles.ramiyer.me/2018/images/2018-09-23-5339.JPG)

When I entered urban Bangalore from around Turahalli, a part of me wanted to stop once I was near home. The other part wanted me to go all the way to the BOTS store. I bought a bar of Snickers at a local store and ate it while riding. Bad idea. The bar was too sweet for me to finish in one go. It made me feel like wanting to throw up. I kept my cool and waded through the Bangalore traffic---that cannot see you if you're riding a bicycle---and reached the store. Lunch was served. I could not pedal back home; I was too tired for it. I took a cab home.

![Talk about encouragement](https://blogfiles.ramiyer.me/2018/images/2018-09-23-5340.JPG)

At home, I did a few simple post-ride stretching exercises to ease the tension on my muscles. I took a shower to cool down. Even after the shower (and in fact, until midnight), I felt very warm, as though I was running a fever. I knew this was normal after a strenuous workout---I've experienced it earlier---but not for that long. At about midnight, I had an episode of perspiration, after which, my skin temperature felt normal.

![At the Finish Line](https://blogfiles.ramiyer.me/2018/images/2018-09-23-5335.JPG)

The next morning when I woke up, I felt a mild pain in the thighs; nothing more. So, in the end, it all worked out very well. Had I been an experienced road biker like the rest, and not committed the few blunders that I did, I may have enjoyed the ride even more. Here are some of the points that I think are lessons I've learnt so far:

1. Never forget food when going on a ride outside the city.
2. Always wear a sunscreen or sun-protective clothing.
3. Get a saddle bag to store things. Get an opaque cover for the phone to avoid heating it by exposure to the sun.
4. Yoga bars, cake and energy gels are better options than Snickers or chocolate bars. Cakes are inconvenient to carry, though.
5. Always wear padded cycling shorts for rides longer than ten kilometres.
6. Check the route map to see what the elevation profile is going to be and choose your gear accordingly.
7. Try to continue pedalling on climbs, even if you're going slow. This keeps your muscles active. If your legs "cool down" too much, getting them to work will be _very_ difficult, especially on climbs.
8. Try to avoid prolonged workouts on sunny days, if you're not already acclimatised to it. The experience is very unpleasant.
9. Do not ride in traffic if you're tired. The frustration worsens your state.
10. Never forget to stretch after the workout.
11. Bend forwards when going down a slope; it makes you more aerodynamic, and reduces loss of momentum; precious momentum that you can use on the subsequent climbs.
12. Try to avoid backpacks, no matter how small they are. On long rides, they can cause shoulder ache.
13. Opt for simple carbohydrates if you're not looking at weight loss. The energy release is easier from carbohydrates, when compared to fat.

Now to some special points on heat and hydration:

1. Drink a few hundred millilitres of water before you start your ride. Keep refilling in swigs throughout the ride.
2. Drinking too much water at a time makes you feel bloated and makes you visit the loo often. On the other hand, drinking less would dehydrate you very quickly in sunny weather. Your body can tell you how much it needs---listen to it and don't wait until it yells "thirsty"; start drinking early.
3. Don't worry if you didn't have to relieve yourself throughout a six-hour ride. As long as you're hydrated, you're good. Sweating can throw out _some_ of the wastes.
4. Start drinking water regularly a few days before the event. This will make your body learn to regulate water more efficiently and control the thickness of your blood plasma, thereby improving the stroke volume.
5. On long rides (especially on hot days), electrolyte-rich water is better than plain water.
6. If you know you have to ride during a warm day, start going on (shorter) rides in hot weather two weeks before the event, so your body learns to handle heat better. This way, your body starts regulating temperature well before it reaches the threshold, and also, your body knows how to handle heat even when the core body temperature is just below 40&deg;&thinsp;C.
7. I think the colour of your clothing is also a variable. If possible, go for some lighter shades.
8. Avoid sunburn. It makes temperature regulation harder.
9. Exercising in the heat can be heavy on your heart, too. One of the heat control mechanisms is to send more blood to the skin to dissipate heat. However, because now the amount of blood available for the muscles is less, your heart has to work overtime to compensate for the decrease in oxygen delivery. To not wear out the heart, the amount of blood sent to the skin decreases---the priorities flip---and that decreases the heat dissipation. This could lead to complications.

I will add more points in subsequent posts when I learn more. For now, these should be able to serve as tips for a long ride on sunny days in Bangalore (or any tropical place). Following the tips and learnings should make your long rides more pleasant and reduce fatigue.

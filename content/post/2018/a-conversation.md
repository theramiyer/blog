---
date: "2018-10-18T00:00:00Z"
tags:
- survivor
title: A Conversation
---

A few days ago, I lost an uncle of mine. The loss was hard, especially given that he was one among the few strictly rational individuals — someone who could think straight even in the face of death. I won’t ramble on. Here is a write-up he shared with me when I visited him after a major attack. Without any further ado, over to the memory of Uncle Sampath:

---

Chennai, 2015

It all started around 2:45 in the afternoon. I could sense about 30 people around me – perhaps tiny ones with giant hands, I guess; there would be no space for all of them around me otherwise. Not even an inch of my chest was spared. They all started crushing my chest with all possible strength.

I remembered a race jockey, whose story of death I had heard. This young man died in 1979 while racing in Chennai, having been hit by the railing that threw him off his horse. He was run over by a dozen horses. He died on the spot but surprisingly, his autopsy report said that none of his ribs was fractured. The pressure created by the running horses had punctured his lungs and heart. He was around 22 when this happened.

Such was the pressure I experienced; I could not sit, stand, stretch — I was very restless. I could not breathe properly and was sweating profusely. All these showed signs of a heart attack to my half-baked knowledge. But an alarm inside me told me to un-bolt the main door lest the rescuers have to break it open. I did that and had a sip of water. Tried to breathe deep at the time when breathing itself was in jeopardy. The next moment, I reached for my phone and called Malathi. To my dismay, the line was busy. I was about to give up. Because sometimes, her call to a loan customer extended well over 10 minutes. Even though the pain was so unbearable, I could not stop at that, and give up the fight. I called again. This time my luck didn’t show the hatred towards me that it usually does — she answered the call. I was only able to whisper that I could not breathe and wanted her to come home immediately.

A few minutes later, Suja arrived and tried to help me. Madan, her colleague, was with her. They suggested that we go down immediately, find an auto and rush to Vijaya Hospital. By the time we could reach the apartment entrance, Suresh had arrived, followed by Malathi in a car. I was immediately rushed to Vijaya Hospital. Malathi had already informed Dr Anatha Narayanan, the cardiologist over there, about the situation.

A team examined me immediately with ECG, which revealed, as told to me later, showed “only a minor variance.” Immediately, an echocardiogram was done which revealed the truth — the whole truth — that some arteries had burst, particularly the main one carrying blood to the brain and other parts of the body. I believe they meant the Aorta. There was damage in the main valve and the vein connecting the left and the right ventricles. To repair all these, the facilities were inadequate at Vijaya Hospital and I was asked to be shifted to either MMM or MIOT. Choice was MIOT since the doctor at MMM had already left for the day.

The next thing I knew, I was in an ambulance, being taken to MIOT.

I recalled with a wry smile, the number of occasions I had watched an ambulance fly past with the siren on, not knowing the seriousness of its occupant. The pain that was mounting up threw me up in the air every now and then. I was only half-conscious.

Meantime, Dr Anantha Narayanan had contacted Dr V. V. Bashi with his diagnosis, and had informed him about my condition. My luck favoured me again, as Dr Bashi had just come out of the operation theatre, having performed his fourth surgery for the day. Although, he was completely exhausted.

Once we reached MIOT Hospital, I was taken to a CT scan room, where a bespectacled doctor was performing the scan. Perhaps the last of the few things I remembered was this, until I saw _Ammutty_ standing close to my bed, smiling at me and asking me how I was. She even held my hands assuring me that I was going to be all right. I could not believe that she was there to see me and perhaps too soon, because I was still thinking that only a few hours had passed by. I was told later that it had been about five or six days after I had been operated upon.

Next day, Chinni was there and I was more delighted, albeit not realising the amount of trouble they had taken to reach Chennai. I started this post with the conversation that was only later revealed to me by Malathi.

This conversation had taken place between Malathi and the team of doctors who had performed the surgery. The doctors were explaining the complications of the surgery. They told her how the time within which I was admitted to MIOT had been critical to my survival. Malathi had said, though, that she considered the day I got this attack as the worst in her life.

Dr Bashi had intervened and pointed out that that the day should be considered the best one of my life, specifying each small incident that had taken place and how each of it had contributed positively. Even if one of the links hadn’t existed, I would not have been available to record any of this.

Perhaps, my father-in-law would’ve mentioned the statement a hundred times, “God was present with us virtually.” Now I know how true his statement was. I am to accept that whatever happens is destined by Him and He knows how to guide us towards the right way. Something better is waiting for me, perhaps. Let’s wait and see.

Now, resting in my bed, I think of the Almighty’s mercy, the helping souls who provided me with adequate blood at the odd hour of that rainy night. I think of the generosity shown by the many unknown faces, particularly the woman who had waited outside the hospital for more than two hours at the hour, with adequate cash support and with moral support to Malathi and the others — the ones who had addressed every single problem with utmost courage and thoughtfulness. Our entire family has been with me throughout, to see me alive, starting from the little Haasini who had provided me with a big Band Aid with sincere hopes that this would cure my “ouchie”, to Aju who patiently waited to see me again, and came running to hug me.

These are the God to me, who preach me about love and faith. I cannot forget the unflinching services offered to me by my father-in-law, Ammutty, Chinni, ESN, Mythili, Govind, Revathi, Lolly, Suresh, Suja and the others. The regular visits by Dharini’s father and mother, and the timely help rendered by Sudharshan cannot be forgotten. Despite the age restriction, Jinju could sneak in to wish me well, which is worth mentioning. I perhaps have – not deliberately — missed scores of other friends like Vijayakumar, Sebastian, Rajendran and others; my Gurukulam friends, who constantly prayed and still are offering their prayers.

Although this sounds like a cliché, last but not the least, I should mention all the [Indian Bank] Valasaravakkam branch staff and the AGM who visited me to wish me well. I must also mention the timely help extended by Sivnandam.

To all of you I have only one thing to offer: my pranams and love.

Love you all.

Here are a few quotes that I would like to add:

> The day will come when my body will lie upon a white sheet neatly tucked under four corners of a mattress located in a hospital; busily occupied with the living and the dying.
> 
> At a certain moment a doctor will determine that my brain has ceased to function and that, for all intents and purposes, my life has stopped.
> 
> When that happens, do not attempt to instill artificial life into my body by the use of a machine, and don’t call this my deathbed, let it be called the bed of life, and let my body be taken from it to help others lead fuller lives.
> 
> Give my sight to the man who has never seen a sunrise, a baby’s face or love in the eyes of a woman.
> 
> Give my heart to a person whose own heart has caused nothing but endless days of pain.
>
> Give my blood to the teenager who was pulled from the wreckage of his car, so that he might live to see his grandchildren play.
>
> Give my kidneys to the one who depends on a machine to exist from week to week.
>
> Take my bones, every muscle, every fiber and nerve in my body and find a way to make a crippled child walk.
>
> Explore every corner of my brain. Take my cells, if necessary, and let them grow so that, someday a speechless boy will shout at the crack of a bat and a deaf girl will hear the sound of rain against her window.
>
> Burn what is left of me and scatter the ashes to the winds to help the flowers grow.
> 
> If you must bury something, let it be my faults, my weakness and all prejudice against my fellow man.
>
> Give my sins to the devil.
>
> Give my soul to God.
>
> If, by chance, you wish to remember me, do it with a kind deed or word to someone who needs you. If you do all I have asked, I will live forever.
>
> — Robert Test

There’s a poem I liked, and was reminded of, while writing this:

> If I forget, –  
> May joy pledge this weak heart to sorrow!  
> If I forget, –  
> May my soul’s coloured summer borrow  
> The hueless tones of storm and rain,  
> Of ruth and terror, shame and pain, –  
> If I forget!
>
> Though you forget, –  
> There is no binding code for beauty;  
> Though you forget, –  
> Love was your charm, but not your duty;  
> And life’s worst breeze must never bring  
> A ruffle to your silken wing, –  
> Though you forget.
>
> If I forget, –  
> The salt creek may forget the ocean;  
> If I forget, –  
> The heart whence flows my heart’s bright motion,  
> May I sink meanlier than the worst,  
> Abandoned, outcast, crushed, accurst, –  
> If I forget!
>
> Though you forget, –  
> No word of mine shall mar your pleasure;  
> Though you forget, –  
> You filled my barren life with treasure,  
> You may withdraw the gift you gave,  
> You still are lord, I still am slave, –  
> Though you forget.
>
> — Sir Edmund William Gosse
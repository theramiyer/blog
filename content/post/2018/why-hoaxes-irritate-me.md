---
date: "2018-05-22T00:00:00Z"
tags:
- opinion
- whatsapp university
title: The Story of Hoaxes
---

Of late, there have been a boatload of hoaxes floating around the Internet. Adding to the chaos primarily is WhatsApp, quickly followed by Facebook and the various other social networks. Instagram used to be the haven for original content, which has now been contaminated by these pictures and videos as well.

The Conspiracy Mill churns out these hoaxes on a daily basis and we consume them "HOT!" From political scandals, "forced conversions", blood and urine in ketchup to ISRO building rockets to bring fuel from the Moon, Einstein gets proven right every day.

As the fat young boy leans back in his chair with oil smeared all over his face and fingers, throws his feet, pops another French Fry into his mouth and gives that mocking giggle, a wave of the shocked emoji zips past across the planet at the speed of light, dreaming that some day there would be a conspiracy theory that said emojis actually travel faster than light, and that North Korea is using these to scatter their weapons by means of teleportation---"Emojiportation: The Greatness of the Yellow Circles". Ah!

It's come to the point where we've started to think that anything can indeed happen in this world. Among us are those who think that the world will simply implode because the petroleum is vanishing from under the oceans and those empty spaces will simply gobble up the crust. The others relate the extraction and the crust to a cheese burst pizzas and say the cheese is petroleum---not just figuratively any more. Then there are others who believe that there is going to be some magical way the Moon pumps fuel into these cavernous spaces underneath our feet and saves our planet. Logic? What's that? After all, an imagination is what drives scientists, right?

A woman balls up cooked rice that's supposed to be her child's lunch, bounces it off the floor, warning us that we've all been eating plastic rice imported from China. Someone kind adds a short video clip of a Chinese machine spewing plastic pellets, which, apparently get shaped in the form of rice. Why shouldn't that be possible? Not like this is some nasty level of overkill for something that naturally occurs. And the Chinese are superheroes after all, who sell phones at a third of the rate that Samsung and Apple sell. Why wouldn't they look for cheaper options for rice? And what's more mouldable than plastic?

The old grandpa in a remote village with that unpronounceable name, after a day's tiring work under the scorching sun on that land which the Sun had sucked all the water out of, reclines on a tree root, wipes his forehead with that piece of cloth on his shoulder, retrieves his phone from his pocket, and unlocks it to be greeted by the five tower segments on his Android phone. He smiles, says, "jio dhan-dhana-dhan", and taps on the WhatsApp notification praising technology, only to be given the bad news about the Chinese invading the rice market as well. How would he go to the Minister of his state? Wouldn't the Minister demand cash worth thrice the land he owns, in order to keep the Chinese at bay? He would need more men with him. Clippy would suddenly pop out on the screen and say, "Hey, would you like to mass-forward that message?" "What's mass-forward?" he'd ask. "Well, the ultimate dream of every WhatsApp message!" Clippy would say. Every message wants to be famous, doesn't it? And then, the lady would pop a rice ball into the child's mouth and give the wicked chuckle.

Every day, some God would want to be saved by a human mob from another human mob that's trying to save their God from another. Every day thousands would drink saccharin-injected water melon juice. Millions would dip their samosas and bhajias in blood and urine with a hint of cocaine (and more cocaine). Every day, the guy at the sandwich stand would wash sewer spinach in fresh water just to show his customers how much he cared about them, laughing within, with the arrogance of knowing that the water is sewer water again, with the colour hidden behind the invisibility cloak by a black magician sitting in a forest in Kerala. Of course, he doesn't mind his hands touching the same water. He's not one to discriminate. And obviously he's wearing gloves; those gloves are safer than the sewer water because they've only been on his scalp and his sweaty back that he'd scratched upon remembering that he had to pay the Sewage Board man some thirty thousand rupees for transporting the sewer water to his backyard.

And that backyard would have a hill made of pure gold that the Russians stole overnight, making our man a vegetable seller, who takes out the frustration on humankind by selling spinach sandwich.

Every time you read such messages, a cell in your brain commits suicide. Every time you forward it, you are abetting cell suicide in others. If you can live with that knowledge, who am I to judge?
